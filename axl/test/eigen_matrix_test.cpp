#include <iostream>

#include <Eigen/Dense>
#include <Eigen/Sparse>

using namespace std;
using namespace Eigen;

int main(int argc, char** argv) {

    MatrixXd M1, M2, M3;
    M1.resize(1, 4);
    M1 << 3, 4, 2, 1;
    cerr << "M1=" << M1 << endl;

    M2.resize(1, 4);
    M2 << 7, 2, 4, 8;
    cerr << "M2=" << M2 << endl;

    M3.resize(2, 4);
    M3 << M1, M2;
    cerr << "M1=" << M1 << endl;
    cerr << "M2=" << M2 << endl;
    cerr << "M3=" << M3 << endl;

    M2 = M3;
    cerr << "M1=" << M1 << endl;
    cerr << "M2=" << M2 << endl;
    cerr << "M3=" << M3 << endl;

    cerr << M1.cols() << " " << M1.rows() << endl;

    cerr << "################################" << endl;

    MatrixXd T, T0;
    T0.resize(4, 1);
    T0 << 1, 2, 3, 4;
    T.resize(4, 4);
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            T(i, j) = T0(j, 0);
        }
    }
    cerr << "T=" << T << endl;

    cerr << "################################" << endl;

    MatrixXd randMat;
    randMat.resize(4, 5);
    randMat << 2, 5, 3, 6, 1, 5, 4, 9, 1, 9, 3, 4, 3, 1, 2, 5, 2, 7, 9, 7;
    cerr << randMat << endl;

    JacobiSVD<MatrixXd> svd(randMat, ComputeFullU | ComputeFullV);
    MatrixXd U = svd.matrixU();
    MatrixXd V = svd.matrixV();
    MatrixXd S;
    S.resize(U.cols(), V.rows());
    for (int k = 0; k < svd.singularValues().size(); k++) {
        S(k, k) = svd.singularValues()(k);
    }

    cerr << endl << U << endl;
    cerr << endl << S << endl;
    cerr << endl << V << endl;

    cerr << endl << U*S*(V.transpose()) << endl;

    cerr << "################################" << endl;

    SparseMatrix<double> sp(3, 4);
    //sp.insert(0, 1) = 3;
    sp.coeffRef(0, 1) = 3;
    cerr << endl << sp << endl;

    cerr << "################################" << endl;

    MatrixXd subMatrix = randMat.block(0, 2, randMat.rows(), randMat.cols() - 2);
    cerr << endl << subMatrix << endl;

    MatrixXd subSubMatrix = subMatrix.block(0, 0, 3, subMatrix.cols());
    cerr << endl << subSubMatrix << endl;

    cerr << "################################" << endl;

    MatrixXd subMat = randMat.block(0, 2, randMat.rows(), randMat.cols() - 2);
    cerr << endl << subMat << endl;

    subMat = subMat.block(0, 0, 3, subMat.cols());
    cerr << endl << "BUG" << endl << subMat << endl;

    cerr << "################################" << endl;

    MatrixXd Mat1(3, 3), Mat2(3, 3);
    Mat1 << 11, 12, 13, 14, 15, 16, 17, 18, 19;

    MatrixXd N1Nd = MatrixXd::Zero(Mat1.rows(), Mat1.cols()*4);
    for (int j = 0; j < 4; j++) {
        N1Nd.block(0, j * Mat1.cols(), Mat1.rows(), Mat1.cols()) = Mat1;
    }

    cerr << endl << N1Nd << endl;

    cerr << "################################" << endl;

    Mat2 << 21, 22, 23, 24, 25, 26, 27, 28, 29;

    Mat1 += Mat2;

    cerr << endl << Mat1 << endl;

    cerr << "################################" << endl;

    Eigen::MatrixXd test(2, 2);
    test << 1.40508, -1.2714, 0.354598, 0.490605;
    Eigen::JacobiSVD<Eigen::MatrixXd> svdTest(test, Eigen::ComputeFullU | Eigen::ComputeFullV);
    std::cerr << "SVDTest Sing" << svdTest.singularValues() << std::endl;

    cerr << "################################" << endl;

    Eigen::MatrixXd A = Eigen::MatrixXd::Random(3, 5);
    Eigen::MatrixXd C2 = Eigen::MatrixXd::Random(5, 5);
    int rk = 2;
    Eigen::MatrixXd A2 = (A*C2).block(0, rk, A.rows(), C2.cols()-rk);

    cerr << "A=" << A << endl;
    cerr << "C2=" << C2 << endl;
    cerr << "(A*C2)=" << (A*C2) << endl;
    cerr << "A2=" << A2 << endl;

    cerr << "################################" << endl;

    Eigen::MatrixXd A3 = A*C2;
    A3.conservativeResize(A3.rows(), 3);

    cerr << "A=" << A << endl;
    cerr << "C2=" << C2 << endl;
    cerr << "(A*C2)=" << (A*C2) << endl;
    cerr << "A3=" << A3 << endl;

    cerr << "################################" << endl;

    Eigen::MatrixXd testResizeA = Eigen::MatrixXd::Random(3, 5);
    cerr << testResizeA << endl;
    testResizeA = Eigen::MatrixXd::Zero(2, 3);
    cerr << testResizeA << endl;


    return EXIT_SUCCESS;
}
