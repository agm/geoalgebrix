#include <string>
#include <set>
#include <math.h>
#include <fstream>

//#define MDEBUG_COUT
#include <geoalgebrix/axldebug.hpp>
#include <geoalgebrix/mdebug.hpp>

#define NEW
#include <geoalgebrix/point.hpp>
//#include <geoalgebrix/algebraic_surface.hpp>
#include <geoalgebrix/algebraic3d.hpp>
#include <geoalgebrix/mesher.hpp>
#include <geoalgebrix/mshr_csg3d.hpp>

#include <axl-config.h>
#include <axlCore/axlReader.h>
#include <axlCore/axlWriter.h>
#include <axlCore/axlMesh.h>
#include <axlCore/axlFactoryRegister.h>
#include <dtkCoreSupport/dtkPluginManager.h>

#include <axlSemiAlgebraic/SurfaceAlgebraic.h>
#include <axlSemiAlgebraic/axel_env.hpp>

//#define Surface     mmx::geoalgebrix::algebraic_surface<double,ENV>
#define Surface     mmx::csg::algebraic3d<double>
#define BoundingBox mmx::geoalgebrix::bounding_box<double>

int main(int argc, char** argv) {

    //mdebug("debug.txt",mdebug::init);
    mdebug()<<"Start mesher"<<argv[1];

    if (argc <2) {
        std::cout<< "usage: "<<argv[0]<< " file.axl"<<std::endl;
        return 1;
    }

    // Loading of Axel plugins
    dtkPluginManager::instance()->setPath(AXL_PLUGIN_DIR);
    dtkPluginManager::instance()->initializeApplication();
    dtkPluginManager::instance()->initialize();
    axlFactoryRegister::initialized();

    //Reading input file
    mdebug()<<"Reading input file";
    axlReader *obj_reader = new axlReader();
    obj_reader->read(argv[1]);

    QList<axlAbstractData *> list = obj_reader->dataSet();
    mdebug()<<"Objects read: "<<list.size();

    axlSemiAlgebraic::SurfaceAlgebraic *obj =  dynamic_cast<axlSemiAlgebraic::SurfaceAlgebraic *> (list.at(0));

    if (!obj) {
        qDebug()<<"Algebraic Surface not valid";
        return 1;
    } else {
        qDebug()<<"Algebraic Surface read";
    }
    qDebug()<<"Algebraic surface"<< obj->write_equation();

    BoundingBox* box = static_cast<BoundingBox*> (obj->bounding_box());
    double sz = box->size();
    Surface * sf = static_cast<Surface *> (obj->surface());
    if(!sf) {
        qDebug() << "SurfaceAlgebraicConverter: problem in toAxlMesh" ;
        return 1;
    }

    mmx::csg::generic* a = new mmx::csg::algebraic3d<double>(sf->equation());//"x^2-y^2+ z^4-0.05");

    mmx::mesher<mmx::mshr_csg3d<double> > mshr(sz*0.05,sz*0.01);
    mshr.set_input(mshr.controler()->init_cell(a,
                                               box->xmin(),box->xmax(),box->ymin(),box->ymax(),box->zmin(),box->zmax()));
    mshr.run();

    qDebug()<<"Subdivided";

    mmx::mshr_csg3d<double>::Mesh* t = new mmx::mshr_csg3d<double>::Mesh;
    mshr.subdivider()->get_tmsh(t);

    axldebug a0("csg3d_mesh.axl",axldebug::init);
    a0.set_color(0,0,255);
    a0<<*t;
    a0.close();

    mmx::mshr_csg3d<double>::Mesh* m= new mmx::mshr_csg3d<double>::Mesh;
    mshr.polygonizer()->get_mesh(m);

    qDebug()<<"Mesh:"<<m->nbv()<<m->nbf();

    axldebug a1("csg3d_surface.axl",axldebug::init);
    a1<<*m;
    a1.close();

    system("axel csg3d_mesh.axl csg3d_surface.axl&");
    //system("axel csg3d_surface.axl&");

}
