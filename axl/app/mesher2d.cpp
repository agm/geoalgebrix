#include <string>
#include <set>
#include <math.h>
#include <fstream>

//#define MDEBUG_COUT
//#define MDEBUG_FILE

#include <geoalgebrix/mdebug.hpp>
#include <geoalgebrix/axldebug.hpp>

#include <geoalgebrix/point.hpp>
//#include <geoalgebrix/algebraic_curve.hpp>

#include <geoalgebrix/algebraic2d.hpp>
#include <geoalgebrix/mshr_alg2d.hpp>
#include <geoalgebrix/mesher.hpp>

#include <axl-config.h>
#include <axlCore/axlReader.h>
#include <axlCore/axlWriter.h>
#include <axlCore/axlMesh.h>
#include <axlCore/axlFactoryRegister.h>
#include <dtkCoreSupport/dtkPluginManager.h>

#include <axlSemiAlgebraic/CurveAlgebraic.h>
#include <axlSemiAlgebraic/axel_env.hpp>

//#define Curve     mmx::geoalgebrix::algebraic_curve<double>
#define Curve       mmx::csg::algebraic2d<double>
#define BoundingBox mmx::geoalgebrix::bounding_box<double>

int main(int argc, char** argv) {

//    mdebug("debug.txt",mdebug::init);
//    mdebug()<<"Start mesher"<<argv[1];

    if (argc <2) {
        std::cout<< "usage: "<<argv[0]<< " file.axl"<<std::endl;
        return 1;
    }

    // Loading of Axel plugins
    dtkPluginManager::instance()->setPath(AXL_PLUGIN_DIR);
    dtkPluginManager::instance()->initializeApplication();
    dtkPluginManager::instance()->initialize();
    axlFactoryRegister::initialized();

    //Reading input file
    mdebug()<<"Reading data";
    axlReader *obj_reader = new axlReader();
    obj_reader->read(argv[1]);

    QList<axlAbstractData *> list = obj_reader->dataSet();
    mdebug()<<"read: "<<list.size();

    axlSemiAlgebraic::CurveAlgebraic *obj =  dynamic_cast<axlSemiAlgebraic::CurveAlgebraic *> (list.at(0));

    if (!obj) {
        mdebug()<<"Algebraic curve not valid";
        return 1;
    } else {
        mdebug()<<"Algebraic curve read";
    }
    BoundingBox* box = static_cast<BoundingBox*> (obj->bounding_box());
    Curve * sf = static_cast<Curve *> (obj->curve());
    if(!sf) {
        mdebug() << "CurveAlgebraicConverter: problem in toAxlMesh" ;
        return 1;
    }

    qDebug()<<"Algebraic curve"<< obj->write_equation()<<" converted to AxlMesh";

    mmx::mesher<mmx::mshr_alg2d_curve<double> > mshr(0.025,0.001);
    mshr.set_input(mshr.controler()->init_cell(sf->equation(),box->xmin(),box->xmax(),box->ymin(),box->ymax()));
    mshr.run();

    axldebug a0("algebraic2d_mesh.axl",axldebug::init);
    a0<<*mshr.controler();
    a0.close();

    mdebug()<<"Mesh:"<<mshr.output()->nbv()<<mshr.output()->nbe();
    mmx::mesh<double> m;
    mmx::get_mesh2d(m,mshr);
    axldebug a1("algebraic2d_curve.axl",axldebug::init);
    a1<<m;
    a1.close();

    std::cout<<"Output: algebraic2d_mesh.axl algebraic2d_curve.axl "<<std::endl;
    system("axel algebraic2d_mesh.axl algebraic2d_curve.axl &");

}
