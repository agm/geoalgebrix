
// /////////////////////////////////////////////////////////////////
// Generated by mmxmake
// /////////////////////////////////////////////////////////////////

//# include "...h"
#include "SurfaceAlgebraic.h"
#include <axlCore/axlFormat.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkLog/dtkLog.h>

#include "axlSemiAlgebraicPlugin.h"


//#include <geoalgebrix/algebraic_surface.hpp>
#include <geoalgebrix/algebraic3d.hpp>
#include <geoalgebrix/bounding_box.hpp>
#include <realroot/polynomial_sparse.hpp>
#include <realroot/polynomial.hpp>

#include <algorithm>
#include <string>
#include <cstring>
#include <iostream>
#include <vector>


#ifndef AXEL
#define ENV mmx::geoalgebrix::default_env
#else
#define ENV axel_env
#endif

//#define Surface  mmx::geoalgebrix::algebraic_surface<double>
#define Surface  mmx::csg::algebraic3d<double>
#define BoundingBox mmx::geoalgebrix::bounding_box<double>

#define Rep double //representation type;
// /////////////////////////////////////////////////////////////////
// SurfaceAlgebraicPrivate
// /////////////////////////////////////////////////////////////////
BEGIN_PLUGIN_NAMESPACE

class SurfaceAlgebraicPrivate
{
    public:
    //To be replaced
    Surface     surface;
    BoundingBox box;

};

// /////////////////////////////////////////////////////////////////
// SurfaceAlgebraic
// /////////////////////////////////////////////////////////////////

SurfaceAlgebraic::SurfaceAlgebraic(void) : axlAbstractSurfaceImplicit(), d(new SurfaceAlgebraicPrivate)
{
    this->setObjectName(this->identifier());
    d->surface = Surface();
    d->box   = BoundingBox();
}

SurfaceAlgebraic::~SurfaceAlgebraic(void)
{
    delete d;
    d = NULL;
}

bool SurfaceAlgebraic::registered(void)
{
    return axlSemiAlgebraicPlugin::dataFactSingleton->registerDataType("SurfaceAlgebraic", createAxlSemiAlgebraicSurfaceAlgebraic);
}

QString SurfaceAlgebraic::description(void) const
{

    QString info = "";
    info += "Algebraic surface\n";
    info += "   Domain: " + write_domain() + "\n";
    info += "   Equation:   " + write_equation() + "\n";

    return info;
}

QString SurfaceAlgebraic::identifier(void) const
{
    return "SurfaceAlgebraic";
}

void SurfaceAlgebraic::setData(void *data)
{
    d->surface = *(static_cast<Surface *>(data));
}

void SurfaceAlgebraic::setSurface(const QVector<double>& box, const QString& eqn) {

    d->surface  = Surface(eqn.toStdString().c_str(), "x y z");

    unsigned v=0, s=0;
    d->box  = BoundingBox();
    for (QVector<double>::const_iterator it= box.begin(); it!= box.end(); it++) {
        d->box(v,s) = (*it);
        s++;
        if(s>1) {v++; s=0;}
    }
}

void* SurfaceAlgebraic::surface(void)
{
    return &(d->surface);
}

void* SurfaceAlgebraic::bounding_box(void) const
{
    return &(d->box);
}

unsigned  SurfaceAlgebraic::nb_equations() const {
    return  d->surface.equation().size();
}

QString SurfaceAlgebraic::write_domain() const {
    QString str; QTextStream out(&str);
    out << QString::number(d->box(0,0)) <<" "
        << QString::number(d->box(0,1)) <<" "
        << QString::number(d->box(1,0)) <<" "
        << QString::number(d->box(1,1)) <<" "
        << QString::number(d->box(2,0)) <<" "
        << QString::number(d->box(2,1));
    return str;
}

QString SurfaceAlgebraic::write_equation() const {
    std::string str = mmx::to_string(d->surface.equation(), mmx::variables("x y z"));
    return QString(str.data());
}



double SurfaceAlgebraic::eval(double *point) const{

    double value = 0;
    value = d->surface.equation()(point[0],point[1],point[2]);


    //    std::vector<double> p(3);
    //    p[0] = point[0];
    //    p[1] = point[1];
    //    p[2] = point[2];

    //    value =  d->surface.equation().at(p);
    //qDebug() << Q_FUNC_INFO <<p[0] << p[1]<< p[2]<< value << write_equation();


    return value;

}

//double SurfaceAlgebraic::eval(double *point) const{
//    double value = 0;
//    const int x =point[0];
//    const int y =point[1];
//    const int z =point[2];

//    QString equation = write_equation();

//    for(int i = 0; i < equation.size(); i++){

//        if(QString::compare(equation.at(i),"x") == 0){
//            if(x >= 0){
//                equation.replace(i,1,QString::number(x));
//            }else{
//                equation.replace(i,1,"("+QString::number(-1*x)+")");
//            }

//        }else if(QString::compare(equation.at(i),"y") ==0){
//            if(y >= 0){
//                equation.replace(i,1,QString::number(y));
//            }else{
//                equation.replace(i,1,"("+QString::number(-1*y)+")");
//            }

//        }else if(QString::compare(equation.at(i),"z") ==0){
//            if(z >= 0){
//                equation.replace(i,1,QString::number(z));
//            }else{
//                equation.replace(i,1,"("+QString::number(-1*z)+")");
//            }

//        }
//    }

//    //qDebug() << Q_FUNC_INFO << 1 << x << y << z << equation;
//    value = parseEval(equation);
//    //qDebug() << Q_FUNC_INFO << 2 << value;
//    return value;

//}

double SurfaceAlgebraic::parseEval( QString equation) const{

    double value = 0;
    double inter = 0;
    double compute = 0;

    int indiceMinus = 0;

    double interMinus = 0;
    double interMult = 1;


    QStringList addition = equation.split("+");
    foreach(QString add, addition){
        QStringList soustraction = add.split("-");
        foreach(QString sous, soustraction){
            QStringList multiplication = sous.split("*");
            foreach(QString currentSymbol, multiplication){
                if (currentSymbol.contains("(") && !currentSymbol.contains("^")){
                    currentSymbol.remove(0,1);
                    int m = currentSymbol.size();
                    currentSymbol.remove(m-1,m);
                    m = -1*currentSymbol.toDouble();
                    currentSymbol.clear();
                    currentSymbol.append(QString::number(m));
                }else if (currentSymbol.contains("(")){
                    QStringList power = currentSymbol.split("^");
                    QString exposant = power.at(1);
                    QString n = power.first();
                    n.remove(0,1);
                    n.remove(n.size()-1, n.size());
                    int time = exposant.toInt();
                    inter = n.toDouble();
                    compute = inter*(-1);
                    for (int j = 0;j < time-1; j++){
                        compute = compute * inter*(-1);
                    }
                    currentSymbol.clear();
                    currentSymbol.append(QString::number(compute));
                }else if (currentSymbol.contains("^")){
                    QStringList power = currentSymbol.split("^");
                    QString exposant = power.at(1);
                    QString n = power.first();
                    int time = exposant.toInt();
                    inter = n.toDouble();
                    compute = inter;
                    for (int j = 0;j < time-1; j++){
                        compute = compute * inter;
                    }
                    currentSymbol.clear();
                    currentSymbol.append(QString::number(compute));
                }else {
                }
                interMult = currentSymbol.toDouble() * interMult;
            }
            if(indiceMinus == 0){
                interMinus = interMult;

            }else{
                interMinus = interMinus - interMult;
            }
            indiceMinus++;
            interMult = 1;
        }
        indiceMinus = 0;
        value = value + interMinus;
        interMinus = 0;
    }

    return value;
}

END_PLUGIN_NAMESPACE
// /////////////////////////////////////////////////////////////////
// Type instantiation
// /////////////////////////////////////////////////////////////////

dtkAbstractData *createAxlSemiAlgebraicSurfaceAlgebraic(void)
{
    return new PLUGIN_NAMESPACE SurfaceAlgebraic;
}

#undef Rep
