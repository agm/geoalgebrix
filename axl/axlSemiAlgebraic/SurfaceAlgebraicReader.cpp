/*****************************************************************************
 * SurfaceAlgebraic Reader for axel
 *****************************************************************************
 *
 *  Created: Fri Feb 10 10:18:17 CET 2012
 *       By: mperrine
 * Comments:
 *           Generated with mmxaxel
 *
 *  Changes:
 *****************************************************************************/
#include "SurfaceAlgebraicReader.h"
#include "SurfaceAlgebraic.h"
#include <dtkCoreSupport/dtkAbstractDataFactory.h>

#include "axlSemiAlgebraicPlugin.h"

//#include <axlCore/....h>

// /////////////////////////////////////////////////////////////////
// SurfaceAlgebraicReader
// /////////////////////////////////////////////////////////////////
BEGIN_PLUGIN_NAMESPACE
SurfaceAlgebraicReader::SurfaceAlgebraicReader(void)  { }

SurfaceAlgebraicReader::~SurfaceAlgebraicReader(void) { }

QString SurfaceAlgebraicReader::identifier(void) const
{
    return "SurfaceAlgebraicReader";
}

QString SurfaceAlgebraicReader::description(void) const
{
    return "SurfaceAlgebraicReader";
}

QStringList SurfaceAlgebraicReader::handled(void) const
{
    return QStringList() << "SurfaceAlgebraic";
}

bool SurfaceAlgebraicReader::registered(void)
{
    return axlSemiAlgebraicPlugin::dataFactSingleton->registerDataReaderType("SurfaceAlgebraicReader", QStringList(), createAxlSemiAlgebraicSurfaceAlgebraicReader);
}

bool SurfaceAlgebraicReader::accept(const QDomNode& node)
{
    QDomElement element = node.toElement();

    if(element.tagName() != "surface")  return false;

    if(element.attribute("type") != "algebraic")  return false;

    if(!hasChildNode(element, "domain"))  return false;

    if(!hasChildNode(element, "polynomial"))  return false;

    return true;
}

bool SurfaceAlgebraicReader::reject(const QDomNode& node)
{
    return !this->accept(node);
}

axlAbstractData *SurfaceAlgebraicReader::read(const QDomNode& node)
{
    if(!accept(node))
        qDebug()<<"Algebraic Surface Not accepted" ;

    QDomElement e = node.toElement();
    //qDebug() << "Algebraic Surface: reading ";

    QDomNodeList nodelist = e.elementsByTagName("domain") ;
    QDomElement  element = nodelist.item(0).toElement() ;
    QStringList  list = element.text().simplified().split(QRegExp("\\s+")) ;

    QVector<double> box;
    foreach(QString s, list) {
        box << s.toDouble() ;
    }

    nodelist = e.elementsByTagName("polynomial") ;
    QString eq = nodelist.item(0).toElement().text() ;

    SurfaceAlgebraic * surface = new SurfaceAlgebraic;

    surface->setSurface(box, eq);


    this->setColorOf(surface,e);
    this->setNameOf  (surface, e);

    //this->setColorOf (surface, e);
    this->setShaderOf(surface, e);

    return surface;
}
END_PLUGIN_NAMESPACE

dtkAbstractDataReader *createAxlSemiAlgebraicSurfaceAlgebraicReader(void)
{
    return new PLUGIN_NAMESPACE SurfaceAlgebraicReader;
}
