/*****************************************************************************
 * SurfaceAlgebraic Writer for axel
 *****************************************************************************
 *
 *  Created: Fri Feb 10 10:18:25 CET 2012
 *       By: mperrine
 * Comments: 
 *   Generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef SurfaceAlgebraicWriter_h
#define SurfaceAlgebraicWriter_h

#include <axlCore/axlAbstractDataWriter.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic { //BEGIN_PLUGIN_NAMESPACE 

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceAlgebraicPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceAlgebraicWriter : public axlAbstractDataWriter
{
    Q_OBJECT

public :
    SurfaceAlgebraicWriter(void);
   ~SurfaceAlgebraicWriter(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;

    static bool registered(void);

public:
    bool accept(dtkAbstractData*);
    bool reject(dtkAbstractData*);
    
    QDomElement write(QDomDocument *doc, dtkAbstractData *data);

private:
};

} // END_PLUGIN_NAMESPACE 

dtkAbstractDataWriter *createAxlSemiAlgebraicSurfaceAlgebraicWriter(void);

# endif
