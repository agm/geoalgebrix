/*****************************************************************************
 * SurfaceRational Converter for axel
 *****************************************************************************
 *
 *  Created: Mar 24 jan 2012 19:01:02 CET
 *       By: mourrain
 * Comments: 
 *   Generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef SurfaceRationalConverter_h
#define SurfaceRationalConverter_h

#include <axlCore/axlAbstractDataConverter.h>
#include "axlSemiAlgebraicPluginExport.h"

class AXLSEMIALGEBRAICPLUGIN_EXPORT axlMesh;

namespace axlSemiAlgebraic { // BEGIN_PLUGIN_NAMESPACE 

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceRationalPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceRationalConverter : public axlAbstractDataConverter
{
    Q_OBJECT

public:
      SurfaceRationalConverter(void);
     ~SurfaceRationalConverter(void);

public:
      QString   description (void) const;
      QStringList fromTypes (void) const;
      QString        toType (void) const;

      static bool registered (void);

public:
    
      axlMesh* toMesh(void);

private:
};

} // END_PLUGIN_NAMESPACE 

dtkAbstractDataConverter *createAxlSemiSurfaceAlgebraicRationalConverter(void);

#endif
