/* CurveAlgebraicConverter.h
 * Created: Sam 21 jan 2012 18:20:12 CET
 *      By: mourrain
 * Comments:
 *      Generated by mmxaxel
 * Change log:
 *
 */
// /////////////////////////////////////////////////////////////////

//#include <geoalgebrix/mdebug.hpp>
#include <geoalgebrix/point.hpp>
//#include <geoalgebrix/algebraic_curve.hpp>
#include <geoalgebrix/algebraic2d.hpp>
#include <geoalgebrix/mshr_alg2d.hpp>
#include <geoalgebrix/mesher.hpp>

#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <axlCore/axlPoint.h>
#include <axlCore/axlMesh.h>
#include "CurveAlgebraic.h"
#include "CurveAlgebraicConverter.h"

#include <axlSemiAlgebraic/CurveAlgebraic.h>
#include <axlSemiAlgebraic/axel_env.hpp>
#include "axlSemiAlgebraicPlugin.h"

// /////////////////////////////////////////////////////////////////
// CurveAlgebraicConverter
// /////////////////////////////////////////////////////////////////
BEGIN_PLUGIN_NAMESPACE

CurveAlgebraicConverter::CurveAlgebraicConverter(void) : axlAbstractDataConverter () { }

CurveAlgebraicConverter::~CurveAlgebraicConverter(void) { } 

QString CurveAlgebraicConverter::description(void) const
{
    return "CurveAlgebraicConverter";
}

QStringList CurveAlgebraicConverter::fromTypes(void) const
{
    return QStringList() << "CurveAlgebraic";
}

QString CurveAlgebraicConverter::toType(void) const
{
    return "axlMesh";
}

bool CurveAlgebraicConverter::registered(void)
{
    return axlSemiAlgebraicPlugin::dataFactSingleton->registerDataConverterType("CurveAlgebraicConverter", QStringList(), "axlMesh", createAxlSemiAlgebraicCurveAlgebraicConverter);
}


/// Method used to convert to an axlMesh.
axlMesh* CurveAlgebraicConverter::toMesh(void)
{
    dtkWarn()<<"Start AlgebraicCurve mesher";

    typedef mmx::csg::algebraic2d<double>                          Curve;
    typedef mmx::geoalgebrix::bounding_box<double>                 BoundingBox;

    CurveAlgebraic * obj = dynamic_cast<CurveAlgebraic *>(this->data ());
    if (!obj) return NULL;
    //qDebug()<< "Converter Algebraic Curve";

    BoundingBox* box = static_cast<BoundingBox*> (obj->bounding_box());
    Curve* cv = static_cast<Curve *> (obj->curve());

    if(!cv) {
        qDebug() << "CurveAlgebraicConverter: problem in toMesh" ;
        return new axlMesh;
    }

    //std::cout<<"Equation  "<<cv->equation()<<std::endl;
    axlMesh* mesh = new axlMesh;
//    typedef axlMesh::Point Point;

    if(cv->nbequation()==1) {
        //qDebug()<<"Meshing planar Curve";
        double sz = box->size();
        mmx::mesher<mmx::mshr_alg2d_curve<double> > mshr(0.025*sz,0.001*sz);
        mshr.set_input(mshr.controler()->init_cell(cv->equation(),box->xmin(),box->xmax(),box->ymin(),box->ymax()));
        mshr.run();

        //qDebug()<<"Mesh:"<<mshr.output()->nbv()<<mshr.output()->nbe();
        for(unsigned i=0; i< mshr.output()->nbv(); i++) {
            mesh->push_back_vertex(mshr.output()->vertex(i)[0], mshr.output()->vertex(i)[1],0.);
        }

        for(unsigned i=0; i< mshr.output()->nbe(); i++) {
            mesh->push_back_edge(mshr.output()->edge(i).first, mshr.output()->edge(i).second);
        }

    } else {

//        Mesher3d* mesher= new Mesher3d(*box);
//        mesher->push_back(cv);
//        mesher->set_smoothness(0.07);
//        mesher->set_precision(0.01);
//        mesher->run();

//        foreach(Mesher3d::Point* p, mesher->vertices()) {
//            mesh->push_back_vertex(p->x(),p->y(),p->z());
//        }

//        //qDebug()<<"Nbe: "<<mesher->output()->edges().size();
//        foreach(Mesher3d::Edge* e, mesher->edges()) {
//            mesh->push_back_edge(mesher->output()->index_of(e->source()),
//                                 mesher->output()->index_of(e->destination()));
//        }
    }

    //qDebug()<< "Done";
    mesh->vertex_show() = false;
    mesh->edge_show()   = true;
    mesh->face_show()   = false;
    mesh->setColor(obj->color());
    mesh->setSize(obj->size());
    mesh->setOpacity(obj->opacity());
    mesh->setShader(obj->shader());

    dtkWarn() << ">> Algebraic Curve mesh --> Vertices: "<<mesh->vertex_count()<<" Edges: "<<mesh->edge_count();
    // qDebug() <<" ";
    // qDebug()<<"Algebraic Curve: meshed";
    return mesh;
}
END_PLUGIN_NAMESPACE

dtkAbstractDataConverter *createAxlSemiAlgebraicCurveAlgebraicConverter(void)
{
    return new PLUGIN_NAMESPACE CurveAlgebraicConverter;
}
