
// /////////////////////////////////////////////////////////////////
// Generated by mmxmake
// /////////////////////////////////////////////////////////////////

#include "SurfaceRational.h"

#include "axlSemiAlgebraicPlugin.h"

#include <axlCore/axlFormat.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkLog/dtkLog.h>

#include <geoalgebrix/rational_surface.hpp>

//#include <Eigen/Dense>

//#include <realroot/polynomial_fcts.hpp>
//#include <realroot/polynomial.hpp>
//#include <realroot/monomial.hpp>

//#include "MatricialPolynomial.hpp"
//#include "UtilsMatrixOpp.hpp"

#include <cmath>

#define Surface mmx::geoalgebrix::rational_surface<double>
#define BoundingBox mmx::geoalgebrix::bounding_box<double>

//typedef Eigen::MatrixXd DynamicMatrix;
//typedef Eigen::VectorXd DynamicVector;

// /////////////////////////////////////////////////////////////////
// SurfaceRationalPrivate
// /////////////////////////////////////////////////////////////////
BEGIN_PLUGIN_NAMESPACE

//#include "MatricialPolynomial.hpp"

class SurfaceRationalPrivate {

public:
    void initPolynomial(void);
    int degreeOfSurf(void) const;
//    void initMatrix(void);
//    DynamicMatrix buildMat(int nu);

public:
    Surface surface;
//    bool isMatrixComputed;
//    QVector<MatricialPolynomial> poly;
//    QVector<DynamicMatrix> mat;

};

// /////////////////////////////////////////////////////////////////
// SurfaceRational
// /////////////////////////////////////////////////////////////////

SurfaceRational::SurfaceRational(void) : axlAbstractSurfaceRational(), d(new SurfaceRationalPrivate) {
    this->setObjectName(this->identifier());
    d->surface = Surface ();
}

SurfaceRational::~SurfaceRational(void) {
    delete d;
    d = NULL;
}

bool SurfaceRational::domainIsRxR(void) const {
    return d->surface.domain().size() == 0;
}

bool SurfaceRational::registered(void) {
    //qDebug()<< "SurfaceRational::registered " << dtkAbstractDataFactory::instance()<< axlSemiAlgebraicPlugin::dataFactSingleton;
    return axlSemiAlgebraicPlugin::dataFactSingleton->registerDataType("SurfaceRational",
                                                                       createAxlSemiSurfaceAlgebraicRational);
}

QString SurfaceRational::description(void) const {
    QString info = "";
    info += "Rational surface\n";
    if (d->surface.domain().size() == 0)
        info += "   Domain: IRxIR\n";
    else
        info += "   Domain: " + write_domain() + "\n";

    info += "   Numerator #1:   " + write_numerator(0) + "\n";
    info += "   Numerator #2:   " + write_numerator(1) + "\n";
    info += "   Numerator #3:   " + write_numerator(2) + "\n";
    info += "   Denominator:      " + write_denominator() + "\n";

    return info;
}

QString SurfaceRational::identifier(void) const {
    return "SurfaceRational";
}

// TODO: maybe remove this (setData already exists)
void SurfaceRational::set(void *data) {
    d->surface = *(static_cast<Surface *>(data));
}

void SurfaceRational::setData(void *data) {
    d->surface = *(static_cast<Surface *>(data));
}

void* SurfaceRational::surface(void) {
    return &(d->surface);
}

QString SurfaceRational::write_domain(void) const {
    QString str; QTextStream out(&str);
    for (unsigned i = 0; i < d->surface.domain().size(); i++) {
        if (i > 0) out << " ";
        out << d->surface.domain(i).x() << " " << d->surface.domain(i).y();
    }
    return str;
}

QString SurfaceRational::write_numerator(unsigned index) const {
    std::string str = mmx::to_string(d->surface.numerator(index), mmx::variables("s t"));
    return QString(str.data());
}

QString SurfaceRational::write_denominator(void) const {
    std::string str = mmx::to_string(d->surface.denominator(), mmx::variables("s t"));
    return QString(str.data());
}

void SurfaceRational::setSurface(QVector<double> domain, QStringList numAndDenom) {

    typedef Surface::Point         Point;
    typedef Surface::Polynomial    Polynomial;
    
    Surface surface;

    if (numAndDenom.size() == 3 || numAndDenom.size() == 4) {
        
        // TODO: improve it!
        // Set domain
        // Pre condition: Domain size must be 0, 6 or 8
        bool fstCoord = true;
        Point p(0, 0);
        foreach (double coord, domain) {
            if (fstCoord) {
                p.x() = coord;
                fstCoord = false;
            } else {
                p.y() = coord;
                surface.add_to_domain(p);
                fstCoord = true;
            }
        }
        
        // Set numerators
        for (int k = 0; k < 3; k++) {
            surface.set_numerator(Polynomial(numAndDenom.at(k).toStdString().c_str()), k);
        }
        // Set denominator
        if (numAndDenom.size() == 4) {
            surface.set_denominator(Polynomial(numAndDenom.at(3).toStdString().c_str()));
        } else {
            surface.set_denominator(1);
        }
        
    } else {
        qDebug() << "Error in SurfaceRational::setSurface, surface may have three or four equations.";
    }

//    d->isMatrixComputed = false;

    d->surface = surface;
}

//void SurfaceRational::initInter(void) {
//    if (!d->isMatrixComputed) {
//        d->initPolynomial();
//        d->initMatrix();
//        d->isMatrixComputed = true;
//    }
//}

//DynamicMatrix SurfaceRational::getMat(int i) const {
//    return d->mat.at(i);
//}

///**
// * \brief Inversion
// *
// * Calcule l'antécédent d'un point sur une surface paramétrée\n
// * NB : La base choisie est importante, cf construction de la matrice M
// *
// * \param point: Le point sur la surface
// * \return les paramatres u0 et v0 de la paramétrisation de la surface
// */
//QPair<double, double> SurfaceRational::getParameters(const axlPoint& point) const {
//    QPair<double, double> res;

//    DynamicMatrix surfMatrix = d->mat.at(3) + point.x()*d->mat.at(0) + point.y()*d->mat.at(1) + point.z()*d->mat.at(2);
//    surfMatrix.transposeInPlace();
//    int cols = surfMatrix.cols();

//    Eigen::JacobiSVD<DynamicMatrix> svd(surfMatrix, Eigen::ComputeFullU | Eigen::ComputeFullV);
//    int rk = rank(svd.singularValues(), surfMatrix.rows(), surfMatrix.cols());

//    DynamicMatrix V = svd.matrixV();
//    DynamicMatrix K = V.block(0, rk, V.rows(), V.cols()-rk);

//    // rk < cols indique simplement que le point est sur la surface
//    if (std::fabs(K(2, 0)) > 10e-20 && (std::abs(rk - cols) == 1)) {
//        // Le calcule suivant dépend du choix de la base de la matrice M
//        res.first  = std::floor((K(0, 0)/K(2, 0))*10e6)/10e6;
//        res.second = std::floor((K(1, 0)/K(2, 0))*10e6)/10e6;
//    }

//    return res;
//}

int SurfaceRationalPrivate::degreeOfSurf(void) const {
    typedef Surface::Polynomial    Polynomial;

    int res = 0;

    for (int k = 0; k < 3; k++) {
        Polynomial num = this->surface.numerator(k);
        int degreeOfNum = degree(num);
        if (res < degreeOfNum)
            res = degreeOfNum;
    }

    Polynomial denom = this->surface.denominator();
    int degreeOfDenom = degree(denom);
    if (res < degreeOfDenom)
        res = degreeOfDenom;

    return res;
}

void SurfaceRationalPrivate::initPolynomial(void) {
//    poly.clear();
//    typedef Surface::Polynomial    Polynomial;

//    int deg = degreeOfSurf();

//    Polynomial currPoly;

    // Numerators
//    for (int k = 0; k < 3; k++) {
//        currPoly = this->surface.numerator(k);
//        MatricialPolynomial currMatPoly(deg);
//        for (Polynomial::const_iterator it = currPoly.begin(); it != currPoly.end(); it++) {
//            Trinomial currTrin((*it)[0], (*it)[1], deg - ((*it)[0] + (*it)[1]), mmx::as<double>(it->coeff()));
//            currMatPoly.addTrinomial(currTrin);
//        }
//        this->poly.push_back(currMatPoly);
//    }

//    // Denominator
//    currPoly = this->surface.denominator();
//    MatricialPolynomial currMatPoly(deg);
//    for (Polynomial::const_iterator it = currPoly.begin(); it != currPoly.end(); it++) {
//        Trinomial currTrin((*it)[0], (*it)[1], deg - ((*it)[0] + (*it)[1]), mmx::as<double>(it->coeff()));
//        currMatPoly.addTrinomial(currTrin);
//    }
//    this->poly.push_back(currMatPoly);
}

//void SurfaceRationalPrivate::initMatrix(void) {
//    mat.clear();

//    int nu = 2*(this->poly.at(0)._degree - 1);
//    DynamicMatrix Mat;
//    Mat = buildMat(nu);

//    Eigen::JacobiSVD<DynamicMatrix> svd(Mat, Eigen::ComputeFullU | Eigen::ComputeFullV);

//    int rk = rank(svd.singularValues(), Mat.rows(), Mat.cols());

//    DynamicMatrix V = svd.matrixV();
//    DynamicMatrix K;
//    K.resize(V.rows(), V.cols()-rk);
//    K = V.block(0, rk, V.rows(), V.cols()-rk);

//    int dim = (nu + 2)*(nu + 1)/2;

//    for (int i = 0; i < 4; i++) {
//        mat.push_back(K.block(i*dim, 0, dim, K.cols()));
//    }
//}

//DynamicMatrix SurfaceRationalPrivate::buildMat(int nu) {
//    DynamicMatrix M1, M2, M3, M4;
//    M1 = this->poly.at(0).getMatrix(nu);
//    M2 = this->poly.at(1).getMatrix(nu);
//    M3 = this->poly.at(2).getMatrix(nu);
//    M4 = this->poly.at(3).getMatrix(nu);

//    DynamicMatrix M;
//    int nbRows = M1.rows();
//    int nbCols = M1.cols() + M2.cols() + M3.cols() + M4.cols();

//    M.resize(nbRows, nbCols);
//    M << M1, M2, M3, M4;

//    return M;
//}

double SurfaceRational::startParam_u(void){
    double r=d->surface.domain(0).x();
    for(unsigned i=1; i< d->surface.domain().size();i++)
        r= std::min(r, d->surface.domain(i).x());
    return r;
}

double SurfaceRational::endParam_u(void){
    double r=d->surface.domain(0).x();
    for(unsigned i=1; i< d->surface.domain().size();i++)
        r= std::max(r, d->surface.domain(i).x());
    return r;
}

double SurfaceRational::startParam_v(void){
    double r=d->surface.domain(0).y();
    for(unsigned i=1; i< d->surface.domain().size();i++)
        r= std::min(r, d->surface.domain(i).y());
    return r;
}

double SurfaceRational::endParam_v(void){
    double r=d->surface.domain(0).y();
    for(unsigned i=1; i< d->surface.domain().size();i++)
        r= std::max(r, d->surface.domain(i).y());
    return r;
}

void SurfaceRational::eval(axlPoint *point, double u, double v) {
    Surface::Point p(0,0,0);
    d->surface.eval(p, u, v);
    point->setCoordinates(p[0],p[1],p[2]);
}

void SurfaceRational::normal(axlPoint *normal, double u,double v)
{
    Surface::Point p(0,0,0);
    d->surface.normal(p, u, v);
    normal->setCoordinates(p[0],p[1],p[2]);
}


void SurfaceRational::eval(double& x, double& y, double& z, double u, double v) {
    Surface::Point p(0,0,0);
    d->surface.eval(p,u,v);
    x=p[0];y=p[1];z=p[2];
}

END_PLUGIN_NAMESPACE

// /////////////////////////////////////////////////////////////////
// Type instantiation
// /////////////////////////////////////////////////////////////////
dtkAbstractData *createAxlSemiSurfaceAlgebraicRational(void) {
    return new PLUGIN_NAMESPACE SurfaceRational;
}

#undef Surface
#undef SurfaceRational
#undef BoundingBox 
