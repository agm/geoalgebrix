/*****************************************************************************
 * SurfaceRational Reader for axel
 *****************************************************************************
 *
 *  Created: Sam 21 jan 2012 20:51:36 CET
 *       By: mourrain
 * Comments: 
 *   Generated with maxel
 *
 *  Changes:
 *****************************************************************************/
#include "SurfaceRationalReader.h"

#include "axlSemiAlgebraicPlugin.h"

#include "SurfaceRational.h"
#include <dtkCoreSupport/dtkAbstractDataFactory.h>

#include <geoalgebrix/rational_surface.hpp>


// /////////////////////////////////////////////////////////////////
// SurfaceRationalReader
// /////////////////////////////////////////////////////////////////
BEGIN_PLUGIN_NAMESPACE

SurfaceRationalReader::SurfaceRationalReader(void)  { }

SurfaceRationalReader::~SurfaceRationalReader(void) { }

QString SurfaceRationalReader::identifier(void) const {
    return "SurfaceRationalReader";
}

QString SurfaceRationalReader::description(void) const {
    return "SurfaceRationalReader";
}

QStringList SurfaceRationalReader::handled(void) const {
    return QStringList() << "SurfaceRational";
}

bool SurfaceRationalReader::registered(void) {
    return axlSemiAlgebraicPlugin::dataFactSingleton->registerDataReaderType("SurfaceRationalReader", QStringList(), createAxlSemiSurfaceAlgebraicRationalReader);
}

bool SurfaceRationalReader::accept(const QDomNode& node) {
     QDomElement element = node.toElement();

    if(element.tagName() != "surface")      return false;
    if(element.attribute("type") != "rational")  return false;

    if(!hasChildNode(element, "domain"))         return false;
    if(!hasChildNode(element, "polynomial"))     return false;

    return true;
}

bool SurfaceRationalReader::reject(const QDomNode& node) {
    return !this->accept(node);
}

axlAbstractData *SurfaceRationalReader::read(const QDomNode& node) {
//    typedef mmx::geoalgebrix::rational_surface<double>  Surface;
//    typedef Surface::Point                            Point;
//    typedef Surface::Polynomial                       Polynomial;
    if(!accept(node))
        dtkWarn() << "Rational surface not accepted";

    QDomElement e = node.toElement();

    //QList<double>   box;
    QDomNodeList    nodelist  = e.elementsByTagName("domain");
    QDomElement     element   = nodelist.item(0).toElement();
    QStringList     domainStr = element.text().simplified().split(QRegExp("\\s+"));
    QVector<double> domain;
    QStringList     numAndDenom;

    foreach (QString coord, domainStr) {
        domain.push_back(coord.toDouble());
    }

    nodelist = e.elementsByTagName("polynomial") ;
    for(int i = 0; i < nodelist.size(); i++) {
      numAndDenom.push_back(nodelist.item(i).toElement().text());
    }

    SurfaceRational* data = new SurfaceRational;
    data->setSurface(domain, numAndDenom);

    this->setNameOf  (data, e);
    this->setColorOf (data, e);
    this->setShaderOf(data, e);

    return data;
}
END_PLUGIN_NAMESPACE

dtkAbstractDataReader* createAxlSemiSurfaceAlgebraicRationalReader(void) {
    return new PLUGIN_NAMESPACE SurfaceRationalReader;
}
