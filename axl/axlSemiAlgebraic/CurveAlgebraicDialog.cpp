/* CurveAlgebraicDialog.cpp
 * Created: Mar  7 fév 2012 18:29:21 CET
 *      By: mourrain
 * Comments:
 *    Generated by mmk
 * Change log:
 *
 */
// /////////////////////////////////////////////////////////////////
#include <cmath>
#include "CurveAlgebraicDialog.h"

#include "CurveAlgebraic.h"
#include <dtkCoreSupport/dtkAbstractData.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkGuiSupport/dtkColorButton.h>

#include "axlSemiAlgebraicPlugin.h"

#include <axlGui/axlInspectorUtils.h>

#include <QtGui>

BEGIN_PLUGIN_NAMESPACE
class CurveAlgebraicDialogPrivate
{
public:
    CurveAlgebraic *data;

    QSlider *sliderSize;

    dtkColorButton *colorButton;

//    QComboBox *comboBoxShader;
//    QCheckBox *checkBoxShader;
//    QLineEdit *dataEditShader;
//    QPushButton *buttonShader;

    QSlider *sliderOpacity;

    QDoubleSpinBox *spinBoxTubeFilterRadius;
};

CurveAlgebraicDialog::CurveAlgebraicDialog(QWidget *parent) : axlInspectorObjectInterface(parent), d(new CurveAlgebraicDialogPrivate)
{
    d->data = NULL;

    d->colorButton = NULL;
    d->sliderSize = NULL;
    d->sliderOpacity = NULL;

//    d->comboBoxShader = NULL;
//    d->checkBoxShader = NULL;
//    d->dataEditShader = NULL;
//    d->buttonShader = NULL;

    d->spinBoxTubeFilterRadius = NULL;
}

CurveAlgebraicDialog::~CurveAlgebraicDialog(void)
{
    delete d;
    d = NULL;
}

bool CurveAlgebraicDialog::registered(void)
{
    return axlInspectorObjectFactory::instance()->registerInspectorObject("CurveAlgebraic", createAxlSemiAlgebraicCurveAlgebraicDialog);
}

QSize CurveAlgebraicDialog::sizeHint(void) const
{
    return QSize(300, 300);
}

void CurveAlgebraicDialog::setData(dtkAbstractData *data)
{
    if( (d->data = dynamic_cast<CurveAlgebraic *>(data)) )
    {
        initWidget();
    }
}

//void CurveAlgebraicDialog::initComboBoxShaderValue(void) {
//    if(d->comboBoxShader)
//    {
//        // First add item of axlShader.qrc, then find shader from shader path
//        QDir dirShader( ":axlShader/shader/");
//        dirShader.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);

//        QFileInfoList list = dirShader.entryInfoList();
//        //        for (int i = 0; i < list.size(); ++i) {
//        //            d->comboBoxShader->addItem(list.at(i).fileName());
//        //        }

//        QSettings settings("inria", "dtk");
//        QString defaultPath;
//        settings.beginGroup("shader");
//        QString defaultPathShader = settings.value("path", defaultPath).toString();
//        defaultPathShader.append("/");

//        QDir defaultDirShader(defaultPathShader);
//        QStringList filters;
//        filters << "*.xml";
//        defaultDirShader.setNameFilters(filters);
//        QFileInfoList list2 = defaultDirShader.entryInfoList();

//        list.append(list2);

//        QStringList items;

//        for (int i = 0; i < list.size(); ++i) {
//            if(!items.contains(list.at(i).fileName()))
//                items << list.at(i).fileName();
//        }

//        qSort(items.begin(), items.end(), caseInsensitiveLessThan);
//        int indInitShader = -1;
//        int indCurrentShader = -1;


//        foreach(QString item, items)
//        {
//            indCurrentShader++;
//            d->comboBoxShader->addItem(item);

//            QFileInfo currentFileInfo(d->dataEditShader->text());

//            if(currentFileInfo.exists())
//            {
//                if(item == currentFileInfo.fileName())
//                    indInitShader =indCurrentShader;
//            }
//        }

//        //init the value from the lineEditShader.
//        if(indInitShader != -1)
//            d->comboBoxShader->setCurrentIndex(indInitShader);

//    }
//}

int CurveAlgebraicDialog::initSizeValue(void)
{
    return 100*(std::log(d->data->size()/0.125))/std::log(2);
}


int CurveAlgebraicDialog::initOpacityValue(void)
{
    double initOpacity = 0.0;
    double opacity = d->data->opacity();
    if(opacity > initOpacity)
        initOpacity = opacity;

    return 100 * (1.0 - initOpacity);
}

//QString CurveAlgebraicDialog::initShaderValue(void)
//{
//    return  d->data->shader();
//}


QColor CurveAlgebraicDialog::initColorValue(void)
{
    return d->data->color();
}

void CurveAlgebraicDialog::initWidget()
{

    ///////COLOR///////////////
    d->colorButton = new dtkColorButton(this);

    QHBoxLayout *layoutColorButton = new QHBoxLayout;
    layoutColorButton->addWidget(new QLabel("Color",this));
    layoutColorButton->addWidget(d->colorButton);
    d->colorButton->setColor(this->initColorValue());

    /////// SIZE ///////////////
    d->sliderSize = new QSlider(Qt::Horizontal, this);
    d->sliderSize->setMaximum(-800);
    d->sliderSize->setMaximum(500);
    d->sliderSize->setValue(initSizeValue());

    QHBoxLayout *layoutSize = new QHBoxLayout;
    layoutSize->addWidget(new QLabel("Size     ",this));
    layoutSize->addWidget(d->sliderSize);

    /////// OPACITY ///////////////
    d->sliderOpacity = new QSlider(Qt::Horizontal, this);
    d->sliderOpacity->setMaximum(100);

    QHBoxLayout *layoutOpacity = new QHBoxLayout;
    layoutOpacity->addWidget(new QLabel("Opacity",this));
    layoutOpacity->addWidget(d->sliderOpacity);
    d->sliderOpacity->setMaximum(100);
    d->sliderOpacity->setValue(0);//initOpacityValue());

//    ///////SHADER///////////////
//    d->comboBoxShader = new QComboBox(this);
//    d->comboBoxShader->setInsertPolicy(QComboBox::InsertAlphabetically);

//    d->checkBoxShader = new QCheckBox(this);
//    d->dataEditShader = new QLineEdit(this);
//    d->buttonShader = new QPushButton(this);
//    d->buttonShader->setText("open");
//    d->dataEditShader->setText(this->initShaderValue());
//    if(d->dataEditShader->text().isEmpty())
//    {
//        d->dataEditShader->setEnabled(false);
//        d->buttonShader->setEnabled(false);
//        d->comboBoxShader->setEnabled(false);
//    }
//    else
//        d->checkBoxShader->setChecked(true);

//    QVBoxLayout *layoutShader = new QVBoxLayout;
//    QHBoxLayout *layoutShader1 = new QHBoxLayout;

//    QLabel *labelShader  = new QLabel("Shader",this);
//    layoutShader1->addWidget(labelShader);
//    layoutShader1->addWidget(d->checkBoxShader);
//    layoutShader1->addWidget(d->comboBoxShader);
//    layoutShader1->addWidget(d->buttonShader);

//    layoutShader1->setStretchFactor(labelShader, 2);
//    layoutShader1->setStretchFactor(d->checkBoxShader, 1);
//    layoutShader1->setStretchFactor(d->comboBoxShader, 4);
//    layoutShader1->setStretchFactor(d->buttonShader, 3);

//    layoutShader->addLayout(layoutShader1);
//    layoutShader->addWidget(d->dataEditShader);

    ///////DIALOG///////////////
    QVBoxLayout *layoutTop = new QVBoxLayout;
    QLabel * layoutTitle = new QLabel("Algebraic Curve:", this);
    layoutTitle->setFixedWidth(270);
    layoutTop->addWidget(layoutTitle);
    layoutTop->addLayout(layoutColorButton);
    layoutTop->addLayout(layoutSize);
    layoutTop->addLayout(layoutOpacity);
//    layoutTop->addLayout(layoutShader);
//    layoutTop->addStretch(10);


    QWidget *top = new QWidget(this);
    top->setLayout(layoutTop);

    connect(d->colorButton, SIGNAL(colorChanged(QColor)), this, SLOT(onColorChanged(QColor)));
    connect(d->sliderSize, SIGNAL(valueChanged(int)), this, SLOT(onSizeChanged(int)));
    connect(d->sliderOpacity, SIGNAL(valueChanged(int)), this, SLOT(onOpacityChanged(int)));

//    connect(d->comboBoxShader, SIGNAL(currentIndexChanged(QString)), this, SLOT(onLineEditShaderChanged(QString)));
//    connect(d->checkBoxShader, SIGNAL(clicked(bool)), this, SLOT(onShaderStateChanged(bool)));
//    connect(d->buttonShader, SIGNAL(clicked()), this, SLOT(openShader()));
//    connect(d->dataEditShader, SIGNAL(textChanged(QString)), this, SLOT(onShaderChanged(QString)));

}

void CurveAlgebraicDialog::onColorChanged(QColor color)
{
    d->data->setColor(color);

//    emit modifiedProperty(d->data, AXL_PROPERTY_COLOR);
//    emit update();
    d->data->touchProperty();
}

void CurveAlgebraicDialog::onSizeChanged(int size)
{
    double size_d = std::pow(2.0,size/100.0-3);
    QVariant variant = d->data->QObject::property("size");
    if(variant.isValid())
    {
        d->data->setSize(size_d);
        //emit dataChangedBySize(d->data, size_d);
        emit tubeFilterRadiusChanged(d->data, size_d);
    } else
        qDebug()<<"No size change";
//    emit update();
}

//void CurveAlgebraicDialog::openShader()
//{
//    if(d->dataEditShader->isEnabled())
//    {
//        QString fileToOpen;
//        fileToOpen = QFileDialog::getOpenFileName(this, tr("Open shader"), "", tr("xml document (*.xml)"));
//        d->dataEditShader->setText(fileToOpen);
//    }
//}

//void CurveAlgebraicDialog::onShaderChanged(QString shader)
//{
//    d->data->setShader(shader);

//    emit dataChangedByShader(d->data, d->dataEditShader->text());
//}

//void CurveAlgebraicDialog::onShaderStateChanged(bool isShader)
//{
//    if(isShader)
//    {
//        d->comboBoxShader->setEnabled(true);
//        d->dataEditShader->setEnabled(true);
//        d->buttonShader->setEnabled(true);
//        onLineEditShaderChanged(d->comboBoxShader->currentText());

//        emit dataChangedByShader(d->data, d->dataEditShader->text());
//    }
//    else
//    {
//        d->comboBoxShader->setEnabled(false);
//        d->dataEditShader->setEnabled(false);
//        d->buttonShader->setEnabled(false);
//    }

//    emit update();
//}

//void CurveAlgebraicDialog::onLineEditShaderChanged(QString shader) {
//    // First add item of axlShader.qrc, then find shader from shader path
//    QDir dirShader( ":axlShader/shader/");
//    dirShader.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);

//    QFileInfo currentFile(dirShader, shader);
//    if(!currentFile.exists())
//    {
//        QSettings settings("inria", "dtk");
//        QString defaultPath;
//        settings.beginGroup("shader");
//        QString defaultPathShader = settings.value("path", defaultPath).toString();
//        defaultPathShader.append("/");

//        QDir defaultDirShader(defaultPathShader);
//        currentFile = QFileInfo(defaultDirShader, shader);

//    }

//    d->dataEditShader->setText(currentFile.absoluteFilePath());
//}

void CurveAlgebraicDialog::onOpacityChanged(int opacity)
{
    double opacity_d = 1.0 - 0.01 * opacity; // range from 0.00 to 1.00
    d->data->setOpacity(opacity_d);
//    emit modifiedProperty(d->data, AXL_PROPERTY_OPACITY);
//    emit update();
    d->data->touchProperty();
}

END_PLUGIN_NAMESPACE

axlInspectorObjectInterface *createAxlSemiAlgebraicCurveAlgebraicDialog(void)
{
    return new PLUGIN_NAMESPACE CurveAlgebraicDialog;
}
