/*****************************************************************************
 * CurveAlgebraic Writer for axel
 *****************************************************************************
 *
 *  Created: Sam 28 jan 2012 16:57:22 CET
 *       By: mourrain
 * Comments: 
 *   Generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef CurveAlgebraicWriter_h
#define CurveAlgebraicWriter_h

#include <axlCore/axlAbstractDataWriter.h>
#include "axlSemiAlgebraicPluginExport.h"

//======================================================================
namespace axlSemiAlgebraic {
//======================================================================

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraicPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraicWriter : public axlAbstractDataWriter
{
    Q_OBJECT

public :
    CurveAlgebraicWriter(void);
   ~CurveAlgebraicWriter(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;

    static bool registered(void);

public:
    bool accept(dtkAbstractData*);
    bool reject(dtkAbstractData*);
    
    QDomElement write(QDomDocument *doc, dtkAbstractData *data);

private:
};

//======================================================================
} 
//======================================================================
dtkAbstractDataWriter *createAxlSemiAlgebraicCurveAlgebraicWriter(void);
//======================================================================
#endif
