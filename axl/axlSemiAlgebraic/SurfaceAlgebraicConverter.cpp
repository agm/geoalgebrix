/* SurfaceAlgebraicConverter.h
 * Created: Fri Feb 10 10:17:58 CET 2012
 *      By: mperrine
 * Comments:
 *    Generated by maxel
 * Change log:
 *
 */
// /////////////////////////////////////////////////////////////////
#include "SurfaceAlgebraicConverter.h"
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <axlCore/axlPoint.h>
#include <axlCore/axlMesh.h>
#include "SurfaceAlgebraic.h"

#include "axlSemiAlgebraicPlugin.h"

#include "axel_env.hpp"
#include <geoalgebrix/algebraic3d.hpp>
#include <geoalgebrix/mesher.hpp>
#include <geoalgebrix/mshr_csg3d.hpp>

#include <axlSemiAlgebraic/SurfaceAlgebraic.h>

//#define Surface  mmx::geoalgebrix::algebraic_surface<double,ENV>
#define Surface mmx::csg::algebraic3d<double>
#define BoundingBox mmx::geoalgebrix::bounding_box<double>
// /////////////////////////////////////////////////////////////////
// SurfaceAlgebraicConverter
// /////////////////////////////////////////////////////////////////
BEGIN_PLUGIN_NAMESPACE

SurfaceAlgebraicConverter::SurfaceAlgebraicConverter(void) : axlAbstractDataConverter () { }

SurfaceAlgebraicConverter::~SurfaceAlgebraicConverter(void) { } 

QString SurfaceAlgebraicConverter::description(void) const
{
    return "SurfaceAlgebraicConverter";
}

QStringList SurfaceAlgebraicConverter::fromTypes(void) const
{
    return QStringList() << "SurfaceAlgebraic";
}

QString SurfaceAlgebraicConverter::toType(void) const
{
    return "axlMesh";
}

bool SurfaceAlgebraicConverter::registered(void)
{
    return axlSemiAlgebraicPlugin::dataFactSingleton->registerDataConverterType("SurfaceAlgebraicConverter", QStringList(), "axlMesh", createAxlSemiAlgebraicSurfaceAlgebraicConverter);
}

/// Method used to convert to an axlMesh.
axlMesh* SurfaceAlgebraicConverter::toMesh(void)
{
    SurfaceAlgebraic * obj = dynamic_cast<SurfaceAlgebraic *>(this->data ());
    if (!obj) return NULL;

    BoundingBox* box = static_cast<BoundingBox*> (obj->bounding_box());
    Surface * sf = static_cast<Surface *> (obj->surface());
    if(!sf) {
       dtkWarn() << "SurfaceAlgebraicConverter: problem in toAxlMesh" ;
        return new axlMesh;
    }

    qDebug()<<"Algebraic Surface"<< obj->write_equation()<<" converted to AxlMesh";

    double sz = box->size();
    mmx::csg::generic* a = new mmx::csg::algebraic3d<double>(sf->equation());
    mmx::mesher<mmx::mshr_csg3d<double> > mshr(0.05*sz,0.01*sz);
    mshr.set_input(mshr.controler()->init_cell(a,
                                               box->xmin(),box->xmax(),box->ymin(),box->ymax(),box->zmin(),box->zmax()));
    mshr.run();
    qDebug() << "Meshed";

    axlMesh *mesh = new axlMesh;
//    typedef axlMesh::Point Point;

//    int c=0;
    Surface::Polynomial dx= diff(sf->equation(),0), dy= diff(sf->equation(),1), dz=diff(sf->equation(),2);

    double x[3];
    for(unsigned i=0; i<mshr.polygonizer()->nbv() ; i++) {
        for(unsigned k=0;k<3;k++)
            x[k]=mshr.polygonizer()->vertex(i)[k];
        mesh->push_back_vertex(x[0],x[1],x[2]);
    }

//        Point* n= new Point(dx(p->x(),p->y(),p->z()), dy(p->x(),p->y(),p->z()), dz(p->x(),p->y(),p->z()));
//        //qDebug() << n->x()<< n->y() << n->z();
//        mesh->push_back_normal(n);
//    }

//    mesh->normal_used() = true;

//    foreach(mmxEdge* e, msh->output()->edges()) {
//        mesh->push_back_edge(msh->output()->index_of(e->source()), msh->output()->index_of(e->destination()));
//    }

//    double cs=0,nx,ny,nz,x0,y0,z0,x1,y1,z1,x2,y2,z2;
    for(unsigned i=0; i<mshr.polygonizer()->nbf() ; i++) {
        axlMesh::Face f;
        for(unsigned k=0;k<mshr.polygonizer()->face(i).size();k++)
            f<<mshr.polygonizer()->face(i).at(k);
        mesh->push_back_face(f);
    }

    mesh->vertex_show() = false;
    mesh->edge_show() = false;
    mesh->face_show() = true;

    //mesh->setSize(0.3);

    mesh->setColor(obj->color());
    mesh->setOpacity(obj->opacity());
    mesh->setShader(obj->shader());

    qDebug()<<">> Graphic: "
           << " Vertices: "<< mesh->vertex_count()
           << " Edges: "<< mesh->edge_count()
           << " Faces: "<< mesh->face_count()
           << " Normals: "<< mesh->normal_count();

    return mesh;
}
END_PLUGIN_NAMESPACE

dtkAbstractDataConverter *createAxlSemiAlgebraicSurfaceAlgebraicConverter(void)
{
    return new PLUGIN_NAMESPACE SurfaceAlgebraicConverter;
}
