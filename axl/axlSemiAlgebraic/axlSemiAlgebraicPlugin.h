/*****************************************************************************
 * axlSemiAlgebraic plugin for axel
 ****************************************************************************/
#ifndef axlSemiAlgebraicPlugin_h
#define axlSemiAlgebraicPlugin_h

#include <dtkCoreSupport/dtkPlugin.h>
#include "axlSemiAlgebraicPluginExport.h"

class dtkAbstractDataFactory;
class dtkAbstractProcessFactory;

class axlSemiAlgebraicPluginPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT axlSemiAlgebraicPlugin: public dtkPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.axlSemiAlgebraicPlugin" FILE "axlSemiAlgebraicPlugin.json")

public:
    axlSemiAlgebraicPlugin(QObject *parent = 0);
   ~axlSemiAlgebraicPlugin(void);

    virtual bool initialize(void);
    virtual bool uninitialize(void);

    virtual QString name(void) const;
    virtual QString description(void) const;

    virtual QStringList tags(void) const;
    virtual QStringList types(void) const;

public:
    static dtkAbstractDataFactory *dataFactSingleton;
    static dtkAbstractProcessFactory *processFactSingleton;

private:
    axlSemiAlgebraicPluginPrivate *d;
};

#endif
