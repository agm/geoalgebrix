/*****************************************************************************
 * CurveAlgebraic Reader for axel
 *****************************************************************************
 *
 *  Created: Sam  5 nov 2011 10:31:10 CET
 *       By: mourrain
 * Comments: 
 *   Generated with mmxaxel
 *
 *  Changes:
 *****************************************************************************/
#include "CurveAlgebraicReader.h"
#include "CurveAlgebraic.h"
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
//#include <axlCore/....h>

#include "axlSemiAlgebraicPlugin.h"


// /////////////////////////////////////////////////////////////////
// CurveAlgebraicReader
// /////////////////////////////////////////////////////////////////
BEGIN_PLUGIN_NAMESPACE

CurveAlgebraicReader::CurveAlgebraicReader(void)  { }

CurveAlgebraicReader::~CurveAlgebraicReader(void) { }

QString CurveAlgebraicReader::identifier(void) const
{
    return "CurveAlgebraicReader";
}

QString CurveAlgebraicReader::description(void) const
{
    return "CurveAlgebraicReader";
}

QStringList CurveAlgebraicReader::handled(void) const
{
    return QStringList() << "CurveAlgebraic";
}

bool CurveAlgebraicReader::registered(void)
{
    return axlSemiAlgebraicPlugin::dataFactSingleton->registerDataReaderType("CurveAlgebraicReader", QStringList(), createAxlSemiAlgebraicCurveAlgebraicReader);
}

bool CurveAlgebraicReader::accept(const QDomNode& node)
{
    dtkWarn()<<"Algebraic curve reader";

    QDomElement element = node.toElement();

    if(element.tagName() != "curve")  return false;

    if(element.attribute("type") != "algebraic")  return false;

    if(!hasChildNode(element, "domain"))  return false;

    if(!hasChildNode(element, "polynomial"))  return false;

    return true;
}

bool CurveAlgebraicReader::reject(const QDomNode& node)
{
    return !this->accept(node);
}

axlAbstractData *CurveAlgebraicReader::read(const QDomNode& node)
{    
    if(!accept(node))
        qDebug()<<"Algebraic Curve Not accepted" ;

    dtkWarn() << "Reading Algebraic Curve:";

    QDomElement e = node.toElement();
    CurveAlgebraic *curve = new CurveAlgebraic;

    QDomNodeList nodelist = e.elementsByTagName("domain") ;
    QDomElement  element = nodelist.item(0).toElement() ;
    QStringList  list = element.text().simplified().split(QRegExp("\\s+")) ;

    QVector<double> box;
    foreach(QString s, list) {
        box << s.toDouble() ;
    }
    curve->setDomain(box);

    nodelist = e.elementsByTagName("polynomial") ;
    if(nodelist.size()>0) curve->addEquation(nodelist.item(0).toElement().text());
    if(nodelist.size()>1) curve->addEquation(nodelist.item(1).toElement().text());

    qDebug()<<"  Domain : "<<curve->write_domain().toStdString().c_str();
    for(unsigned i =0; i< curve->nbEquations();i++)
        qDebug()<<"  Equation"<<QString::number(i).toStdString().c_str()<<":" <<curve->write_equation().toStdString().c_str();

    this->setNameOf  (curve, e);
    this->setColorOf (curve, e);
    this->setSizeOf  (curve, e);
    this->setShaderOf(curve, e);

    return curve;
}
END_PLUGIN_NAMESPACE

dtkAbstractDataReader *createAxlSemiAlgebraicCurveAlgebraicReader(void)
{
  return new PLUGIN_NAMESPACE CurveAlgebraicReader;
}
