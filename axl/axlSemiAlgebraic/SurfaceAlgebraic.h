
/*****************************************************************************
 * SurfaceAlgebraic data for axel
 ****************************************************************************/
# ifndef SurfaceAlgebraic_hpp
# define SurfaceAlgebraic_hpp

# include <axlCore/axlAbstractSurfaceImplicit.h>
# include "axlSemiAlgebraicPluginExport.h"
#include <axlCore/axlPoint.h>

namespace axlSemiAlgebraic {

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceAlgebraicPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceAlgebraic : public axlAbstractSurfaceImplicit
{
    Q_OBJECT
public:
    SurfaceAlgebraic(void);
    virtual ~SurfaceAlgebraic(void);

    virtual QString description(void) const;
    virtual QString identifier (void) const;

    static bool registered(void);

    void *surface(void);
    void *bounding_box(void) const;

    unsigned nb_equations() const;

    QString write_domain() const;
    QString write_equation() const;

    double eval(double *point) const;

public slots:
    void setData(void *data);
    void setSurface(const QVector<double>&, const QString&);

private:
    double parseEval(QString equation) const;

private:
    SurfaceAlgebraicPrivate *d;
};

} // namespace axlSemiAlgebraic

dtkAbstractData *createAxlSemiAlgebraicSurfaceAlgebraic(void);

# endif
