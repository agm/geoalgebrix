/*****************************************************************************
 * CurveAlgebraic Converter for axel
 *****************************************************************************
 *
 *  Created: Sam 21 jan 2012 18:20:12 CET
 *       By: mourrain
 * Comments: 
 *       Generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef CurveAlgebraicConverter_h
#define CurveAlgebraicConverter_h

#include <axlCore/axlAbstractDataConverter.h>
#include "axlSemiAlgebraicPluginExport.h"

class AXLSEMIALGEBRAICPLUGIN_EXPORT axlMesh;
//======================================================================
namespace axlSemiAlgebraic {
//======================================================================
class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraicPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraicConverter : public axlAbstractDataConverter
{
    Q_OBJECT

public :
      CurveAlgebraicConverter(void);
     ~CurveAlgebraicConverter(void);

public:
    QString   description (void) const;
    QStringList fromTypes (void) const;
    QString        toType (void) const;

    static bool registered (void);

public:
    
    axlMesh* toMesh(void);

private:
};
//======================================================================
}
//======================================================================

dtkAbstractDataConverter *createAxlSemiAlgebraicCurveAlgebraicConverter(void);

#endif
