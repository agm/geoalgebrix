/*****************************************************************************
 * CurveRational Converter for axel
 *****************************************************************************
 *
 *  Created: Tue Feb 14 10:54:00 CET 2012
 *       By: mperrine
 * Comments: 
 *   Generated with maxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef CurveRationalConverter_h
#define CurveRationalConverter_h

#include <axlCore/axlAbstractDataConverter.h>
#include "axlSemiAlgebraicPluginExport.h"

class AXLSEMIALGEBRAICPLUGIN_EXPORT axlMesh;

namespace axlSemiAlgebraic { // BEGIN_PLUGIN_NAMESPACE 

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveRationalPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveRationalConverter : public axlAbstractDataConverter
{
    Q_OBJECT

public:
      CurveRationalConverter(void);
     ~CurveRationalConverter(void);

public:
      QString   description (void) const;
      QStringList fromTypes (void) const;
      QString        toType (void) const;

      static bool registered (void);

public:
    
      axlMesh* toMesh(void);

private:
};

} // END_PLUGIN_NAMESPACE 

dtkAbstractDataConverter *createAxlSemiAlgebraicCurveRationalConverter(void);

# endif
