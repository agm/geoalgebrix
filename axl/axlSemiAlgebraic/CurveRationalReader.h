/*****************************************************************************
 * CurveRational Reader for axel
 *****************************************************************************
 *
 *  Created: Tue Feb 14 10:53:46 CET 2012
 *       By: mperrine
 * Comments: 
 *   Generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef CurveRationalReader_hpp
#define CurveRationalReader_hpp
class QDomNode;

#include <axlCore/axlAbstractDataReader.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic {

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveRationalPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveRationalReader : public axlAbstractDataReader
{
    Q_OBJECT

public :
      CurveRationalReader(void);
     ~CurveRationalReader(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;
    
    static bool registered(void);

public:
    bool accept(const QDomNode& node);
    bool reject(const QDomNode& node);
    
    axlAbstractData *read(const QDomNode& node);

};

} // namespace axlSemiAlgebraic

dtkAbstractDataReader *createAxlSemiAlgebraicCurveRationalReader(void);

# endif
