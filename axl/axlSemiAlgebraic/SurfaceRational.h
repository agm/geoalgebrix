
/*****************************************************************************
 * SurfaceRational data for axel
 ****************************************************************************/
# ifndef SurfaceRational_hpp
# define SurfaceRational_hpp

#include <axlCore/axlAbstractSurfaceRational.h>
#include "axlSemiAlgebraicPluginExport.h"
//#include <Eigen/Dense>

namespace axlSemiAlgebraic { // BEGIN_PLUGIN_NAMESPACE 

//class mmx::geoalgebrix::rational_surface<double>::Polynomial;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceRationalPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceRational : public axlAbstractSurfaceRational
{
  Q_OBJECT
public:
             SurfaceRational(void);
    virtual ~SurfaceRational(void);

    bool domainIsRxR(void) const;

    virtual QString description(void) const;
    virtual QString identifier (void) const;

    static bool registered(void);

public:
    void* surface(void);

    QString write_domain      (void) const;
    QString write_numerator   (unsigned) const;
    QString write_denominator (void) const;

//    Eigen::MatrixXd getMat(int i) const;

    void setSurface(QVector<double> domain, QStringList numAndDenom);
//    void initInter(void);
//    QPair<double, double> getParameters(const axlPoint& point) const;

    void eval(axlPoint *point, double u, double v);
    void eval(double& x, double& y, double& z, double u, double v);
    void normal(axlPoint *normal, double u,double v);

    double startParam_u(void);
    double endParam_u(void);
    double startParam_v(void);
    double endParam_v(void);

public slots:
    void set(void *data);
    void setData(void *data);

private:
    SurfaceRationalPrivate *d;
};
} // END_PLUGIN_NAMESPACE 

dtkAbstractData *createAxlSemiSurfaceAlgebraicRational(void);

# endif
