
/*****************************************************************************
 * CurveRational data for axel
 ****************************************************************************/
# ifndef CurveRational_hpp
# define CurveRational_hpp

#include <axlCore/axlAbstractData.h>
#include <axlCore/axlAbstractCurveRational.h>
#include "axlSemiAlgebraicPluginExport.h"

//#include <Eigen/Dense>

namespace axlSemiAlgebraic {

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveRationalPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveRational : public axlAbstractCurveRational
{
    Q_OBJECT
public:
             CurveRational(void);
    virtual ~CurveRational(void);

    virtual QString description(void) const;
    virtual QString identifier (void) const;

    static bool registered(void);

public:

    //Eigen::MatrixXd getMat(int i) const;

    void* curve(void);

    QString write_domain      (void) const;
    QString write_numerator   (unsigned) const;
    QString write_denominator (void) const;

    int degreeOfCurve() const;
    //Eigen::MatrixXd getCoeffsByExpoenent();

    void setCurve(QVector<double> domain, QStringList list);
//    void initInter(void);
//    double getParameters(const axlPoint& point) const;

    axlPoint eval(double t);
    void eval(axlPoint* point, double t);

    double startParam_t(void);
    double endParam_t(void);

public slots:
    void setData(void *data);

private:
     CurveRationalPrivate *d;
};

} // namespace axlSemiAlgebraic

dtkAbstractData *createAxlSemiAlgebraicCurveRational(void);

# endif
