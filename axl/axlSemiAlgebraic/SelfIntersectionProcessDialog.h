/* SelfIntersectionProcessDialog.h
 * Created: Dim 24 jui 2012 19:45:04 CEST
 *      By: mourrain
 * Comments: generated by maxl
 * 
 * Change log:
 * 
 */
#ifndef SelfIntersectionProcessDialog_h
#define SelfIntersectionProcessDialog_h

# include <axlCore/axlAbstractData.h>
# include <axlGui/axlInspectorToolFactory.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic { //BEGIN_PLUGIN_NAMESPACE 

class SelfIntersectionProcessDialogPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SelfIntersectionProcessDialog : public axlInspectorToolInterface
{
    Q_OBJECT

public:
     SelfIntersectionProcessDialog(QWidget *parent = 0);
    ~SelfIntersectionProcessDialog(void);

    static bool registered(void);

signals:
    void dataInserted(axlAbstractData *data);

public slots:
    void run(void);
 
private:
    SelfIntersectionProcessDialogPrivate *d;
};
} // END_PLUGIN_NAMESPACE 

axlInspectorToolInterface *createAxlSemiAlgebraicSelfIntersectionProcessDialog(void);

#endif