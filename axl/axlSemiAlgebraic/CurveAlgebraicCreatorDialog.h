#ifndef CurveAlgebraicCREATORPROCESSDIALOG_H
#define CurveAlgebraicCREATORPROCESSDIALOG_H

# include <axlCore/axlAbstractData.h>
# include <axlGui/axlInspectorToolFactory.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic { // BEGIN_PLUGIN_NAMESPACE

class CurveAlgebraicCreatorDialogPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraicCreatorDialog : public axlInspectorToolInterface {
    Q_OBJECT

public:
    CurveAlgebraicCreatorDialog(QWidget* parent = 0);
    virtual ~CurveAlgebraicCreatorDialog();

    static bool registered(void);
    QVector<double> parseDomain(const QString& domainQString) const;

signals:
    void dataInserted(axlAbstractData* data);

public slots:
    void fillFields(int surfaceIndex);
    void run(void);

private:
    CurveAlgebraicCreatorDialogPrivate* d;

};
} // END_PLUGIN_NAMESPACE

dtkAbstractProcess* createProcessCreatorCurveAlgebraic(void);
axlInspectorToolInterface* createAxlSemiAlgebraicCurveAlgebraicCreatorDialog(void);

#endif // CurveAlgebraicCREATORPROCESSDIALOG_H
