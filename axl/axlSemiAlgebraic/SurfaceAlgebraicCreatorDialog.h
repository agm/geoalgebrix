#ifndef SurfaceAlgebraicCREATORPROCESSDIALOG_H
#define SurfaceAlgebraicCREATORPROCESSDIALOG_H

# include <axlCore/axlAbstractData.h>
# include <axlGui/axlInspectorToolFactory.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic { // BEGIN_PLUGIN_NAMESPACE

class SurfaceAlgebraicCreatorDialogPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceAlgebraicCreatorDialog : public axlInspectorToolInterface {
    Q_OBJECT

public:
    SurfaceAlgebraicCreatorDialog(QWidget* parent = 0);
    virtual ~SurfaceAlgebraicCreatorDialog();

    static bool registered(void);
    QVector<double> parseDomain(const QString& domainQString) const;

signals:
    void dataInserted(axlAbstractData* data);

public slots:
    void fillFields(int surfaceIndex);
    void run(void);

private:
    SurfaceAlgebraicCreatorDialogPrivate* d;

};
} // END_PLUGIN_NAMESPACE

dtkAbstractProcess* createProcessCreatorSurfaceAlgebraic(void);
axlInspectorToolInterface* createAxlSemiAlgebraicSurfaceAlgebraicCreatorDialog(void);

#endif // SurfaceAlgebraicCREATORPROCESSDIALOG_H
