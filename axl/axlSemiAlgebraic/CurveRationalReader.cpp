/*****************************************************************************
 * CurveRational Reader for axel
 *****************************************************************************
 *
 *  Created: Tue Feb 14 10:53:46 CET 2012
 *       By: mperrine
 * Comments: 
 *           Generated with mmxaxel
 *
 *  Changes:
 *****************************************************************************/
#include "CurveRationalReader.h"
#include "CurveRational.h"
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <geoalgebrix/rational_curve.hpp>

#include "axlSemiAlgebraicPlugin.h"


//#include <QtGui>

// /////////////////////////////////////////////////////////////////
// CurveRationalReader
// /////////////////////////////////////////////////////////////////
BEGIN_PLUGIN_NAMESPACE
CurveRationalReader::CurveRationalReader(void)  { }

CurveRationalReader::~CurveRationalReader(void) { }

QString CurveRationalReader::identifier(void) const
{
    return "CurveRationalReader";
}

QString CurveRationalReader::description(void) const
{
    return "CurveRationalReader";
}

QStringList CurveRationalReader::handled(void) const
{
    return QStringList() << "CurveRational";
}

bool CurveRationalReader::registered(void)
{
    return axlSemiAlgebraicPlugin::dataFactSingleton->registerDataReaderType("CurveRationalReader", QStringList(), createAxlSemiAlgebraicCurveRationalReader);
}

bool CurveRationalReader::accept(const QDomNode& node)
{
     QDomElement element = node.toElement();

     if(element.tagName() != "curve")      return false;
     if(element.attribute("type") != "rational")  return false;

     if(!hasChildNode(element, "domain"))         return false;
     if(!hasChildNode(element, "polynomial"))     return false;

     return true;
}

bool CurveRationalReader::reject(const QDomNode& node)
{
    return !this->accept(node);
}

axlAbstractData *CurveRationalReader::read(const QDomNode& node) {
//    typedef mmx::geoalgebrix::rational_curve<double>  Curve;
//    typedef Curve::Point                            Point;
//    typedef Curve::Polynomial                       Polynomial;
    if(!accept(node))
        qDebug()<<"Algebraic Curve Not accepted" ;

    QDomElement e = node.toElement();

    //QList<double>   box;
    QDomNodeList    nodelist = e.elementsByTagName("domain") ;
    QDomElement     element  = nodelist.item(0).toElement() ;
    QStringList     domainStr     = element.text().simplified().split(QRegExp("\\s+")) ;
    QVector<double> domain;
    QStringList     numAndDenom;

    domain << domainStr.at(0).toDouble() << domainStr.at(1).toDouble();

    nodelist = e.elementsByTagName("polynomial") ;
    for(int i = 0; i < nodelist.size(); i++) {
      numAndDenom.push_back(nodelist.item(i).toElement().text());
    }

    CurveRational* data = new CurveRational;
    data->setCurve(domain, numAndDenom);

    this->setNameOf  (data, e);
    this->setColorOf (data, e);
    this->setSizeOf  (data, e);
    this->setShaderOf(data, e);

    return data;
}
END_PLUGIN_NAMESPACE

dtkAbstractDataReader *createAxlSemiAlgebraicCurveRationalReader(void) {
    return new PLUGIN_NAMESPACE CurveRationalReader;
}
