/*****************************************************************************
 * CurveRational Dialog for axel
 *****************************************************************************
 *
 *  Created: Tue Feb 14 10:53:39 CET 2012
 *       By: mperrine
 * Comments:
 *   Generated with maxel
 *
 *  Changes:
 *
 *****************************************************************************/
#ifndef CurveRationalDialog_h
#define CurveRationalDialog_h

#include <axlGui/axlInspectorObjectFactory.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic {

class CurveRationalDialogPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveRationalDialog : public axlInspectorObjectInterface
{
    Q_OBJECT

public:
    CurveRationalDialog(QWidget *parent = 0);
    ~CurveRationalDialog(void);

    QSize sizeHint(void) const;
    QVector<double> parseDomain(const QString& minBound, const QString& maxBound) const ;
    static bool registered(void);

signals:
    void colorChanged(QColor color,  dtkAbstractData *data);

    void dataChangedByShader(dtkAbstractData *data, QString isophoteShaderXml);
    //void dataChangedByOpacity(dtkAbstractData *data, double opacity);
    //void dataChangedByColor(dtkAbstractData *data, double red, double green, double blue);

    //void dataChangedByGeometry(dtkAbstractData *data);

    void update(void);

public slots:
    void setData(dtkAbstractData *data);
    void onColorChanged(QColor color);
    void onSizeChanged(int opacity);
    void onOpacityChanged(int opacity);
//    void openShader(void);
//    void onShaderStateChanged(bool isShader);
//    void onShaderChanged(QString);
//    void onLineEditShaderChanged(QString);

    void onEqAndDomainChanged(void);

private :
//    void initComboBoxShaderValue(void);
    void initWidget(void);
    int initSizeValue(void);
    int initOpacityValue(void);
//    QString initShaderValue(void);
    QColor initColorValue(void);
    QStringList initNumAndDenom(void);
    QStringList initDomain(void);

private:
    CurveRationalDialogPrivate *d;
};
} // END_PLUGIN_NAMESPACE 

axlInspectorObjectInterface *createAxlSemiAlgebraicCurveRationalDialog(void);

# endif
