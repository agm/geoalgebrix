/*****************************************************************************
 * CurveRational Writer for axel
 *****************************************************************************
 *
 *  Created: Tue Feb 14 10:53:53 CET 2012
 *       By: mperrine
 * Comments: 
 *   Generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef CurveRationalWriter_h
#define CurveRationalWriter_h

#include <axlCore/axlAbstractDataWriter.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic { //BEGIN_PLUGIN_NAMESPACE 

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveRationalPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveRationalWriter : public axlAbstractDataWriter
{
    Q_OBJECT

public :
    CurveRationalWriter(void);
   ~CurveRationalWriter(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;

    static bool registered(void);

public:
    bool accept(dtkAbstractData*);
    bool reject(dtkAbstractData*);
    
    QDomElement write(QDomDocument *doc, dtkAbstractData *data);

private:
};

} // END_PLUGIN_NAMESPACE 

dtkAbstractDataWriter *createAxlSemiAlgebraicCurveRationalWriter(void);

# endif
