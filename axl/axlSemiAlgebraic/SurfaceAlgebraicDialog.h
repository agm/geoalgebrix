/*****************************************************************************
 * SurfaceAlgebraic Dialog for axel
 *****************************************************************************
 *
 *  Created: Fri Feb 10 10:17:29 CET 2012
 *       By: mperrine
 * Comments: 
 *   Generated with maxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef SurfaceAlgebraicDialog_h
#define SurfaceAlgebraicDialog_h

#include <axlGui/axlInspectorObjectFactory.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic {

class SurfaceAlgebraicDialogPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceAlgebraicDialog : public axlInspectorObjectInterface
{
    Q_OBJECT

public:
      SurfaceAlgebraicDialog(QWidget *parent = 0);
     ~SurfaceAlgebraicDialog(void);

    QSize sizeHint(void) const;

    static bool registered(void);

signals:
    void colorChanged(QColor color,  dtkAbstractData *data);

    void dataChangedByShader(dtkAbstractData *data, QString isophoteShaderXml);
    //void dataChangedByOpacity(dtkAbstractData *data, double opacity);
    //void dataChangedByColor(dtkAbstractData *data, double red, double green, double blue);

    void update(void);

public slots:
    void setData(dtkAbstractData *data);

    void onColorChanged(QColor color);

    void onOpacityChanged(int opacity);

    void openShader(void);
    void onShaderStateChanged(bool isShader);
    void onShaderChanged(QString);
    void onLineEditShaderChanged(QString);


private :
    void initComboBoxShaderValue(void);

private:
   SurfaceAlgebraicDialogPrivate *d;
};
} // END_PLUGIN_NAMESPACE 

axlInspectorObjectInterface *createAxlSemiAlgebraicSurfaceAlgebraicDialog(void);

# endif
