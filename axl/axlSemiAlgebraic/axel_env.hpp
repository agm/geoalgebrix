#ifndef geoalgebrix_axl_def_axel_hpp
#define geoalgebrix_axl_def_axel_hpp
#include <geoalgebrix/mesh.hpp>
#include <geoalgebrix/ssi/ssi_def.hpp>
#include <axlCore/axlAbstractSurfaceParametric.h>

struct axel_env {};

namespace mmx { namespace geoalgebrix {

template<class DEF, class C> struct use<DEF,C,axel_env>: public use<DEF,C,default_env> {};

//template<class C>
//struct mesh_def<C,axel_env> {

//    typedef axlMesh Mesh;
//    typedef typename Mesh::Point Point;
//    typedef QPair<int,int> Edge;
//    typedef QList<int> Face;

//};

template<class C>
struct use<mmx::geoalgebrix::ssi_def,C,axel_env> {

    typedef axlAbstractSurfaceParametric ParametricSurface;
    typedef axlMesh Mesh;

    static void sample(const ParametricSurface* s, C* r, unsigned m, unsigned n, C* u, C*v){

        double  su = ((ParametricSurface*)s)->startParam_u(),
                du = (((ParametricSurface*)s)->endParam_u()-su)/(m-1),
                sv = ((ParametricSurface*)s)->startParam_v(),
                dv = (((ParametricSurface*)s)->endParam_v()-sv)/(n-1);
        double *u0 = u, *v0 = v;

        for (unsigned i=0; i<m;i++){ *u=su; su+=du; u++; }
        for (unsigned j=0; j<n;j++){ *v=sv; sv+=dv; v++; }

        //axlPoint* p =new axlPoint;
        u=u0;
        for (unsigned i=0; i<m;i++,u++) {
            v=v0;
            for (unsigned j=0; j<n;j++,v++) {
                ((ParametricSurface*)s)->eval(r[0], r[1], r[2],*u,*v);
                r+=3;
            }
        }
    }

    static void eval(ParametricSurface* s, C* r, const C* u, unsigned n) {
        for (unsigned i=0; i<n;i++) {
            s->eval(r[0],r[1],r[2],u[0],u[1]);
            r+=3;
            u+=2;
        }
    }
};

}
}
#endif //geoalgebrix_axl_def_axel_hpp
