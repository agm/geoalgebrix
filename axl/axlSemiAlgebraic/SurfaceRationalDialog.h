/*****************************************************************************
 * SurfaceRational Dialog for axel
 *****************************************************************************
 *
 *  Created: Mar  7 fév 2012 18:31:55 CET
 *       By: mourrain
 * Comments: 
 *   Generated with maxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef SurfaceRationalDialog_h
#define SurfaceRationalDialog_h

#include <axlGui/axlInspectorObjectFactory.h>
#include "axlSemiAlgebraicPluginExport.h"

#define Surface mmx::geoalgebrix::rational_surface<double>
namespace axlSemiAlgebraic {

class SurfaceRationalDialogPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceRationalDialog : public axlInspectorObjectInterface
{
    Q_OBJECT

public:
      SurfaceRationalDialog(QWidget *parent = 0);
     ~SurfaceRationalDialog(void);

    QSize sizeHint(void) const;

    QVector<double> parseDomain(const QString& domainQString) const;
    static bool registered(void);

signals:
    void colorChanged(QColor color,  dtkAbstractData *data);

    void dataChangedByShader(dtkAbstractData *data, QString isophoteShaderXml);
    void dataChangedByOpacity(dtkAbstractData *data, double opacity);
    void dataChangedByColor(dtkAbstractData *data, double red, double green, double blue);
    //void dataChangedByDomain(dtkAbstractData *data, QString domain);
    //void dataChangedByEquation(dtkAbstractData *data, QStringList numAndDenom);
    void dataChangedByGeometry(dtkAbstractData *data);

    void update(void);

public slots:
    void setData(dtkAbstractData *data);
    void onColorChanged(QColor color);
    void onOpacityChanged(int opacity);
    void openShader(void);
    void onShaderStateChanged(bool isShader);
    void onShaderChanged(QString);
    void onLineEditShaderChanged(QString);
    void onEqAndDomainChanged(void);


private :
    void initComboBoxShaderValue(void);
    void initWidget(void);
    int initOpacityValue(void);
    QString initShaderValue(void);
    QColor initColorValue(void);
    QStringList initNumAndDenom(void);
    QString initDomain(void);

private:
   SurfaceRationalDialogPrivate *d;
};
} // END_PLUGIN_NAMESPACE 

axlInspectorObjectInterface *createAxlSemiSurfaceAlgebraicRationalDialog(void);

# endif
