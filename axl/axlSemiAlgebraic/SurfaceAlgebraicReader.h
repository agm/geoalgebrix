/*****************************************************************************
 * SurfaceAlgebraic Reader for axel
 *****************************************************************************
 *
 *  Created: Fri Feb 10 10:18:17 CET 2012
 *       By: mperrine
 * Comments: 
 *   Generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef SurfaceAlgebraicReader_hpp
#define SurfaceAlgebraicReader_hpp
class QDomNode;

#include <axlCore/axlAbstractDataReader.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic {

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceAlgebraicPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceAlgebraicReader : public axlAbstractDataReader
{
    Q_OBJECT

public :
      SurfaceAlgebraicReader(void);
     ~SurfaceAlgebraicReader(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;
    
    static bool registered(void);

public:
    bool accept(const QDomNode& node);
    bool reject(const QDomNode& node);
    
    axlAbstractData *read(const QDomNode& node);

};

} // namespace axlSemiAlgebraic

dtkAbstractDataReader *createAxlSemiAlgebraicSurfaceAlgebraicReader(void);

# endif
