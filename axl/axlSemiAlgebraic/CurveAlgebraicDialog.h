/*****************************************************************************
 * CurveAlgebraic Dialog for axel
 *****************************************************************************
 *
 *  Created: Mar  7 fév 2012 18:29:21 CET
 *       By: mourrain
 * Comments:
 *   Generated with mmk
 *
 *  Changes:
 *
 *****************************************************************************/
#ifndef CurveAlgebraicDialog_h
#define CurveAlgebraicDialog_h

#include <axlGui/axlInspectorObjectFactory.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic {

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraicDialogPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraicDialog : public axlInspectorObjectInterface
{
    Q_OBJECT

public:
    CurveAlgebraicDialog(QWidget *parent = 0);
    ~CurveAlgebraicDialog(void);

    QSize sizeHint(void) const;

    static bool registered(void);

signals:

    void colorChanged(QColor color,  dtkAbstractData *data);

//    void dataChangedByColor(dtkAbstractData *data, double red, double green, double blue);
//    void dataChangedBySize(dtkAbstractData *data, double size);
//    void dataChangedByOpacity(dtkAbstractData *data, double opacity);
//    void dataChangedByShader(dtkAbstractData *data, QString isophoteShaderXml);

    void dataChangedByGeometry(dtkAbstractData *data);
    void update(void);

public slots:
    void setData(dtkAbstractData *data);

    void onColorChanged(QColor color);
    void onSizeChanged(int sizes);
    void onOpacityChanged(int opacity);

//    void openShader(void);
//    void onShaderStateChanged(bool isShader);
//    void onShaderChanged(QString shader);
//    void onLineEditShaderChanged(QString shader);

private:

    void    initWidget(void);
    int     initSizeValue(void);
    int     initOpacityValue(void);
    QColor  initColorValue(void);

//    QString initShaderValue(void);
//    void    initComboBoxShaderValue(void);

    CurveAlgebraicDialogPrivate *d;
};
} // END_PLUGIN_NAMESPACE 

axlInspectorObjectInterface *createAxlSemiAlgebraicCurveAlgebraicDialog(void);

# endif
