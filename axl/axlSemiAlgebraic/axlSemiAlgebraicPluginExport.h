#ifndef AXLSEMIALGEBRAICPLUGINEXPORT_H
#define AXLSEMIALGEBRAICPLUGINEXPORT_H

#ifdef WIN32
    #ifdef axlSemiAlgebraicPlugin_EXPORTS
        #define AXLSEMIALGEBRAICPLUGIN_EXPORT __declspec(dllexport) 
    #else
        #define AXLSEMIALGEBRAICPLUGIN_EXPORT __declspec(dllimport) 
    #endif
#else
    #define AXLSEMIALGEBRAICPLUGIN_EXPORT
#endif

#define BEGIN_PLUGIN_NAMESPACE namespace axlSemiAlgebraic {
#define PLUGIN_NAMESPACE  axlSemiAlgebraic::
#define END_PLUGIN_NAMESPACE }
#endif
