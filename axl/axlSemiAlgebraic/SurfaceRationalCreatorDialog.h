#ifndef RATIONALSURFACECREATORPROCESSDIALOG_H
#define RATIONALSURFACECREATORPROCESSDIALOG_H

# include <axlCore/axlAbstractData.h>
# include <axlGui/axlInspectorToolFactory.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic { // BEGIN_PLUGIN_NAMESPACE

class SurfaceRationalCreatorDialogPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceRationalCreatorDialog : public axlInspectorToolInterface {
    Q_OBJECT

public:
    SurfaceRationalCreatorDialog(QWidget* parent = 0);
    virtual ~SurfaceRationalCreatorDialog();

    static bool registered(void);
    QVector<double> parseDomain(const QString& domainQString) const;

signals:
    void dataInserted(axlAbstractData* data);

public slots:
    void fillFields(int surfaceIndex);
    void run(void);

private:
    SurfaceRationalCreatorDialogPrivate* d;

};
} // END_PLUGIN_NAMESPACE

dtkAbstractProcess* createProcessCreatorSurfaceRational(void);
axlInspectorToolInterface* createAxlSemiSurfaceAlgebraicRationalCreatorDialog(void);

#endif // RATIONALSURFACECREATORPROCESSDIALOG_H
