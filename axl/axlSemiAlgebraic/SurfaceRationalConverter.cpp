/* SurfaceRationalConverter.h
 * Created: Mar 24 jan 2012 19:01:02 CET
 *      By: mourrain
 * Comments: 
 *    Generated by maxel
 * Change log:
 * 
 */
// /////////////////////////////////////////////////////////////////
#include "SurfaceRationalConverter.h"
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <axlCore/axlPoint.h>
#include <axlCore/axlMesh.h>
#include <geoalgebrix/rational_surface.hpp>
#include "SurfaceRational.h"

#include "axlSemiAlgebraicPlugin.h"


// /////////////////////////////////////////////////////////////////
// SurfaceRationalConverter
// /////////////////////////////////////////////////////////////////
BEGIN_PLUGIN_NAMESPACE

SurfaceRationalConverter::SurfaceRationalConverter(void) : axlAbstractDataConverter () { }

SurfaceRationalConverter::~SurfaceRationalConverter(void) { } 

QString SurfaceRationalConverter::description(void) const
{
    return "Converter from SurfaceRationalConverter to axlMesh";
}

QStringList SurfaceRationalConverter::fromTypes(void) const
{
    return QStringList() << "SurfaceRationalConverter" << "axlAbstractSurface";
}

QString SurfaceRationalConverter::toType(void) const
{
    return "axlMesh";
}

bool SurfaceRationalConverter::registered(void)
{
    return axlSemiAlgebraicPlugin::dataFactSingleton->registerDataConverterType("SurfaceRationalConverter", QStringList(), "axlMesh", createAxlSemiSurfaceAlgebraicRationalConverter);
}

/// Method used to convert to an axlMesh.
axlMesh* SurfaceRationalConverter::toMesh(void)
{
    typedef mmx::geoalgebrix::rational_surface<double>  Surface;

    SurfaceRational* obj = dynamic_cast<SurfaceRational*>(this->data());
    if (!obj) return NULL;

    Surface* surf = static_cast<Surface*>(obj->surface());
    axlMesh* mesh = mmx::geoalgebrix::as_mesh<axlMesh,Surface>::convert(*surf);
    //as_mesh(mesh,*surf);
    mesh->setColor(obj->color());
    mesh->setOpacity(obj->opacity());
    mesh->setShader(obj->shader());

    return mesh;
}
END_PLUGIN_NAMESPACE

dtkAbstractDataConverter *createAxlSemiSurfaceAlgebraicRationalConverter(void)
{
    return new PLUGIN_NAMESPACE SurfaceRationalConverter;
}
