#include "CurveRational.h"
#include "CurveRationalCreatorDialog.h"
#include "axlSemiAlgebraicPlugin.h"

#include <axlCore/axlAbstractProcess.h>

#include <dtkCoreSupport/dtkAbstractProcessFactory.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkLog/dtkLog.h>
#include <dtkGuiSupport/dtkColorButton.h>

#include <QtGui>

BEGIN_PLUGIN_NAMESPACE

class CurveRationalCreatorDialogPrivate {
public:
    QSlider *sliderOpacity;
    dtkColorButton *colorButton;
    QLineEdit* domainMinEdit;
    QLineEdit* domainMaxEdit;
    QLineEdit* equation1Edit;
    QLineEdit* equation2Edit;
    QLineEdit* equation3Edit;
    QLineEdit* equation4Edit;
    QComboBox* predefinedObjectsComboBox;
    QString eq1String;
    QString eq2String;
    QString eq3String;
    QString eq4String;
    QLabel* errorMessage;
};

CurveRationalCreatorDialog::CurveRationalCreatorDialog(QWidget* parent) :
    axlInspectorToolInterface(parent),
    d(new CurveRationalCreatorDialogPrivate) {

    int qLineEditWidth = 200;

    //OPACITY//
    d->sliderOpacity = new QSlider(Qt::Horizontal, this);

    QHBoxLayout *layoutOpacity = new QHBoxLayout;
    layoutOpacity->addWidget(new QLabel("Opacity",this));
    layoutOpacity->addWidget(d->sliderOpacity);
    d->sliderOpacity->setMaximum(100);

    //COLOR//
    d->colorButton = new dtkColorButton(this);

    QHBoxLayout *layoutColorButton = new QHBoxLayout;
    layoutColorButton->addWidget(new QLabel("Color",this));
    layoutColorButton->addWidget(d->colorButton);
    d->colorButton->setColor(QColor("green"));

    //ERROR//
    d->errorMessage = new QLabel("Error in domain field: please specify two different bounds.\nYou can also let these two fields empty to work on [0,1].");
    d->errorMessage->setStyleSheet("color: #ff0000;");
    d->errorMessage->setVisible(false);

    //PREDEFINED OBJECTS//
    d->predefinedObjectsComboBox = new QComboBox;
    d->predefinedObjectsComboBox->addItem("Twisted Cubic");
    d->predefinedObjectsComboBox->addItem("Parabola #1");
    d->predefinedObjectsComboBox->addItem("Parabola #2");
    d->predefinedObjectsComboBox->addItem("Alpha");
    d->predefinedObjectsComboBox->addItem("Line");

    connect(d->predefinedObjectsComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(fillFields(int)));

    //DOMAIN//
    QHBoxLayout* domainLayout = new QHBoxLayout;
    d->domainMinEdit = new QLineEdit("-1.2");
    d->domainMinEdit->setToolTip("Enter a min bound value");
    d->domainMinEdit->setFixedWidth(qLineEditWidth/4);
    d->domainMaxEdit = new QLineEdit("1.2");
    d->domainMaxEdit->setToolTip("Enter a max bound value");
    d->domainMaxEdit->setFixedWidth(qLineEditWidth/4);
    domainLayout->addWidget(new QLabel("Min bound: "));
    domainLayout->addWidget(d->domainMinEdit);
    domainLayout->addWidget(new QLabel("Max bound: "));
    domainLayout->addWidget(d->domainMaxEdit);

    //EQUATIONS//
    QString equationType = "Enter an equation like 3s^4 -2";

    QFormLayout* equation1Layout = new QFormLayout;
    d->equation1Edit = new QLineEdit("s", this);
    d->equation1Edit->setToolTip(equationType);
    d->equation1Edit->setFixedWidth(qLineEditWidth);
    equation1Layout->addRow("Numerator x:", d->equation1Edit);

    QFormLayout* equation2Layout = new QFormLayout;
    d->equation2Edit = new QLineEdit("s^2", this);
    d->equation2Edit->setToolTip(equationType);
    d->equation2Edit->setFixedWidth(qLineEditWidth);
    equation2Layout->addRow("Numerator y:", d->equation2Edit);

    QFormLayout* equation3Layout = new QFormLayout;
    d->equation3Edit = new QLineEdit("s^3", this);
    d->equation3Edit->setToolTip(equationType);
    d->equation3Edit->setFixedWidth(qLineEditWidth);
    equation3Layout->addRow("Numerator z:", d->equation3Edit);

    QFormLayout* equation4Layout = new QFormLayout;
    d->equation4Edit = new QLineEdit("1", this);
    d->equation4Edit->setToolTip(equationType);
    d->equation4Edit->setFixedWidth(qLineEditWidth);
    equation4Layout->addRow("Denominator:", d->equation4Edit);

    fillFields(0);

    //RUN BUTTON//
    QPushButton* createButton = new QPushButton("Create", this);
    connect(createButton, SIGNAL(clicked()), this, SLOT(run()));

    //LAYOUTS//
    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(new QLabel("CurveRationalCreatorDialog"));
    mainLayout->addWidget(d->errorMessage);
    mainLayout->addWidget(d->predefinedObjectsComboBox);
    mainLayout->addLayout(domainLayout);
    mainLayout->addLayout(equation1Layout);
    mainLayout->addLayout(equation2Layout);
    mainLayout->addLayout(equation3Layout);
    mainLayout->addLayout(equation4Layout);
    mainLayout->addLayout(layoutOpacity);
    mainLayout->addLayout(layoutColorButton);
    mainLayout->addWidget(createButton);
}

CurveRationalCreatorDialog::~CurveRationalCreatorDialog() {
    delete d;
    d = NULL;
}

bool CurveRationalCreatorDialog::registered(void) {
    // TODO: change "axlCreateCurveRational" name and choose a better one
    axlSemiAlgebraicPlugin::processFactSingleton->registerProcessType("axlCurveRationalCreator", createProcessCreatorCurveRational, "axlAbstractCreator");
    return axlInspectorToolFactory::instance()->registerInspectorTool("axlCurveRationalCreator", createAxlSemiAlgebraicCurveRationalCreatorDialog);
}

QVector<double> CurveRationalCreatorDialog::parseDomain(const QString& minBound, const QString& maxBound) const {
    QVector<double> res;

    if (!minBound.isEmpty() && !maxBound.isEmpty())
        res << minBound.toDouble() << maxBound.toDouble();
    else if (minBound.isEmpty() && maxBound.isEmpty())
        res << 0 << 1;

    return res;
}

void CurveRationalCreatorDialog::fillFields(int curveIndex) {
    switch (curveIndex) {
    case 0:
        d->eq1String = "s";
        d->eq2String = "s^2";
        d->eq3String = "s^3";
        d->eq4String = "1";
        break;
    case 1:
        d->eq1String = "s";
        d->eq2String = "s^2";
        d->eq3String = "0";
        d->eq4String = "1";
        break;
    case 2:
        d->eq1String = "s";
        d->eq2String = "0.5-s^2";
        d->eq3String = "0";
        d->eq4String = "1";
        break;
    case 3:
        d->eq1String = "1-s^2";
        d->eq2String = "s-s^3";
        d->eq3String = "0";
        d->eq4String = "1";
        break;
    case 4:
        d->eq1String = "s";
        d->eq2String = "s";
        d->eq3String = "s";
        d->eq4String = "1";
        break;
    default:
        d->eq1String = "s";
        d->eq2String = "s^2";
        d->eq3String = "s^3";
        d->eq4String = "1";
        break;
    }
    d->equation1Edit->setText(d->eq1String);
    d->equation2Edit->setText(d->eq2String);
    d->equation3Edit->setText(d->eq3String);
    d->equation4Edit->setText(d->eq4String);
}

void CurveRationalCreatorDialog::run(void) {
    // Warning! new without delete: deletion handled by axlInspectorObjectManager
    CurveRational* rationalCurve = new CurveRational;

    QVector<double> domain = parseDomain(d->domainMinEdit->text(), d->domainMaxEdit->text());
    bool isDomainOk = (domain.size() == 2);
    d->errorMessage->setVisible(!isDomainOk);
    if (!isDomainOk) {
        return;
    }

    QStringList numAndDenom;
    numAndDenom << d->equation1Edit->text() << d->equation2Edit->text() << d->equation3Edit->text() << d->equation4Edit->text();

    rationalCurve->setCurve(domain, numAndDenom);

    rationalCurve->setColor(d->colorButton->color());
    double opacity = 1.0 - 0.01 * d->sliderOpacity->value();
    rationalCurve->setOpacity(opacity);

    emit dataInserted(rationalCurve);
}

END_PLUGIN_NAMESPACE

dtkAbstractProcess* createProcessCreatorCurveRational(void) {
    axlAbstractProcess* process = new axlAbstractProcess;
    process->setDescription("processCreatorCurveRational create a new Rational Curve");
    process->setIdentifier("processCreatorCurveRational");

    return process; // to pass the factory
}

axlInspectorToolInterface* createAxlSemiAlgebraicCurveRationalCreatorDialog(void) {
    return new PLUGIN_NAMESPACE CurveRationalCreatorDialog;
}
