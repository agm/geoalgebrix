/*****************************************************************************
 * SelfIntersection Process for axel
 *****************************************************************************
 *
 *  Created: Dim 24 jui 2012 19:45:04 CEST
 *       By: mourrain
 * Comments: generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
# ifndef SelfIntersectionProcess_h
# define SelfIntersectionProcess_h

# include <axlCore/axlAbstractData.h>
# include <dtkCoreSupport/dtkAbstractProcess.h>
# include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic { // BEGIN_PLUGIN_NAMESPACE 

class SelfIntersectionProcessPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SelfIntersectionProcess: public dtkAbstractProcess
{
    Q_OBJECT

public:
             SelfIntersectionProcess(void);
    virtual ~SelfIntersectionProcess(void);

    virtual QString description(void) const;
    virtual QString identifier(void) const;

    static bool registered(void);

    dtkAbstractData *output(void);

public slots:
    void setInput(dtkAbstractData *data);

public slots:
    int update(void);

private:
    SelfIntersectionProcessPrivate *d;
};
} // END_PLUGIN_NAMESPACE 

dtkAbstractProcess *createAxlSemiAlgebraicSelfIntersectionProcess(void);

# endif //SelfIntersectionProcess_h
