#ifndef RATIONALCURVECREATORPROCESSDIALOG_H
#define RATIONALCURVECREATORPROCESSDIALOG_H

# include <axlCore/axlAbstractData.h>
# include <axlGui/axlInspectorToolFactory.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic { // BEGIN_PLUGIN_NAMESPACE

class CurveRationalCreatorDialogPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveRationalCreatorDialog : public axlInspectorToolInterface {
    Q_OBJECT

public:
    CurveRationalCreatorDialog(QWidget* parent = 0);
    virtual ~CurveRationalCreatorDialog();

    static bool registered(void);
    QVector<double> parseDomain(const QString& minBound, const QString& maxBound) const;

signals:
    void dataInserted(axlAbstractData* data);

public slots:
    void fillFields(int curveIndex);
    void run(void);

private:
    CurveRationalCreatorDialogPrivate* d;

};
} // END_PLUGIN_NAMESPACE

dtkAbstractProcess* createProcessCreatorCurveRational(void);
axlInspectorToolInterface* createAxlSemiAlgebraicCurveRationalCreatorDialog(void);

#endif // RATIONALCURVECREATORPROCESSDIALOG_H
