/*****************************************************************************
 * CurveAlgebraic Reader for axel
 *****************************************************************************
 *
 *  Created: Sam  5 nov 2011 10:31:10 CET
 *       By: mourrain
 * Comments: 
 *   Generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef CurveAlgebraicReader_hpp
#define CurveAlgebraicReader_hpp
class QDomNode;

#include <axlCore/axlAbstractDataReader.h>
#include "axlSemiAlgebraicPluginExport.h"

//======================================================================
namespace axlSemiAlgebraic {
//======================================================================

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraicPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraicReader : public axlAbstractDataReader
{
    Q_OBJECT

public :
      CurveAlgebraicReader(void);
     ~CurveAlgebraicReader(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;
    
    static bool registered(void);

public:
    bool accept(const QDomNode& node);
    bool reject(const QDomNode& node);
    
    axlAbstractData *read(const QDomNode& node);

};
//======================================================================
}
//======================================================================
dtkAbstractDataReader *createAxlSemiAlgebraicCurveAlgebraicReader(void);

#endif
