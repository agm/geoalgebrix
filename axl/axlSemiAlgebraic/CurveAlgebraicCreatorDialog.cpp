#include "CurveAlgebraic.h"
#include "CurveAlgebraicCreatorDialog.h"
#include "axlSemiAlgebraicPlugin.h"

#include <axlCore/axlAbstractProcess.h>

#include <dtkCoreSupport/dtkAbstractProcessFactory.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkLog/dtkLog.h>
#include <dtkGuiSupport/dtkColorButton.h>

#include <QtGui>

BEGIN_PLUGIN_NAMESPACE

class CurveAlgebraicCreatorDialogPrivate {
public:
    QSlider *sliderOpacity;
    dtkColorButton *colorButton;
    QSlider *sliderSize;
    QLineEdit* domainEdit;
    QLineEdit* equationEdit;
    QComboBox* predefinedObjectsComboBox;
    QString eqString;
    QLabel* errorMessage;
};

CurveAlgebraicCreatorDialog::CurveAlgebraicCreatorDialog(QWidget* parent) :
    axlInspectorToolInterface(parent),
    d(new CurveAlgebraicCreatorDialogPrivate) {

    int qLineEditWidth = 200;

    //OPACITY//
    d->sliderOpacity = new QSlider(Qt::Horizontal, this);
    QHBoxLayout *layoutOpacity = new QHBoxLayout;
    layoutOpacity->addWidget(new QLabel("Opacity",this));
    layoutOpacity->addWidget(d->sliderOpacity);
    d->sliderOpacity->setMaximum(100);

    //COLOR//
    d->colorButton = new dtkColorButton(this);
    QHBoxLayout *layoutColorButton = new QHBoxLayout;
    layoutColorButton->addWidget(new QLabel("Color",this));
    layoutColorButton->addWidget(d->colorButton);
    d->colorButton->setColor(QColor("blue"));

    //SIZE//
    d->sliderSize = new QSlider(Qt::Horizontal, this);
    QHBoxLayout *layoutSize = new QHBoxLayout;
    layoutSize->addWidget(new QLabel("Size",this));
    layoutSize->addWidget(d->sliderSize);
    d->sliderSize->setMinimum(-800);
    d->sliderSize->setMaximum(500);
    d->sliderSize->setValue(-100);


    //ERROR//
    d->errorMessage = new QLabel("Error in domain field: please specify 3 or 4 points.\nYou can also let this field empty to work on IRxIR.");
    d->errorMessage->setStyleSheet("color: #ff0000;");
    d->errorMessage->setVisible(false);

    d->predefinedObjectsComboBox = new QComboBox;
    d->predefinedObjectsComboBox->addItem("Circle");
    d->predefinedObjectsComboBox->addItem("Cubic");
    d->predefinedObjectsComboBox->addItem("Clebsh");
    d->predefinedObjectsComboBox->addItem("Quartic Cylinder");
    d->predefinedObjectsComboBox->addItem("Sextic");

    connect(d->predefinedObjectsComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(fillFields(int)));

    //DOMAIN//
    QFormLayout* domainLayout = new QFormLayout;
    d->domainEdit = new QLineEdit("-2 2 -2 2 -2 2");
    d->domainEdit->setToolTip("Enter a domain as the intervals for x, y, z: -2 2 -1 1 0 3\n which represent the box [-2,2]x[-1,1]x[0,3].");
    d->domainEdit->setFixedWidth(qLineEditWidth);
    domainLayout->addRow("Domain:", d->domainEdit);

    //EQUATIONS//
    QString equationType = "Enter an equation like x^4 + y^4 -1";

    QFormLayout* equationLayout = new QFormLayout;
    d->equationEdit = new QLineEdit;
    d->equationEdit->setToolTip(equationType);
    d->equationEdit->setFixedWidth(qLineEditWidth);
    equationLayout->addRow("Equation:", d->equationEdit);

    fillFields(0);

    //RUN BUTTON//
    QPushButton* createButton = new QPushButton("Create", this);
    connect(createButton, SIGNAL(clicked()), this, SLOT(run()));

    //LAYOUTS//
    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(new QLabel("Algebraic Curve Creator"));
    mainLayout->addWidget(d->errorMessage);
    mainLayout->addWidget(d->predefinedObjectsComboBox);
    mainLayout->addLayout(domainLayout);
    mainLayout->addLayout(equationLayout);
    mainLayout->addLayout(layoutSize);
    mainLayout->addLayout(layoutOpacity);
    mainLayout->addLayout(layoutColorButton);
    mainLayout->addWidget(createButton);
}

CurveAlgebraicCreatorDialog::~CurveAlgebraicCreatorDialog(void) {
    delete d;
    d = NULL;
}

bool CurveAlgebraicCreatorDialog::registered(void) {
    // TODO: change "axlCreateCurveAlgebraic" name and choose a better one
    axlSemiAlgebraicPlugin::processFactSingleton->registerProcessType("axlCurveAlgebraicCreator", createProcessCreatorCurveAlgebraic, "axlAbstractCreator");
    return axlInspectorToolFactory::instance()->registerInspectorTool("axlCurveAlgebraicCreator", createAxlSemiAlgebraicCurveAlgebraicCreatorDialog);
}

QVector<double> CurveAlgebraicCreatorDialog::parseDomain(const QString& domainQString) const {
    QVector<double> res;

    QString domainTrimmed = domainQString.trimmed();

    if (!domainTrimmed.isEmpty()) {
        QStringList coordsList = domainTrimmed.split(QRegExp(" "));

        for (int k = 0; k < coordsList.size(); k++) {
            if (!coordsList.at(k).isEmpty())
                res.push_back(coordsList.at(k).toDouble());
        }
    }

    return res;
}

void CurveAlgebraicCreatorDialog::fillFields(int surfaceIndex) {
    switch (surfaceIndex) {
    case 0:
        d->eqString = "x^2+y^2-1";
        break;
    case 1:
        d->eqString = "x^2+y^2+3.2*x^3-9.6*x*y^2";
        break;
    case 2:
        d->eqString = "81*x^3+81*y^3-189*x^2*y-189*x*y^2+126*x*y-9*x-9*y-9*z+1";
      break;
    case 3:
        d->eqString = "y^2*x^2+y^2*z^2+0.01*x^2+0.01*z^2-0.01";
        break;
    case 4:
        d->eqString = "z^6+x^3-y^3*x-0.1";
        break;
    default:
        d->eqString = "x^2+y^2+z^2-1";
        break;
    }
    d->equationEdit->setText(d->eqString);
}

void CurveAlgebraicCreatorDialog::run(void) {
    // Warning! new without delete: deletion handled by axlInspectorObjectManager
    CurveAlgebraic* curve = new CurveAlgebraic;

    QVector<double> domain = parseDomain(d->domainEdit->text());
    bool isDomainOk = (domain.size() == 0 || domain.size() == 4 || domain.size() == 6);
    d->errorMessage->setVisible(!isDomainOk);
    if (!isDomainOk) {
        return;
    }

    curve->setDomain(domain);

    curve->addEquation(d->equationEdit->text());
    curve->setColor(d->colorButton->color());

    curve->setSize(std::pow(2.0,(double)d->sliderSize->value()/100.0-3));

    curve->setOpacity(1.0-0.01*d->sliderOpacity->value());

    emit dataInserted(curve);
}

END_PLUGIN_NAMESPACE

dtkAbstractProcess* createProcessCreatorCurveAlgebraic(void) {
    axlAbstractProcess* process = new axlAbstractProcess;
    process->setDescription("processCreatorCurveAlgebraic create a new Rational Surface");
    process->setIdentifier("processCreatorCurveAlgebraic");

    return process; // to pass the factory
}

axlInspectorToolInterface* createAxlSemiAlgebraicCurveAlgebraicCreatorDialog(void) {
    return new PLUGIN_NAMESPACE CurveAlgebraicCreatorDialog;
}
