/*****************************************************************************
 * SurfaceRational Reader for axel
 *****************************************************************************
 *
 *  Created: Sam 21 jan 2012 20:51:36 CET
 *       By: mourrain
 * Comments: 
 *   Generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef SurfaceRationalReader_hpp
#define SurfaceRationalReader_hpp
class QDomNode;

#include <axlCore/axlAbstractDataReader.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic { // BEGIN_PLUGIN_NAMESPACE 

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceRationalPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceRationalReader : public axlAbstractDataReader
{
    Q_OBJECT

public :
      SurfaceRationalReader(void);
     ~SurfaceRationalReader(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;
    
    static bool registered(void);

public:
    bool accept(const QDomNode& node);
    bool reject(const QDomNode& node);
    
    axlAbstractData *read(const QDomNode& node);

};

} // END_PLUGIN_NAMESPACE 

dtkAbstractDataReader *createAxlSemiSurfaceAlgebraicRationalReader(void);

#endif
