
/*****************************************************************************
 * CurveAlgebraic data for axel
 ****************************************************************************/
#ifndef CurveAlgebraic_h
#define CurveAlgebraic_h

#include <axlCore/axlAbstractCurveImplicit.h>
#include "axlSemiAlgebraicPluginExport.h"
//======================================================================
namespace axlSemiAlgebraic {
//======================================================================
class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraicPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT CurveAlgebraic : public axlAbstractCurveImplicit
{
    Q_OBJECT
public:
             CurveAlgebraic(void);
    virtual ~CurveAlgebraic(void);

    virtual QString description(void) const;
    virtual QString identifier(void) const;

    static  bool    registered(void);

    void *curve(void);
    void *bounding_box(void) const;

    unsigned nbEquations() const;

    QString write_domain() const;
    QString write_equation(unsigned i=0) const;

public slots:
    void set(void *data);
    void setDomain(const QVector<double>&);
    void addEquation(const QString&);
private:
    CurveAlgebraicPrivate *d;
};

//======================================================================
}
//======================================================================

dtkAbstractData *createAxlSemiAlgebraicCurveAlgebraic(void);

# endif
