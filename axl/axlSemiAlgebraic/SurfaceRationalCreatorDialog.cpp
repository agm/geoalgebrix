#include "SurfaceRational.h"
#include "SurfaceRationalCreatorDialog.h"
#include "axlSemiAlgebraicPlugin.h"

#include <axlCore/axlAbstractProcess.h>

#include <dtkCoreSupport/dtkAbstractProcessFactory.h>
#include <dtkCoreSupport/dtkAbstractDataFactory.h>
#include <dtkLog/dtkLog.h>
#include <dtkGuiSupport/dtkColorButton.h>

#include <QtGui>

BEGIN_PLUGIN_NAMESPACE

class SurfaceRationalCreatorDialogPrivate {
public:
    QSlider *sliderOpacity;
    dtkColorButton *colorButton;
    QLineEdit* domainEdit;
    QLineEdit* equation1Edit;
    QLineEdit* equation2Edit;
    QLineEdit* equation3Edit;
    QLineEdit* equation4Edit;
    QComboBox* predefinedObjectsComboBox;
    QString eq1String;
    QString eq2String;
    QString eq3String;
    QString eq4String;
    QLabel* errorMessage;
};

SurfaceRationalCreatorDialog::SurfaceRationalCreatorDialog(QWidget* parent) :
    axlInspectorToolInterface(parent),
    d(new SurfaceRationalCreatorDialogPrivate) {

    int qLineEditWidth = 200;

    //OPACITY//
    d->sliderOpacity = new QSlider(Qt::Horizontal, this);

    QHBoxLayout *layoutOpacity = new QHBoxLayout;
    layoutOpacity->addWidget(new QLabel("Opacity",this));
    layoutOpacity->addWidget(d->sliderOpacity);
    d->sliderOpacity->setMaximum(100);

    //COLOR//
    d->colorButton = new dtkColorButton(this);

    QHBoxLayout *layoutColorButton = new QHBoxLayout;
    layoutColorButton->addWidget(new QLabel("Color",this));
    layoutColorButton->addWidget(d->colorButton);
    d->colorButton->setColor(QColor("blue"));

    //ERROR//
    d->errorMessage = new QLabel("Error in domain field: please specify 3 or 4 points.\nYou can also let this field empty to work on IRxIR.");
    d->errorMessage->setStyleSheet("color: #ff0000;");
    d->errorMessage->setVisible(false);

    d->predefinedObjectsComboBox = new QComboBox;
    d->predefinedObjectsComboBox->addItem("Roman");
    d->predefinedObjectsComboBox->addItem("Sphere");
    d->predefinedObjectsComboBox->addItem("Lined Sphere");
    d->predefinedObjectsComboBox->addItem("Lined Pillow");
    d->predefinedObjectsComboBox->addItem("Lined Sphere Bumped");

    connect(d->predefinedObjectsComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(fillFields(int)));

    //DOMAIN//
    QFormLayout* domainLayout = new QFormLayout;
    d->domainEdit = new QLineEdit;//("-1 -1 -1 1 1 1 1 -1");
    d->domainEdit->setToolTip("You can let this field empty to work on IRxIR.\nOtherwise, enter a domain with point coordinates: -10 -8 -7 5 4 12...\nEnter 3 or 4 points (ie 6 or 8 numbers).");
    d->domainEdit->setFixedWidth(qLineEditWidth);
    domainLayout->addRow("Domain:", d->domainEdit);

    //EQUATIONS//
    QString equationType = "Enter an equation like 3s^4 + 2*st^2 -1";

    QFormLayout* equation1Layout = new QFormLayout;
    d->equation1Edit = new QLineEdit;
    d->equation1Edit->setToolTip(equationType);
    d->equation1Edit->setFixedWidth(qLineEditWidth);
    equation1Layout->addRow("Numerator x:", d->equation1Edit);

    QFormLayout* equation2Layout = new QFormLayout;
    d->equation2Edit = new QLineEdit;
    d->equation2Edit->setToolTip(equationType);
    d->equation2Edit->setFixedWidth(qLineEditWidth);
    equation2Layout->addRow("Numerator y:", d->equation2Edit);

    QFormLayout* equation3Layout = new QFormLayout;
    d->equation3Edit = new QLineEdit;
    d->equation3Edit->setToolTip(equationType);
    d->equation3Edit->setFixedWidth(qLineEditWidth);
    equation3Layout->addRow("Numerator z:", d->equation3Edit);

    QFormLayout* equation4Layout = new QFormLayout;
    d->equation4Edit = new QLineEdit;
    d->equation4Edit->setToolTip(equationType);
    d->equation4Edit->setFixedWidth(qLineEditWidth);
    equation4Layout->addRow("Denominator:", d->equation4Edit);

    fillFields(0);

    //RUN BUTTON//
    QPushButton* createButton = new QPushButton("Create", this);
    connect(createButton, SIGNAL(clicked()), this, SLOT(run()));

    //LAYOUTS//
    QVBoxLayout* mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(new QLabel("SurfaceRationalCreatorDialog"));
    mainLayout->addWidget(d->errorMessage);
    mainLayout->addWidget(d->predefinedObjectsComboBox);
    mainLayout->addLayout(domainLayout);
    mainLayout->addLayout(equation1Layout);
    mainLayout->addLayout(equation2Layout);
    mainLayout->addLayout(equation3Layout);
    mainLayout->addLayout(equation4Layout);
    mainLayout->addLayout(layoutOpacity);
    mainLayout->addLayout(layoutColorButton);
    mainLayout->addWidget(createButton);
}

SurfaceRationalCreatorDialog::~SurfaceRationalCreatorDialog(void) {
    delete d;
    d = NULL;
}

bool SurfaceRationalCreatorDialog::registered(void) {
    // TODO: change "axlCreateSurfaceRational" name and choose a better one
    axlSemiAlgebraicPlugin::processFactSingleton->registerProcessType("axlSurfaceRationalCreator", createProcessCreatorSurfaceRational, "axlAbstractCreator");
    return axlInspectorToolFactory::instance()->registerInspectorTool("axlSurfaceRationalCreator", createAxlSemiSurfaceAlgebraicRationalCreatorDialog);
}

QVector<double> SurfaceRationalCreatorDialog::parseDomain(const QString& domainQString) const {
    QVector<double> res;

    QString domainTrimmed = domainQString.trimmed();

    if (!domainTrimmed.isEmpty()) {
        QStringList coordsList = domainTrimmed.split(QRegExp(" "));

        for (int k = 0; k < coordsList.size(); k++) {
            if (!coordsList.at(k).isEmpty())
                res.push_back(coordsList.at(k).toDouble());
        }
    }

    return res;
}

void SurfaceRationalCreatorDialog::fillFields(int surfaceIndex) {
    switch (surfaceIndex) {
    case 0:
        d->eq1String = "s*t";
        d->eq2String = "t";
        d->eq3String = "s";
        d->eq4String = "s^2+t^2+1";
        break;
    case 1:
        d->eq1String = "s^2-t^2-1";
        d->eq2String = "2*s";
        d->eq3String = "2*s*t";
        d->eq4String = "s^2+t^2+1";
        break;
    case 2:
        d->eq1String = "1-s^2-t^2+s^2*t^2";
        d->eq2String = "2*s-2*s*t^2";
        d->eq3String = "2*t+2*t*s^2";
        d->eq4String = "1+s^2+t^2+s^2*t^2";
        break;
    case 3:
        d->eq1String = "1-s^2-t^2+s^2*t^2";
        d->eq2String = "4*s+4*s*t^2";
        d->eq3String = "4*t+4*t*s^2";
        d->eq4String = "1+s^2+t^2+s^2*t^2";
        break;
    case 4:
        d->eq1String = "1-s^2-t^2+s^2*t^2";
        d->eq2String = "2*s-2*s*t^2";
        d->eq3String = "2*t-2*t*s^2";
        d->eq4String = "1+s^2+t^2+s^2*t^2";
        break;
    default:
        d->eq1String = "s*t";
        d->eq2String = "t";
        d->eq3String = "s";
        d->eq4String = "s^2+t^2+1";
        break;
    }
    d->equation1Edit->setText(d->eq1String);
    d->equation2Edit->setText(d->eq2String);
    d->equation3Edit->setText(d->eq3String);
    d->equation4Edit->setText(d->eq4String);
}

void SurfaceRationalCreatorDialog::run(void) {
    // Warning! new without delete: deletion handled by axlInspectorObjectManager
    SurfaceRational* rationalSurface = new SurfaceRational;

    QVector<double> domain = parseDomain(d->domainEdit->text());
    bool isDomainOk = (domain.size() == 0 || domain.size() == 6 || domain.size() == 8);
    d->errorMessage->setVisible(!isDomainOk);
    if (!isDomainOk) {
        return;
    }

    QStringList numAndDenom;
    numAndDenom << d->equation1Edit->text() << d->equation2Edit->text() << d->equation3Edit->text() << d->equation4Edit->text();

    rationalSurface->setSurface(domain, numAndDenom);

    rationalSurface->setColor(d->colorButton->color());
    double opacity = 1.0 - 0.01 * d->sliderOpacity->value();
    rationalSurface->setOpacity(opacity);

    emit dataInserted(rationalSurface);
}

END_PLUGIN_NAMESPACE

dtkAbstractProcess* createProcessCreatorSurfaceRational(void) {
    axlAbstractProcess* process = new axlAbstractProcess;
    process->setDescription("processCreatorSurfaceRational create a new Rational Surface");
    process->setIdentifier("processCreatorSurfaceRational");

    return process; // to pass the factory
}

axlInspectorToolInterface* createAxlSemiSurfaceAlgebraicRationalCreatorDialog(void) {
    return new PLUGIN_NAMESPACE SurfaceRationalCreatorDialog;
}
