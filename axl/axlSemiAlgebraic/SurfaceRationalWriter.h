/*****************************************************************************
 * SurfaceRational Writer for axel
 *****************************************************************************
 *
 *  Created: Dim 29 jan 2012 14:30:13 CET
 *       By: mourrain
 * Comments: 
 *   Generated with mmxaxel
 *
 *  Changes:
 * 
 *****************************************************************************/
#ifndef SurfaceRationalWriter_h
#define SurfaceRationalWriter_h

#include <axlCore/axlAbstractDataWriter.h>
#include "axlSemiAlgebraicPluginExport.h"

namespace axlSemiAlgebraic { //BEGIN_PLUGIN_NAMESPACE 

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceRationalPrivate;

class AXLSEMIALGEBRAICPLUGIN_EXPORT SurfaceRationalWriter : public axlAbstractDataWriter
{
    Q_OBJECT

public :
    SurfaceRationalWriter(void);
   ~SurfaceRationalWriter(void);

public:
    QString identifier(void) const;
    QString description(void) const;
    QStringList handled(void) const;

    static bool registered(void);

public:
    bool accept(dtkAbstractData*);
    bool reject(dtkAbstractData*);
    
    QDomElement write(QDomDocument *doc, dtkAbstractData *data);

private:
};

} // END_PLUGIN_NAMESPACE 

dtkAbstractDataWriter *createAxlSemiSurfaceAlgebraicRationalWriter(void);

# endif
