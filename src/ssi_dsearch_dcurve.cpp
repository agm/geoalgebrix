#include <geoalgebrix/ssi/ssi_dsearch.hpp>

namespace mmx {
namespace ssi {
  
  dcurve::dcurve( const point2& l, const point2& r, const point3& i ) 
  {
    owner = this;
    left.push_back(l);
    right.push_back(r);
    inter.push_back(i);
    size = 1;
  };
  
  dcurve::dcurve( const point2& l0, const point2& l1, const point2& r0, const point2& r1, 
		  const point3& i0, const point3& i1 )
  {
    owner = this;
    left.push_back(l0);
    left.push_back(l1);
    right.push_back(r0);
    right.push_back(r1);
    inter.push_back(i0);
    inter.push_back(i1);
    size = 2;
  };
  
  
  dcurve * curve( pdpoint_t * p )
  { 
    dcurve * tmp      = p->curve;
    while( tmp->owner != tmp ) tmp = tmp->owner;
    p->curve->owner   = tmp;
    //      p->curve = tmp;
    return tmp;
  };
  
  dcurve *  curve( sdpoint_t * p )
  { 
    dcurve *  tmp      = p->curve;
    while( tmp->owner != tmp ) tmp = tmp->owner;
    p->curve->owner   = tmp;
    //      p->curve = tmp;
    return tmp;
  };
  
  
  void extremums( sdpoint_t* dst, dcurve* src )
  {
    dst[0].curve = src;
    dst[0].idl   = src->left.begin(); 
    dst[0].idr   = src->right.begin();
    dst[1].curve = src;
    dst[1].idl   = --(src->left.end()); 
    dst[1].idr   = --(src->right.end());
  };
  
  void extremums( pdpoint_t * dst, dcurve * c )
  {
    dst[0].curve = dst[1].curve = dst[2].curve = dst[3].curve  = c; 
    dst[0].a = &(dst[1]);  dst[1].a = &(dst[0]);
    dst[0].ref   = c->left.begin(); dst[1].ref   = c->right.begin();
    dst[2].a = &(dst[3]);  dst[3].a = &(dst[2]);
    dst[2].ref   = --(c->left.end()); dst[3].ref   = --(c->right.end());
  };
  
  
  void append( dcurve *  a, dcurve   * b ) 
  { 
    a->left.splice ( a->left.end(),  b->left  );
    a->right.splice( a->right.end(), b->right );
    a->inter.splice( a->inter.end(), b->inter );
    b->owner = a; 
    a->size += b->size;
  };
  
  void prepend( dcurve * a, dcurve * b ) 
  { 
    a->left.splice( a->left.begin(), b->left );  
    a->right.splice( a->right.begin(), b->right );
    a->inter.splice( a->inter.begin(), b->inter );
    b->owner = a; 
    a->size += b->size;
  };
  
  void reverse( dcurve * c ) 
  { c->left.reverse(); c->right.reverse(); c->inter.reverse(); };

  void link( sdpoint_t* a, sdpoint_t* b){ _link(a,b); };
  
  void link( pdpoint_t* a, pdpoint_t* b)
  {
    if ( isright(a) ) swap(curve(a));
    if ( isright(b) ) swap(curve(b));
    _link(a,b);
  };
  
  void satisfy_links(  curves_links_t::iterator it )
  {
      std::list< ppair_t > * l = &(it->second);
      std::vector< ppair_t > starts;
      std::vector< ppair_t > ends;
      
       starts.reserve(10);
       ends.reserve(10);
   
       for (  std::list< ppair_t >::iterator p = l->begin(); 
	     p != l->end(); p ++ )
	 {
	   if ( isfirst(p->first) ) starts.push_back( *p );
	   else if ( islast(p->first) ) ends.push_back( *p );
	 };
      
       if ( starts.size() == 1 )
	 {
	   if ( isextrem( starts[0].first ) && isextrem( starts[0].second ) )
   	    if ( curve(starts[0].first) != curve(starts[0].second))
   	      link( starts[0].first, starts[0].second );
	 };
      
       if ( ends.size() == 1 )
	 {
	   
	   if ( isextrem( ends[0].first ) && isextrem( ends[0].second ) )
	     if ( curve(ends[0].first) != curve(ends[0].second) )
	       link( ends[0].first, ends[0].second );
	 };
    };
  
};

} //namespace mmx
