#include <geoalgebrix/csg_generic.hpp>

//========================================================
namespace mmx {
namespace csg {
//--------------------------------------------------------
Intersection::Intersection(generic* o1, generic* o2) {
    m_ops.push_back(o1);
    m_ops.push_back(o2);
}

Intersection::Intersection(generic* o1, generic* o2, generic* o3) {
    m_ops.push_back(o1);
    m_ops.push_back(o2);
    m_ops.push_back(o3);
}

Intersection::Intersection(generic* o1, generic* o2, generic* o3, generic* o4) {
    m_ops.push_back(o1);
    m_ops.push_back(o2);
    m_ops.push_back(o3);
    m_ops.push_back(o4);
}

Range Intersection::eval(double* cl) {
    Range R = m_ops[0]->eval(cl);
    for(unsigned k=1;k<m_ops.size(); k++){
        R = min(R, m_ops[k]->eval(cl));
    }
    return R;
}

regularity_t Intersection::regularity(double* cl, bool store) {
    regularity_t r = INSIDE, a;
    int ctr=0;
    for (unsigned k=0; k< m_ops.size(); k++) {
        if( (a=m_ops[k]->regularity(cl,store)) == OUTSIDE ) {
            if(store) this->set_reg(OUTSIDE);
            return OUTSIDE;
        } else {
            if(ISBOUNDARY(a)) {
                ctr++;
                r = a;
            }
        }
    }

    if(ctr==2)
        r = BOUNDARY_CENTER;
    else if(ctr>2)
        r = UNKNOWN;

    if(store) this->set_reg(r);

    /* if(m_ops.size()==4)
        mdebug()<<"Intersection reg:"<<r<<"<-"<<m_ops[0]->regularity(cl,false)<<m_ops[1]->regularity(cl,false)
               <<m_ops[2]->regularity(cl,false)<<m_ops[3]->regularity(cl,false)
              <<" box:"<<cl[0]<<cl[1]<<cl[2]<<cl[3];
    else if(m_ops.size()==3)
        mdebug()<<"Intersection reg:"<<r<<"<-"<<m_ops[0]->regularity(cl,false)<<m_ops[1]->regularity(cl,false)
               <<m_ops[2]->regularity(cl,false);
    else
        mdebug()<<"Intersection reg:"<<r<<"<-"<<m_ops[0]->regularity(cl,false)<<m_ops[1]->regularity(cl,false);
    */
    return r;
}

int Intersection::tag(double x, double y) {
    int r = 1;
    for (unsigned k=0; k<m_ops.size() && r !=0; k++) {
        r *= m_ops[k]->tag(x,y);
    }
    //mdebug()<<"tag inter:"<<r;
    return r;
}

int Intersection::tag(double x, double y, double z) {
    int r = 1;
    for (unsigned k=0; k<m_ops.size() && r !=0; k++) {
        r *= m_ops[k]->tag(x,y,z);
    }
    return r;
}

void Intersection::active(Sequence& seq) {
    //mdebug()<<"Intersection"<<this->get_reg();
    if(this->get_reg() != OUTSIDE && !ISINSIDE(this->get_reg()) ){
        for(unsigned i=0;i<m_ops.size();i++) {
            //mdebug()<<"active"<<m_ops[i]->get_reg();
            m_ops[i]->active(seq);
        }
    }
}

//--------------------------------------------------------------------
} /* namespace csg */
} /* namespace mmx */
//====================================================================
