#include <geoalgebrix/ssi/ssi_def.hpp>
//#include <geoalgebrix/ssi/chrono.h>

#define TMPL template<class C, class V>
//#define ParametricSurface geoalgebrix::parametric_surface<double>

namespace mmx {
namespace  ssi {


  void shiftm( vector3 * v, unsigned sz, const aabb3 & box )
  {
    //    double m,M;
    for ( vector3 * src = v; src != v+sz; src ++ )
      for ( int i = 0; i < 3; (*src)[i] -= box[i].m, i ++ );
  };

    
  void scale( vector3 * p, double s ){ 
    for ( int i = 0; i < 4; i ++ )
      for ( int d = 0; d < 3; d ++ )
      p[i][d] *= s;
  };


}

} //namespace mmx    

# undef ParametricSurface
