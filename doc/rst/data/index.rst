================
Geometric object
================

.. toctree::
   :maxdepth: 1

   rational_curve.rst
   rational_surface.rst

