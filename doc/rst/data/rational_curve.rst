Rational curve
==============

A rational curve is the image of a parametrization 

.. math::
   t \mapsto (\frac{f_{1}(t)}{f_{0}(t)},\frac{f_{2}(t)}{f_{0}(t)},\frac{f_{3}(t)}{f_{0}(t)})

given by 3 polynomials :math:`f_1, f_2, f_3` for the numerators and a fourth one :math:`f_0`  for the denominator. The parameter t is in an interval :math:`[a,b]`.

Format:

.. literalinclude:: ../../../data/rational_curve.axl
   :lines: 2-8
   :language: xml

Here is an example with the denominator :math:`f_0(t)=1` given as the last polynomial.
The interval for :math:`t` is :math:`[-1,1]`. It is given between the tag ``domain``.
	      
.. image:: ../../img/rational_curve.png
   :height: 100px
   :align: center


