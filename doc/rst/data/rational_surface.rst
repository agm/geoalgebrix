Rational surface
================

A rational surface is the image of a parametrization

.. math::
   (s,t) \mapsto (\frac{f_{1}(s,t)}{f_{0}(s,t)},\frac{f_{2}(s,t)}{f_{0}(s,t)},\frac{f_{3}(s,t)}{f_{0}(s,t)})


given by 3 polynomials for the numerators and a fourth one for the denominator.

.. image:: ../../img/rational_surface.png 
   :height: 100px 
   :align: center 

This figure shows 5 rational surfaces with different domains for the parameters, one has a quadrangular domain and the other ones have a triangular domain.

Format:

The polynomials are in the variables :math:`s,t`. The last one is the denominator, the other are the numerators.

The domain is discribed by a polygon of points in the plane:

  * Triangular domain: described by 3 points::

      <domain>x1 x1 x2 y2 x3 y3</domain>
      

  * Quadrangular domain: described by 4points::

      <domain>x1 x1 x2 y2 x3 y3 x4 y4</domain>
    
    corresponding to the boundary of a quandrangle in the plane:

In the next example, the parameter domain is the triangle defined by the points [0 0], [1 0], [0 1]:

.. literalinclude:: ../../../data/rational_surface.axl
   :lines: 2-7
   :language: xml

In the following example, the parameter domain is the quadrangle defined by the points
[1 0], [2 1], [1 2], [0 1]:

.. literalinclude:: ../../../data/rational_surface.axl
   :lines: 30-36
   :language: xml
