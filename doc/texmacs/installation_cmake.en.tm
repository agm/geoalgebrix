<TeXmacs|1.0.7.3>                                              

<style|<tuple|mmxdoc|mathemagix>>

<\body><tmdoc-title|Download and installation of <name|semialgebraictools>>

  <subsection|Download>
  
<strong|Source code.> 
  
The latest version under development can be obtained with:

  <\shell-fragment>
      git clone git clone git://dtk.inria.fr/axel/semialgebraictools.git semialgebraictools

  </shell-fragment>  
 

  <subsection|Dependencies>


  Here is the list of package(s) to be installed before configuring <name|semialgebraictools> from its source code:

  <\with|par-mode|center>
    <block*|<tformat|<table|<row|<cell|<verbatim|src>>|<cell|<verbatim|rpm>>|<cell|<verbatim|deb>>|<cell|<verbatim|port (Mac)>>>|<row|<cell|<hlink|<verbatim|gmp-4.3.1>|ftp://ftp.gnu.org/gnu/gmp/gmp-4.3.1.tar.gz>>|<cell|<verbatim|gmp-devel>>|<cell|<verbatim|libgmp-dev>>|<cell|<verbatim|gmp>>>>>>
  </with>
  
  To construct the glue (<verbatim|GLUE=ON>) with the mmx interpreter, the following packages should be installed:

  <\with|par-mode|center>
    <block*|<tformat|<table|<row|<cell|<verbatim|src>>|<cell|<verbatim|rpm>>|<cell|<verbatim|deb>>|<cell|<verbatim|port (Mac)>>>|<row|<cell|<verbatim|numerix>>|<cell|<verbatim|numerix>>|<cell|<verbatim|-->>|<cell|<verbatim|-->>>>>>
  </with>
  
  To construct the plugins for axel (<verbatim|AXL=ON>), the following packages should be installed:

  <\with|par-mode|center>
    <block*|<tformat|<table|<row|<cell|<verbatim|src>>|<cell|<verbatim|rpm>>|<cell|<verbatim|deb>>|<cell|<verbatim|port (Mac)>>>|<row|<cell|<verbatim|Eigen>>|<cell|<verbatim|Eigen>>|<cell|<verbatim|-->>|<cell|<verbatim|-->>>>>>
  </with>

  <subsection|Configuration>

  To run the configuration, <hlink|<verbatim|cmake>|http://www.cmake.org/>
  (version at least 2.8) should be available on your platform.

  <strong|Local installation.> The package can be configured
  out-of-source (eg. in a folder <verbatim|../build>) as follows:

    <\shell-fragment>
      cmake ../(path_to_package)

      make
    </shell-fragment>                                                    

The folder where the construction will be run is in this case <verbatim|../build>.

   <strong|Global installation.> If you want to install it globally in your
  environment after it is compiled, you can run the following instructions
  (possibly with the <em|superuser> rights):

    <\shell-fragment>
      cmake ../(path_to_package)
      -DCMAKE_INSTALL_PREFIX=<with|color|brown|\<less\>install_dir\<gtr\>>

      make

      make install
    </shell-fragment>

  where <verbatim|<with|color|brown|\<less\>install_dir\<gtr\>>> is the
  folder where to install the package so that the libraries go in
  <verbatim|<with|color|brown|\<less\>install_dir\<gtr\>>/lib>; and the
  headers in <verbatim|<with|color|brown|\<less\>install_dir\<gtr\>>/include>.
  If not specified, the package is installed by default in
  <verbatim|/usr/local>.

  To see the operations performed during the <verbatim|make> command, you can use:\ 

    <\shell-fragment>
      make VERBOSE=1
    </shell-fragment>

<subsection|How to set locally your environment variables>

  In case you have not run <verbatim|make install>, you can set up your environment locally in the folder <verbatim|build> with:

   <\shell-fragment>
     cd ../build

     source local_env
   </shell-fragment>

<subsection|Using semialgebraictools>

  To use the package <name|semialgebraictools> in a <verbatim|cmake> project, you can
  simply add the following lines in your file <verbatim|CMakeLists.txt>:

    <\cpp-fragment>
      find_package(Semialgebraictools)

      include_directories (${SEMIALGEBRAICTOOLS_INCLUDE_DIR})

      link_directories (${SEMIALGEBRAICTOOLS_LIBRARY_DIR})

      link_libraries (${SEMIALGEBRAICTOOLS_LIBRARIES})

    </cpp-fragment>
  
<tmdoc-copyright|2012|>
<tmdoc-license|Permission is granted to copy, distribute and/or modify this
document under the terms of the <hlink|GNU General Public
License|http://www.gnu.org/licenses/gpl.txt>. If you don't have this file,
write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.>

</body>
