#set (PKG_NAME SEMIALGEBRAICTOOLS)
#set (PKG_HTML_NAME semialgebraictools)
set (PKG_HTML_DIR  html)

######################################################################
# Build documentation 
######################################################################
message ("-- html documentation in ${PKG_HTML_DIR}")

configure_file ("${CMAKE_CURRENT_SOURCE_DIR}/rst/conf.py.in"
		"${CMAKE_CURRENT_SOURCE_DIR}/rst/conf.py" )		

add_custom_target (${PKG}html
		   COMMAND sphinx-build -a -b html -d ${CMAKE_BINARY_DIR}/doc/doctrees ${CMAKE_CURRENT_SOURCE_DIR}/rst ${CMAKE_BINARY_DIR}/${PKG_HTML_DIR} 
                   WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
                   COMMENT "build html doc")

add_custom_target (${PKG}latex
                   COMMAND sphinx-build -a -b latex -d ${CMAKE_BINARY_DIR}/doc/doctrees ${CMAKE_CURRENT_SOURCE_DIR}/rst ${CMAKE_BINARY_DIR}/latex
		   COMMAND cd ${CMAKE_BINARY_DIR}/latex && pdflatex -interaction=nonstopmode Axel.tex
                   WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
                   COMMENT "build sphinx doc")



add_custom_command (OUTPUT ${CMAKE_BINARY_DIR}/${PKG_HTML_DIR}/.setup
		    COMMAND cp -r ${CMAKE_CURRENT_SOURCE_DIR}/img ${PKG_HTML_DIR}
                    WORKING_DIRECTORY ${CMAKE_BINARY_DIR})

add_custom_target (${PROJECT_NAME}_html ALL
                   DEPENDS
                     ${CMAKE_BINARY_DIR}/${PKG_HTML_DIR}/.setup
                   WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
                   COMMENT  "html documentation")

add_custom_target (clean_${PROJECT_NAME}_html
                   COMMAND rm  -f ${PKG_HTML_DIR}/.doxygen ${PKG_HTML_DIR}/.setup ${PKG_HTML_DIR}/*.html 
                   WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
                   COMMENT "clean html setup and dox files")
