# Try to find the LAPACK librairies
#  LAPACK_FOUND - system has LAPACK lib
#  LAPACK_LIBRARIES - Libraries needed to use LAPACK

if (LAPACK_INCLUDE_DIR AND LAPACK_LIBRARIES)
  # Already in cache, be silent
  set(LAPACK_FIND_QUIETLY TRUE)
endif (LAPACK_INCLUDE_DIR AND LAPACK_LIBRARIES)

find_library(LAPACK_LIBRARIES NAMES lapack liblapack)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(LAPACK DEFAULT_MSG LAPACK_LIBRARIES)

mark_as_advanced(LAPACK_LIBRARIES)
