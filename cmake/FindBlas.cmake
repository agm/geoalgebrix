# Try to find the BLAS librairies
#  BLAS_FOUND - system has BLAS lib
#  BLAS_LIBRARIES - Libraries needed to use BLAS

if (BLAS_INCLUDE_DIR AND BLAS_LIBRARIES)
  # Already in cache, be silent
  set(BLAS_FIND_QUIETLY TRUE)
endif (BLAS_INCLUDE_DIR AND BLAS_LIBRARIES)

find_library(BLAS_LIBRARIES NAMES blas libblas)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(BLAS DEFAULT_MSG BLAS_LIBRARIES)

mark_as_advanced(BLAS_LIBRARIES)
