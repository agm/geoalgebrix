/**********************************************************************
 * PACKAGE  : geoalgebrix
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once

#include <realroot/polynomial_bernstein.hpp>
#include <realroot/polynomial_sparse.hpp>
#include <geoalgebrix/solver_secant.hpp>
#include <geoalgebrix/foreach.hpp>
#include <geoalgebrix/regularity.hpp>
#include <geoalgebrix/tmsh.hpp>
#include <geoalgebrix/algebraic3d.hpp>
#include <geoalgebrix/point.hpp>
#include <geoalgebrix/tmsh_vertex.hpp>
#include <geoalgebrix/mesh.hpp>
//#include <geoalgebrix/csg_cell3d.hpp>
//#include <geoalgebrix/csg_controler3d.hpp>

#include <geoalgebrix/subdivider.hpp>
//#include <geoalgebrix/polygonizer3d_mc.hpp>
#include <geoalgebrix/polygonizer3d_cms.hpp>

#define TMPL template<class CELL, class VERTEX>
#define CTRL controler3d<CELL,VERTEX>

//====================================================================
namespace mmx {
//--------------------------------------------------------------------
namespace csg {

//--------------------------------------------------------------------
/*!
 * \brief The csg_cell3d struct
 *   - The cell is determined by its 8 corner indices (cell)
 *   - It has a regularity index @regularity()
 */
struct cell3d: public tmsh::cell<3> {

    typedef double             C;
    typedef cell3d         Cell;

public:

    cell3d(): cell<3>(), m_reg(OUTSIDE) {  m_center=-1;}

    cell3d(const cell3d& cel):
        cell<3>(cel), m_reg(cel.m_reg), m_center(cel.m_center) { }

    int center() const { return m_center; }
    void set_center (int n) { m_center=n; }

    regularity_t      m_reg;

    int m_center;
    std::vector<csg::generic* > m_active;

};

template<class CELL, class VERTEX>  struct controler3d;
}
//--------------------------------------------------------------------
namespace slrv {

template<class R> struct evaluator;

/**
 * @brief The evaluator<csg::controler3d<CELL,VERTEX> > struct
    It provides the edge polynomial for a given polynomial on a cell.
*/
TMPL struct evaluator<csg::controler3d<CELL,VERTEX> > {
    typedef csg::controler3d<CELL,VERTEX> Controler;
    typedef typename Controler::EdgePolynomial EdgePolynomial;
    template<class CL, class POL>
    static void edge_polynomial(EdgePolynomial& f, Controler* ctrl, CL* cl,  const POL& pol, int v, int s) {
        std::vector<double> bx(4);
        bx[0]=ctrl->xmin(*cl);
        bx[1]=ctrl->xmax(*cl);
        bx[2]=ctrl->ymin(*cl);
        bx[3]=ctrl->ymax(*cl);
        polynomial<double, with<Bernstein> > p;
        let::assign(p.rep(), tensor::bernstein<double>(pol.rep(),bx));
        tensor::face(f,p,v,s);
    }
};
//--------------------------------------------------------------------
} /* namespace slvr */
//--------------------------------------------------------------------
namespace csg {

template<class CELL, class VERTEX>
/**
 * @brief The csg::controler3d struct
 */
struct controler3d: tmsh::controler<3,CELL,VERTEX>
{
    typedef double  C;
    typedef CELL    Cell;
    typedef VERTEX  Vertex;
    typedef CTRL    Controler;

    // typedef polynomial< C, with<Bernstein> >        EdgePolynomial;
    typedef polynomial< C, with<Sparse,DegRevLex> > Polynomial;
    typedef std::vector<VERTEX*>                    Solution;
    typedef std::vector<int>                        Multiplicity;
    typedef point<double,3> Point;

    controler3d() {}

    Cell* init_cell(csg::generic* o, double xmin, double xmax, double ymin, double ymax, double zmin, double zmax);

    regularity_t regularity(Cell* cl);

    void tag_corner    (Cell* cl);
    int  tag_of_middle (Cell* cl, int i0, int i1);

    void process_regular (Cell* cl);
    void process_singular(Cell* cl);

    void subdivide(int v, Cell* cl, Cell*& left, Cell*& right) {
        left  = new Cell(*cl);
        right = new Cell(*cl);
        this->tmsh::controler<3,CELL,VERTEX>::subdivide(v,*cl,*left,*right);
    }

    void set_active(CELL* cl);

    int insert_edge_point(Cell *cl, int c0, int c1, int v);

    std::vector< int > calculate_partial(std::vector< int > points_final,unsigned z,int regular,int a1,int b1,int c1,int d1);
    std::vector<int> single_edgepts(int a1,int b1,int v);
    std::vector<double> calculate_distances(std::vector<int> points,int regular);


    int regularity_direction(int v,int A,int B,int C,int D,int times);
    int singular_point(){return singular;}
    int minimum_in_cell(int a,int b,int c,int d,int e ,int f ,int g ,int h);

    void edge_point (Cell* cl);
    void edge_point(CELL* cl, unsigned f);

    void center_point   (Cell* cl);

    std::vector<Cell*> m_regular;
    std::vector<Cell*> m_singular;
    csg::generic*      m_obj;
    std::vector<int> singular_points;



    int singular;

};
//--------------------------------------------------------------------
/*!
 * @brief CTRL::init_cell
 * @param o
 * @param xmin
 * @param xmax
 * @param ymin
 * @param ymax
 * @return the cell associated to the csg object @c o with given min-max y-x coordinates.
 */
TMPL
CELL* CTRL::init_cell(csg::generic* o,
                      double xmin, double xmax, double ymin, double ymax, double zmin, double zmax)
{

    Vertex p[8] = {
        Vertex(xmin,ymin,zmin),
        Vertex(xmax,ymin,zmin),
        Vertex(xmin,ymax,zmin),
        Vertex(xmax,ymax,zmin),
        Vertex(xmin,ymin,zmax),
        Vertex(xmax,ymin,zmax),
        Vertex(xmin,ymax,zmax),
        Vertex(xmax,ymax,zmax)
    };
    this->tmsh::controler<3,CELL,VERTEX>::init(p);

    CELL* res= new CELL;

    m_obj= o;

    return res;
}


/*!
 * @brief CTRL::regularity
 * @param cl cell pointer
 * @return the regularity of the cell.
 */
TMPL regularity_t CTRL::regularity(CELL *cl) {

    double box[6] = {this->xmin(*cl), this->xmax(*cl),
                     this->ymin(*cl), this->ymax(*cl),
                     this->zmin(*cl), this->zmax(*cl)
                    };
    regularity_t r = m_obj->regularity(box, true);
    cl->regularity(r);

    return r;
}

TMPL
void CTRL::tag_corner(CELL *cl) {

    if(this->vertex(cl->idx(0)).tag()!=-1)
        this->vertex(cl->idx(0)).tag(m_obj->tag(this->xmin(*cl),this->ymin(*cl),this->zmin(*cl)));
    if(this->vertex(cl->idx(1)).tag()!=-1)
        this->vertex(cl->idx(1)).tag(m_obj->tag(this->xmax(*cl),this->ymin(*cl),this->zmin(*cl)));
    if(this->vertex(cl->idx(2)).tag()!=-1)
        this->vertex(cl->idx(2)).tag(m_obj->tag(this->xmin(*cl),this->ymax(*cl),this->zmin(*cl)));
    if(this->vertex(cl->idx(3)).tag()!=-1)
        this->vertex(cl->idx(3)).tag(m_obj->tag(this->xmax(*cl),this->ymax(*cl),this->zmin(*cl)));

    if(this->vertex(cl->idx(4)).tag()!=-1)
        this->vertex(cl->idx(4)).tag(m_obj->tag(this->xmin(*cl),this->ymin(*cl),this->zmax(*cl)));
    if(this->vertex(cl->idx(5)).tag()!=-1)
        this->vertex(cl->idx(5)).tag(m_obj->tag(this->xmax(*cl),this->ymin(*cl),this->zmax(*cl)));
    if(this->vertex(cl->idx(6)).tag()!=-1)
        this->vertex(cl->idx(6)).tag(m_obj->tag(this->xmin(*cl),this->ymax(*cl),this->zmax(*cl)));
    if(this->vertex(cl->idx(7)).tag()!=-1)
        this->vertex(cl->idx(7)).tag(m_obj->tag(this->xmax(*cl),this->ymax(*cl),this->zmax(*cl)));

}


TMPL int CTRL::tag_of_middle(CELL* cl, int i0, int i1) {

    double x = (this->vertex(i0)[0]+this->vertex(i1)[0])/2.0,
            y = (this->vertex(i0)[1]+this->vertex(i1)[1])/2.0,
            z = (this->vertex(i0)[2]+this->vertex(i1)[2])/2.0;
    return m_obj->tag(x,y,z);
}

/*!
 * @brief CTRL::single_edgepts
 * @param a1 first end point
 * @param b1 second end point
 * @param v direction of edge
 * @return vector of the edgepoints present from a1 to b1 in direction of v
 */
TMPL
std::vector<int> CTRL::single_edgepts(int a1,int b1,int v)
{

    std::vector<int> return_points;
    int n=a1;

    while(n!=b1 && n!=-1)
    {
        if(this->vertex(n).tag()==-1)
        {
            return_points.push_back(n);
        }
        n=this->vertex(n).next(v);


    }

    if(n!=-1)
    {if(this->vertex(n).tag()==-1)
        {
            return_points.push_back(n);
        }}
    if(return_points.size()>0)
    {
        return return_points;
    }
    else
    {
        return_points.push_back(-1);
        return return_points;
    }

}


/*!
 * @brief CTRL::calculate_distances
 * @param points circular list of points inserted on the face
 * @param regular regularity for that particular face
 * @return vector of the coordinates accoding to the regularity of all the points in the list
 */
TMPL
std::vector<double> CTRL::calculate_distances(std::vector<int> points,int regular)
{
    std::vector<double> distances;
    for(unsigned i=0;i<points.size();i++)
    {
        Vertex a=this->vertex(points[i]);
//        Point A1(a.operator[](0),a.operator[](1),a.operator[](2));
        distances.push_back((double)a.operator[](regular));
    }
    return distances;
}


/*!
 * @brief CTRL::minimum_in_cell
 * @param a corner 1
 * @param b corner 2
 * @param c corner 3
 * @param d corner 4
 * @param e corner 5
 * @param f corner 6
 * @param g corner 7
 * @param h corner 8
 * @return point in tmsh if minimum exists in cell else -1
 */
TMPL
int CTRL::minimum_in_cell(int a,int b,int c,int d,int e ,int f ,int g ,int h)
{
    csg::algebraic3d<double>* ab = dynamic_cast<csg::algebraic3d<double>*>(m_obj);
    Polynomial xyz=ab->equation();
    Vertex a01=this->vertex(a);
    Vertex b01=this->vertex(b);
    Vertex c01=this->vertex(c);
    Vertex d01=this->vertex(d);
    Vertex e01=this->vertex(e);
    Vertex f01=this->vertex(f);
    Vertex g01=this->vertex(g);
    Vertex h01=this->vertex(h);
    Point A1(a01.operator[](0),a01.operator[](1),a01.operator[](2));
            Point B1(b01.operator[](0),b01.operator[](1),b01.operator[](2));
            Point C1(c01.operator[](0),c01.operator[](1),c01.operator[](2));
            Point D1(d01.operator[](0),d01.operator[](1),d01.operator[](2));
            Point E1(e01.operator[](0),e01.operator[](1),e01.operator[](2));
            Point F1(f01.operator[](0),f01.operator[](1),f01.operator[](2));
            Point G1(g01.operator[](0),g01.operator[](1),g01.operator[](2));
            Point H1(h01.operator[](0),h01.operator[](1),h01.operator[](2));
            Polynomial differential0,differential1,differential2;
    differential0=diff(xyz,0);
    differential1=diff(xyz,1);
    differential2=diff(xyz,2);
    Polynomial *abc=&differential0;
    Polynomial *def=&differential1;
    Polynomial *ghi=&differential2;
    slvr::solver_secant<Point,csg::algebraic3d<double>::Polynomial> slvr(&xyz);
    slvr.add_function(abc);
    slvr.add_function(def);
    slvr.add_function(ghi);
    slvr.minimizer(A1,B1,C1,D1,E1,F1,G1,H1);
    if(slvr.nb_sol()>0)
    {
        if(slvr.val(0)<slvr.m_eps)
        {
            Vertex m(slvr.solution(0).x(),slvr.solution(0).y(),slvr.solution(0).z());
            int point;
            point=this->insert_vertex(m);

            return point;

        }
        else
        {
            return -1;
        }
    }
    else
    {
        return -1;
    }


}

/*!
 * @brief CTRL::regularity_direction
 * @param v direction of the partial derivative to be added in the solver
 * @param a1 corner 1
 * @param b1 corner 2
 * @param c1 corner 3
 * @param d1 corner 4
 * @param times number of times this function is getting called.It has to be 1 and if then the solution is less than epsilon,we have a singular point
 * @return 1 for oppposite direction is regularity,2 for same direction is regularity,-1 for singular case
 */
TMPL
int CTRL::regularity_direction(int v,int a1,int b1,int c1,int d1,int times)
{

    csg::algebraic3d<double>* ab = dynamic_cast<csg::algebraic3d<double>*>(m_obj);
    Polynomial xyz=ab->equation();
    Vertex a01=this->vertex(a1);
    Vertex b01=this->vertex(b1);
    Vertex c01=this->vertex(c1);
    Vertex d01=this->vertex(d1);
    Point A1(a01.operator[](0),a01.operator[](1),a01.operator[](2));
    Point B1(b01.operator[](0),b01.operator[](1),b01.operator[](2));
    Point C1(c01.operator[](0),c01.operator[](1),c01.operator[](2));
    Point D1(d01.operator[](0),d01.operator[](1),d01.operator[](2));

    Polynomial differential;

    differential=diff(xyz,v);

    if(times<=1) {

        Polynomial *abc=&differential;

        slvr::solver_secant<Point,csg::algebraic3d<double>::Polynomial> slvr(&xyz);

        slvr.add_function(abc);

        slvr.minimizer(A1,B1,C1,D1);

        if(slvr.nb_sol()>0) {
            if(slvr.val(0)>(slvr.m_eps)*1.e2)
            {
                return 1;
            }
            else
            {
                if(times==1)
                {
                    //std::cout<<std::endl<<"singular asdasdasd asdasdsadasdasdasdas dasdasdasdas "<<v<<" "<<slvr.solution(0).x()<<" "<<slvr.solution(0).y()<<" "<<slvr.solution(0).z()<<std::endl;
                    Vertex m(slvr.solution(0).x(),slvr.solution(0).y(),slvr.solution(0).z());
                    singular=this->insert_vertex(m);
                    singular_points.push_back(singular);
                    return 0;
                }
                return 0;}
        } else {
            return 2;
        }
    } else {
        mdebug()<< ">>>>>>>>> Function regularity_direction called more than once.";
        return -2;
    }
}

/*!
 * @brief CTRL::calculate_partial
 * @param points_final circular list of points inserted in that face
 * @param z face constant
 * @param regular regularituy of the face
 * @param a1 corner 1
 * @param b1 corner 2
 * @param c1 corner 3
 * @param d1 corner 4
 * @return the partial indices of the points in the face in a circular list manner
 */
TMPL
std::vector< int > CTRL::calculate_partial(std::vector< int > points_final,unsigned z,int regular,int a1,int b1,int c1,int d1)
{
    csg::algebraic3d<double>*  ab= dynamic_cast<csg::algebraic3d<double>*>(m_obj);
    Polynomial xyz=ab->equation();
    Polynomial differential0;
    Polynomial differential1;
    int sign_partial;
    std::vector< int > output_partial;
    int a = tmsh::cell<3>::FaceDir[z][0];
    int b = tmsh::cell<3>::FaceDir[z][1];
    Vertex a01=this->vertex(a1);
    Vertex b01=this->vertex(b1);
    Vertex c01=this->vertex(c1);
    Vertex d01=this->vertex(d1);

//    Point A1(a01.operator[](0),a01.operator[](1),a01.operator[](2));
//            Point B1(b01.operator[](0),b01.operator[](1),b01.operator[](2));
//            Point C1(c01.operator[](0),c01.operator[](1),c01.operator[](2));
//            Point D1(d01.operator[](0),d01.operator[](1),d01.operator[](2));

    differential0=diff(xyz,a);
    differential1=diff(xyz,b);
    for (unsigned i=0;i<points_final.size();i++)
    {

        Vertex m=this->vertex(points_final[i]);

        Point C(m.operator[](0),m.operator[](1),m.operator[](2));
                double abc=differential0(C.x(),C.y(),C.z());

        int pq=1;
        while (pq<=3)
        {
            if(abc!=0)
            {
                if(abc>0)
                {
                    abc=1;
                }
                else
                {
                    abc=-1;
                }
                pq=5;
            }
            else
            {
                differential0=diff(differential0,a);
                abc=differential0(C.x(),C.y(),C.z());
                pq++;
            }

        }


        double cd=differential1(C.x(),C.y(),C.z());
        int rs=1;
        while (rs<=3)
        {
            if(cd!=0)
            {
                if(cd>0)
                {
                    cd=1;
                }
                else
                {
                    cd=-1;
                }
                rs=5;
            }
            else
            {
                differential1=diff(differential1,b);
                cd=differential1(C.x(),C.y(),C.z());
                rs++;
            }

        }

        sign_partial=(int)abc*(int)cd;

        if(points_final[i]==a1)
        {
            if(pq%2==0||rs%2==0)
            {
                if(sign_partial>0){
                    sign_partial=2;
                }
                else if(sign_partial<0)
                {
                    sign_partial=1;
                }
            }
            else
            {
                if(sign_partial>0)
                {
                    sign_partial=1;
                }
                else if(sign_partial<0)
                {
                    sign_partial=2;
                }
            }

        }
        else if(points_final[i]==d1)
        {
            if(pq%2==0||rs%2==0)
            {
                if(sign_partial>0){
                    sign_partial=-1;
                }
                else if(sign_partial<0)
                {
                    sign_partial=2;
                }
            }
            else
            {
                if(sign_partial>0)
                {
                    sign_partial=2;
                }
                else if(sign_partial<0)
                {
                    sign_partial=-1;
                }
            }

        }
        else if(points_final[i]==b1)
        {
            if(a==regular)
            {
                if(pq%2==0||rs%2==0)
                {
                    if(sign_partial>0){
                        sign_partial=2;
                    }
                    else if(sign_partial<0)
                    {
                        sign_partial=-1;
                    }
                }
                else
                {
                    if(sign_partial>0)
                    {
                        sign_partial=-1;
                    }
                    else if(sign_partial<0)
                    {
                        sign_partial=2;
                    }
                }

            }
            else
            {
                if(pq%2==0||rs%2==0)
                {
                    if(sign_partial>0){
                        sign_partial=2;
                    }
                    else if(sign_partial<0)
                    {
                        sign_partial=1;
                    }
                }
                else
                {
                    if(sign_partial>0)
                    {
                        sign_partial=1;
                    }
                    else if(sign_partial<0)
                    {
                        sign_partial=2;
                    }
                }

            }

        }

        else if(points_final[i]==c1)
        {
            if(a==regular)
            {
                if(pq%2==0||rs%2==0)
                {
                    if(sign_partial>0){
                        sign_partial=1;
                    }
                    else if(sign_partial<0)
                    {
                        sign_partial=2;
                    }
                }
                else
                {
                    if(sign_partial>0)
                    {
                        sign_partial=2;
                    }
                    else if(sign_partial<0)
                    {
                        sign_partial=1;
                    }
                }
            }
            else
            {
                if(pq%2==0||rs%2==0)
                {
                    if(sign_partial>0){
                        sign_partial=2;
                    }
                    else if(sign_partial<0)
                    {
                        sign_partial=-1;
                    }
                }
                else
                {
                    if(sign_partial>0)
                    {
                        sign_partial=-1;
                    }
                    else if(sign_partial<0)
                    {
                        sign_partial=2;
                    }
                }
            }
        }
        else
        {
            if(pq%2==0||rs%2==0)
            {
                sign_partial=sign_partial*2;
            }
            sign_partial=sign_partial*-1;
        }
        output_partial.push_back(sign_partial);

    }
    return output_partial;

}

//--------------------------------------------------------------------
TMPL
void CTRL::edge_point(CELL* cl, unsigned f) {

    if(cl->m_active.size()==0) return;

    int c0, c1, n0, n1, n, curr_dir;

    csg::algebraic3d<double>* ab = dynamic_cast<csg::algebraic3d<double>*>(cl->m_active[0]);
    Polynomial xyz = ab->equation();

    for(unsigned i=0;i<4;i++)
    {
        c0 = tmsh::cell<3>::FaceEdge[i][0];
        c1 = tmsh::cell<3>::FaceEdge[i][1];
        curr_dir = tmsh::cell<3>::FaceDir[f][i/2];

        n0 = cl->idx(tmsh::cell<3>::Face[f][c0]);
        n1 = cl->idx(tmsh::cell<3>::Face[f][c1]);

        Point A(this->vertex(n0)[0],this->vertex(n0)[1],this->vertex(n0)[2]);
        Point B(this->vertex(n1)[0],this->vertex(n1)[1],this->vertex(n1)[2]);

        slvr::solver_secant<Point,csg::algebraic3d<double>::Polynomial> slvr(&xyz);
        slvr.critical_points(A,B);

        //mdebug()<<"sol: "<< *xyz<<n0<<" -- "<< n1<<"\n "<<A<<"-"<<B<<"  :: "<< slvr.nb_sol();
        for(unsigned k=0;k<slvr.nb_sol();k++)
        {
            if(slvr.val(k)<slvr.m_eps)
            {
                Vertex sol(slvr.solution(k).x(),slvr.solution(k).y(),slvr.solution(k).z());
                n = this->insert_vertex(sol,n0,n1,curr_dir);
                this->vertex(n).tag(-1);
                //mdebug()<<"sol: insert"<< slvr.solution(k)<< "idx::"<<n;
            }
        }
    }

}

//--------------------------------------------------------------------
TMPL
/**
 * @brief CTRL::insert_edge_point
 * @param cl cell
 * @param c0 first vertex index
 * @param c1 second vertex index
 * @param v  direction of the edge
 * @param seq active surfaces of the cell
 * @return the index of the inserted point
 *
 * Insert the point of the surface in the edge between the points c0 and c1.
 */
int CTRL::insert_edge_point(CELL* cl, int n0, int n1, int v) {


    //int n0 = cl->idx(c0), n1 = cl->idx(c1);
    //    std::cout<<std::endl<< "insert_edge_point"
    //           <<" ["<< this->vertex(n0)[0]<<this->vertex(n0)[1]<<this->vertex(n0)[2]
    //           <<"--"<< this->vertex(n1)[0]<<this->vertex(n1)[1]<<this->vertex(n1)[2]
    //           <<"]";
    csg::algebraic3d<double>* a = dynamic_cast<csg::algebraic3d<double>*>(cl->m_active[0]);

    if(! a) {
        std::cout<<std::endl<< "insert_edge_point: not Algebraic";
        return -1;
    }

    //double v0 = a->m_polynomial(this->vertex(n0)[0],this->vertex(n0)[1],this->vertex(n0)[2])
    //        ,v1 = a->m_polynomial(this->vertex(n1)[0],this->vertex(n1)[1],this->vertex(n1)[2]);

    Vertex m;

    int r = slvr::solve_secant(m, a->m_polynomial, this->vertex(n0), this->vertex(n1));
    if(r == -1) {
        this->vertex(n0).tag(-1);
        //std::cout<<std::endl<<"tag -1"<<n0;
        return n0;
    } else if(r == 1) {
        this->vertex(n1).tag(-1);
        //std::cout<<std::endl<<"tag -1"<<n0;
        return n1;
    } else {

        // std::cout<<std::endl<<"insert_vertex"<<n0<<n1;
        // Insert vertex m between n0 and n1 in direction v
        r=this->insert_vertex(m, n0, n1,v);

        //std::cout<<std::endl<<"inserted"<<r;
        this->vertex(r).tag(-1);
        return r;
    }
}
//--------------------------------------------------------------------
TMPL
void CTRL::edge_point(CELL* cl) {

    //this->tag_corner(cl);
    //return;

    if(cl->m_active.size()==0) return;

    int n;
    for(int i=0; i<12; i++) {
        if( this->vertex(cl->idx(tmsh::cell<3>::Edge[i][0])).tag() !=
                this->vertex(cl->idx(tmsh::cell<3>::Edge[i][1])).tag() ) {
            n = insert_edge_point(cl,
                                  cl->idx(tmsh::cell<3>::Edge[i][0]),
                    cl->idx(tmsh::cell<3>::Edge[i][1]),
                    tmsh::cell<3>::EdgeDir[i],
                    cl->m_active);
            //            mdebug()<< "\n>>>"<< cl->idx(tmsh::cell<3>::Edge[i][0])
            //                    << cl->idx(tmsh::cell<3>::Edge[i][1])
            //                    << tmsh::cell<3>::EdgeDir[i]<<"::"<<n;
        }
    }

}

//--------------------------------------------------------------------
TMPL
void CTRL::center_point(CELL* cl) {
    int n0 = this->insert_vertex(this->middle(cl));
    //std::cout<<std::endl<<"center"<<n0;
    cl->set_center(n0);
}

TMPL
void CTRL::set_active(CELL* cl) {
    m_obj->active(cl->m_active);
}

TMPL
void CTRL::process_regular(CELL* cl) {
    this->set_active(cl);
    this->tag_corner(cl);
    //this->edge_point(cl);
    m_regular.push_back(cl);
}

TMPL
void CTRL::process_singular(CELL* cl) {
    this->set_active(cl);
    this->tag_corner(cl);
    //this->edge_point(cl);
    m_singular.push_back(cl);
}
//--------------------------------------------------------------------
} /* namespace csg */
//--------------------------------------------------------------------

template<class C>
struct mshr_csg3d {
    typedef  mesh<C>                        Mesh;
    typedef  tmsh::vertex<3,double>         Vertex;
    typedef  csg::cell3d                    Cell;
    typedef  csg::controler3d<Cell, Vertex> Controler;
    typedef  subdivider<Controler>          Subdivider;
    //typedef  polygonizer3d_mc<Controler>   Polygonizer;
    typedef  polygonizer3d_cms<Controler>   Polygonizer;
    //typedef  Solver;
};

//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================
#undef TMPL
#undef CTRL
