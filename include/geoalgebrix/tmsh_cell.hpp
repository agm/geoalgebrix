/**********************************************************************
 * PACKAGE  : geoalgebrix
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once

#include <algorithm>
#include <vector>
#include <geoalgebrix/regularity.hpp>
#include <geoalgebrix/tuple.hpp>

//====================================================================
namespace mmx {
namespace tmsh {
//--------------------------------------------------------------------

template<int N> struct cell;

template<>
struct cell<2> {
    typedef tuple<2,int> Tuple;

    cell(void): m_reg(OUTSIDE) {
        for(unsigned i=0;i<4;i++) corners[i]=i;
    }

    cell(const cell<2>& cel): m_reg(cel.m_reg) {
        for(unsigned i=0;i<4;i++) corners[i]=cel[i];
    }

    cell(int idx[4]): m_reg(0) {
        for(unsigned i=0;i<4;i++) corners[i]=idx[i];
    }

    int  operator[](int i) const { return corners[i]; }
    int& operator[](int i)       { return corners[i]; }

    int  idx(int i) const { return corners[i]; }
    int& idx(int i)       { return corners[i]; }

    void regularity(int r)       { m_reg = r; }
    int  regularity(void)  const { return m_reg; }

    void add_center(int i)       { m_center.push_back(i); }
    int  center(int i)     const { return (m_center[i]); }
    bool has_center(void)  const { return (m_center.size() > 0); }

    const std::vector<int>& boundary(void) const  { return m_boundary;}
    std::vector<int>&       boundary(void)        { return m_boundary;}
    int                     boundary(int k) const { return m_boundary[k]; }

    void add_boundary_point(int i, int mu) {
        if(std::find(m_boundary.begin(), m_boundary.end(),i) == m_boundary.end())
            m_boundary.push_back(i);
        if(mu>1) m_boundary.push_back(i);
    }

    static unsigned dim;

public:
    int  corners[4];

    static tuple<2,int>  Face[2][2];
    static tuple<2,int>  FaceEdge[1];
    static unsigned      nb_fedge;


private:
    int              m_reg;
    std::vector<int> m_boundary;
    std::vector<int> m_center;
};

/*
unsigned cell<2>::dim = 3;
unsigned cell<2>::nb_fedge = 1;

tuple<2,int> cell<2>::FaceEdge[1] = {
    tuple<2,int>(0,1)
};

tuple<2,int> cell<2>::Face[2][2]={
    {tuple<2,int>(0,2), tuple<2,int>(1,3)},
    {tuple<2,int>(0,1), tuple<2,int>(2,3)}
};
*/
//--------------------------------------------------------------------

//--------------------------------------------------------------------
template<>
/**
 * @brief The cell<3> struct
 * The directions, vertices and edges are numbered as follows:
 *
 * \begincode
 *    v=2           v=1
 *     |            /
 *     |           /
 *     |   6 ----6---- 7
 *     |  /|     /    /|
 *     | 7 |    /    5 |
 *     |/  11  /    /  10
 *     4 ----4---- 5   |
 *     |   | /     |   |
 *     |   2 ----2-|-- 3
 *     8  /        9  /
 *     | 3         | 1
 *     |/          |/
 *     0 ----0---- 1 --------- v = 0
 * \endcode
 */
struct cell<3> {
    typedef tuple<4,int> Tuple;

    cell(void) {
        for(unsigned i=0;i<8;i++) corners[i]=i;
    }

    cell(int idx[8]) {
        for(unsigned i=0;i<8;i++) corners[i]=idx[i];
    }

    int  operator[](int i) const { return corners[i]; }
    int& operator[](int i)       { return corners[i]; }

    void regularity(int r)       { m_reg = r; }
    int  regularity(void)  const { return m_reg; }

    int  idx(int i)        const { return corners[i]; }
    int& idx(int i)              { return corners[i]; }

    int  lowercorner(int v, int s) const { return corners[Face[2*v+s][0]]; }
    int  uppercorner(int v, int s) const { return corners[Face[2*v+s][3]]; }

    void insert_edge(int n0, int n1) {
        if(n0 != n1) {
//            mdebug()<<"12"<<m_edges.size();
            std::pair<int,int> abc(n0,n1);
//            mdebug()<<"abc"<<abc.first<<abc.second;
            m_edges.push_back(abc);

        }
//        mdebug()<<"13";
    }

    // edges on the boundary of the cell
    std::vector<std::pair<int,int> > m_edges;

    int corners[8];

    static unsigned dim;
    static unsigned FaceDir[6][2];
    static unsigned Edge[12][2];
    static unsigned EdgeDir[12];
    static unsigned Face[6][4];
    static unsigned FaceEdge[4][2];
    static unsigned nb_fedge;
private:
    int              m_reg;
};

/*
unsigned cell<3>::dim = 3;

unsigned cell<3>::FaceDir[6][2] = {
    {1,2}, {1,2},
    {0,2}, {0,2},
    {0,1}, {0,1}
};

unsigned cell<3>::Edge[12][2] = {
    {0,1}, {1,3}, {2,3}, {0,2},
    {4,5}, {5,7}, {6,7}, {4,6},
    {0,4}, {1,5}, {3,7}, {2,6}
};

unsigned cell<3>::EdgeDir[12] = {
    0,1,0,1,
    0,1,0,1,
    2,2,2,2
};

unsigned cell<3>::Face[6][4]={
    {0,2,4,6}, {1,3,5,7},
    {0,1,4,5}, {2,3,6,7},
    {0,1,2,3}, {4,5,6,7}
};

unsigned cell<3>::nb_fedge = 4;

unsigned cell<3>::FaceEdge[4][2] = {
    {0,1}, {2,3}, {0,2}, {1,3}
};
*/
//--------------------------------------------------------------------
} /* namespace tmsh */
} /* namespace mmx */
//====================================================================
#undef TMPL
