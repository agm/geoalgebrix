/**********************************************************************
 * PACKAGE  : geoalgebrix
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once

#include <vector>
#include <list>
#include <iostream>
#include <math.h>

#include <geoalgebrix/mdebug.hpp>
#include <geoalgebrix/point.hpp>
#include <geoalgebrix/tmsh_cell.hpp>
#include <geoalgebrix/tmsh_vertex.hpp>

#define TMPL template <class CELL, class VERTEX>
#define SELF controler<3,CELL,VERTEX>

//====================================================================
namespace mmx {
namespace tmsh {
//--------------------------------------------------------------------
template <int N, class CELL, class VERTEX> struct controler;

template <class CELL, class VERTEX>
struct controler<3,CELL,VERTEX> {

    typedef VERTEX Vertex;
    typedef CELL   Cell;
    typedef point<double,3> Point;

    controler(): m_eps(1e-5) {}

    const Vertex&  vertex(int i) const { return m_vertices[i]; }
    Vertex&        vertex(int i)       { return m_vertices[i]; }

    void init(VERTEX cl[]);

    double  min(int v, const CELL& c);
    double  max(int v, const CELL& c);

    double xmin(const CELL& c);
    double ymin(const CELL& c);
    double zmin(const CELL& c);

    double xmax(const CELL& c);
    double ymax(const CELL& c);
    double zmax(const CELL& c);

    double xsize(const CELL& c);
    double ysize(const CELL& c);
    double zsize(const CELL& c);
    double  size(const CELL& c);
    double  size(CELL* c) { return this->size(*c); }

    Vertex middle(Cell* cl);

    int  split_direction(const Cell& cl);

    int  insert_vertex(const Vertex &p);
    int  insert_vertex(const Vertex& p, int i0, int i1 , int v);
    int  insert_middle(int i0, int i1, int v);

    void insert_edge  (int i0, int i1, int v);
    void insert_edge  (int i0, int i1);

    int  insert_cell  (const Cell& c);

    int neighbor(int idx, int f, int &w);

    int find_edge_point(int n0, int n1, int v);

    void subdivide(int v, const CELL& c0, CELL& left, CELL& right);

    unsigned nbv() const { return m_vertices.size(); }
    unsigned nbc() const { return m_cells.size(); }

    bool is_identifiable(double a, double b) const {
        return (fabs(a-b)<m_eps);
    }

    Point point_return(int v){
        Vertex m;
        m=this->vertex(v);
        Point A(m.operator[](0),m.operator[](1),m.operator[](2));
                return A;
    }
    void set_tag_corner(CELL* cl, int t);

    void insert_lexico(std::list<int>& l, int n, int v0, int v1);
    void sweep_face_point(CELL *cl, int f);

    void face_cells(std::vector<int>& res, CELL* cl, int f);

    template <class MESH> void get_tmsh(MESH* m);

    void add_regular_cell(CELL* cl) { m_regular.push_back(cl); }
    void add_singular_cell(CELL* cl) { m_singular.push_back(cl); }

    std::vector<Cell*> m_regular;
    std::vector<Cell*> m_singular;

private:
    double              m_eps;
    std::vector<VERTEX> m_vertices;
    std::vector<CELL>   m_cells;
};


TMPL
void SELF::init(VERTEX p[]) {
    for(unsigned k=0;k< 2*CELL::Tuple::size; k++)
        this->insert_vertex(p[k]);
    for(unsigned v=0;v< VERTEX::dim;v++) {
        for(unsigned k=0;k< CELL::Tuple::size; k++)
            this->insert_edge(CELL::Face[2*v][k], CELL::Face[2*v+1][k], v);
    }
}

TMPL
double SELF::min(int v, const CELL &cl) {
    return (this->vertex(cl[0])[v]);
}

TMPL
double SELF::max(int v, const CELL &cl) {
    return (this->vertex(cl[2*v+1])[v]);
}

TMPL
double SELF::xmin(const CELL &cl) {
    return (this->vertex(cl[0])[0]);
}

TMPL
double SELF::ymin(const CELL &cl) {
    return (this->vertex(cl[0])[1]);
}

TMPL
double SELF::zmin(const CELL &cl) {
    return (this->vertex(cl[0])[2]);
}

TMPL
double SELF::xmax(const CELL &cl) {
    return (this->vertex(cl[1])[0]);
}

TMPL
double SELF::ymax(const CELL &cl) {
    return (this->vertex(cl[2])[1]);
}

TMPL
double SELF::zmax(const CELL &cl) {
    return (this->vertex(cl[4])[2]);
}

TMPL
double SELF::xsize(const CELL &cl) {
    return (this->vertex(cl[1])[0]-this->vertex(cl[0])[0]);
}

TMPL
double SELF::ysize(const CELL &cl) {
    return (this->vertex(cl[2])[1]-this->vertex(cl[0])[1]);
}

TMPL
double SELF::zsize(const CELL &cl) {
    return (this->vertex(cl[4])[2]-this->vertex(cl[0])[2]);
}

TMPL
double SELF::size(const CELL& c) {
    return std::max(std::max(this->xsize(c),this->ysize(c)),this->zsize(c));
}

TMPL VERTEX SELF::middle(CELL* cl) {

    double x = (this->vertex(cl->idx(0))[0]+this->vertex(cl->idx(1))[0])/2.0,
            y = (this->vertex(cl->idx(0))[1]+this->vertex(cl->idx(2))[1])/2.0,
            z = (this->vertex(cl->idx(0))[2]+this->vertex(cl->idx(4))[2])/2.0;
    return Vertex(x,y,z);
}

//--------------------------------------------------------------------
TMPL
/*!
 * \brief SELF::find_edge_point
 * \param cl: cell
 * \param c0: index of first point
 * \param n1: index of last point
 * \param v: direction
 *
 * \return the index of the first point with tag -1 between n0 and n1.
 *
 * If it does not exists, returns n1;
 */
int SELF::find_edge_point(int n0, int n1, int v) {

    int n = n0;

    //    mdebug()<<c0<<"->"<<c1<<" v:"<<v<< "tag:"<<this->controler()->vertex(n).tag()
    //           <<this->controler()->vertex(n1).tag();
    //    mdebug()<<" -->"<<n;
    while(n != n1 && n != -1 && this->vertex(n).tag() != -1) {
        // mdebug()<<" -->"<<n<< "tag:"<<this->controler()->vertex(n).tag();
        n = this->vertex(n).next(v);
    }

    if(n == n1 && this->vertex(n).tag() != -1)
        mdebug()<<"edge_point not found in"<<n0<<"--"<<n1;
    //mdebug()<<" ==>"<<n;
    return n;
}


TMPL
int SELF::split_direction(const CELL& cl)
{
    double s[3] = { this->xsize(cl), this->ysize(cl), this->zsize(cl) };
    int v = 0;
    if (s[1]>s[0]) v = 1;
    if (s[2]>s[v]) v = 2;
    return v;
}

TMPL
void SELF::subdivide(int v, const CELL &c0, CELL &left, CELL& right) {

    Vertex p;

    int i0, i1;
    int f0 = 2*v, f1 = 2*v+1;

    int nwi[4];
    for(unsigned k=0;k<CELL::Tuple::size;k++) {

        i0 = c0[CELL::Face[f0][k]];
        i1 = c0[CELL::Face[f1][k]];

        set_middle(p, this->vertex(i0), this->vertex(i1));

        nwi[k] = this->insert_middle(i0,i1,v);
        //nwi[k] = this->insert_middle(i0,i1);
        //        mdebug()<<"insert middle"<<nwi[k]<<i0<<i1<<v<<"   "
        //                   << this->vertex(i0)[0]<< this->vertex(i0)[1]<<this->vertex(i0)[2]
        //                   <<"-"
        //                   << this->vertex(i1)[0]<< this->vertex(i1)[1]<<this->vertex(i1)[2]
        //                   << "-->"
        //                   << this->vertex(nwi[k])[0]<< this->vertex(nwi[k])[1]<<this->vertex(nwi[k])[2];
        left [CELL::Face[f0][k]]=c0[CELL::Face[f0][k]];
        right[CELL::Face[f1][k]]=c0[CELL::Face[f1][k]];

        left [CELL::Face[f1][k]]=nwi[k];
        right[CELL::Face[f0][k]]=nwi[k];

    }

    for(unsigned k=0;k<CELL::nb_fedge;k++) {
        int d = (k/2)*(v!=2?2:1)+ (1-(k/2))*(v==0?1:0);
        //mdebug()<<"edge dir"<<d;
        insert_edge(nwi[CELL::FaceEdge[k][0]],nwi[CELL::FaceEdge[k][1]],d);
    }

}

//--------------------------------------------------------------------
TMPL
int SELF::insert_vertex(const VERTEX &p) {
    int i=m_vertices.size();
    m_vertices.push_back(p);
    return i;
}

//--------------------------------------------------------------------
TMPL
/**
 * @brief SELF::insert_vertex
 * @param p  vertex
 * @param i0 index of the first vertex on the edge
 * @param i1 index of the last vertex on the edge
 * @param v  direction of the edge
 * @return the index of the inserted point.
 *
 * Insert the vertex p between the vertices of index i0 and i1 in the direction v.
 */
int SELF::insert_vertex(const VERTEX &p, int i0, int i1, int v) {
    //    mdebug()<<"insert_vertex v:"<<v << " "<<i0<<i1<<"   "<<p[0]<<p[1]<<p[2]
    //           <<" ["<< vertex(i0)[0]<<vertex(i0)[1]<<vertex(i0)[2]
    //          <<"--"<< vertex(i1)[0]<<vertex(i1)[1]<<vertex(i1)[2]
    //         <<"]";
    //    mdebug()<<"ngb"<< vertex(i0).next(0)<<vertex(i0).next(1)<<vertex(i0).next(2)
    //           << vertex(i1).previous(0)<<vertex(i1).previous(1)<<vertex(i1).previous(2);

    if(this->is_identifiable(p[v],vertex(i0)[v]))
        return i0;
    if(this->is_identifiable(p[v],vertex(i1)[v]))
        return i1;

    int k, k0=i0, k1 = vertex(i0).next(v);
    //mdebug()<<"insert loop"<<k0<<k1<<vertex(k1)[0]<<vertex(k1)[1]<<vertex(k1)[2];
    while(vertex(k1).next(v)>=0 && p[v]>vertex(k1)[v]+m_eps) {
        k0 = k1;
        k  = k1;
        k1 = vertex(k).next(v);
        //mdebug()<<"insert loop"<<k0<<k1<<vertex(k1)[0]<<vertex(k1)[1]<<vertex(k1)[2];
    }

    int j=(this->is_identifiable(p[v],vertex(k1)[v])?k1:insert_vertex(p));

    int j1 = (j==k1?vertex(k1).next(v):k1);
    //mdebug()<<"insert"<<i0<<i1<<"  "<<k0<<k1<<"  "<<j<<j1;
    m_vertices[k0].m_neighbor[2*v]   = j;
    m_vertices[j1].m_neighbor[2*v+1] = j;
    m_vertices [j].m_neighbor[2*v+1] = k0;
    m_vertices [j].m_neighbor[2*v]   = j1;

    // mdebug()<<"insert idx"<<j
    // <<vertex(j).ngbr(0,0)<< vertex(j).ngbr(0,1)
    // <<vertex(j).ngbr(1,0)<< vertex(j).ngbr(1,1);
    return j;
}

//--------------------------------------------------------------------
TMPL
int SELF::insert_middle(int i0, int i1, int v) {
    Vertex p;
    for(unsigned i=0;i<Vertex::dim;i++) {
        if(i!= v)
            p[i] = this->vertex(i0)[i];
        else
            p[i] = (this->vertex(i0)[i]+this->vertex(i1)[i])/2;
    }

    return insert_vertex(p,i0,i1,v);
}

//--------------------------------------------------------------------
TMPL
void SELF::insert_edge(int i0, int i1, int v) {
    m_vertices[i0].m_neighbor[2*v]=i1;
    m_vertices[i1].m_neighbor[2*v+1]=i0;
}

//--------------------------------------------------------------------
TMPL
void SELF::insert_edge(int i0, int i1) {
    int v=(vertex(i0)[0]!=vertex(i1)[0]?0:(vertex(i0)[1]!=vertex(i1)[1]?1:2));
    insert_edge(i0, i1, v);
}

//--------------------------------------------------------------------
TMPL
int SELF::insert_cell(const CELL &c) {
    int i=m_cells.size();
    m_cells.push_back(c);
    return i;
}

//--------------------------------------------------------------------
TMPL
void SELF::set_tag_corner(CELL* cl, int t) {
    for(unsigned k=0; k<4; k++) {
        this->vertex(cl->vertex_idx(k)).tag(t);
    }
}

//--------------------------------------------------------------------
/**
 * @brief neighbor on the face f in the direction associated to w.
 *
 * Compute the next vertex index in direction associated to w and
 * update w by (w+1)%4 if such an element exists.
 *
 * The direction associated to w is FaceDir[f][w%2] if w <2
 * and "-" FaceDir[f][w%2] if w >= 2;
**/
TMPL
int SELF::neighbor(int idx, int f, int &w) {

    if(idx == -1) return -1;

    int v0 = w%2;
    int n = (w<2?this->vertex(idx).next(CELL::FaceDir[f][v0])
               :this->vertex(idx).previous(CELL::FaceDir[f][v0]));

    if (n != -1) {

        int w1 = (w+1)%4, v1 = w1%2;
        int n1 = (w1<2?this->vertex(n).next(CELL::FaceDir[f][v1])
                     :this->vertex(n).previous(CELL::FaceDir[f][v1]));

        if(n1 != -1) {
            w = w1;
        }
    }
    //mdebug()<<":"<<n<<w<<this->vertex(n).tag();
    //mdebug()<<"neighbor:"<<idx<<"->"<<n;
    return n;
}

//--------------------------------------------------------------------
TMPL
/**
 * @brief SELF::sweep_insert
 * @param l list of corner indices
 * @param n initial index
 * @param v0 first coordinate direction
 * @param v1 second coordinate direction
 *
 * Insert vertex n in the list l by lexicographic increasing order
 * in the coordinates (v0,v1).
 * No insertion if there is a repetition of indices.
 */
void SELF::insert_lexico(std::list<int>& l, int n,  int v0, int v1) {

    std::list<int>::iterator it = l.begin();
    while(it != l.end() &&
          ( this->vertex(n)[v0] > this->vertex(*it)[v0] + this->m_eps ||
            (is_identifiable(this->vertex(n)[v0], this->vertex(*it)[v0]) &&  this->vertex(n)[v1] > this->vertex(*it)[v1]+this->m_eps) )
          ) it++;

    if( *it != n) l.insert(it, n);

}

//---------------------------------------------------------------------
TMPL
/**
 * @brief sweep_face_point. Compute all the points of tag -1 on a face of a cell.
 * @param cl cell
 * @param f face
 *
 * Compute all the points of tag -1 on a face of a cell.
 * The face is determined by f = 2*v+s where v in {0,1,2} is the direction v
 * and s in {0,1} is the side of the face in this direction.
 */
void SELF::sweep_face_point(CELL* cl, int f) {

//    int v = f/2, s = f%2;
    unsigned d[2]= {cell<3>::FaceDir[f][0], cell<3>::FaceDir[f][1]};

    int n = cl->idx(cell<3>::Face[f][0]), m;

    double M0 = this->vertex(cl->idx(cell<3>::Face[f][3]))[d[0]] + this->m_eps,
            M1 = this->vertex(cl->idx(cell<3>::Face[f][3]))[d[1]] + this->m_eps;

    std::vector<int> res;
    std::list<int> l;
    if( n != -1)
        l.push_front(n);

    while(l.size()>0) {
        n = l.front();
        l.pop_front();
        if(this->vertex(n).tag()==-1) {
            res.push_back(n);
        }

        m = this->vertex(n).next(d[1]);
        if(m != -1 && this->vertex(m)[d[1]] <= M1)
            insert_lexico(l, m, d[0], d[1]);

        m = this->vertex(n).next(d[0]);
        if(m != -1 && this->vertex(m)[d[0]]<= M0)
            insert_lexico(l, m, d[0], d[1]);
    }

    // Use res ...
}


//---------------------------------------------------------------------
TMPL
/**
 * @brief face_cells. Compute all the left-lower corners of a subcell on a face.
 * @param cl cell
 * @param f face
 *
 * Compute all the points of tag -1 on a face of a cell.
 * The face is determined by f = 2*v+s where v in {0,1,2} is the direction v
 * and s in {0,1} is the side of the face in this direction.
 */
//---------------------------------------------------------------------
void SELF::face_cells(std::vector<int>& res, CELL* cl, int f) {

    unsigned d[2] = { cell<3>::FaceDir[f][0], cell<3>::FaceDir[f][1]};

    int n = cl->idx(cell<3>::Face[f][0]), n0, n1;
    //res.push_back(n);
    int c;
    double M0 = this->vertex(cl->idx(cell<3>::Face[f][3]))[d[0]] + this->m_eps,
            M1 = this->vertex(cl->idx(cell<3>::Face[f][3]))[d[1]] + this->m_eps;

    std::list<int> l;
    if( n != -1) l.push_front(n);

    while(l.size()>0) {
        n = l.front();
        l.pop_front();

        n0 = this->vertex(n).next(d[0]);
        n1 = this->vertex(n).next(d[1]);

        if(this->vertex(n).tag() == -1) mdebug()<<":"<<n;//<<n0<<n1;

        c = 0;
        if(n0 != -1 && this->vertex(n0)[d[0]] < M0) {
            insert_lexico(l, n0, d[0], d[1]);
            c++;
        }
        if(n1 != -1 && this->vertex(n1)[d[1]] < M1)  {
            insert_lexico(l, n1, d[0], d[1]);
            c++;
        }

        if(c>1) res.push_back(n);
    }
}

//--------------------------------------------------------------------
TMPL
template <class MESH>
        void SELF::get_tmsh(MESH* m) {

    for(unsigned i=0;i< this->nbv();i++) {
        m->add_vertex(this->vertex(i)[0], this->vertex(i)[1], this->vertex(i)[2]);
    }

    for(unsigned i=0;i<this->nbv();i++) {
        for(unsigned j=0; j< SELF::Cell::dim;j++)
            if(this->vertex(i).m_neighbor[2*j]>=0)
                m->add_edge(i,this->vertex(i).m_neighbor[2*j]);
    }

}

//--------------------------------------------------------------------
template<class OSTREAM, class CELL, class VERTEX>
OSTREAM& operator<<(OSTREAM& os, const SELF& tmsh) {
    os<<"<mesh size=\"0.1\" color=\"0 0 255\">\n";
    unsigned c=0;
    for(unsigned i=0;i<tmsh.nbv();i++) {
        for(unsigned j=0; j<SELF::Vertex::dim;j++)
            if(tmsh.vertex(i).m_neighbor[2*j]>=0)
                c++;
    }
    os<<"<count>"<< tmsh.nbv()<<" "<<c<<" 0</count>\n";
    os<<"<points>\n";
    for(unsigned i=0;i<tmsh.nbv();i++) {
        os<< tmsh.vertex(i)[0]<<" "<<tmsh.vertex(i)[1]<<" "<<tmsh.vertex(i)[2]<<"\n";
    }
    os<<"</points>\n";

    os<<"<edges>\n";
    for(unsigned i=0;i<tmsh.nbv();i++) {
        for(unsigned j=0; j<SELF::Vertex::dim;j++)
            if(tmsh.vertex(i).m_neighbor[2*j]>=0)
                os<<"2 "<<i<<" "<< tmsh.vertex(i).m_neighbor[2*j]<<"\n";
    }
    os<<"</edges>\n";
    os<<"</mesh>\n";
    return os;
}

//--------------------------------------------------------------------
} /* namespace tmsh */
//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================
#undef SELF
#undef TMPL

