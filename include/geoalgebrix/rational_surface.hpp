/*****************************************************************************
 * M a t h e m a g i x
 *****************************************************************************
 * RationalSurface
 * 2009-05-08
 * Bernard Mourrain & Julien Wintz
 *****************************************************************************
 *               Copyright (C) 2006 INRIA Sophia-Antipolis
 *****************************************************************************
 * Comments :
 ****************************************************************************/
# ifndef geoalgebrix_rational_surface_hpp
# define geoalgebrix_rational_surface_hpp

# include <realroot/ring_sparse.hpp>
# include <realroot/ring_bernstein_tensor.hpp>
//# include <geoalgebrix/algebraic_set.hpp>
# include <geoalgebrix/parametric_surface.hpp>
# include <math.h>
# define TMPL_DEF template<class C, int N=3>
# define TMPL  template<class C, int N>
# define ParametricSurface  parametric_surface<C>
# define SELF rational_surface<C,N>
//====================================================================
namespace mmx { namespace geoalgebrix {

TMPL_DEF
class rational_surface: public ParametricSurface {

    public:
    typedef C                                      Scalar;
    typedef typename mesh<C>::Point     Point;
    typedef typename mesh<C>::PointIterator   PointIterator;

//    typedef typename algebraic_set<C,V>::Polynomial Polynomial;
    typedef polynomial< C, with<Sparse,DegRevLex> > Polynomial;

    rational_surface(void):  ParametricSurface(), m_w(1) {};
    //  rational_surface(const BoundingBox &) ;

    rational_surface(const Polynomial& X, const Polynomial& Y, const Polynomial& Z, const Polynomial& W=1):
        ParametricSurface() {
        set_range(0,1,0,1);
        m_w=W;
        m_p[0]=X; m_p[1]=Y; m_p[2]=Z;
    }

    Point*     eval(Scalar u, Scalar v) const ;
    void       eval(Point&, Scalar u, Scalar v) const ;
    void       eval(Scalar&, Scalar&, Scalar&, Scalar u, Scalar v) const ;

    void       normal(Point&, Scalar u, Scalar v) const ;
    void       normal(Scalar&, Scalar&, Scalar&, Scalar u, Scalar v) const ;

    Scalar     umin() const;
    Scalar     umax() const;
    Scalar     vmin() const;
    Scalar     vmax() const;

    Point*     operator() (const Scalar& u, const Scalar& v) const ;

    void       set_range(Scalar umin, Scalar umax, Scalar vmin, Scalar vmax) ;
    void       add_to_domain(const Point& p) ;
    Seq<Point> domain(void) const;
    Point      domain(int i) const {return m_domain[i];}

    Polynomial denominator()    const { return m_w; }
    Polynomial numerator(int i) const { return m_p[i]; }


    void       set_denominator(const Polynomial& d)  { m_w=d; }
    void       set_numerator  (const Polynomial& p, int i)  { m_p[i]=p; }

    private:
    std::vector<Point> m_domain;
    Polynomial m_w, m_p[N];
} ;


TMPL typename SELF::Point* 
SELF::eval(Scalar u, Scalar v) const {
    Scalar w = m_w(u,v);
    Point* p = new Point(m_p[0](u,v)/w, m_p[1](u,v)/w, m_p[2](u,v)/w);
    return p ;
}

TMPL void 
SELF::eval(Point& p, Scalar u, Scalar v) const {
    Scalar w=m_w(u,v);
    if (w != 0) {
        p.setx(m_p[0](u,v)/w);
        p.sety(m_p[1](u,v)/w);
        p.setz(m_p[2](u,v)/w);
    } else {
        std::cerr << "Division by 0 in rational_surface::eval(Point& p, Scalar u, Scalar v) const" << std::endl;
    }
}

TMPL void
SELF::normal(Point& p, Scalar u, Scalar v) const {
    normal(p[0],p[1],p[2],u,v);

}
TMPL void
SELF::normal(Scalar& x, Scalar& y, Scalar &z, Scalar u, Scalar v) const {
    Polynomial dw0 = diff(m_w,0) , dw1 = diff(m_w,1),
            dx0 = diff(m_p[0],0),dx1 = diff(m_p[0],1),
            dy0 = diff(m_p[1],0), dy1 = diff(m_p[1],1),
            dz0 = diff(m_p[2],0), dz1 = diff(m_p[2],1);

    Scalar w=m_w(u,v);
    if (w != 0) {

        Scalar x0 = (dx0(u,v)* w - m_p[0](u,v)*dw0(u,v))/(w*w) ;
        Scalar x1 = (dx1(u,v)* w - m_p[0](u,v)*dw1(u,v))/(w*w);
        Scalar y0 = (dy0(u,v)* w - m_p[1](u,v)*dw0(u,v))/(w*w) ;
        Scalar y1 = (dy1(u,v)* w - m_p[1](u,v)*dw1(u,v))/(w*w);
        Scalar z0 = (dz0(u,v)* w - m_p[2](u,v)*dw0(u,v))/(w*w) ;
        Scalar z1 = (dx1(u,v)* w - m_p[2](u,v)*dw1(u,v))/(w*w);

        x= (y0 * z1) - (z0 * y1);
        y= -(x0 * z1) + (z0 * x1);
        z= (x0 * y1) - (y0 * x1);
    } else {
        std::cerr << "Division by 0 in rational_surface::eval(Scalar& x, Scalar& y, Scalar &z, Scalar u, Scalar v const" << std::endl;
    }
    //const scalar<double> temp = x*x + y*y + z*z;
    //scalar<double> norm = sqrt(temp);
    //Scalar temp2 = norm;
    //if (norm != 0) {
    Scalar a = sqrtf(x*x+y*y+z*z);
    if (a != 0) {
    x /= a;
    y /= a;
    z /= a;


   } else{
        std::cerr << "Division by 0 in rational_surface::eval(Scalar& x, Scalar& y, Scalar &z, Scalar u, Scalar v const" << std::endl;
    }

}
TMPL void
SELF::eval(Scalar& x, Scalar& y, Scalar &z, Scalar u, Scalar v) const {
    Scalar w=m_w(u,v);
    if (w != 0) {
        x=m_p[0](u,v)/w;
        y=m_p[1](u,v)/w;
        z=m_p[2](u,v)/w;
    } else {
        std::cerr << "Division by 0 in rational_surface::eval(Scalar& x, Scalar& y, Scalar &z, Scalar u, Scalar v const" << std::endl;
    }
}

TMPL C
SELF::umin(void) const {
    Scalar r=m_domain[0].x();
    for(unsigned i=1; i<m_domain.size();i++)
        r=std::min(r,m_domain[i].x());
    return r;
}

TMPL C
SELF::umax(void) const {
    Scalar r=m_domain[0].x();
    for(unsigned i=1; i<m_domain.size();i++)
        r=std::max(r,m_domain[i].x());
    return r;
}

TMPL C
SELF::vmin(void) const {
    Scalar r=m_domain[0].y();
    for(unsigned i=1; i<m_domain.size();i++)
        r=std::min(r,m_domain[i].y());
    return r;
}

TMPL C
SELF::vmax(void) const {
    Scalar r=m_domain[0].y();
    for(unsigned i=1; i<m_domain.size();i++)
        r=std::max(r,m_domain[i].y());
    return r;
}

TMPL typename SELF::Point* 
SELF::operator() (const Scalar& u, const Scalar& v) const {
    return this->eval(u,v);
}

TMPL void 
SELF::set_range(Scalar umin, Scalar umax, Scalar vmin, Scalar vmax) {
    m_domain.resize(0);
    m_domain.push_back(Point(umin,vmin));
    m_domain.push_back(Point(umax,vmin));
    m_domain.push_back(Point(umin,vmax));
    m_domain.push_back(Point(umax,vmin));
}

TMPL void 
SELF::add_to_domain(const Point& p) {
    this->m_domain.push_back(p);
}


TMPL Seq<typename SELF::Point>
SELF::domain() const {
    return m_domain;
}

template<class MESH, class OBJ> struct as_mesh;


template<class MESH, class C, int N>
struct as_mesh<MESH,SELF> {

    static MESH* convert(const SELF& surf, unsigned m = 50, unsigned n = 50) {

    MESH*  mesh = new MESH;
//    typedef typename SELF::Scalar Scalar;
    typedef typename SELF::Point Point;
//    typedef typename SELF::Polynomial Polynomial;
//    typedef typename MESH::Point MPoint;
    //std::cout<<"domain: "<<surf.domain()<<std::endl;
    //std::cout<<"numer:  "<<surf.numerator(0)<<" "<<surf.numerator(1)<<" "<<surf.numerator(2)<<std::endl;
    //std::cout<<"denom:  "<<surf.denominator()<<std::endl;
    if (surf.domain().size() != 0 && surf.domain().size() != 3 && surf.domain().size() != 4)
        return mesh;

    int c=0;

    // If there is no domain specified, the user wants to work on IRxIR
    Point A1, dA1,
            U, B0, B1, dB0, P(0,0,0);
    if (surf.domain().size() == 0) {
        if (m == 0 || n == 0) {
            std::cerr << "Error in as_mesh: division by 0";
            return mesh;
        }
        double umin = -1.0+10e-6, umax = -umin;

        double ds = (umax - umin) / (double)m;
        double dt = (umax - umin) / (double)n;

        for (double s = umin; s <= umax; s += ds) {
            for (double t = umin; t <= umax; t += dt) {
                surf.eval(P,s/(1-s*s), t/(1-t*t));
                //MPoint* p= new MPoint(P.x(),P.y(),P.z());
                mesh->push_back_vertex(P.x(),P.y(),P.z());
                if (s > umin) {
                    if (t > umin) {
                        mesh->push_back_face(c-n-1, c-1, c);
                    }
                    if (t < umax) {
                        mesh->push_back_face(c-n, c-n-1, c);
                    }
                }
                c++;
            }
        }
    } else {
        Point A0 = surf.domain(0), dA0 = (surf.domain(1)-A0)/m;

        if (surf.domain().size()==3) {
            A1 = surf.domain(2);
            dA1 = dA0;
            B0=A0; B1=A1;
            for (int i=0;i<=(int)m;i++) {
                U=B0;
                Point dB0= (B1-B0)/m;
                for (int j=0;j<=(int)m-i;j++) {
                    surf.eval(P,U.x(),U.y());

                    //MPoint* p= new MPoint(P.x(),P.y(),P.z());
                    mesh->push_back_vertex(P.x(),P.y(),P.z());
                    if (i> 0) {
                        if (j>0) mesh->push_back_face(c-m+i-2, c-1, c);
                        //	  if (j<(int)m-i)
                        mesh->push_back_face(c-m+i-1, c-m+i-2, c);
                    }
                    c++;
                    U=U+dB0;
                }
                B0 = B0 + dA0;
                B1 = B1 + dA0;
            }
        } else if (surf.domain().size()==4) {
            A1 = surf.domain(3);
            dA1 = (surf.domain(2)-A1)/m;
            B0=A0; B1=A1;
            for (unsigned i=0;i<=m;i++) {
                U=B0;
                Point dB0= (B1-B0)/n;
                for (unsigned j=0;j<=n;j++) {
                    surf.eval(P,U.x(),U.y());
                    //MPoint* p= new MPoint(P.x(),P.y(),P.z());
                    mesh->push_back_vertex(P.x(),P.y(),P.z());
                    if (i> 0) {
                        if (j>0) mesh->push_back_face(c-n-1, c-1, c);
                        if (j<n) mesh->push_back_face(c-n, c-n-1, c);
                    }
                    c++;
                    U=U+dB0;
                }
                B0 = B0 + dA0;
                B1 = B1 + dA1;
            }
        }
    }
    //std::cout<< mesh->normal_count()<< std::endl;
}
};
//====================================================================
} /* namespace geoalgebrix */
//====================================================================
} /* namespace mmx */
//====================================================================
# undef TMPL
# undef TMPL1
# undef TMPL_DEF
# undef Scalar
# undef Point
# undef SELF
# undef ParametricSurface
# undef AXEL
# endif // rational_surface_hpp
