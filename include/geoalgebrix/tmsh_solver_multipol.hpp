#pragma once

#include <utility>
//#include <realroot/Interval.hpp>
#include <realroot/polynomial_bernstein.hpp>
#include <realroot/solver_uv_sleeve.hpp>
#include <realroot/bernstein_univariate.hpp>
#include <realroot/solver_mv_bernstein.hpp>
#include <realroot/solver_UvBrnApprox.hpp>
#include <realroot/solver_MvBrnApprox.hpp>

//====================================================================
namespace mmx {
//--------------------------------------------------------------------
#define TMPL template<class CONTROLER>
#define SLVR tmsh_solver_multipol<CONTROLER>

template<class CONTROLER>
class tmsh_solver_multipol {
public:
    typedef CONTROLER                             Controler;
    typedef typename Controler::Cell              Cell;
    typedef typename Controler::Vertex            Vertex;
    typedef polynomial< double, with<Bernstein> > Polynomial;
    typedef std::vector<int>                      Multiplicity;
    typedef solver<UvBrnApprox<double> >          SolverUv;

    tmsh_solver_multipol(Controler* ctrl): d(ctrl) {}

    void edge_point(Cell* cl, const std::vector<Polynomial> &pols);
    void edge_point(Cell* cl, const std::vector<Polynomial> &pols, int v, int s);

    unsigned nb_sol(void)    const { return m_sol.size(); };
    int solution(unsigned i) const { return m_sol[i]; };
    int mu(unsigned i)       const { return m_mul[i]; };
    int curve_no(unsigned i) const { return m_curve_no[i]; };

    void clear(void) { m_sol.resize(0); m_mul.resize(0); m_curve_no.resize(0);}

private:
    Controler* d;
    std::vector<int>   m_sol;
    std::vector<int>   m_mul;
    std::vector<int>   m_curve_no;
};

TMPL
void SLVR::edge_point(Cell* cl, const std::vector<Polynomial> & pols, int v, int s) {

//    mdebug()<<"edge_point"<<pols.size()<<v<<s<<cl->regularity();
    int n0 = cl->vertex_idx(tmsh::cell<2>::Face[v][s][0]),
        n1 = cl->vertex_idx(tmsh::cell<2>::Face[v][s][1]);

    // Instead of representing multiplicities, this represents the curve the
    // solution came from.
    Multiplicity mus;
    Seq<double> sols;
    // Find all solutions for each of the polynomials.
    for (int i = 0; i < pols.size(); ++i) {
        mdebug()<<"pol"<<i<<" : "<< pols[i];
        Polynomial f;
        tensor::face(f,pols[i],v,s);

        Seq<double> temp_sol;
        double orig_sol;

        SolverUv slv;
        SolverUv::Solutions temp_sol_int = slv.solve(f);
        for (int j = 0; j < temp_sol_int.size(); ++j) {
            temp_sol << (lower(temp_sol_int[j])
                         + upper(temp_sol_int[j])) / 2.;
//            mdebug()<<"sol"<<j<<lower(temp_sol_int[j]->domain());
        }
        mdebug()<<i<<temp_sol.size();
        std::vector<double> at(2);

        // Loop over each solution
        for (int j = 0; j < temp_sol.size(); ++j) {

            // Adjust it to the correct coordinates here.
//            mdebug()<<"adjusting from"<<temp_sol[j]<<"to"<<d->adjust_back(temp_sol[j], v, cl);
            orig_sol = temp_sol[j];
            temp_sol[j] = d->adjust_back(temp_sol[j], !v, cl);

            if (v == 1) {
                at[0] = temp_sol[j];
                at[1] = (s==0?d->ymin(*cl):d->ymax(*cl));
            } else {
                at[0] = (s==0?d->xmin(*cl):d->xmax(*cl));
                at[1] = temp_sol[j];
            }
//            double this_val = d->at(pols[i], at, cl);
//            mdebug()<<" at"<<at[0]<<at[1]<<":"<<this_val;
//            mdebug()<<"   between"<<d->xmin(*cl)<<d->xmax(*cl)<<d->ymin(*cl)<<d->ymax(*cl);

            bool min = true;
            // Loop over all the other polynomials
            int k;
            for (k = 0; k < pols.size(); ++k) {
                // Skip this polynomial
                if (k == i) {
                    continue;
                }

                // Get the value at the other polynomial
                double other_val = d->at(pols[k], at, cl);
//                mdebug()<<" at other"<<k<<":"<<other_val;
                // If it's not the minimum value, break
                if (other_val < 0) {
                    min = false;
                    break;
                }
            }

            // If it is the minimum value, add it to the list.
            if (min) {
                sols.push_back(temp_sol[j]);
                mus.push_back(i);
//                mdebug()<<"* positive"<<temp_sol[j]<<this_val<<"orig:"<<orig_sol;
//            } else {
//                mdebug()<<"- negative"<<temp_sol[j]<<this_val<<"vs"<<d->at(pols[k], at, cl);
            }
        }

    }

    int idx;
    if(v==1){
        double y=(s==0?d->ymin(*cl):d->ymax(*cl));
        for (unsigned i=0; i<sols.size(); i++) {
            //lp.push_back(new VERTEX(sol[i],y));
            idx = d->insert_vertex(Vertex(sols[i],y), n0, n1);
            d->vertex(idx).tag(1);
            m_sol.push_back(idx);
            m_mul.push_back(mus[i]);
            //cl->insert_boundary(idx, mu[i]);
//            mdebug()<<"inserted: "<<sols[i]<<y<<n0<<n1<<idx;
        }
    } else {
        double x=(s==0?d->xmin(*cl):d->xmax(*cl));
        for (unsigned i=0; i<sols.size(); i++) {
            //lp.push_back(new VERTEX(x,sol[i]));
            idx = d->insert_vertex(Vertex(x,sols[i]), n0, n1);
            d->vertex(idx).tag(1);
            m_sol.push_back(idx);
            m_mul.push_back(mus[i]);
            //cl->insert_boundary(idx, mu[i]);
//            mdebug()<<"inserted: "<<x<<sols[i]  <<n0<<n1<<idx;
        }
    }
    // mdebug()<<"edge_point"<<sol.size();
}

TMPL
void SLVR::edge_point(Cell* cl, const std::vector<Polynomial>& pols) {
    this->clear();
    edge_point(cl, pols, 1, 0);
    edge_point(cl, pols, 0, 1);
    edge_point(cl, pols, 1, 1);
    edge_point(cl, pols, 0, 0);
}


} /* namespace mmx */
//====================================================================
#undef TMPL
#undef SLVR
