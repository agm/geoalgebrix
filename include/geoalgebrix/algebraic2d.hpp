//--------------------------------------------------------
#pragma once

#include <geoalgebrix/csg_generic.hpp>

#define TMPL template < class C >
#define SELF algebraic2d<C>
//========================================================
namespace mmx {
namespace csg {
//--------------------------------------------------------

template< class C >
struct algebraic2d: public generic {
    typedef polynomial< C, with<Sparse,DegRevLex> > Polynomial;

    algebraic2d(): m_polynomial(), m_outside(0), m_inside(1) {};

    algebraic2d(const Polynomial& pol, int o=0,int i=1)
        : m_polynomial(pol), m_outside(o), m_inside(i) {};

    algebraic2d(std::string s, int o=0,int i=1)
        : m_polynomial(s.c_str()), m_outside(o), m_inside(i) {};

    virtual Range eval(double* cl);

    virtual regularity_t  regularity(double* cl, bool store = true);
    virtual int   tag(double x, double y);
    virtual int   tag(double x, double y, double z);
    virtual void  active(Sequence& seq);

    const Polynomial&      equation(int i=0) const { return this->m_polynomial; }
    Polynomial&            equation(int i=0)       { return this->m_polynomial; }

    unsigned nbequation() const { return 1; }
    void     add_equation(const char* s, const char*var) {
        m_polynomial = Polynomial(s,variables(var)) ;
    }

    Polynomial m_polynomial;
    int m_outside, m_inside;
};

TMPL
Range SELF::eval(double* cl) {
    return m_polynomial(Range(cl[0],cl[1]),Range(cl[2],cl[3]));
}

TMPL
regularity_t SELF::regularity(double* cl, bool store) {
    Range X(cl[0],cl[1]), Y(cl[2],cl[3]);
    Range I = m_polynomial(X,Y);
    //mdebug()<<"eval"<<m_polynomial<<"at"<<X<<Y<<" -> "<<I;
    if (I.lower()*I.upper()>0) {
        //return INSIDE;
        if(I.lower()>0) {
            if(store) this->set_reg(INSIDE);
            //mdebug()<<"algebraic2d  reg:"<<2;
            return INSIDE;
        } else {
            if(m_outside) {
                if(store) this->set_reg(INSIDE);
                return INSIDE;
            } else {
                if(store) this->set_reg(OUTSIDE);
                return OUTSIDE;
            }
        }
    }

    Polynomial dy = diff(m_polynomial,1);
    I = dy(X,Y);
    //mdebug()<<"regular dy "<<I;
    if(I.lower()*I.upper() >= 0) {
        if(store) this->set_reg(BOUNDARY_REGULAR1);
        //mdebug()<<"algebraic2d  reg:"<<3;
        return BOUNDARY_REGULAR1;
    }

    Polynomial dx = diff(m_polynomial,0);
    I = dx(X,Y);
    //mdebug()<<"regular dx "<<I;
    if(I.lower()*I.upper() >= 0) {
        if(store) this->set_reg(BOUNDARY_REGULAR2);
        //mdebug()<<"algebraic2d  reg:"<<4;
        return BOUNDARY_REGULAR2;
    }

    if(store) set_reg(BOUNDARY_SINGULAR);
    //mdebug()<<"algebraic2d  reg:"<<1;
    return BOUNDARY_SINGULAR;
}

TMPL
int SELF::tag(double x, double y) {
    if(m_polynomial(x,y) > 0) {
        //mdebug()<<"tag Algebraic "<< m_inside;
        return m_inside;
    } else {
        //mdebug()<<"tag SELF "<< m_outside;
        return m_outside;
    }
}

TMPL
int SELF::tag(double x, double y, double z) {
    if(m_polynomial(x,y,z) > 0) {
        //mdebug()<<"tag algebraic2d "<< m_inside;
        return m_inside;
    } else {
        //mdebug()<<"tag algebraic2d "<< m_outside;
        return m_outside;
    }
}

TMPL
void SELF::active(Sequence& seq){
    //mdebug()<<"Intersection"<<this->get_reg();
    if(this->get_reg() != OUTSIDE && !ISINSIDE(this->get_reg())) {
        seq.push_back(this);
    }
}

//--------------------------------------------------------------------
} /* namespace csg */
} /* namespace mmx */
//====================================================================
#undef TMPL
#undef SELF
