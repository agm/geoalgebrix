/**********************************************************************
 * PACKAGE  : geoalgebrix
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once 

#include <vector>
#include <utility>
#include <geoalgebrix/regularity.hpp>
#include <geoalgebrix/node.hpp>

//#define XREGULAR 3
//#define YREGULAR 4

#define TMPL  template<class CONTROLER>
#define SELF  polygonizer2d<CONTROLER>

//====================================================================
namespace mmx {
//--------------------------------------------------------------------
template<class CONTROLER >
struct polygonizer2d_order {
    polygonizer2d_order(CONTROLER* d, unsigned v): m_d(d), m_v(v) {}
    bool operator() (int i,int j) { return (m_d->vertex(i)[m_v]<m_d->vertex(j)[m_v]);}
    CONTROLER* m_d;
    unsigned m_v;
};

template<class CONTROLER >
class polygonizer2d {
public:

    typedef CONTROLER                  Controler;
    typedef typename CONTROLER::Cell   Cell;
    typedef std::vector<Cell*>         Input;
    typedef typename Controler::Vertex Point;
    typedef node<Cell>                 Node;
    typedef std::pair<int,int>         Edge;
    typedef std::vector<int>           Face;

    polygonizer2d(Controler* ctrl);

    ~polygonizer2d(void) ;

    void    set_input    (Node* n)   {};
    void    set_regular  (Input* lc) { m_regular = lc; }
    void    set_singular (Input* lc) { m_singular = lc; }

    Input*  regular_cells (void) { return m_regular; }
    Input*  singular_cells(void) { return m_singular; }

    Controler* controler() { return d; }

    void run(void);

    unsigned nbv() const { return d->nbv(); }
    unsigned nbe() const { return m_edge.size(); }
    unsigned nbf() const { return 0; }

    Point vertex(int i) const { return d->vertex(i); }
    Edge  edge(int i)   const { return m_edge[i]; }

    /* Remove all vertices, edges and faces from the polygonizer2d. */
    virtual void clear();

    void add_edge (int i0, int i1) { m_edge.push_back(Edge(i0,i1)); }

//    template<class MESH> void get_output(MESH* out);

private:
    Controler*          d;
    Input*              m_regular;
    Input*              m_singular;

public:
    std::vector< Edge > m_edge;
};
//--------------------------------------------------------------------
TMPL SELF::polygonizer2d(Controler* ctrl):
    d(ctrl)
{ 
    //m_output = new Output;
}
//--------------------------------------------------------------------
TMPL SELF::~polygonizer2d(void) {
    //    delete m_output;
}

//--------------------------------------------------------------------
TMPL void SELF::clear() {

}

//--------------------------------------------------------------------
TMPL void SELF::run() {

//    unsigned c=0;
    foreach(Cell* cl, *this->regular_cells()) {
        // Insert cell boundary points in mesh.
        //mdebug()<<"polygonize regular cell";

        if(cl->boundary().size()==2) {
            this->add_edge(cl->boundary(0),cl->boundary(1));
        } else if(cl->boundary().size()>0) {
            
            if(cl->regularity() == BOUNDARY_REGULAR1) {
                std::sort(cl->boundary().begin(),cl->boundary().end(),
                          polygonizer2d_order<CONTROLER>(d,0));
            } else if(cl->regularity() == BOUNDARY_REGULAR2) {
                std::sort(cl->boundary().begin(),cl->boundary().end(),
                          polygonizer2d_order<CONTROLER>(d,1));
            }
            for(int i=0; i<cl->boundary().size()-1;i+=2) {
                this->add_edge(cl->boundary(i), cl->boundary(i+1));
            }
        }
    }
    
    mdebug()<<"polygonizer2d: regular cells done";

    // First singular point is the center of the cell
    foreach(Cell* cl, *this->singular_cells()) {
        if(cl->has_center()) {
            for(int i=0; i<cl->boundary().size();i++) {
                this->add_edge(cl->center(0), cl->boundary(i));
            }
        }
    }

}

//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================
#undef TMPL1
#undef TMPL
#undef SELF 

