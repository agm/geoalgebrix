#pragma once
#include <geoalgebrix/algebraic2d.hpp>

#define TMPL template< class C >
#define SELF algebraic3d<C>
//--------------------------------------------------------
namespace mmx {
namespace csg {
//--------------------------------------------------------
TMPL
struct algebraic3d: public algebraic2d<C> {
    typedef polynomial< C, with<Sparse,DegRevLex> > Polynomial;

    algebraic3d(): algebraic2d<C>() {}
    algebraic3d(const Polynomial& pol, int o=0,int i=1): algebraic2d<C>(pol,o,i) {}
    algebraic3d(std::string s, int o=0,int i=1): algebraic2d<C>(s,o,i) {}
    algebraic3d(const char * s, const char* var): algebraic2d<C>() {this->add_equation(s, var); }

    virtual Range eval(double* cl);
    virtual regularity_t  regularity(double* cl, bool store = true);
    virtual int   tag(double x, double y, double z);
    virtual void  active(generic::Sequence& seq);

    Polynomial& equation()       { return this->m_polynomial; }
    Polynomial& equation() const { return this->m_polynomial; }

};

TMPL
Range SELF::eval(double* cl) {
    return this->m_polynomial(Range(cl[0],cl[1]),Range(cl[2],cl[3]),Range(cl[4],cl[5]));
}

TMPL
regularity_t SELF::regularity(double* cl, bool store)
{
    Range X(cl[0],cl[1]), Y(cl[2],cl[3]), Z(cl[4],cl[5]);
    Range I = this->m_polynomial(X,Y,Z);
    //mdebug()<<"eval"<<m_polynomial<<"at"<<X<<Y<<Z<<" -> "<<I;
    if (I.lower()*I.upper()>0) {
        //return INSIDE;
        if(I.lower()>0) {
            if(store) this->set_reg(INSIDE);
            //mdebug()<<"Algebraic3 reg:"<<2;
            return INSIDE;
        } else {
            if(this->m_outside) {
                if(store) this->set_reg(INSIDE);
                return INSIDE;
            } else {
                if(store) this->set_reg(OUTSIDE);
                return OUTSIDE;
            }
        }
    }

    int c = 0;
    Polynomial dz = diff(this->m_polynomial,2);
    I = dz(X,Y,Z);
    //mdebug()<<"regular dz "<<I;
    if(I.lower()*I.upper() > 0) {
        c++;
    }

    Polynomial dy = diff(this->m_polynomial,1);
    I = dy(X,Y,Z);
    //mdebug()<<"regular dy "<<I;
    if(I.lower()*I.upper() > 0) c++;


    Polynomial dx = diff(this->m_polynomial,0);
    I = dx(X,Y,Z);
    //mdebug()<<"regular dx "<<I;
    if(I.lower()*I.upper() > 0) c++;


    if (c>1) {
        if(store) this->set_reg(BOUNDARY_REGULAR1);
        //mdebug()<<"Algebraic3 reg:"<<4;
        return BOUNDARY_REGULAR1;
    } else {
        if(store) this->set_reg(UNKNOWN);
        //mdebug()<<"singular";
        return UNKNOWN;
    }
}

TMPL
int SELF::tag(double x, double y, double z) {
    if(this->m_polynomial(x,y,z) > 0) {
        //mdebug()<<"tag Algebraic3"<< m_inside;
        return this->m_inside;
    } else {
        //mdebug()<<"tag Algebraic3"<< m_outside;
        return this->m_outside;
    }
}

TMPL
void SELF::active(generic::Sequence& seq){
    //mdebug()<<"Intersection"<<this->get_reg();
    if(this->get_reg() != OUTSIDE && !ISINSIDE(this->get_reg())) {
        seq.push_back(this);
    }
}

//--------------------------------------------------------------------
} /* namespace csg */
} /* namespace mmx */
//====================================================================
#undef TMPL
#undef SELF
