/**********************************************************************
 * PACKAGE  : geoalgebrix
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once 

#include <vector>
#include <list>
#include <utility>
#include <geoalgebrix/color.hpp>
#include <geoalgebrix/node.hpp>
#include <geoalgebrix/polygonizer2d.hpp>

#define TMPL  template<class CONTROLER>
#define SELF  polygonizer2d_region<CONTROLER>

//====================================================================
namespace mmx {
//--------------------------------------------------------------------

template<class CONTROLER >
class polygonizer2d_region: public polygonizer2d<CONTROLER> {
public:

    typedef CONTROLER                 Controler;
    typedef typename CONTROLER::Cell  Cell;
    typedef std::vector<Cell*>        Input;
    typedef std::pair<int,int>        Edge;
    typedef std::vector<int>          Face;

    polygonizer2d_region(Controler* ctrl);

    ~polygonizer2d_region(void) ;

    void run(void);

    /* Remove all vertices, edges and faces from the polygonizer2d_region. */
    virtual void clear(void);

    //void add_face (const Face& f)  { m_face.push_back(f); }
    void add_face (const Face& f, int t);

    void face_regular  (Cell* cl);
    void face_centered (Cell* cl);
    int  face_tag      (const Face& f);

    template<class MESH> void get_mesh(MESH* out);

    unsigned nbf() const { return m_face.size(); }

    Face  face(int i)   const { return m_face[i]; }

public:
    std::vector< Face > m_face;
    std::vector< int >  m_color;
    std::vector< int >  m_tags;
};
//--------------------------------------------------------------------
TMPL SELF::polygonizer2d_region(Controler* ctrl):
    polygonizer2d<CONTROLER>(ctrl)
{ 
    //m_output = new Output;
}
//--------------------------------------------------------------------
TMPL SELF::~polygonizer2d_region(void) {
    //    delete m_output;
}

//--------------------------------------------------------------------
TMPL void SELF::clear() {

}
//--------------------------------------------------------------------
TMPL void SELF::add_face (const Face& f, int t)  {

    m_face.push_back(f);

    int m = 0;
    bool found = false;
    while (m < m_tags.size() && !found) {
        if(m_tags[m]==t)
            found =true;
        else
            m++;
        //mdebug()<<"  color"<<m_tags[m]<<t;
    }

    if(!found) m_tags.push_back(t);
    m_color.push_back(m);
    //mdebug()<<"  color"<<m;

}

//--------------------------------------------------------------------
/*!
 * \brief SELF::face_regular
 * \param cl
 * \param edge map which associates to a point index, the index of the edge it contains.
 *        the index of edges start from 1.
 * \param nbf
 */
TMPL void SELF::face_regular(Cell* cl) {

    /* if(edge.size()!=cl->m_boundary.size()) {
        mdebug()<<"edge"<<edge.size()<<"boundary"<<cl->m_boundary.size();
        for(unsigned k=0;k< cl->m_boundary.size(); k++)
            std::cout<<cl->m_boundary[k]<<" ";  std::cout<<std::endl;
        for(unsigned k=0;k< cl->m_boundary.size(); k++)
            std::cout<<edge[cl->m_boundary[k]]<<" "; std::cout<<std::endl;
        for(unsigned k=0;k< cl->m_boundary.size(); k++)
            std::cout<<this->controler()->vertex(cl->m_boundary[k]).tag()<<" "; std::cout<<std::endl;

        for(unsigned k=0;k< 4; k++)
            std::cout<<cl->idx(k)<<" "; std::cout<<std::endl;

        std::cout<<this->controler()->vertex(cl->idx(0))[0]<<" "<<this->controler()->vertex(cl->idx(3))[0]
                                                          <<" "<<this->controler()->vertex(cl->idx(0))[1]<<" "<<this->controler()->vertex(cl->idx(3))[1];
        std::cout<<std::endl;
    } */

    if(cl->boundary().size() >0) {

        typedef std::pair<int,int> Pair;
        std::list<Pair> edges;
        for(unsigned k=0; k< cl->boundary().size();k+=2) {
            edges.push_back( Pair(cl->boundary(k), cl->boundary(k+1)) );
            edges.push_back( Pair(cl->boundary(k+1), cl->boundary(k)) );
        }

        int n0, e0, w, n;

        while(edges.size()) {
            //mdebug()<<"size"<<edges.size();
            Face face;
            e0 = edges.front().first;
            n  = edges.front().second;
            n0 = e0;
            face.push_back(n0);
            face.push_back(n);
            edges.pop_front();

            // loop goes around the face until it comes back to n0.
            while(n != n0) {

                face.push_back(n);
                if( this->controler()->vertex(n).tag() == -1 ) {
                    // find the last point on the edges connected to vertex n.
                    bool found = false;
                    for(std::list<Pair>::iterator it = edges.begin(); it != edges.end();) {
                        if(it->first == n && it->second != e0) {
                            e0 = n;
                            n  = it->second;
                            face.push_back(n);
                            it = edges.erase(it);
                            found = true;
                        } else {
                            it++;
                        }
                    }
                    if (!found) {
                        w = this->controler()->side_of(n, *cl);
                        n = this->controler()->neighbor(n, w);
                    }
                } else {
                    n = this->controler()->neighbor(n, w);
                }
            }

            int t = -1;
            for(unsigned j=0; j < face.size(); j++)
                t = std::max(t, this->controler()->vertex(face[j]).tag());

            if (t == -1 ) {
                mdebug()<<"====>> tag regular face "<<-1<<t;
            }

            //mdebug()<<" tag face"<<t;
            if(t>=INSIDE) {
                this->add_face(face,t);
            }
        }
    } else {
        int t = this->controler()->vertex(cl->idx(0)).tag();
        if (t >= INSIDE ) {
            Face face;
            face.push_back(cl->idx(0));
            face.push_back(cl->idx(1));
            face.push_back(cl->idx(3));
            face.push_back(cl->idx(2));
            this->add_face(face,t);
        }
    }


}

TMPL void SELF::face_centered(Cell* cl) {
    mdebug()<<"face_centered";
    assert(cl->has_center());

    std::vector<Face> faces;
    //std::vector<int>  types;

    // Start with the first corner point
    int n, w = 0;
    n = this->controler()->neighbor(cl->idx(0),w);
    //mdebug()<<"boundary"<<n;
    while (this->controler()->vertex(n).tag() != -1 && n != cl->idx(0)) {
        n = this->controler()->neighbor(n,w);
        //mdebug()<<"boundary"<<n;
    }
    //mdebug()<<"---";
    int n0 = n, center = cl->center(0);
    this->controler()->vertex(center).tag(-1);

    Face temp_face;
    temp_face.push_back(center);

    temp_face.push_back(n0);
    n = this->controler()->neighbor(n,w);
    //mdebug()<<"boundary"<<n<<this->controler()->vertex(n).tag();
    while(n != n0) {
        temp_face.push_back(n);
        if(this->controler()->vertex(n).tag() == -1) {
            faces.push_back(temp_face);
            temp_face.clear();
            temp_face.push_back(center);
            temp_face.push_back(n);
        }
        n = this->controler()->neighbor(n,w);
        //mdebug()<<"boundary"<<n<<this->controler()->vertex(n).tag();
    }
    temp_face.push_back(n);
    faces.push_back(temp_face);

    int t;

    for (unsigned i = 0; i < faces.size(); ++i) {
        t = -1;
        for(unsigned j=0; j < faces[i].size(); j++) {
            //mdebug()<<"face"<<i<< faces[i][j];
            t = std::max(t, this->controler()->vertex(faces[i][j]).tag());
        }

        if( t == -1) {
            t = this->controler()->tag_of_middle(cl,faces[i][1],faces[i][2]);
            mdebug()<<"---->> tag face"<<-1<<t;
        }
        //mdebug()<<" tag centered face"<<t;

        if (t != OUTSIDE) {
            this->add_face(faces[i],t);
        }
    }
}
//--------------------------------------------------------------------
TMPL int SELF::face_tag(const Face& f) {
    int tag = 0;
    for(int v = 0; v<f.size(); v++) {
        tag= std::max(tag, this->controler()->vertex(f[v]).tag());
    }
    //std::cout<<"tag "<<tag<<std::endl;
    return tag;
}

//--------------------------------------------------------------------
TMPL void SELF::run() {

    //unsigned c=0;
    foreach(Cell* cl, *this->regular_cells()) {
        // Insert cell boundary points in mesh.
        //mdebug()<<"polygonize regular cell";
        //c=0;
        //std::map<int, int> edge;
        if(cl->boundary().size()==2 && !cl->has_center()) {
            this->add_edge(cl->boundary(0), cl->boundary(1));
            //c++;
            //edge[cl->m_boundary[0]]=c;
            //edge[cl->m_boundary[1]]=c;

            face_regular(cl);

        } else if(cl->boundary().size()>0) {
            if(!cl->has_center()) {
                if(cl->regularity() == BOUNDARY_REGULAR1)
                    std::sort(cl->boundary().begin(),cl->boundary().end(),
                              polygonizer2d_order<CONTROLER>(this->controler(),0));
                else if(cl->regularity() == BOUNDARY_REGULAR2)
                    std::sort(cl->boundary().begin(),cl->boundary().end(),
                              polygonizer2d_order<CONTROLER>(this->controler(),1));

                if(cl->regularity() == BOUNDARY_CENTER)
                    mdebug()<<">>>> Center";
                for(int i=0; i<cl->boundary().size()-1;i+=2) {
                    this->add_edge(cl->boundary(i), cl->boundary(i+1));
                    //c++;
                    //edge[cl->m_boundary[i]]=c;
                    //edge[cl->m_boundary[i+1]]=c;
                }
                face_regular(cl);
            } else {
                if(cl->has_center()) {
                    //edge[cl->center(0)]=1;
                    for(int i=0; i< cl->boundary().size();i++) {
                        this->add_edge(cl->center(0), cl->boundary(i));
                        //c++;
                        //edge[cl->m_boundary[i]]=c;
                    }
                    face_centered(cl);
                }
            }
        } else {
            face_regular(cl);
        }

    }
    
    mdebug()<<"polygonizer2d_region: regular cells done"<<this->regular_cells()->size();

    foreach(Cell* cl, *this->singular_cells()) {
        if(cl->has_center()) {
            // The center of the cell is the singular point.
            //std::map<int, int> edge;
            //c=0;
            //edge[cl->center(0)]=1;
            for(int i=0; i< cl->boundary().size();i++) {
                this->add_edge(cl->center(0), cl->boundary(i));
                //c++;
                //edge[cl->m_boundary[i]]=c;
            }
            face_centered(cl);
        }
    }

    mdebug()<<"polygonizer2d_region: singular cells done"<<this->singular_cells()->size();

}

//--------------------------------------------------------------------
TMPL
template<class MESH>
        void SELF::get_mesh(MESH* out) {
    typedef typename MESH::Point Point;
    typedef typename MESH::Face  Face;

    int N=10, v;
    mmx::color lcolors[10] = {
        mmx::color(255,0,0),
        mmx::color(125,175,225),
        mmx::color(175,125,75),
        mmx::color(0, 175, 50),
        mmx::color(255,128,64),
        mmx::color(255,224,32),
        mmx::color(100,205,255),
        mmx::color(255,155,50),
        mmx::color(175,25,250),
        mmx::color(120,175,25)
    };
    mmx::color black(0,0,0);

    for(unsigned i=0; i<this->controler()->nbv() ; i++) {
        out->add_vertex(new Point(this->controler()->vertex(i)[0], this->controler()->vertex(i)[1]));
        out->add_color(black);
    }

    for(unsigned i=0; i<this->m_edge.size() ; i++)
        out->add_edge(this->m_edge[i].first, this->m_edge[i].second);

    for(unsigned i=0; i<m_face.size() ; i++) {

        Face * f = new Face;

        for(unsigned j=0; j< m_face.at(i).size(); j++) {
            v = m_face.at(i)[j];
            if( out->get_color(v) == black) {
                out->set_color(v, lcolors[m_color[i]%N]);
            } else if (out->get_color(v) != lcolors[m_color[i]%N] ) {

                // Duplicate the vertex for correct coloring.
                out->add_vertex(out->vertex(v)->x(), out->vertex(v)->y(), out->vertex(v)->z());
                out->add_color(lcolors[m_color[i]%N]);
                v = out->nbv()-1;
            }
            f->insert(v);
        }

        out->add_face(f);

    }
}

//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================
#undef TMPL
#undef SELF 

