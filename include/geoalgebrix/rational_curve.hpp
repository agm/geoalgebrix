/*****************************************************************************
 * M a t h e m a g i x
 *****************************************************************************
 * RationalCurve
 * 2010-04-03
 *****************************************************************************
 *               Copyright (C) 2006 INRIA Sophia-Antipolis
 *****************************************************************************
 * Comments :
 ****************************************************************************/
#pragma once

#include <realroot/GMP.hpp>
#include <realroot/ring_monomial_tensor.hpp>
#include <geoalgebrix/mesh.hpp>
#include <geoalgebrix/point.hpp>
#include <geoalgebrix/bounding_box.hpp>
//# include <geoalgebrix/parametric_curve.hpp>

#define TMPL_DEF template<class C>
#define TMPL template<class C>
#define TMPL1 template<class V>
//#define REF     REF_OF(V)
#define SELF rational_curve<C>
#define GRAPHIC mesh<C>
#define AXEL viewer<axel>
//====================================================================
namespace mmx { namespace geoalgebrix {

//struct rational_curve_def {};

///** Default definitions for rational curves.
//     - \c Polynomial : the polynomial type used to represent the numerators and denominator.
//     - \c Range : the type of interval bounds for the parameters.
//**/
//template<class C> struct use<rational_curve_def, C> {
//    typedef C Range;
//    typedef polynomial< C, with<MonomialTensor> > Polynomial;
//};

/** Rational curve.
**/
TMPL_DEF
class rational_curve {
    //    : public parametric_curve<C,V> {
public:
    typedef bounding_box<C>   BoundingBox;
    //  typedef parametric_curve<C,V> ParametricCurve;
    typedef C Scalar;
    typedef point<C> Point;
//    typedef polynomial< C, with<Sparse,DegRevLex> > Polynomial;
    typedef polynomial< C, with<MonomialTensor> > Polynomial;

    rational_curve(void) : m_tmin(0), m_tmax(1) {}

    //  rational_curve(const BoundingBox & box):
    //    ParametricCurve(box), m_tmin(0), m_tmax(1) {}

    rational_curve(const Polynomial& X, const Polynomial& Y, const Polynomial& Z, const Polynomial& W=1)
        : m_tmin(0), m_tmax(1), m_w(W)
    {
        m_p.push_back(X); m_p.push_back(Y); m_p.push_back(Z);
    }

    rational_curve(double m, double M, const Polynomial& X, const Polynomial& Y, const Polynomial& Z, const Polynomial& W=1)
        : m_tmin(m), m_tmax(M), m_w(W)
    {
        m_p.push_back(X); m_p.push_back(Y); m_p.push_back(Z);
    }

    rational_curve(const SELF& c)
        : m_tmin(c.tmin()), m_tmax(c.tmax()),  m_w(c.m_w)
    {
        m_p=c.m_p;
    }

    int dimension() const { return m_p.size();}

    ~rational_curve(void) {};

    double tmin(void) const { return this->m_tmin; }
    double tmax(void) const { return this->m_tmax; }
    void   set_range(double tmin, double tmax);

    Point* eval (const double& t) const;
    Point* operator() (double t);
    void   eval   (double t, double * x, double * y, double * z)  const ;
    void   eval   (Point&,  Scalar ) const;

    //  void   subdivide (ParametricCurve*& a, ParametricCurve*& b, double t ) const ;

    const Polynomial& denominator()    const { return m_w; }
    const Polynomial& numerator(int i) const { return m_p[i]; }

    void       set_denominator(const Polynomial& d)  { m_w=d; }
    void       set_numerator  (const Polynomial& p, int i)  { if (m_p.size() > i) m_p[i]=p; else m_p.push_back(p); }

    Seq<Point *> critical_points(void) ;
    Seq<Point *> extremal_points(void) ;
    Seq<Point *> singular_points(void) ;

private:
    double m_tmin ;
    double m_tmax ;

    Polynomial m_w;
    std::vector<Polynomial> m_p;
};

TMPL typename SELF::Point* 
SELF::eval(const double& t) const {
    Scalar v=as<Scalar>(t);
    double w = as<double>(m_w(v));
    Point* p = new Point(m_p[0](v)/w, m_p[1](v)/w, m_p[2](v)/w);
    return p ;
}

TMPL void 
SELF::eval(Point& v, Scalar p) const {
    Scalar w=m_w(p);
    v.setx(m_p[0](p)/w);
    v.sety(m_p[1](p)/w);
    v.setz(m_p[2](p)/w);
}

TMPL typename SELF::Point* 
SELF::operator() (double t) { return eval(t); }


TMPL void 
SELF::set_range(double tmin, double tmax) {
    m_tmin=tmin;
    m_tmax=tmax;
}

TMPL Seq<typename SELF::Point *>
SELF::critical_points(void) {
    Seq<Point *> l ;
    // ...
    return l ;
}

TMPL Seq<typename SELF::Point *> 
SELF::extremal_points(void) {
    Seq<Point *> l ;
    // ...
    return l ;
}

TMPL Seq<typename SELF::Point *> 
SELF::singular_points(void)
{
    Seq<Point *> l ;
    // ...
    return l ;
}

//TMPL void
//SELF::subdivide(ParametricCurve*& a, ParametricCurve*& b, double t ) const {
//  SELF
//    * ar = new SELF(*this),
//    * br = new SELF(*this);
//  ar->set_range(tmin(),t);
//  br->set_range(t,tmax());

//  a = dynamic_cast<ParametricCurve*>(ar);
//  b = dynamic_cast<ParametricCurve*>(br);

//}

//TMPL struct viewer; struct axel;
//TMPL AXEL&
//operator<<(AXEL& os, const SELF& c) {
//    os<<"\n <curve type=\"rational\"  color=\""<<(int)(255*os.color.r)<<" "<<(int)(255*os.color.g)<<" "<<(int)(255*os.color.b)<<"\">\n";
//    os<<"   <domain>"<< c.tmin()<<" "<<c.tmax()<<"</domain>\n";
//    for(int i=0; i<c.dimension();i++){
//        os<<"   <polynomial>";
//        print(os,c.numerator(i),variables("t"));
//        os<<"</polynomial>\n";
//    }
//    os<<"   <polynomial>";
//    print(os,c.denominator(),variables("t"));
//    os<<"</polynomial>\n";
//    os<<" </curve>\n";
//    return os;
//}

//--------------------------------------------------------------------
TMPL GRAPHIC*
as_mesh(const SELF& c, unsigned N= 100) {
    //  typedef typename mesh<C,V>::Point Point;
    //  typedef typename mesh<C,V>::Edge  Edge;
    typedef C                    Scalar;

    GRAPHIC* r = new GRAPHIC; //(N+1, 2*N,0);

    Scalar t=c.tmin(),dt=(c.tmax()-c.tmin())/N;
    Scalar p[3];
    for(unsigned i=0;i<=N;i++) {
        Scalar w=c.denominator()(t);
        for(unsigned j=0;j<3;j++)
            p[j] = c.numerator(j)(t)/w;
        r->push_back_vertex(p[0], p[1], p[2]);
        t+=dt;
    }

    for(unsigned i=0;i<N;i++) {
        r->add_edge(i,i+1);
    }

    return r;
}

//====================================================================
} ; // namespace geoalgebrix
                //====================================================================
              } ; // namespace mmx
//====================================================================
# undef TMPL
# undef TMPL1
# undef TMPL_DEF
# undef REF
# undef Scalar
# undef Point
# undef SELF
# undef ParametricCurve
# undef AXEL
# undef GRAPHIC
