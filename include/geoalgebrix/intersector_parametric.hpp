#ifndef SHAPE_INTERSECTOR_PARAMETRIC_HPP
#define SHAPE_INTERSECTOR_PARAMETRIC_HPP

# include <geoalgebrix/ssi/ssi_dsearch.hpp>
# include <geoalgebrix/ssi/ssiqtsl.hpp>

#define TMPL template<class C, class V>

namespace mmx { namespace geoalgebrix {

/** Intersection of parametric surfaces.
  This process class computes either
    - the self-intersection curve of a parametric surface when the input arguments are equal, or
    - the intersection curve of two parametric surfaces when the input arguments differ.
  The result is a mesh that contains a sequence of points and edges.
  The input parametric surface should be defined over a rectangular parameter domains.
  The class use<ssi_def,C,V> provides the definitions for
     - the parametric surface type: \c ParametricSurface,
     - The mesh type: \c Mesh,
     - the evaluation function: \c eval.
  **/
template<class C,class V= default_env>
struct intersector_parametric {
    typedef typename use<ssi_def,C,V>::ParametricSurface Input;
    typedef typename use<ssi_def,C,V>::Mesh Output;
    typedef typename use<ssi_def,C,V>::Mesh::Point Point;

    intersector_parametric(): u_spl(200), v_spl(200) {
        m_input1 = NULL;
        m_input2 = NULL;
        m_output = NULL;
    }

    intersector_parametric(Input *s): u_spl(200), v_spl(200), m_input1(s), m_input2(s) {
        m_output = NULL;
    }

    intersector_parametric(Input *s1, Input* s2): u_spl(200), v_spl(200), m_input1(s1), m_input2(s2) {
        m_output = NULL;
    }
    /// Set the number of samples for both parameters.
    void set_sample(int n) { u_spl=n; v_spl=n;}
    /// Set the number of samples for each parameter.
    void set_sample(int n, int m) { u_spl=n; v_spl=m;}

    void set_input (Input* s) { m_input1 = s; m_input2 = s; }
    void set_input (Input* s1, Input* s2) { m_input1 = s1; m_input2 = s2; }
    void set_input1(Input* s) { m_input1 = s; }
    void set_input2(Input* s) { m_input2 = s; }

    Output* output() {return m_output;}

    void run ();

private:
    void run_inter (void);
    void run_self  (void);
    int u_spl, v_spl;
    Input  * m_input1, * m_input2;
    Output * m_output;
};

TMPL void intersector_parametric<C,V>::run(void) {
    if( m_input1 == m_input2 )
        this->run_self();
    else
        this->run_inter();
}
TMPL void intersector_parametric<C,V>::run_self(void) {
    mmx::ssi::dsearch<C,V> r(m_input1,u_spl,v_spl);

    m_output = new Output;

    C x,y,z;
    for ( unsigned i = 0; i < r.nbcurves; i++ )
        for ( unsigned k = 0; k < r.sizes[i]; k++ )
        {
            //std::cout<< r.lefts[i][k][0] <<" "<< r.lefts[i][k][1]<<std::endl;
            m_input1->eval(x,y,z, r.lefts[i][k][0],r.lefts[i][k][1]);
            m_output->push_back_vertex(new Point(x,y,z));
            Point* p= new Point;
            m_input1->eval(p->x(),p->y(),p->z(), r.lefts[i][k][0],r.lefts[i][k][1]);
            //std::cout<<"Eval "<<p->x()<<" "<<p->y()<< " "<<p->z()<<std::endl;
            //output->push_back_vertex(p);

        };

    int np=0;
    for ( unsigned i = 0; i < r.nbcurves; i++ )
        for (unsigned k = 0; k < r.sizes[i]; k+=2 )
        {
            m_output->push_back_edge(np,np+1);
            np+=2;
        };

}

TMPL void intersector_parametric<C,V>::run_inter(void) {
    std::cerr<<"Intersect:"<< u_spl <<std::endl;

    ssiqtsl<C,V> r(m_input1, m_input2, u_spl);

    m_output = new Output;
    int nbv =r.spcs.size();

    for ( int i = 0; i < nbv; i ++) {
        Point* p = new Point(r.spcs[i][0], r.spcs[i][1], r.spcs[i][2]);
        m_output->push_back_vertex(p);
    }

    for ( int i = 0; i < nbv; i+=2) {
        m_output->push_back_edge(i, i+1);
    }

}

//=================================================================
}
              }

#undef TMPL

#endif // SHAPE_INTERSECTOR_PARAMETRIC_HPP
