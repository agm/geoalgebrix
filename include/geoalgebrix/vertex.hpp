/*****************************************************************************
 * M a t h e m a g i x
 *****************************************************************************
 * vertex
 * 2008-03-21
 * Julien Wintz
 *****************************************************************************
 *               Copyright (C) 2006 INRIA Sophia-Antipolis
 *****************************************************************************
 * Comments :
 ****************************************************************************/

# ifndef geoalgebrix_vertex_hpp
# define geoalgebrix_vertex_hpp

# include <geoalgebrix/point.hpp>

# define TMPL_DEF template<class C, int N= 3, class V=default_env>
# define TMPL  template<class C, int N, class V>
# define TMPL1 template<class K>
# define SELF  vertex<C,N,V>
# undef Scalar
# undef Point
# define IDX -1
//====================================================================
namespace mmx { namespace geoalgebrix {
//====================================================================

TMPL_DEF class vertex; // TMPL_DEF struct vertex;

struct vertex_def {};

TMPL
class vertex : public point<C,N,V>
{
public:
  typedef C Scalar;
  typedef point<C,N,V> Point;

  vertex(void) ;
  vertex(Scalar x, Scalar y) ;
  vertex(Scalar x, Scalar y, Scalar z, int idx = IDX) ;
  vertex(const Point & p) ;
  vertex(const SELF& p) ;
  
  bool     operator == (const SELF & other) const ;
  bool     operator != (const SELF & other) const ;
  SELF & operator  = (const SELF & other) ;

  // Scalar   operator [] (const int & i) const ;
  //  Scalar & operator [] (const int & i) ;
  //private:
  int index() const     {return m_idx;}
  //public:
  int set_index(int i)  {m_idx=i;return m_idx;}

  int nface() const     {return m_nf;}

private:
  int  m_idx ;
  int  m_nf ;
};

TMPL
SELF::vertex(void) : Point(), m_idx(IDX), m_nf(-1)
{
}

TMPL
SELF::vertex(Scalar x, Scalar y) :Point(x,y,0), m_idx(IDX), m_nf(-1)
{
}

TMPL
SELF::vertex(Scalar x, Scalar y, Scalar z, int idx) :Point(x,y,z), m_idx(IDX), m_nf(idx)
{
}

TMPL
SELF::vertex(const Point& p) :Point(p.x(),p.y(),p.z()), m_idx(IDX), m_nf(IDX)
{
}

TMPL
SELF::vertex(const SELF & other) : Point(other), m_idx(other.index()),  m_nf(other.nface())
{
}

TMPL
SELF & SELF::operator = (const SELF & other) 
{
  if(this == &other)
    return *this ;
  
  this->x() = other.x() ;
  this->y() = other.y() ;
  this->z() = other.z() ;
  
  return * this ;
}

TMPL bool 
SELF::operator == (const SELF & other) const {
  return ((this->x() == other.x()) && (this->y() == other.y()) && (this->z() == other.z())) ;
}

TMPL bool 
SELF::operator != (const SELF & other) const {
  return ((this->x() != other.x()) || (this->y() != other.y()) || (this->z() != other.z())) ;
}

// TMPL typename mmx_t<scalar_of,K>::type & 
// SELF::operator [] (const int & i) {
//   switch(i) {
//   case 0:
//     return this->x() ; 
//     break ;
//   case 1:
//     return this->y() ;
// 	break ;
//   case 2:
//     return this->z() ;
//     break ;
//   default:
// 	break ;
//   }
  
//   return *(new Scalar(0)) ;
// }

// TMPL typename SELF::Scalar
// SELF::operator [] (const int & i) const {
//   switch(i) {
//   case 0:
//     return this->x() ; 
//     break ;
//   case 1:
//     return this->y() ;
// 	break ;
//   case 2:
//     return this->z() ;
//     break ;
//     default:
//       break ;
//   }
  
//   return (Scalar)0 ;
// }

TMPL inline typename SELF::Scalar read (const SELF& v, unsigned i) { return v[i]; }

//====================================================================
template<class V> struct use<vertex_def,V>
  : public use<geoalgebrix_def,V>
{
  typedef vertex<double> Vertex;
  typedef vertex<double> Point;

  static inline void point_insertor(Seq<Vertex*>& vertices, Vertex *p) {
    //    std::cout<<p->x()<<" "<<p->y()<<" "<<p->z()<<" "<<p->index()<< " ";
    if(p->index()<0) {
      p->set_index(vertices.size());
      vertices << p;
    }
    //      std::cout<<p->index()<<std::endl;
  }
};

//====================================================================
} ; // namespace geoalgebrix
  //--------------------------------------------------------------------

template<class OSTREAM, class K> OSTREAM&
operator<<(OSTREAM& os, const geoalgebrix::vertex<K>& p) {
  os <<p.x()<<" "<<p.y()<<" "<<p.z();
  return os;
}
  //====================================================================
} ; // namespace mmx
//====================================================================
# undef TMPL_DEF
# undef TMPL
# undef TMPL1
# undef SELF
# undef IDX
# endif // geoalgebrix_vertex_hpp
