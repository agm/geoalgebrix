/**********************************************************************
 * PACKAGE  : geoalebrix
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once

#include <vector>
#include <utility>
#include <geoalgebrix/mdebug.hpp>
#include <geoalgebrix/node.hpp>
#include <geoalgebrix/polygonizer3d.hpp>
#include <cmath>
#include <geoalgebrix/tmsh_cell.hpp>
#include <geoalgebrix/point.hpp>


#define TMPL  template<class CONTROLER>
#define SELF  polygonizer3d_cms<CONTROLER>
#define LINE(a,b) std::cout<<std::endl<<"<line name=\"l"<<a<<"-"<<b<<"\"><point>"\
    <<this->controler()->vertex(a)[0] \
    <<this->controler()->vertex(a)[1] \
    <<this->controler()->vertex(a)[2] \
    <<"</point> <point>"\
    <<this->controler()->vertex(b)[0] \
    <<this->controler()->vertex(b)[1] \
    <<this->controler()->vertex(b)[2] \
    <<"</point></line>"

//====================================================================
namespace mmx {
//--------------------------------------------------------------------

template<class CONTROLER >
class polygonizer3d_cms: public polygonizer3d<CONTROLER> {
public:

    typedef CONTROLER                 Controler;
    typedef typename CONTROLER::Cell  Cell;
    typedef node<Cell>                Node;
    typedef std::pair<int,int>        Edge;
    typedef std::vector<int>          Face;
    typedef point<double,3> Point;

    polygonizer3d_cms(void);
    polygonizer3d_cms(Controler* ctrl);

    ~polygonizer3d_cms(void) ;

    /* Remove all vertices, edges and faces from the polygonizer3d. */
    virtual void clear();

    void set_input(Node *n) { m_input= n; }

    bool is_active(Node*);
    bool is_adjacent(Node*, Node*);

    double xmin(Node *n) {return n->dim[0]; }
    double xmax(Node *n) {return n->dim[1]; }
    double ymin(Node *n) {return n->dim[2]; }
    double ymax(Node *n) {return n->dim[3]; }
    double zmin(Node *n) {return n->dim[4]; }
    double zmax(Node *n) {return n->dim[5]; }

    void insert_edge(int v0, int v1, Node* n0, Node* n1);
    void get_dual_vertex(Node*);
    void get_dual_edge  (Node*, Node*);
    void get_corners_of_face(Node* n1, Node* n2);
    unsigned face_of(Node* n1, Node* n2);

    void edges_of_face(Node* n1, Node* n2, int c);
    void faces_of_cell(const std::vector< std::pair<int,int> >& edges);
    void edges_of_cell(Cell *cl);

    void process_dual_edge(Node* n1, Node* n2);

    void run(void);

    void connection(int v);

    int  solve_for_regularity(int A,int B,int C,int D,unsigned z);
    void process_dual_edge_new(Node* n1,Node* n2);

    void singular_faces(const std::vector< std::pair<int,int> >& edges,int n);

    std::vector<int> calculating_signed_indices(std::vector< int > partial_sign,std::vector<int> rows,int regularity,unsigned z);
    int used_points(std::vector<int> usedpoints);

    void connection_algo(Node* n1,Node* n2,int regularity,std::vector<int> points,std::vector<int> indices,std::vector<int> rows,std::vector<double> distances,std::vector<int> used);

    Node* m_input;

private:
    int m_corners[4];
    int m_dir;
    int m_edgepts[4];

};
//--------------------------------------------------------------------
TMPL SELF::polygonizer3d_cms(void): polygonizer3d<CONTROLER>(new Controler)
{

}

//--------------------------------------------------------------------
TMPL SELF::polygonizer3d_cms(Controler* ctrl): polygonizer3d<CONTROLER>(ctrl)
{

}

//--------------------------------------------------------------------
TMPL SELF::~polygonizer3d_cms(void)
{

}

//--------------------------------------------------------------------
TMPL void SELF::clear() {

}
//--------------------------------------------------------------------
TMPL
bool SELF::is_active (Node* n) {
    return (n->cell()->regularity() != 0);
}
//--------------------------------------------------------------------
TMPL
bool SELF::is_adjacent (Node* n1, Node* n2) {

    //std::cout<<std::endl<<"is adjacent";

    if(this->xmax(n1) < this->xmin(n2)) return false;
    if(this->xmax(n2) < this->xmin(n1)) return false;

    if(this->ymax(n1) < this->ymin(n2)) return false;
    if(this->ymax(n2) < this->ymin(n1)) return false;

    if(this->zmax(n1) < this->zmin(n2)) return false;
    if(this->zmax(n2) < this->zmin(n1)) return false;

    double xmin(std::max(this->xmin(n1),this->xmin(n2)));
    double xmax(std::min(this->xmax(n1),this->xmax(n2)));
    double ymin(std::max(this->ymin(n1),this->ymin(n2))) ;
    double ymax(std::min(this->ymax(n1),this->ymax(n2))) ;
    double zmin(std::max(this->zmin(n1),this->zmin(n2))) ;
    double zmax(std::min(this->zmax(n1),this->zmax(n2))) ;

    int c = 0;
    if(xmin > xmax) return false; else if (xmin==xmax) c++;
    if(ymin > ymax) return false; else if (ymin==ymax) c++;
    if(zmin > zmax) return false; else if (zmin==zmax) c++;

    return (c<2);
}

//--------------------------------------------------------------------
TMPL void SELF::get_dual_vertex(Node* n) {
    if(!is_active(n)) return;

    if(!n->is_leaf()) {
        this->get_dual_vertex(n->left());
        this->get_dual_vertex(n->right());
        get_dual_edge(n->left(), n->right());
        return;
    }
    //_vertex(this, n);
}

//--------------------------------------------------------------------
TMPL void SELF::get_dual_edge(Node* n1, Node* n2) {

    if(!is_adjacent(n1,n2)) {
        //std::cout<<std::endl<<"dual_edge not adjacent";
        return;
    }

    if(!is_active(n1) && !is_active(n2)) return;

    if(!n1->is_leaf()) {
        if (is_adjacent(n1->left(), n2))  get_dual_edge(n1->left(),  n2);
        if (is_adjacent(n1->right(),n2))  get_dual_edge(n1->right(), n2);
        //get_dual_face(n1->right(), n1->left(), n2, n2);
        return;
    }

    if(!n2->is_leaf()){
        if (is_adjacent(n1,n2->left()))   get_dual_edge(n1,n2->left());
        if (is_adjacent(n1,n2->right()))  get_dual_edge(n1,n2->right());
        //get_dual_face(n1, n1, n2->left(), n2->right());
        return;
    }
    // std::cout<<std::endl<<"aidaidaji";

#ifdef NEW
    this->process_dual_edge_new(n1,n2);
#else
    this->process_dual_edge(n1,n2);
#endif
}

//--------------------------------------------------------------------
TMPL
/**
 * @brief insert_edge
 * @param v0 index of first vertex
 * @param v1 index of second vertex
 * @param n1 node of first cell
 * @param n2 node of second cell
 *
 * Insert the edge [v0,v1] in the two cells of n0, n1.
 */
void SELF::insert_edge(int v0, int v1, Node* n1, Node* n2) {

    //if(n2->cell())
        n2->cell()->insert_edge(v0,v1);

    //if(n1->cell())
        n1->cell()->insert_edge(v0,v1);

}

//--------------------------------------------------------------------
TMPL
void SELF::edges_of_cell(Cell *cl) {

    for(unsigned i=0;i<cl->m_edges.size();i++)
        this->add_edge(cl->m_edges[i].first, cl->m_edges[i].second);
}
//--------------------------------------------------------------------
TMPL
/**
 **brief process_dual_edge
 **param n1 first node
 **param n2 second node
 *
 * Process the face between the two cells of the two nodes.
 */
void SELF::process_dual_edge(Node* n1, Node* n2) {

    unsigned F = face_of(n1,n2), f = F/2;

    Cell * cl;
    if(F%2) {
        for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[f][i]);
        cl = n2->cell();
    } else {
        for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[f][i]);
        cl = n1->cell();
    }
    this->controler()->edge_point(cl,f);

    unsigned c = 0;
    if( this->controler()->vertex(m_corners[0]).tag() != this->controler()->vertex(m_corners[1]).tag()) {
        m_edgepts[0]=this->controler()->find_edge_point(m_corners[0],m_corners[1],
                tmsh::cell<3>::FaceDir[f][0]);
        c |= 1;
    }

    if( this->controler()->vertex(m_corners[0]).tag() != this->controler()->vertex(m_corners[2]).tag()) {
        m_edgepts[3]=this->controler()->find_edge_point(m_corners[0],m_corners[2],
                tmsh::cell<3>::FaceDir[f][1]);
        c |= 8;
    }

    if( this->controler()->vertex(m_corners[2]).tag() != this->controler()->vertex(m_corners[3]).tag()) {
        m_edgepts[2]=this->controler()->find_edge_point(m_corners[2],m_corners[3],
                tmsh::cell<3>::FaceDir[f][0]);
        c |= 4;
    }

    if( this->controler()->vertex(m_corners[1]).tag() != this->controler()->vertex(m_corners[3]).tag()) {
        m_edgepts[1]=this->controler()->find_edge_point(m_corners[1],m_corners[3],
                tmsh::cell<3>::FaceDir[f][1]);
        c |= 2;
    }

    this->edges_of_face(n1,n2,c);
}

TMPL
/**
 * @brief polygonizer3d_cms<CONTROLER>::process_dual_edge_new
 * @param n1 Node 1
 * @param n2 Node 2
 * Process the face between the two cells of the two nodes.
 */
void SELF::process_dual_edge_new(Node* n1,Node* n2)
{
    unsigned F = face_of(n1,n2), f = F/2;

    Cell * cl;
    if(F%2) {
        for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[f][i]);
        cl = n2->cell();
    } else {
        for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[f][i]);
        cl = n1->cell();
    }
    this->controler()->edge_point(cl,f);
    std::vector<std::vector<int>> edgepts;


    for(unsigned i=0;i<4;i++)
    {

        int x=m_corners[i%3];
        int y=m_corners[(i%3)+1+(i%2)];
        int v = tmsh::cell<3>::FaceDir[f][i%2];
        std::vector<int> temp;
        temp=this->controler()->single_edgepts(x,y,v);
        edgepts.push_back(temp);
    }

    int regularity;
    if(m_corners[0]!=-1 && m_corners[1]!=-1 && m_corners[2]!=-1 && m_corners[3]!=-1)
    { regularity=this->solve_for_regularity(m_corners[0],m_corners[1],m_corners[2],m_corners[3],f);}
    else
    {
        regularity=-1;
    }
    mdebug()<<regularity<<edgepts.size();


    std::vector<int> circular_points,circular_rows,circular_indices;std::vector<double> circular_distances;
    std::vector<int> used1;

    for(unsigned j=0;j<edgepts[3].size();j++)
    {
        if(edgepts[3][0]!=-1)
        {

            circular_points.push_back(edgepts[3][j]);
            if(edgepts[3][j]==m_corners[0]||edgepts[3][j]==m_corners[1]||edgepts[3][j]==m_corners[2]||edgepts[3][j]==m_corners[3])
            {
                circular_rows.push_back(4);
            }
            else
            {
                circular_rows.push_back(3);
            }
            used1.push_back(0);
        }
    }

    for(unsigned j=0;j<edgepts[2].size();j++)
    {
        if(edgepts[2][0]!=-1)
        {

            circular_points.push_back(edgepts[2][j]);

            if(edgepts[2][j]==m_corners[0]||edgepts[2][j]==m_corners[1]|edgepts[2][j]==m_corners[2]||edgepts[2][j]==m_corners[3])
            {
                circular_rows.push_back(4);
            }
            else
            {
                circular_rows.push_back(2);
            }
            used1.push_back(0);
        }
    }
    for(unsigned j=0;j<edgepts[1].size();j++)
    {
        if(edgepts[1][0]!=-1)
        {

            circular_points.push_back(edgepts[1][edgepts[1].size()-j-1]);

            if(edgepts[1][edgepts[1].size()-j-1]==m_corners[0]||edgepts[1][edgepts[1].size()-j-1]==m_corners[1]||edgepts[1][edgepts[1].size()-j-1]==m_corners[2]||edgepts[1][edgepts[1].size()-j-1]==m_corners[3])
            {
                circular_rows.push_back(4);
            }
            else
            {
                circular_rows.push_back(1);
            }
            used1.push_back(0);
        }
    }
    for(unsigned j=0;j<edgepts[0].size();j++)
    {
        if(edgepts[0][0]!=-1)
        {

            circular_points.push_back(edgepts[0][edgepts[0].size()-j-1]);

            if(edgepts[0][edgepts[0].size()-j-1]==m_corners[0]||edgepts[0][edgepts[0].size()-j-1]==m_corners[1]||edgepts[0][edgepts[0].size()-j-1]==m_corners[2]||edgepts[0][edgepts[0].size()-j-1]==m_corners[3])
            {
                circular_rows.push_back(4);
            }
            else
            {
                circular_rows.push_back(0);
            }
            used1.push_back(0);
        }
    }

    int repeat=-1;
    int k=0;


    while(k<circular_points.size())
    {

        if(circular_points[k]==repeat)
        {


            circular_rows.erase(circular_rows.begin()+k);
            circular_points.erase(circular_points.begin()+k);
            used1.erase(used1.begin()+k);
            k--;

        }

        repeat=circular_points[k];
        k++;
    }
    if(circular_points.size()>1)
    {
        if(circular_points[0]==circular_points[circular_points.size()-1])
        {
            circular_rows.erase(circular_rows.begin()+(circular_rows.size()-1));
            circular_points.erase(circular_points.begin()+(circular_points.size()-1));
            used1.erase(used1.begin()+(used1.size()-1));
        }
    }



    if(regularity==-1)
    {
        circular_indices.push_back(0);
        circular_distances.push_back(0);
        this->connection_algo(n1,n2,regularity,circular_points,circular_indices,circular_rows,circular_distances,used1);

    }
    else if(circular_points.size()>2)
    {

        std::vector<int> partial;
        partial=this->controler()->calculate_partial(circular_points,f,regularity,m_corners[0],m_corners[1],m_corners[2],m_corners[3]);
        circular_indices=this->calculating_signed_indices(partial,circular_rows,regularity,f);
        circular_distances=this->controler()->calculate_distances(circular_points,regularity);

        for(unsigned i=0;i<circular_distances.size();i++)
        {
            mdebug()<<circular_distances[i];
        }

        this->connection_algo(n1,n2,regularity,circular_points,circular_indices,circular_rows,circular_distances,used1);

    }
    else
    {
        circular_indices.push_back(0);
        circular_distances.push_back(0);

        this->connection_algo(n1,n2,regularity,circular_points,circular_indices,circular_rows,circular_distances,used1);

    }

}


TMPL
/**
 * @brief polygonizer3d_cms<CONTROLER>::solve_for_regularity
 * @param A corner 0
 * @param B corner 1
 * @param C corner 2
 * @param D corner 3
 * @param z face constant
 * @return regularity of the face
 */
int SELF::solve_for_regularity(int A,int B,int C,int D,unsigned z)
{

    for(unsigned i=0;i<=1;i++)
    {
        int v=tmsh::cell<3>::FaceDir[z][i%2];

        int return_value=this->controler()->regularity_direction(v,A,B,C,D,i);

        if(return_value==1)
        {
            return tmsh::cell<3>::FaceDir[z][(i+1)%2];
        }
        else if(return_value==2)
        {

            return v;
        }

    }

    return -1;

}


TMPL
/**
 * @brief polygonizer3d_cms<CONTROLER>::calculating_signed_indices
 * @param partial_sign partial indices of the points inserted in the face
 * @param rows row numbers of the points inserted in the cell in the order as 0,1,2,3 in anticlockwise starting from the bottom row
 * @param regularity regularity of the face
 * @param z face constant
 * @return the final signed indices of the points inserted in the face and is called only when the number of points are greater than 2
 */
std::vector<int> SELF::calculating_signed_indices(std::vector<int>  partial_sign,std::vector<int> rows,int regularity,unsigned z)
{

    std::vector<int> indices;



    if(rows.size()==partial_sign.size())
    {for (unsigned j=0;j<partial_sign.size();j++)
        {


            if(rows[j]==4)
            {
                indices.push_back(partial_sign[j]);
            }
            else if(regularity==0||(regularity==1 && tmsh::cell<3>::FaceDir[z][0]==1))
            {
                if(((rows[j])%2)==0)
                {
                    indices.push_back((partial_sign[j])*(std::pow(-1,rows[j]/2)));
                }
                else
                {
                    indices.push_back((std::pow(-1,(rows[j]/2)+1)));
                }

            }
            else if(regularity==2||(regularity==1 && tmsh::cell<3>::FaceDir[z][0]==0))
            {

                if(((rows[j])%2)==0)
                {
                    indices.push_back((std::pow(-1,rows[j]/2)));
                }
                else
                {
                    indices.push_back((partial_sign[j])*(std::pow(-1,(rows[j]/2)+1)));
                }

            }
        }}
    else
    {
        for(unsigned i=0;i<rows.size();i++)
        {
            indices.push_back(0);
        }

    }


    return indices;


}


TMPL
/**
 * @brief polygonizer3d_cms<CONTROLER>::used_points
 * @param usedpoints vector storing the status of the points in the connection that whether they have been used or not
 * @return the number of non used points in the circular list
 */
int SELF:: used_points(std::vector<int> usedpoints)
{
    int a=0;
    for (unsigned i=0;i<usedpoints.size();i++)
    {
        if(usedpoints[i]==0)
        {
            a++;
        }
    }
    return a;
}


TMPL
/**
 * @brief polygonizer3d_cms<CONTROLER>::connection_algo
 * @param n1 Node 1
 * @param n2 Node 2
 * @param regularity regularity of the face
 * @param points circular list of points inserted in the face
 * @param indices circular list of indices of points inserted in the face
 * @param rows circular list of rows of points inserted in the face
 * @param distances circular list of coordinates of points(according to regularity) inserted in the face
 * @param used circular list of status of points inserted in the face
 *
 * Does the connection for a particular face
 */
void SELF::connection_algo(Node* n1, Node* n2, int regularity,
                           std::vector<int> points,
                           std::vector<int> indices,
                           std::vector<int> rows,
                           std::vector<double> distances,
                           std::vector<int> used)
{

    if(regularity==0||regularity==1||regularity==2)
    {
        if(points.size()==2)
        {
            this->insert_edge(points[0],points[1],n1,n2);
        }
        else if(points.size()>2)
        {
            if(points.size()==rows.size() && rows.size()==indices.size() && indices.size()==distances.size())
            {
                for(unsigned i=0;i<points.size();i++)
                {
                    if(indices[i]==0) {
                        used[i]=1;
                    }
                }

                int x=this->used_points(used);

                while(x>1)
                {
                    if(points.size()==2)
                    {
                        this->insert_edge(points[0],points[1],n1,n2);
                        points.erase(points.begin());indices.erase(indices.begin());
                        points.erase(points.begin());indices.erase(indices.begin());
                    }
                    else
                    {
                        int i=0;

                        int tempor=0;
                        int dist;

                        for(unsigned k=0;k<indices.size();k++)
                        {
                            if((indices[k]==-1||indices[k]==-2) && used[k]==0)
                            {
                                if(tempor==0)
                                {
                                    dist=distances[k];
                                    tempor++;
                                    i=k;

                                }
                                else
                                {
                                    if(distances[k]<dist)
                                    {
                                        i=k;
                                        dist=indices[k];
                                    }
                                }
                            }
                        }

                        int j=i;
                        int l,m;
                        if(j==0)
                        {
                            l=points.size()-1;
                        }
                        else
                        {
                            l=j-1;
                        }
                        while(used[l]!=0 || l==j)
                        {
                            if(l==0)
                            {
                                l=points.size()-1;
                            }
                            else
                            {
                                l--;
                            }
                        }
                        m=(j+1)%points.size();
                        while(used[m]!=0 || m==j)
                        {
                            m=(m+1)%points.size();
                        }

                        if(distances[l]<distances[m])
                        {
                            if(j!=l)
                            {
                                j=l;
                            }
                            else
                            {
                                j=m;
                            }
                        }
                        else
                        {
                            if(j!=m)
                            {
                                j=m;
                            }
                            else
                            {
                                j=m;
                            }
                        }
                        int y=points[i];
                        int z=points[j];
                        this->insert_edge(y,z,n1,n2);

                        if(indices[i]==-2)
                        {
                            indices[i]=1;
                            if(indices[j]==2)
                            {
                                indices[j]=-1;
                            }
                            else
                            {
                                used[j]=1;
                                j=0;i=0;
                            }
                        }
                        else if(indices[i]==2)
                        {
                            indices[i]=-1;
                            if(indices[j]==-2)
                            {
                                indices[j]=1;
                            }
                            else
                            {
                                used[j]=1;j=0;i=0;
                            }
                        }
                        else  if(indices[j]==-2)
                        {
                            indices[j]=1;

                            used[i]=1;
                            j=0;i=0;

                        }
                        else if(indices[j]==2)
                        {
                            indices[j]=-1;

                            used[i]=1;
                            j=0;i=0;

                        }
                        else
                        {
                            used[j]=1;used[i]=1;i=0;j=0;
                        }
                    }
                    x=this->used_points(used);}
            }

        }}
    else
    {
        for (unsigned i=0;i<points.size();i++)
        {
            int singular_po=this->controler()->singular_point();
            this->insert_edge(singular_po,points[i],n1,n2);
        }
    }

}


//--------------------------------------------------------------------
TMPL
/**
 * @brief get_corners_of_face
 * @param n1 first node
 * @param n2 second node
 *
 * Store the index of the corners of the face between the cells of n1 and n2
 * in the array m_corners.
 */
void SELF::get_corners_of_face(Node* n1, Node* n2) {

    if(this->xmax(n1) == this->xmin(n2)) {
        m_dir = 0;
        if(this->ymin(n1) <= this->ymin(n2) && this->ymax(n1) >= this->ymax(n2)
                && this->zmin(n1) <= this->zmin(n2) && this->zmax(n1) >= this->zmax(n2))
            for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[0][i]);
        else
            for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[1][i]);
    } else if(this->xmax(n2) == this->xmin(n1)) {
        m_dir = 0;
        if(this->ymin(n1) <= this->ymin(n2) && this->ymax(n1) >= this->ymax(n2)
                && this->zmin(n1) <= this->zmin(n2) && this->zmax(n1) >= this->zmax(n2))
            for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[1][i]);
        else
            for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[0][i]);
    } else if(this->ymax(n1) == this->ymin(n2)) {
        m_dir = 1;
        if( (this->xmin(n1) <= this->xmin(n2)) && (this->xmax(n2) <= this->xmax(n1))
                && (this->zmin(n1) <= this->zmin(n2)) && (this->zmax(n2) <= this->zmax(n1))) {
            for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[2][i]);
        } else {
            for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[3][i]);
        }
    } else if(this->ymax(n2) == this->ymin(n1)) {
        m_dir = 1;
        if(this->xmin(n1) <= this->xmin(n2) && this->xmax(n1) >= this->xmax(n2)
                && this->zmin(n1) <= this->zmin(n2) && this->zmax(n1) >= this->zmax(n2))
            for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[3][i]);
        else
            for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[2][i]);
    } else if(this->zmax(n1) == this->zmin(n2)) {
        m_dir = 2;
        if(this->xmin(n1) <= this->xmin(n2) && this->xmax(n1) >= this->xmax(n2)
                && this->ymin(n1) <= this->ymin(n2) && this->ymax(n1) >= this->ymax(n2))
            for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[4][i]);
        else
            for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[5][i]);
    } else if(this->zmax(n2) == this->zmin(n1)) {
        m_dir = 2;
        if(this->xmin(n1) <= this->xmin(n2) && this->xmax(n1) >= this->xmax(n2)
                && this->ymin(n1) <= this->ymin(n2) && this->ymax(n1) >= this->ymax(n2))
            for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[5][i]);
        else
            for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[4][i]);
    }

}
//--------------------------------------------------------------------
TMPL
/**
 * @brief face_of
 * @param n1 first node
 * @param n2 second node
 *
 * common face of the cells of the two nodes.
 *
 * return 2*f+s with
 *   - f the index of the face
 *   - s = 0 if it is the face of n1 and s=1 if it is the face of n2
 */
unsigned SELF::face_of(Node* n1, Node* n2) {

    if(this->xmax(n1) == this->xmin(n2)) {
        //m_dir = 0;
        if(this->ymin(n1) <= this->ymin(n2) && this->ymax(n1) >= this->ymax(n2)
                && this->zmin(n1) <= this->zmin(n2) && this->zmax(n1) >= this->zmax(n2))
            return 1;
        //for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[0][i]);
        else
            return 2;
        //for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[1][i]);
    } else if(this->xmax(n2) == this->xmin(n1)) {
        //m_dir = 0;
        if(this->ymin(n1) <= this->ymin(n2) && this->ymax(n1) >= this->ymax(n2)
                && this->zmin(n1) <= this->zmin(n2) && this->zmax(n1) >= this->zmax(n2))
            return 3;
        //for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[1][i]);
        else
            return 0;
        //for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[0][i]);
    } else if(this->ymax(n1) == this->ymin(n2)) {
        //m_dir = 1;
        if( (this->xmin(n1) <= this->xmin(n2)) && (this->xmax(n2) <= this->xmax(n1))
                && (this->zmin(n1) <= this->zmin(n2)) && (this->zmax(n2) <= this->zmax(n1))) {
            return 5;
            //for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[2][i]);
        } else {
            return 6;
            //for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[3][i]);
        }
    } else if(this->ymax(n2) == this->ymin(n1)) {
        //m_dir = 1;
        if(this->xmin(n1) <= this->xmin(n2) && this->xmax(n1) >= this->xmax(n2)
                && this->zmin(n1) <= this->zmin(n2) && this->zmax(n1) >= this->zmax(n2))
            return 7;
        //for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[3][i]);
        else
            return 4;
        //for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[2][i]);
    } else if(this->zmax(n1) == this->zmin(n2)) {
        //m_dir = 2;
        if(this->xmin(n1) <= this->xmin(n2) && this->xmax(n1) >= this->xmax(n2)
                && this->ymin(n1) <= this->ymin(n2) && this->ymax(n1) >= this->ymax(n2))
            return 9;
        //for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[4][i]);
        else
            return 10;
        //for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[5][i]);
    } else if(this->zmax(n2) == this->zmin(n1)) {
        //m_dir = 2;
        if(this->xmin(n1) <= this->xmin(n2) && this->xmax(n1) >= this->xmax(n2)
                && this->ymin(n1) <= this->ymin(n2) && this->ymax(n1) >= this->ymax(n2))
            return 11;
        //for(unsigned i=0;i<4;i++) m_corners[i] = n2->cell()->idx(tmsh::cell<3>::Face[5][i]);
        else
            return 8;
        //for(unsigned i=0;i<4;i++) m_corners[i] = n1->cell()->idx(tmsh::cell<3>::Face[4][i]);
    }
    return 0;
}

//--------------------------------------------------------------------
TMPL
/**
 * @brief SELF::edges_of_face
 * @param n0 node of the first cell
 * @param n1 node of the second cell
 * @param c index depending on the sign of the corners
 *
 * Connect the points on the face shared by the two cells of n0 and n1
 * according to the index c.
 */
void SELF::edges_of_face(Node* n0, Node* n1, int c) {
    //std::cout<<std::endl<<"case"<< c;
    switch (c) {
    case 0:
        break;
    case 1:
        break;
    case 2:
        break;
    case 3:
        this->insert_edge(m_edgepts[0],m_edgepts[1],n0,n1);
        break;
    case 4:
        break;
    case 5:
        this->insert_edge(m_edgepts[0],m_edgepts[2],n0,n1);
        break;
    case 6:
        this->insert_edge(m_edgepts[1],m_edgepts[2],n0,n1);
        break;
    case 7:
        // Ambiguity 0 1 2
        if (m_edgepts[1]==m_edgepts[2]) {
            this->insert_edge(m_edgepts[0],m_edgepts[1],n0,n1);
        } else if (m_edgepts[0]==m_edgepts[1]) {
            this->insert_edge(m_edgepts[1],m_edgepts[2],n0,n1);
        } else {
            std::cout<<std::endl<< "Ambiguity 0 1 2:"<<m_edgepts[0]<<m_edgepts[1]<<m_edgepts[2];
            LINE(m_edgepts[0],m_edgepts[1]);
            LINE(m_edgepts[1],m_edgepts[2]);
            this->insert_edge(m_edgepts[0],m_edgepts[1],n0,n1);
            this->insert_edge(m_edgepts[1],m_edgepts[2],n0,n1);
        }
        break;
    case 8:
        break;
    case 9:
        this->insert_edge(m_edgepts[0],m_edgepts[3],n0,n1);
        break;
    case 10:
        this->insert_edge(m_edgepts[1],m_edgepts[3],n0,n1);
        break;
    case 11:
        // Ambiguity 3 0 1
        if(m_edgepts[0]==m_edgepts[1]) {
            this->insert_edge(m_edgepts[3],m_edgepts[0],n0,n1);
        } else if (m_edgepts[0]==m_edgepts[3]) {
            this->insert_edge(m_edgepts[0],m_edgepts[1],n0,n1);
        } else {
            std::cout<<std::endl<< "Ambiguity 3 0 1:"<<m_edgepts[3]<<m_edgepts[0]<<m_edgepts[1];
            LINE(m_edgepts[3],m_edgepts[0]);
            LINE(m_edgepts[0],m_edgepts[1]);
            this->insert_edge(m_edgepts[3],m_edgepts[0],n0,n1);
            this->insert_edge(m_edgepts[0],m_edgepts[1],n0,n1);
        }
        break;
    case 12:
        this->insert_edge(m_edgepts[2],m_edgepts[3],n0,n1);
        break;
    case 13:
        // Ambiguity 2 3 0
        if(m_edgepts[3]==m_edgepts[0]) {
            this->insert_edge(m_edgepts[2],m_edgepts[3],n0,n1);
        } else if(m_edgepts[2]==m_edgepts[3]) {
            this->insert_edge(m_edgepts[3],m_edgepts[0],n0,n1);
        } else {
            std::cout<<std::endl<< "Ambiguity 2 3 0:"<<m_edgepts[2]<<m_edgepts[3]<<m_edgepts[0];
            LINE(m_edgepts[2],m_edgepts[3]);
            LINE(m_edgepts[0],m_edgepts[0]);
            this->insert_edge(m_edgepts[2],m_edgepts[3],n0,n1);
            this->insert_edge(m_edgepts[3],m_edgepts[0],n0,n1);
        }
        break;
    case 14:
        // Ambiguity 1 2 3
        if(m_edgepts[1]==m_edgepts[2]) {
            this->insert_edge(m_edgepts[2],m_edgepts[3],n0,n1);
        } else if(m_edgepts[2]==m_edgepts[3]) {
            this->insert_edge(m_edgepts[1],m_edgepts[2],n0,n1);
        } else {
            std::cout<<std::endl<< "Ambiguity 1 2 3:"<<m_edgepts[1]<<m_edgepts[2]<<m_edgepts[3];
            LINE(m_edgepts[1],m_edgepts[2]);
            LINE(m_edgepts[2],m_edgepts[3]);
            this->insert_edge(m_edgepts[2],m_edgepts[3],n0,n1);
            this->insert_edge(m_edgepts[1],m_edgepts[2],n0,n1);
        }
        break;
    case 15:
        if(m_edgepts[0] == m_edgepts[1] && m_edgepts[2] == m_edgepts[3]) {
            this->insert_edge(m_edgepts[0],m_edgepts[2],n0,n1);
        } else if(m_edgepts[0] == m_edgepts[3] && m_edgepts[1] == m_edgepts[2]) {
            this->insert_edge(m_edgepts[0],m_edgepts[2],n0,n1);
        } else {
            // Ambiguity 0 1 2 3
            std::cout<<std::endl<< "Ambiguity 0 1 2 3:"
                    <<m_edgepts[0]<<m_edgepts[1]<<m_edgepts[2]<<m_edgepts[3];
            this->insert_edge(m_edgepts[0],m_edgepts[2],n0,n1);
            this->insert_edge(m_edgepts[1],m_edgepts[3],n0,n1);
        }
        break;
    }

}

TMPL
void SELF::singular_faces(const std::vector< std::pair<int,int> >& edges,int n)
{
    //mdebug()<<"this is it"<<n<<edges.size();

    for (unsigned i=0;i<edges.size();i++)
    {
        Face f;
        f.push_back(n);
        f.push_back(edges[i].first);
        f.push_back(edges[i].second);
        this->add_face(f);
    }



}

//--------------------------------------------------------------------
TMPL
/**
* @brief faces_of_cell
* @param faces
* @param edges sequence of edges
*
* Extract loops from the sequence of edges.
*/
void SELF::faces_of_cell(const std::vector< std::pair<int,int> >& edges)
{
    //std::cout<<std::endl<<"\nfaces of cell:"<<edges.size();
    //for(unsigned i=0;i<edges.size();i++)
    //    std::cout<<std::endl<<" "<<edges[i].first<<"-"<<edges[i].second<<"  ";
    //std::cout<<std::endl;
    //for(unsigned i=0;i<edges.size();i++) LINE(edges[i].first, edges[i].second);
    //std::cout<<std::endl;

    std::map<int,std::vector<int> > eof;
    std::map<int,bool>              unused;
    for(unsigned i=0; i<edges.size(); i++) {
        eof[edges[i].first].push_back(edges[i].second);
        eof[edges[i].second].push_back(edges[i].first);
        unused[edges[i].first]=true;
        unused[edges[i].second]=true;
    }
    //std::cout<<std::endl<<"loop";

    std::vector<int> tmp;
    unsigned cur, nxt;
    bool notfound;
    for (std::map<int,std::vector<int> >::iterator it=eof.begin(); it!=eof.end(); ++it)
        if(unused[it->first]) {
            Face f;
            //std::cout<<std::endl<<"new face";
            f.push_back(it->first);
            //unused[it->first]=true;
            //f.push_back(edges[i].second);
            cur=it->first;

            //std::cout<<std::endl<<"+ face"<<edges[i].first;
            //std::cout<<std::endl<<"+ face"<<edges[i].second;
            //std::cout<<std::endl<<"j="<<j;
            //nxt = it->first;

            while(unused[cur]) {
                unused[cur] = false;
                notfound = true;
                //std::cout<<std::endl<<"j="<<j;
                for(unsigned k=0; notfound && k<eof[cur].size();k++) {
                    nxt = eof[cur][k];
                    //std::cout<<std::endl<<"edge"<<e <<"for"<<nxt<<":"<<unused[e];
                    if(unused[nxt]) {
                        notfound=false;
                        cur = nxt;
                        f.push_back(nxt);
                    }
                }
                if(notfound) break;
            }

            // Points linked to the initial point if the loop is not closed
            tmp.resize(0);
            cur=it->first;
            for(unsigned k=0; notfound && k<eof[cur].size();k++) {
                nxt = eof[cur][k];
                //std::cout<<std::endl<<"edge"<<e <<"for"<<nxt<<":"<<unused[e];
                if(unused[nxt]) {
                    notfound=false;
                    cur = nxt;
                    tmp.push_back(nxt);
                }
            }
            while(unused[cur]) {
                unused[cur] = false;
                notfound = true;
                //std::cout<<std::endl<<"j="<<j;
                for(unsigned k=0; notfound && k<eof[cur].size();k++) {
                    nxt = eof[cur][k];
                    //std::cout<<std::endl<<"edge"<<e <<"for"<<nxt<<":"<<unused[e];
                    if(unused[nxt]) {
                        notfound=false;
                        cur = nxt;
                        tmp.push_back(nxt);
                    }
                }
                if(notfound) break;
            }
            for(int k=1;k<=tmp.size();k++) f.push_back(tmp[tmp.size()-k]);

            if(f.size()>2) this->add_face(f);

            //            std::cout<<std::endl<<"add_face"<<f.size();
            //            for(unsigned k=0;k<f.size();k++) std::cout<<std::endl<<" "<<f[k];
            //            std::cout<<std::endl;

        }
}

//--------------------------------------------------------------------
TMPL void SELF::run() {

    // Faces of the interior of the cell
    this->get_dual_vertex(m_input);

    //std::cout<<std::endl<<"Boundary cells";

    // Faces on the boundary of the initial cell.
    Node * outside = new Node;
    Cell * cl = new Cell;
    cl->regularity(INSIDE);
    int s, v;
    outside->set_cell(cl);

    for(unsigned f=0;f<6;f++) {
        for(unsigned i=0;i<4;i++) {
            cl->idx(tmsh::cell<3>::Face[2*(f/2)][i])=m_input->cell()->idx(tmsh::cell<3>::Face[f][i]);
            cl->idx(tmsh::cell<3>::Face[2*(f/2)+1][i])=m_input->cell()->idx(tmsh::cell<3>::Face[f][i]);
        }
        s = f%2; v = f/2;
        for(unsigned i=0;i<6;i++) outside->dim[i]=i%2;
        outside->dim[2*v]=2*s-1;
        outside->dim[2*v+1]=2*s;

        this->get_dual_edge(m_input,outside);
    }

    //std::cout<<std::endl<<"regular cells";
#define REG
#ifdef REG
    foreach (Cell* cl, *this->regular_cells()) {
        this->edges_of_cell(cl);
        this->faces_of_cell(cl->m_edges);
    }
#endif

//#define SING
#ifdef SING
    //std::cout<<std::endl<<"singular cells";
    foreach (Cell* cl, *this->singular_cells()) {
        //std::cout<<std::endl<<"this is here";
        this->edges_of_cell(cl);
#ifdef NEW
        int x = this->controler()->minimum_in_cell(cl->idx(0),cl->idx(1),cl->idx(2),cl->idx(3),cl->idx(4),cl->idx(5),cl->idx(6),cl->idx(7));
        //std::cout<<std::endl<<x;
        if(x!=-1)
        {
            this->singular_faces(cl->m_edges,x);
        }
        else
#endif
        {
            mdebug()<<"---------> singular point not found";
            this->faces_of_cell(cl->m_edges);
        }
    }
#endif //SING
}

//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================
#undef TMPL
#undef SELF

