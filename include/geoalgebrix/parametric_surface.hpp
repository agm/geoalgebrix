/*****************************************************************************
 * M a t h e m a g i x
 *****************************************************************************
 * ParametricSurface
 * 2008-03-28
 * Julien Wintz
 *****************************************************************************
 *               Copyright (C) 2006 INRIA Sophia-Antipolis
 *****************************************************************************
 * Comments :
 ****************************************************************************/
#pragma once

# include <geoalgebrix/bounding_box.hpp>
# include <realroot/Seq.hpp>
# include <geoalgebrix/mesh.hpp>
//# include <geoalgebrix/surface.hpp>

# define TMPL template<class C>
//# define TMPL1 template<class V>
# define SELF parametric_surface<C>
//# define Surface surface<V>
//====================================================================
namespace mmx { namespace geoalgebrix {

template<class C>
class parametric_surface
//    : public surface<V>
{
public:

    typedef C      Scalar;
    typedef typename mesh<C>::Point         Point;
    typedef typename mesh<C>::PointConstIterator    PointIterator;

    parametric_surface(void) {} ;
    //  parametric_surface(const BoundingBox & box): surface(box){}

    //  virtual void set_grid( int m, int n ) = 0;

    // virtual Point* eval(Scalar u, Scalar v)            const = 0 ;
    // virtual void   eval(Point& p, double u, double v)  const = 0 ;
    virtual void   eval(Scalar&, Scalar&,Scalar&, Scalar u, Scalar v)  const = 0 ;
    // virtual Point* operator() (double u, double v)     const { return this->eval(u,v); }
    virtual void   eval(Scalar* lp, const Scalar* u, int n) const ;

    virtual void sample ( PointIterator lp, const Scalar* u, unsigned m, const Scalar* v, unsigned n ) const ;


    //  virtual void sample ( Point* lp, const double * uv, int n );
    virtual void sample ( PointIterator lp, unsigned m, unsigned n ) const ;
    virtual void sample ( Scalar* lp, unsigned m, unsigned n, Scalar* u, Scalar* v) const;

    //  virtual ParametricCurve * iso_u( Scalar u )                                               const = 0;
    //  virtual ParametricCurve * iso_v( Scalar v )                                               const = 0;

    //  IParametricSurface * gauss_map() const = 0;
    //  virtual IGLGood * triangulate(void) = 0 ;

    virtual double umin(void) const = 0 ;
    virtual double umax(void) const = 0 ;
    virtual double vmin(void) const = 0 ;
    virtual double vmax(void) const = 0 ;

    //virtual void   get_range(double & umin, double & umax, double & vmin, double & vmax ) const = 0;

} ;

TMPL void 
SELF::eval(Scalar* r, const Scalar* u, int n) const {
    Point p;
    for (int i=0; i<n;i++) {
        this->eval(p.x(),p.y(),p.z(),u[0],u[1]);
        *r=p.x();r++;
        *r=p.y();r++;
        *r=p.z();r++;
        u+=2;
    }
}


TMPL void
SELF::sample( PointIterator p, unsigned m, unsigned n ) const {
    assert(m>1 && n>1);
    Scalar
            hu=(this->umax() - this->umin())/(m-1), u=this->umin(),
            hv=(this->vmax() - this->vmin())/(n-1), v;
    Point pp;
    for (unsigned i=0; i<m;i++) {
        v=vmin();
        for (unsigned j=0; j<n;j++,p++) {
            this->eval((*p)->x(),(*p)->y(),(*p)->z(),u,v);
            v+=hv;
        }
        u+=hu;
    }
}

TMPL void
SELF::sample (Scalar* r, unsigned m,  unsigned n, Scalar* u, Scalar* v) const {
    assert(m>1 && n>1);
    Scalar
            hu=(this->umax() - this->umin())/(m-1),
            hv=(this->vmax() - this->vmin())/(n-1);

    Scalar * u0 =u, su=umin(), * v0=v, sv=vmin();
    for (unsigned i=0; i<m;i++){ *u=su; su+=hu; u++; }
    for (unsigned j=0; j<n;j++){ *v=sv; sv+=hv; v++; }

    u=u0;
    for (unsigned i=0; i<m;i++,u++) {
        v=v0;
        for (unsigned j=0; j<n;j++,v++) {
            this->eval(r[0],r[1],r[2],*u,*v);
//            *r=x;std::cout<<*r<<" ";
            r+=3;
//            *r=y;std::cout<<*r<<" ";
//            r++;
//            *r=z;std::cout<<*r<<" ";
//            r++;
//            std::cout<<std::endl;
        }
    }
}

TMPL void 
SELF::sample (PointIterator p, const Scalar* u, unsigned m, const   Scalar* v, unsigned n) const {
    assert(m>1 && n>1);
    const Scalar * v0=v;

    for (unsigned i=0; i<m;i++,u++) {
        v=v0;
        for (unsigned j=0; j<n;j++,p++,v++) {
            this->eval((*p)->x(),(*p)->y(),(*p)->z(),*u,*v);
        }
    }
}


} ; // namespace geoalgebrix
              } ; // namespace mmx
# undef TMPL
//# undef TMPL1
//# undef Point
//# undef Scalar
//# undef Surface
# undef SELF
