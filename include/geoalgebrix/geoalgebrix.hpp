/*****************************************************************************
 * M a t h e m a g i x
 *****************************************************************************
 * geoalgebrix
 * 2008-03-21
 * Julien Wintz && Bernard Mourrain
 *****************************************************************************
 *               Copyright (C) 2006 INRIA Sophia-Antipolis
 *****************************************************************************
 * Comments :
 ****************************************************************************/
# ifndef geoalgebrix_geoalgebrix_hpp
# define geoalgebrix_geoalgebrix_hpp
# define TMPL template<class K>
# define SELF geometric<K>
//====================================================================
#ifndef MMX_FOREACH
struct MmxForeachContainerBase {};

template <typename T> class MmxForeachContainer : public MmxForeachContainerBase {
public:
    inline MmxForeachContainer(const T& t): c(t), brk(0), i(c.begin()), e(c.end()) {} ;
    const T c ;
    mutable int brk ;
    mutable typename T::const_iterator i, e ;
    inline bool condition() const { 
	return (!brk++ && i != e) ; 
    }
} ;

template <typename T>
inline T * mmxForeachpointer(const T &) {
    return 0 ;
}

template <typename T>
inline MmxForeachContainer<T>
mmxForeachContainerNew(const T& t) {
    return MmxForeachContainer<T>(t) ;
}

template <typename T> inline const MmxForeachContainer<T> *mmxForeachContainer(const MmxForeachContainerBase *base, const T *) {
    return static_cast<const MmxForeachContainer<T> *>(base) ;
}

# define MMX_FOREACH(variable, container) \
     if(0) {} else for (const MmxForeachContainerBase & _container_ = mmxForeachContainerNew(container); \
          mmxForeachContainer(&_container_, true ? 0 : mmxForeachpointer(container))->condition(); \
          ++mmxForeachContainer(&_container_, true ? 0 : mmxForeachpointer(container))->i) \
         for (variable = *mmxForeachContainer(&_container_, true ? 0 : mmxForeachpointer(container))->i; \
              mmxForeachContainer(&_container_, true ? 0 : mmxForeachpointer(container))->brk; \
              --mmxForeachContainer(&_container_, true ? 0 : mmxForeachpointer(container))->brk)

# endif

#ifndef foreach
# define foreach MMX_FOREACH
#endif

//====================================================================
namespace mmx {


# define TMPL_TYPE(NAME, X) typename NAME<X>::T
# define TYPE(NAME, X) NAME<X>::T

# define WITH(N0, K1, N2) typedef typename use<N0##_def,K1>::N2 N2

//====================================================================
namespace geoalgebrix {
//====================================================================
  struct default_env {};

  template<class A, class C = double, class B= default_env> struct use;

  struct ref_def {};

  template<> struct use<ref_def>        { typedef default_env Ref; };
  //  template<> struct use<ref_def,double> { typedef default_env Ref; };

# define REF_OF(N0) typename use<ref_def,double,N0>::Ref
# define DECLARE_REF_OF(V0,R1) struct use<ref_def,double,V0 > {typedef R1 Ref; };

//--------------------------------------------------------------------
//  template<class A, class B> struct join2 {};

//struct mesh_def {};
struct scalar_def {};

template<> struct use<scalar_def>        { typedef double Scalar; };
//--------------------------------------------------------------------

TMPL class geometric; // TMPL struct geometric;

//--------------------------------------------------------------------
struct geoalgebrix_def {};

struct default_1d {};
struct default_2d {};
struct default_3d {};

template<class C> struct use<geoalgebrix_def,C> { 
  typedef geometric<default_env> Shape; 
};

# define SHAPE_OF(V) use<geoalgebrix_def,double,V>::Shape

//--------------------------------------------------------------------
TMPL class geometric {
public:
  geometric(void) {
    m_x = m_y = m_z = 0.0 ;
    m_q0 = m_q1 = m_q2 = m_q3 = 0.0 ;
  }
  
  virtual ~geometric(void) {} ;

  bool position(double * x, double * y, double * z) ;
  bool orientation(double * q0, double * q1, double * q2, double * q3) ; 

protected:
    double m_x, m_y, m_z ;
    double m_q0, m_q1, m_q2, m_q3 ;
} ;

TMPL bool 
SELF::position(double * x, double * y, double * z) {
  *x = this->m_x ;
  *y = this->m_y ;
  *z = this->m_z ;
  return true ;
}

TMPL bool 
SELF::orientation(double * q0, double * q1, double * q2, double * q3) {
  *q0 = this->m_q0 ;
  *q1 = this->m_q1 ;
  *q2 = this->m_q2 ;
  *q3 = this->m_q3 ;
  return true ;
}

//--------------------------------------------------------------------
TMPL struct process {};

# define PROCESS_OF(V) process<V>

//====================================================================
} ; // namespace geoalgebrix
} ; // namespace mmx
//====================================================================
# undef TMPL
# undef SELF
# endif // geoalgebrix_geoalgebrix_hpp
