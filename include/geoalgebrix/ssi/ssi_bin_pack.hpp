#ifndef BIN_PACK
#define BIN_PACK

#define bin_pack2(a,b,n) (((a)<<n)|(b))
#define bin_pack2_1(a,b) bin_pack2(a,b,1)
#define bin_pack2_2(a,b) (((a)<<2)|(b))
#define bin_pack2_3(a,b) (((a)<<3)|(b))
#define bin_pack4_1(a,b,c,d) bin_pack2_2(bin_pack2_1(a,b),bin_pack2_1(c,d))

#endif
