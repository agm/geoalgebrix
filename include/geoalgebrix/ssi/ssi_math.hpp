#ifndef MATH_MATH_H
#define MATH_MATH_H
#include <algorithm>
#include <math.h>

template<class K> inline
K abs( const K& k ) { return std::abs(k); };

template<class K> inline
K det22( const K& a, const K& b, 
	 const K& c, const K& d )
{ return a*d - b*c; };

template<class K>
void tcoff33( K& r00, K& r01, K& r02, 
	      K& r10, K& r11, K& r12,
	      K& r20, K& r21, K& r22,
	      const K& a00, const K& a01, const K& a02,
	      const K& a10, const K& a11, const K& a12,
	      const K& a20, const K& a21, const K& a22 )
{
	r00 = a11*a22-a12*a21;
	r01 = -a01*a22+a02*a21;
	r02 = a01*a12-a02*a11;
	r10 = -a10*a22+a12*a20;
	r11 = a00*a22-a02*a20;
	r12 = -a00*a12+a02*a10;
	r20 = a10*a21-a11*a20;
	r21 = -a00*a21+a01*a20;
	r22 = a00*a11-a01*a10;
      };

      template<class K>
      void inv33( K& r00, K& r01, K& r02, 
		  K& r10, K& r11, K& r12,
		  K& r20, K& r21, K& r22,
		  const K& a00, const K& a01, const K& a02,
		  const K& a10, const K& a11, const K& a12,
		  const K& a20, const K& a21, const K& a22 )

      {
	tcoff33( r00, r01, r02, 
		 r10, r11, r12,
		 r20, r21, r22,
		 a00, a01, a02,
		 a10, a11, a12,
		 a20, a21, a22 );
	K idet = 1.0/(a00*r00+a10*r01+a20*r02);
	r00 *= idet;
	r01 *= idet;
	r02 *= idet;
	r10 *= idet;
	r11 *= idet;
	r12 *= idet;
	r20 *= idet;
	r21 *= idet;
	r22 *= idet;
      };

      template<class K>
      K det33( const K& a00, const K& a01, const K& a02,
	       const K& a10, const K& a11, const K& a12,
	       const K& a20, const K& a21, const K& a22 )
      {
	K tmp[9];
	tcoff33( tmp[0], tmp[1], tmp[2],
		 tmp[3], tmp[4], tmp[5],
		 tmp[6], tmp[7], tmp[8],
		 a00,    a01,    a02, 
		 a10,    a11,    a12,
		 a20,    a21,    a22 );
	return   a00 * tmp[0] + a10 * tmp[1] + a20 * tmp[2];
      };

      template<class K> inline
      K Sqrt( const K& x , const K& prec = 1e-19)
      {
	
	K r  = x/2;
	do 
	  {
	    r  = (r+(x/r))/2;
	  }
	while( abs(r*r-x) > prec );
	return r;
      };
      
      template<> inline
      double Sqrt( const double& x, const double& prec ) { return sqrt(x); };
      template<> inline
      float  Sqrt( const float&  x, const float& prec  ) { return sqrt(x); };

#endif
