#ifndef GEOM_FSVECTOR_H
#define GEOM_FSVECTOR_H
#include "patterns.h"
//----------------------------------------------------------------------------
//                          fsvector: Fixed Size Vector
//----------------------------------------------------------------------------
template <class K, unsigned n > 
struct fsvector
{
  typedef K               field;
  typedef K               value_type;
  typedef unsigned        size_type;
  typedef unsigned        index_t;
  static const  int dimension = n;
  K _data[n];
  public:
  const K  *  data() const { return _data; };
  const unsigned  size() const { return     n; };
  //                                 contructors
  // static const int dimension() { return n; };
  fsvector();
  fsvector( const K& k    )                     ; // coordinates are set to k 
  fsvector( const K * src )                     ; // values are copied from src
  //  fsvector( const K[n] );
  //                                  accessors
  K&       operator[]( unsigned i )            ; // read  access to coordinate i
  const K& operator[]( unsigned i ) const      ; // write access to coordinate i
  K * begin() { return &(_data[0]); };

  //                           vector space operations
  
  fsvector& operator+=( const fsvector& v )      ; // add       v  to vector 
  fsvector& operator-=( const fsvector& v )      ; // substract v  to vector
  fsvector& operator*=( const K& k  )           ; // multiply all coordinates by k
  fsvector& operator/=( const K& k  )           ; // divide   all coordinates by k
  fsvector& operator/=( const fsvector& v )      ; // project on v space
  fsvector& operator%=( const fsvector& v )      ; // project on v orthogonal space
  
  fsvector  operator-(                    ) const; // return -vector
  fsvector  operator+( const fsvector& v  ) const; // return vector + v
  fsvector  operator-( const fsvector& v  ) const; // return vector - v
  

  K        operator*( const fsvector& v  ) const; // return the dotproduct vector*v
  
  fsvector  operator^( const fsvector& v  ) const; // cross-product 

};

//                                     functions

//                                  comparisons (lex order)

template<class K, unsigned n>
inline bool operator< ( const fsvector<K,n>& v0, const fsvector<K,n>& v1 )
{ return lexicomesh_comp(v0,v1,n,n,1,1) == -1; };
template<class K, unsigned n>
inline bool operator<=( const fsvector<K,n>& v0, const fsvector<K,n>& v1 )
{ return lexicomesh_comp(v0,v1,n,n,1,1) <=  0; };
template<class K, unsigned n>
inline bool operator> ( const fsvector<K,n>& v0, const fsvector<K,n>& v1 )
{ return lexicomesh_comp(v0,v1,n,n,1,1) ==  1;  };
template<class K, unsigned n>
inline bool operator>=( const fsvector<K,n>& v0, const fsvector<K,n>& v1 )
{ return lexicomesh_comp(v0,v1,n,n,1,1) >=  0;  };

//                                    norms
template<class K, unsigned n> inline K 
norm_euc( const fsvector<K,n>& v )
{
  K r;
  patterns::norm_euc(r,v,n,1); 
  return r;
};

template<class K,unsigned n> inline void 
norm_euc( K& r, const fsvector<K,n>& v )
{ norm_euc( r, v, n,1  ); };
template<class K,unsigned n> inline void
norm_max( K& r, const fsvector<K,n>& v )
{ norm_max(r,v, n,1); };

//                                 constructors

template<class K,unsigned n>
inline
fsvector<K,n>::fsvector<K,n>(){};

template<class K,unsigned n>
inline
fsvector<K,n>::fsvector<K,n>( const K& k )
{ 
  patterns::fill(*this,k,n);
};

template<class K,unsigned n>
inline
fsvector<K,n>::fsvector<K,n>( const K * src )
{ 
  patterns::copy(*this,src,n);
};

template<class K,unsigned n>
inline K& 
fsvector<K,n>::operator[]( unsigned i )
{
  return _data[i];
};

template<class K,unsigned n>
inline const K& 
fsvector<K,n>::operator[]( unsigned i ) const
{
  return _data[i];
};

//                            vector space operations

template<class K,unsigned n>
inline fsvector<K,n>& 
fsvector<K,n>::operator+=( const fsvector& v )
{
  patterns::padd1(*this,v,n);
  return (*this);
};

template<class K,unsigned n>
inline fsvector<K,n>& 
fsvector<K,n>::operator-=( const fsvector& v )
{
  patterns::psub1(*this,v,n);
  return (*this);
};

template<class K,unsigned n>
inline fsvector<K,n>& 
fsvector<K,n>::operator*=( const K& k )
{
  patterns::smul1(*this,k,n);
  return (*this);
};

template<class K,unsigned n>
inline fsvector<K,n>& 
fsvector<K,n>::operator/=( const K& k )
{
  patterns::sdiv1(*this,k,n);
  return (*this);
};

template<class K,unsigned n>
inline fsvector<K,n> 
fsvector<K,n>::operator-() const
{
  fsvector<K,n> temp;
  patterns::neg2(temp,*this,n);
  return temp;
};

template<class K,unsigned n>
inline fsvector<K,n> 
fsvector<K,n>::operator+( const fsvector<K,n>& v ) const
{
  fsvector<K,n> temp;
  patterns::padd2(temp,*this,v,n);
  return temp;
};

template<class K,unsigned n>
inline fsvector<K,n> 
fsvector<K,n>::operator-( const fsvector<K,n>& v ) const
{
  fsvector<K,n> temp;
  patterns::psub2(temp,*this,v,n);
  return temp;
};

template<class K,unsigned n>
inline K fsvector<K,n>::operator*( const fsvector<K,n>& v ) const 
{
  K temp;
  patterns::dot(temp,*this,v,n);
  return temp;
};

template<class K,unsigned n>
inline void crossprod( fsvector<K,n>& res, 
		       const fsvector<K,n>& a, const fsvector<K,n>& b )
{ patterns::cross(res,a,b,n); };


template<class K,unsigned n>
inline fsvector<K,n> 
fsvector<K,n>::operator^( const fsvector<K,n>& v ) const
{
  fsvector<K,n> temp;
  crossprod(  temp, *this, v );
  return temp;
};
 
template<class K,unsigned n>
inline fsvector<K,n> 
operator*( const K& k, const fsvector<K,n>& v )
{
  fsvector<K,n> temp;
  patterns::smul2(temp,v,k,n);
  return temp;
};

template<class K,unsigned n>
inline fsvector<K,n> 
operator*( const fsvector<K,n>& v, const K& k )
{
  fsvector<K,n> temp;
  patterns::smul2(temp,v,k,n);
  return temp;
};

template<class K,unsigned n>
inline fsvector<K,n> 
operator/( const fsvector<K,n>& v, const K& k )
{
  fsvector<K,n> temp;
  patterns::sdiv2(temp,v,k,n);
  return temp;
};

template<class K,unsigned n>
inline std::ostream& 
operator<<( std::ostream& out, const fsvector<K,n>& v )
{
   out << "( ";
   for ( unsigned i = 0; i < n; i++ )
     out << v[i] << " ";
   out << ')';
   return out;
};


template<class K,unsigned n>
inline K norm_max( const fsvector<K,n>& v )
{
  K temp;
  patterns::norm_max(temp,v,n,1);
  return temp;
};

#ifndef PIPO
template< class number, unsigned size >
inline void 
scale( fsvector< number, size >& v, const fsvector< number, size>& s )
{ patterns::pmul1( v.data(), s.data(), size ); };

template< class number, unsigned size >
inline void
inv  ( fsvector< number, size >& v )
{ patterns::inv1( v.data(), size ); };
#endif
#endif




