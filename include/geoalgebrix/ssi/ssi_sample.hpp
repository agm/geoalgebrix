#ifndef SYNAPS_SHAPE_GAIA_SAMPLE_H
#define SYNAPS_SHAPE_GAIA_SAMPLE_H

#include <geoalgebrix/ssi/ssi_def.hpp>
#include <geoalgebrix/parametric_surface.hpp>

#define TMPL template<class C, class V>
//#define ParametricSurface geoalgebrix::parametric_surface<double>

namespace mmx {
namespace ssi {

TMPL
struct sample
{
    typedef typename mmx::geoalgebrix::use<mmx::geoalgebrix::ssi_def,C,V>::ParametricSurface ParametricSurface;
    const ParametricSurface * m_psurf;
    double * m_uvals;
    double * m_vvals;
    coord_t  m_nrows;
    coord_t  m_ncols;
    vector3 * m_svals;
    sample( const ParametricSurface * s, int m, int n );
    ~sample();
    vector3 * base() const { return m_svals; };
    int nrows() const { return m_nrows; };
    int ncols() const { return m_ncols; };
    inline const double & uvalue( int i ) const { return m_uvals[i]; };
    inline const double & vvalue( int i ) const { return m_vvals[i]; };
};

TMPL
sample<C,V>::sample(const ParametricSurface * s, int m, int n )
{
    std::cout<<"Sample "<<m<<" "<<n<<std::endl;
    //    double _st = time();
    m_psurf = s;
    m_uvals = new double[3*m*n+m+n];
    m_vvals = m_uvals + m;
    m_svals = (fxv<double,3>*)(m_vvals + n);
    m_nrows = m;
    m_ncols = n;

    mmx::geoalgebrix::use<mmx::geoalgebrix::ssi_def,C,V>::sample(s,m_vvals+n,m,n,m_uvals,m_vvals);
    //s->sample(m_vvals+n,m,n,m_uvals,m_vvals);
    //  std::cout << "s = " << (time()-_st) << std::endl;
};

TMPL
sample<C,V>::~sample() { delete[] m_uvals; };
};

} //namespace mmx

# undef  ParametricSurface
# endif
