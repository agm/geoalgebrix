#ifndef SYNAPS_SHAPE_GAIA_QNODE_H
#define SYNAPS_SHAPE_GAIA_QNODE_H

#include <geoalgebrix/ssi/ssi_def.hpp>
#include <geoalgebrix/ssi/ssi_sample.hpp>

#define TMPL template<class C, class V>

namespace mmx {
namespace ssi
{

  template <class C, class V>
  struct qnode
  {
    aabb3 box;
    coord_t umin,umax,vmin,vmax;
    qnode<C,V> * l, *r;
    qnode<C,V> * father;
    bool leaf() const { return (l == 0); };
    qnode(): l(0), r(0) {};
    void split_u();
    void split_v();
    void fill( vector3 ** qp, sample<C,V> * s  ) const;
    void fill( vector3 * qp, sample<C,V> * s  ) const;
    void convert( vector2 *v, sample<C,V> * s, int n ) const ;
    void mbox( sample<C,V> * s );
    void split( sample<C,V> * s );
    ~qnode();
  };

  TMPL std::ostream& operator<<( std::ostream& o, const qnode<C,V>& );
  TMPL inline coord_t du  ( const qnode<C,V> *  q ) { return q->umax-q->umin; };
  TMPL inline coord_t dv  ( const qnode<C,V> *  q ) { return q->vmax - q->vmin; };
  TMPL inline coord_t umin( const qnode<C,V> *  q ) { return q->umin; };
  TMPL inline coord_t umax( const qnode<C,V> *  q ) { return q->umax; };
  TMPL inline coord_t vmin( const qnode<C,V> *  q ) { return q->vmin; };
  TMPL inline coord_t vmax( const qnode<C,V> *  q ) { return q->vmax; };
  TMPL inline bool leaf( qnode<C,V> const * const q ) { return (q->l == 0); };
  TMPL inline bool unit( const qnode<C,V> * q ) { return du(q)==1 && dv(q) == 1; };
  TMPL inline bool inside( coord_t u, coord_t v, qnode<C,V> * q )
  {
    if ( q->umin  > u ) return false;
    if ( q->umax <= u ) return false;
    if ( q->vmin  > v ) return false;
    if ( q->vmax <= v ) return false;
    return true;
  };
  TMPL qnode<C,V> * search( coord_t u, coord_t v, qnode<C,V> * start );
  TMPL inline qnode<C,V> * up  ( qnode<C,V> * q )  { return search( q->umin + 1, q->vmin, q ); };
  TMPL inline qnode<C,V> * down( qnode<C,V> * q )  { return search( q->umin - 1, q->vmin, q ); };
  TMPL inline qnode<C,V> * left( qnode<C,V> * q )  { return search( q->umin, q->vmin - 1, q ); };
  TMPL inline qnode<C,V> * right( qnode<C,V> * q ) { return search( q->umin, q->vmin + 1, q );  };

TMPL
std::ostream& operator<<( std::ostream& o, const qnode<C,V> & q )
{
  o << "[ " << q.umin << ", " << q.umax << " ] x ";
  o << "[ " << q.vmin << ", " << q.vmax << " ]";
  return o;
};

TMPL
void qnode<C,V>::split_u()
{
  l = new qnode();
  r = new qnode();
  l->umin = umin;
  l->umax = (umin+umax)/2;
  r->umin = (umin+umax)/2;
  r->umax = umax;
  l->vmin = r->vmin = vmin;
  l->vmax = r->vmax = vmax;
  l->father = r->father = this;
};

TMPL
void qnode<C,V>::split_v()
{
l = new qnode();
r = new qnode();
l->vmin = vmin;
l->vmax = (vmin+vmax)/2;
r->vmin = l->vmax;
r->vmax = vmax;
l->umin = r->umin = umin;
l->umax = r->umax = umax;
l->father = this;
r->father = this;
};


TMPL
qnode<C,V>::~qnode()
{
if  (l ) delete l;
if ( r ) delete r;
};

TMPL
void qnode<C,V>::fill( vector3 ** qp, sample<C,V> *  s  ) const
{
qp[0] = s->base() + this->umin*s->m_ncols + this->vmin;
qp[1] = s->base() + this->umax*s->m_ncols + this->vmin;
qp[2] = qp[1]  + this->vmax - this->vmin;
qp[3] = qp[0]  + this->vmax - this->vmin;
};

TMPL
void qnode<C,V>::split( sample<C,V> * s)
{

if ( l ) return;
if ( du(this) > dv(this) )
  {
    this->split_u();
    l->mbox(s);
    r->mbox(s);
  }
else if ( dv(this) > 1 )
  {
    this->split_v();
    l->mbox(s);
    r->mbox(s);
  };

};

TMPL
void qnode<C,V>::mbox( sample<C,V> * s )
{
vector3 * qp[4];
fill( qp, s );
for ( int i = 0; i < 3; i ++ ) box[i].m = box[i].M = qp[0]->data[i];
for ( int i = 1; i < 4; i ++ )
  for ( int d = 0; d < 3; d ++ )
    {
      if ( qp[i]->data[d] < box[d].m ) box[d].m = qp[i]->data[d];
      else {  if ( qp[i]->data[d] > box[d].M ) box[d].M = qp[i]->data[d]; };
    };
};

TMPL
void qnode<C,V>::fill( vector3   *qp, sample<C,V> * s  ) const
{
vector3  *r = s->base() + this->umin*s->m_ncols + this->vmin;
qp[0] = *r;
qp[3] = *(r + this->vmax - this->vmin);
r = s->base() + this->umax*s->m_ncols + this->vmin;
qp[1] = *r;
qp[2] = *(r + this->vmax - this->vmin);
};

TMPL
qnode<C,V> * search( coord_t u, coord_t v, qnode<C,V> * start )
{
qnode<C,V> * tmp = start;
do { tmp=tmp->father; } while(tmp && !inside(u,v,tmp) );
if ( !tmp ) return 0;
while(1){
  if ( leaf(tmp) ) return tmp;
  if ( inside(u,v,tmp->l) ) { tmp = tmp->l; continue; };
  if ( inside(u,v,tmp->r) ) { tmp = tmp->r; continue; };
  return 0;
};
};

TMPL
void qnode<C,V>::convert( vector2 * p, sample<C,V> * s, int n  ) const
{
assert(du(this)==1 && dv(this)==1);
double u = s->uvalue(umin);
double v = s->vvalue(vmin);
double vu = s->uvalue(umax)-u;
double vv = s->vvalue(vmax)-v;
for ( int i = 0; i < n; i ++ )
  {
    p[i][0] = u + p[i][0]*vu;
    p[i][1] = v + p[i][1]*vv;
  };
}

} //namespace geoalgebrix_ssi
} //namespace mmx
#endif
