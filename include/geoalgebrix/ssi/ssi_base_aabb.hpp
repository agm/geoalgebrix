#ifndef SYNAPS_SHAPE_BASE_AABB_H
#define SYNAPS_SHAPE_BASE_AABB_H
#include <iostream>
#include <realroot/Interval.hpp>
#include <geoalgebrix/fxv.hpp>

namespace mmx {

template<class C, unsigned N>
struct aabb
{
  fxv< Interval<C>, N >  data;
  Interval<C> & operator[]( int i ) { return data[i]; };
  const Interval<C> & operator[]( int i ) const { return data[i]; };
};

template<class C, unsigned N> inline
void fill( aabb<C,N>& box, const fxv<C,N> & v ) 
{ 
  for ( unsigned i = 0; i < N; box[i] = v[i], i ++  ); 
};

template<class C, unsigned N> inline
void fill( aabb<C,N> & box, const fxv<C,N> * v, unsigned sz )
{
  init(box,v[sz-1]);
  double m,M;
  const fxv<C,N> * src;
  for ( src = v; src != v+sz; src ++ )
    {
      for ( unsigned i = 0; i < 3; i++ )
	{
	  if ( (*src)[i] > (*(src+1))[i] ) { m = (*(src+1))[i]; M =  (*src)[i];    }
	  else                             { m = (*src)[i];     M = (*(src+1))[i]; }; 
	  if ( box[i].m > m ) box[i].m = m;
	  if ( box[i].M < M ) box[i].M = M;
	};
    };
};

template<class C, unsigned N> inline
void fill( aabb<C,N>& box, const fxv<C,N>& a, const fxv<C,N>& b )
{
  for ( unsigned d = 0; d < N; d++ )
    {
      if ( a[d] < b[d] ) 
	{ box[d].m = a[d]; box[d].M = b[d]; }
      else 
	{ box[d].m = b[d]; box[d].M = a[d]; };
    };
};

template<class C, unsigned N> inline
void hull( aabb<C,N>& h, const aabb<C,N>& a, const aabb<C,N>& b )
{
  for ( unsigned i = 0; i < N; i++ )
    {
      h[i].m = std::min(a[i].m,b[i].m);
      h[i].M = std::max(a[i].M,b[i].M);
    };
};

template<class C, unsigned N> inline
void hull( aabb<C,N>& h, const aabb<C,N> & b )
{
  for ( unsigned i = 0; i < 3; i ++ )
    if ( h[i].m > b[i] ) h[i].m = b[i]; else if ( h[i].M < b[i] ) h[i].M = b[i];
};

template<class C, unsigned N> inline
bool intersectp( const aabb<C,N>& a, const aabb<C,N>& b )
{
  for ( unsigned i = 0; i < 3; i ++ )
    if ( a[i].m > b[i].M || b[i].m > a[i].M ) return false;
  return true;  
};


template<class C, unsigned N> inline
C lmax( const aabb<C,N> & box )
{
  C l,s;
  l = box[0].width();
  for ( unsigned i = 1; i < 3; i ++ )
    {
      s = box[i].width();
      if ( s > l ) l = s;
    };
  return l;
};

template<class C, unsigned N> inline
C lmin( const aabb<C,N>& box )
{
  C l,s;
  l = box[0].width();
  for ( unsigned i = 1; i < 3; i ++ )
    {
      s = box[i].width();
      if ( s < l ) l = s;
    };
  return l;
};

template<class C, unsigned N> inline
void fill( aabb<C,N> & box, const fxv<C,N> & a, const fxv<C,N> & b, const fxv<C,N> & c, const fxv<C,N> & d  )
{
  aabb<C,N> tmp; 
  fill(box,a,b);
  fill(tmp,c,d);
  hull(box,tmp);
};

template<class C, unsigned N> inline
std::ostream & operator<<( std::ostream& o, const aabb<C,N> & b )
{
  o << "[ ";
  for ( int i = 0; i < N; i ++ ) o << b[i] << " ";
  o << "] ";
  return o;
};
} //namespace mmx
#endif





