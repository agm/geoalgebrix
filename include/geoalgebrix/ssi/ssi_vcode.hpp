#ifndef VCODE_H
#define VCODE_H
#include <geoalgebrix/ssi/ssi_bin_pack.hpp>

typedef unsigned vcode_t;


#define xcode(c) ((c&0xf00000) >> 20)
#define ycode(c) ((c&0x00f000) >> 12)
#define zcode(c) ((c&0x0000f0) >>  4)


#define Z_MASK  0xf
#define Y_MASK  (Z_MASK<<4)
#define X_MASK  (Y_MASK<<4)
#define NZ_MASK (3 << 12)
#define NY_MASK (NZ_MASK << 2)
#define NX_MASK (NY_MASK << 2)
#define EPS 1e-12
#define gt(a,b) ((a)>(b)+EPS)
#define lt(a,b) ((b)>(a)+EPS)
#define ps(a)   ((a)>EPS)
#define ng(a)   (EPS>(a));
template<class K>
 vcode_t vcode( const K& p00, const K& p10, const K& p01 )
{
#define xu (p10[0]-p00[0])
#define xv (p01[0]-p00[0])
#define yu (p10[1]-p00[1])
#define yv (p01[1]-p00[1])
#define zu (p10[2]-p00[2])
#define zv (p01[2]-p00[2])
  unsigned code = 0;

  // NX
  double nx,ny,nz;
  //  double anx,any,anz;
  nx = yu*zv - yv*zu;
  ny = xu*zv - xv*zu;
  nz = xu*yv - xv*yu;

  code |= nx > 0;
  code <<= 1;
  code |= ny > 0;
  code <<= 1;
  code |= nz > 0;
  code <<= 1;
  
  for ( unsigned i = 0; i < 3; i++ )
    {
      code |= gt(p10[i],p00[i]);
      code <<= 1;
      code |= lt(p10[i],p00[i]);
      code <<= 1;
      code |= gt(p01[i],p00[i]);
      code <<= 1;
      code |= lt(p01[i],p00[i]);
      code <<= 1;
    };

  return code;
};


template<class K>
 vcode_t vcode2( const K& p00, const K& p10, const K& p01 )
{
#define xu (p10[0]-p00[0])
#define xv (p01[0]-p00[0])
#define yu (p10[1]-p00[1])
#define yv (p01[1]-p00[1])
#define zu (p10[2]-p00[2])
#define zv (p01[2]-p00[2])
  unsigned code = 0;

  for ( unsigned i = 0; i < 3; i++ )
    {
      code |= (p10[i] > 0);
      code <<= 1;
      code |= (p01[i] > 0);
      code <<= 1;
      code |= (p00[i] > 0);
      code <<= 1;
    };

  return code;
};



template<class Pt, class K>
 vcode_t vcode( Pt& n, const K& p00, const K& p10, const K& p01 )
{
#define xu (p10[0]-p00[0])
#define xv (p01[0]-p00[0])
#define yu (p10[1]-p00[1])
#define yv (p01[1]-p00[1])
#define zu (p10[2]-p00[2])
#define zv (p01[2]-p00[2])
  unsigned code = 0;

  // NX
  n[0]  = yu*zv-yv*zu;
  code |= (n[0]>0);
  code <<=1;
  code |= (n[0]<0);
  code <<=1;


  // NY
  n[1]  = zu*xv-xu*zv;
  code |= (n[1]>0);
  code <<=1;  
  code |= (n[1]<0);
  code <<=1;

  // NZ 
  n[2] = xu*yv-xv*yu;
  code |= (n[2]>0);
  code <<=1;
  code |= (n[2]<0);
  code <<=1;
  
  for ( unsigned i = 0; i < 3; i++ )
    {
      code |= (p10[i] > p00[i]);
      code <<= 1;
      code |= (p10[i] < p00[i]);
      code <<= 1;
      code |= (p01[i] > p00[i]);
      code <<= 1;
      code |= (p01[i] < p00[i]);
      code <<= 1;
    };

  return code;
};



template<class Pt, class K>
  vcode_t vcode( Pt& n00, Pt& n10, Pt& n01,  const K& p00, const K& p10, const K& p01 )
{
#define xu (p10[0]-p00[0])
#define xv (p01[0]-p00[0])
#define yu (p10[1]-p00[1])
#define yv (p01[1]-p00[1])
#define zu (p10[2]-p00[2])
#define zv (p01[2]-p00[2])
  unsigned code = 0;

  for ( unsigned i = 0; i < 3; i++ )
    {
      code |= (n10[i] > n00[i]-1e-6);
      code <<= 1;
      //      code |= (n10[i]-1e-6 < n00[i]);
      //      code <<= 1;
      code |= (n01[i] > n00[i]-1e-6);
      code <<= 1;
      //      code |= (n01[i]-1e-6 < n00[i]);
      //      code <<= 1;
    };
  
  for ( unsigned i = 0; i < 3; i++ )
    {
      code |= (p10[i] > p00[i]);
      code <<= 1;
      //      code |= (p10[i] < p00[i]);
      //      code <<= 1;
      //      code |= (p01[i] > p00[i]);
      //      code <<= 1;
      code |= (p01[i] < p00[i]);
      code <<= 1;
    };

  return code;
};


// a verifier !!
inline bool coherent_code( vcode_t c0, vcode_t c1 )
{
  return false;
  #define XY_MASK (X_MASK|Y_MASK|NZ_MASK)
  #define YZ_MASK (Y_MASK|Z_MASK|NX_MASK)
  #define ZX_MASK (Z_MASK|X_MASK|NY_MASK)
  
  #define XNZ_MASK (X_MASK|NZ_MASK)
  #define XNY_MASK (X_MASK|NY_MASK)
  #define YNZ_MASK (Y_MASK|NZ_MASK)
  #define YNX_MASK (Y_MASK|NX_MASK)
  #define ZNX_MASK (Z_MASK|NX_MASK)
  #define ZNY_MASK (Z_MASK|NY_MASK)
  
  unsigned denom = c0 & c1;
  #define CLASSIC
  #ifdef CLASSIC
  if ( denom )
    {
      return 
	( (denom & XY_MASK) == (c0 & XY_MASK) )  ||
	( (denom & YZ_MASK) == (c0 & YZ_MASK) )  ||
	( (denom & ZX_MASK) == (c0 & ZX_MASK) ); 
    };
  #else
  if ( denom )
    {
      return 
	((denom & X_MASK) && ( ((denom & XNZ_MASK) == (c0 & XNZ_MASK))    ||
			       ((denom & XNY_MASK) == (c0 & XNY_MASK)) )) ||
	((denom & Y_MASK) && ( ((denom & YNZ_MASK) == (c0 & YNZ_MASK))    ||
			       ((denom & YNX_MASK) == (c0 & YNX_MASK)) )) ||
	((denom & Z_MASK) && ( ((denom & ZNY_MASK) == (c0 & ZNY_MASK))    ||
			       ((denom & ZNX_MASK) == (c0 & ZNX_MASK)) ));
	
	
	

    }
  #endif
  return false;
  
};



#endif
