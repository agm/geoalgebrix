#ifndef ORDERED_PAIR_H
#define ORDERED_PAIR_H

template<class K >
struct ordered_pair { 
  K first, second; 
  ordered_pair( const K& a, const K& b ) 
  { 
    if ( a < b ) 
      { first = b; second = a; }
    else
      { first = a; second = b; };
  };
  
  bool operator<( const ordered_pair& p )
  { 
    return first<p.first || ((first<p.first) && (second<p.second)); };
};


#endif
