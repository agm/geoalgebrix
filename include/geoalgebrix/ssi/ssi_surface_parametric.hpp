# ifndef geoalgebrix_parametric_surface_ssi_hpp
# define geoalgebrix_parametric_surface_ssi_hpp
# include <geoalgebrix/parametric_surface.hpp>
# include <geoalgebrix/mesh.hpp>
# include <geoalgebrix/ssi/ssi_dsearch.hpp>
# include <geoalgebrix/ssi/ssiqtsl.hpp>

# define TMPL template<class K>
# define ParametricSurface geoalgebrix::parametric_surface<K>
# define Mesh           geoalgebrix::mesh<K>


namespace mmx {  namespace geoalgebrix {

TMPL Mesh*
intersection( ParametricSurface * srfa, ParametricSurface * srfb )
{
    typedef typename Mesh::Point         Point;
    typedef typename Mesh::Edge          Edge;
    typedef typename Mesh::PointIterator PointIterator;

    ssiqtsl<double> ssi( srfa, srfb, 100 );

    int nbv =ssi.spcs.size();

    Mesh * result = new Mesh(nbv);
    //    IntersectionResult * result = new IntersectionResult(IGLGood::E_LINE,ssi.spcs.size(),ssi.spcs.size());

    //std::copy(ssi.spcs.begin(),ssi.spcs.end(),(fxv<double,3>*)(result->glg->vertices));
    PointIterator p=result->begin();
    for ( int i = 0; i < nbv; i ++, p++ ) {
      p->setx(ssi.spcs[i][0]);
      p->sety(ssi.spcs[i][1]);
      p->setz(ssi.spcs[i][2]);
    }

    Point * p1, * p2;
    for ( int i = 0; i < nbv; i+=2) {
      p1= &result->vertex(i);
      p2= &result->vertex(i+1);
      result->push_edge(new Edge(p1,p2));
    }
    //	result->glg->indexes[i] = i;

    return result;
};

TMPL Mesh *
selfintersection(const ParametricSurface * surf)
{
    typedef typename Mesh::Point Point;
    typedef typename Mesh::Edge  Edge;
    typedef typename Mesh::PointIterator PointIterator;
    ssi::dsearch<double,K> ds(surf,200,200) ;
    
    int nbv, nbi, a, c, s ;
    
    for(nbv = 0, nbi = 0, c = 0 ; c < (int)ds.nbcurves ; c++)
    {
        s = ds.sizes[c]; 
        nbv += s;        
        nbi += (s-2)*2+2;
    }

    Mesh * result = new Mesh(nbv);
    //    SelfIntersectionResult * result = new SelfIntersectionResult(IGLGood::E_LINE, nbv, nbi, (int)ds.nbcurves);
    
    fxv<double,2> * prms = new fxv<double,2>[nbv];

    a = 0;
    for(c = 0 ; c < (int)ds.nbcurves ; c++) {
      //  result->lprm->params[c].size = (int)ds.sizes[c] ;
      //        result->rprm->params[c].size = (int)ds.sizes[c] ;
      //        result->lprm->params[c].prms = new fxv<double, 2>[(int)ds.sizes[c]] ;
      //        result->rprm->params[c].prms = new fxv<double, 2>[(int)ds.sizes[c]] ;
      for(int i = 0 ; i < (int)ds.sizes[c] ; i++, a++) {
	prms[a] = ds.lefts[c][i] ;
	//            result->lprm->params[c].prms[i] = ds.lefts[c][i] ;
	//            result->rprm->params[c].prms[i] = ds.rights[c][i] ;
      }
    }

    PointIterator p=result->begin();
    for(int i=0; i< nbv; i++,p++)
      surf->eval(*p,prms[i][0],prms[i][1]) ;

    //    int ci = 0 ;
    a = 0 ;  
    Point * p1, * p2;
    for(c = 0 ; c < (int)ds.nbcurves ; c++) {
        for(int i = 0 ; i < (int)ds.sizes[c]-1 ; i++, a++) {
	  //            result->glg->indexes[ci]   = a;
	  //            result->glg->indexes[ci+1] = a+1;
	  //            ci += 2;
	  p1= &result->vertex(a);
	  p2= &result->vertex(a+1);
	  result->push_edge(new Edge(p1,p2));
        }
        a++;
    }
    return result;
}
  } //  namespace geoalgebrix
} // namespace mmx

# undef TMPL
# undef Mesh
# undef ParametricSurface
# endif //geoalgebrix_parametric_surface_ssi_hpp
