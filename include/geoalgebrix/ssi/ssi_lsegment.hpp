#ifndef SYNAPS_SHAPE_GAIA_LSEGMENT_H
#define SYNAPS_SHAPE_GAIA_LSEGMENT_H

#include <geoalgebrix/ssi/ssi_sample.hpp>

#define TMPL template<class C, class V>
//#define ParametricSurface geoalgebrix::parametric_surface<double>

namespace mmx {
namespace ssi {

  struct mark_t
  {
    rid_t     head;
    coord_t   j;
    vcode_t code;
    mark_t  * next;
    inline coord_t a() { return j;             };
    inline coord_t b() { return (this+1)->a(); };
  };

  TMPL
  struct lsegment: sample<C,V>
  {
    typedef typename sample<C,V>::ParametricSurface ParametricSurface;
    typedef coord_t bounds_t[2];

    struct region_t
    {
      coord_t _umin, _umax;
      vcode_t _code;
      void  * data;
      bool operator<( const region_t& b ) const { return _umin < b._umin;         };
      coord_t min   ( coord_t u         ) const { return ((bounds_t*)data)[u][0]; };
      coord_t max   ( coord_t u         ) const { return ((bounds_t*)data)[u][1]; };
      coord_t umin  ()                    const { return _umin; };
      coord_t umax  ()                    const { return _umax; };
      inline bool inside( coord_t u, coord_t v )
      {
    if  ( u >= umin() && u < umax() )
      if ( v >= min(u) && v < max(u) ) return true;
    return false;
      };
      vcode_t code() const { return _code; };
      friend class lsegment_t;
    };

    mark_t   *  marks;
    unsigned    _size;
    unsigned *  lines;


    unsigned nmarks  () const { return _size; };
    void lines_changes();
  public:

    void rfree( region_t& r )
    {
      free((void*)(((bounds_t*)(r.data)) + r._umin));
    };



    mark_t * begin( unsigned i ) { return &(marks[lines[i]]); };
    mark_t * end  ( unsigned i ) { return &(marks[lines[i+1]-1]);};

    std::vector< region_t > regions;
    graph_t * grp;
    void pushm( vcode_t code, coord_t j );
    void pushr( coord_t i, mark_t * p );
    void addneighbors( mark_t * m0, mark_t * m1 );
    void promote( region_t& r );
    void convert_regions();
    void find_regions();
  public:
    double _lt;

    lsegment( const ParametricSurface * s , unsigned _w, unsigned _h );
    unsigned size() const { return regions.size(); };
    region_t& operator[]( unsigned i ) { return regions[i]; };

    ~lsegment();
  };

  TMPL
  void lsegment<C,V>::pushm(vcode_t code, coord_t j)
  {
    marks[_size].code = code;
    marks[_size].j    = j;
    marks[_size].head =-1;
    marks[_size].next = 0;
    _size++;
  }

  TMPL
  void lsegment<C,V>::addneighbors( mark_t * m0, mark_t * m1 )
  {
    if ( coherent_code( m0->code, m1->code )  )
      {
        grp->add_link(m0->head,m1->head);
        grp->add_link(m1->head,m0->head);
      };
  };

  TMPL
  void lsegment<C,V>::pushr( coord_t i, mark_t * p )
  {
    p->head =  regions.size();
    regions.push_back( region_t() );
    region_t * r =&(regions.back());
    r->_umin = i;
    r->_umax = 1;
    r->data = (void*)p;
    r->_code = p->code;
    grp->add_node();
  };

  TMPL
  void lsegment<C,V>::lines_changes()
  {
    int i,j;
    lines = (unsigned*)(malloc(sizeof(unsigned)*this->nrows()));
    unsigned max_marks = this->nrows()*this->ncols();
    while ( !(marks = (mark_t*)(malloc(sizeof(mark_t)*max_marks))) ) max_marks >>= 1;
    vector3 * ptr  = this->base();
    for ( i = 0; i < this->nrows()-1; i++ )
      {
        lines[i] = this->nmarks();
        vcode_t code = vcode(*ptr,*(ptr+this->ncols()),*(ptr+1));
        pushm(code,0);
        for (  j = 0; j < this->ncols()-1; j++, ptr++ )
          {
            vcode_t tmp = vcode(*ptr,*(ptr+this->ncols()),*(ptr+1));
            if ( tmp != code )
              { code = tmp; this->pushm(code,j); };
          };
        pushm(code,this->ncols()-1);
        ptr++;
      };
    lines[this->nrows()-1] = nmarks();
    marks = (mark_t*)realloc(marks,sizeof(mark_t)*nmarks());
  };

  TMPL
  void lsegment<C,V>::find_regions()
  {
    mark_t * p;
    for ( p = begin(0); p != end(0); p++ )
      pushr(0,p);
    for ( p = begin(0)+1; p != end(0); p++ )
      addneighbors(p,p-1);

    int ncolsTemp = this->ncols();
    mark_t **neighbors;
    neighbors = new mark_t*[ncolsTemp];
    unsigned n_neigh;
    coord_t i;
#define INTERVALTEST ( std::abs(std::min(down->b(),up->b())-std::max(down->a(),up->a())) > 1 )
    for ( i = 0; i < this->nrows()-2; i++ )
      {
        mark_t * startup = begin(i);
        for ( mark_t * down = begin(i+1); down != end(i+1); down++ )
          {
            while ( startup->b() <= down->a() ) startup++;
            mark_t * up = startup;
            n_neigh = 0;
            while( down->b() > up->a() )
              if ( (up->next == 0) && ( up->code == down->code ) && INTERVALTEST)
                {
                  regions[up->head]._umax++;
                  up->next = down;
                  down->head = up->head, up++;
                  break;
                }
              else
                neighbors[n_neigh++] = up, up++;

              if ( down->head == -1 ) pushr( i+1, down );

              for ( unsigned k = 0; k < n_neigh; k ++ ) addneighbors(down,neighbors[k]);
              while( down->b() > up->a() ){ addneighbors(down,up); up++;}
          };
      };
  };

  TMPL
  void lsegment<C,V>::promote( region_t& r )
  {
    mark_t     * mpointer;
    bounds_t * bpointer;
    mpointer = (mark_t*)(r.data);
    r.data   = (void*)(malloc(sizeof(bounds_t)*(r._umax)));
    bpointer = (bounds_t*)(r.data);

    for ( unsigned i = 0; i < r._umax; i++ )
      {
        bpointer[i][0] = mpointer->a();
        bpointer[i][1] = mpointer->b();
        mpointer = mpointer->next;
      };

    r._umax += r._umin;
    r.data  = (void*)(((bounds_t*)(r.data))-r._umin);
  };

  TMPL
  void lsegment<C,V>::convert_regions()
    {
      for ( unsigned i = 0; i < regions.size(); i++ )
        promote( regions[i] );
    };



  TMPL
  lsegment<C,V>::lsegment( const ParametricSurface * s , unsigned _w, unsigned _h ) : sample<C,V>(s,_w,_h)
  {
    //_st = time();
    _size = 0;
    lines_changes  ();
    grp = new graph_t();
    find_regions   ();
    convert_regions();
    delete[] marks;
    delete[] lines;
    //    std::cout << "l = " << (time()-_st) << std::endl;
  };

  TMPL
  lsegment<C,V>::~lsegment()
  {
    delete grp;
    for ( unsigned i = 0; i < regions.size(); i++ )
      rfree(regions[i]);
  };

} //namespace geoalgebrix_ssi
} //namespace mmx

# undef ParametricSurface
# endif
