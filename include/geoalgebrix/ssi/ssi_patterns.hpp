#ifndef PATTERNS_H
#define PATTERNS_H

// common loops: used to allow global optimization of code by the changes of 
// these routines for example an instantiation of MMX/SSE specific routines 
// for some arithmetic tasks in double or float

namespace patterns {
  /*
#include "arith.h"
#include "arith-shortcuts.h"
#include "copy.h"
#include "fill.h"
#include "io.h"
#include "dsp1d.h"
#include "algo1d-shortcuts.h"
#include "dsp2d.h"
#include "pckvectorop.h"
  */
}

#endif
