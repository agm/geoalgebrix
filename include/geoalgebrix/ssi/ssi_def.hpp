#ifndef SYNAPS_SHAPE_GAIA_H
#define SYNAPS_SHAPE_GAIA_H

#include <geoalgebrix/geoalgebrix.hpp>
#include <geoalgebrix/parametric_surface.hpp>

#include <geoalgebrix/fxv.hpp>
#include <geoalgebrix/ssi/ssi_base_aabb.hpp>
#include <geoalgebrix/ssi/ssi_igraph.hpp>
#include <geoalgebrix/ssi/ssi_vcode.hpp>
#include <geoalgebrix/mesh.hpp>

namespace mmx {

static const int cfsz_ = 0;
static const int cflq_ = 1;
static const int cfex_ = 2;
static const int cfbg_ = 3;

static const int cfhsz = 1 /* cfsz_*/ + 1 /* cflq_  */ + 1/* cfex_ */;

namespace geoalgebrix {

struct ssi_def {};

template<class C>
struct use<ssi_def,C> {
    typedef parametric_surface<C> ParametricSurface;
    typedef mesh<C> Mesh;

    static void sample(const ParametricSurface* s, C* lp, unsigned m, unsigned n, C* u, C* v) {
        s->sample(lp,m,n,u,v);
    }

    static void eval(ParametricSurface* s, C* r, const C* u, unsigned n) {
        for (unsigned i=0; i<n;i++) {
            s->eval(r[0],r[1],r[2],u[0],u[1]);
            r+=3;
            u+=2;
        }
    }
};

} // namespace geoalgebrix

namespace ssi
{
    typedef Interval<double,3>         interval;
    typedef fxv<double,2>              vector2;
    typedef fxv<double,3>              vector3;
    typedef aabb<double,3>             aabb3;
    typedef fxv<double,4>              vector4;
    typedef igraph   graph_t;
    typedef vector3                    point3;
    typedef vector2                    point2;
    typedef unsigned short             coord_t;
    typedef short                      rid_t;
    template <class C, class V> struct qnode;
    //typedef qnode<C,V>* qtree;

    // dsearch

    inline void fill_quad_box( vcode_t c, aabb3& box,
                               const vector3& p0, const vector3& p1,
                               const vector3& p2, const vector3& p3  ) { fill(box,p0,p1,p2,p3); };
}

} //namespace mmx

#endif
