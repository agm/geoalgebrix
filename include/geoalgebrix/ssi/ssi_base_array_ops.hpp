#ifndef SYNAPS_SHAPE_ARRAY_OPS_H
#define SYNAPS_SHAPE_ARRAY_OPS_H

#include <iostream>
#include <stdlib.h>
#include <realroot/texp_ringof.hpp>

namespace mmx {

template<typename A, typename B, unsigned N> inline
void add( A (&a)[N], const B (&b)[N] ) 
{ for ( unsigned i = 0; i < N; a[i] += b[i], i++ ); };

template<typename A, typename B, typename C, unsigned N> inline
void add( A (&a)[N], const B (&b)[N], const C (&c)[N] )  
{ for ( unsigned i = 0; i < N; a[i] = b[i]+c[i], i ++); };

template<typename A, typename B, unsigned N> inline 
void sub( A (&a)[N], const B (&b)[N] ) 
{ for ( unsigned i = 0; i < N; a[i] -= b[i], i++ ); };

template<typename A, typename B, typename C, unsigned N> inline 
void sub( A (&a)[N], const B (&b)[N], const C (&c)[N] ) 
{ for ( unsigned i = 0; i < N; a[i] = b[i]-c[i], i ++); };

template<typename A, typename C, unsigned N> inline
void scmul( A (&a)[N], const C& c )
{ for ( unsigned i = 0; i < N; a[i] *= c, i ++ ); };

template<typename A, typename B, unsigned N, typename W> inline
void scmul( A (&a)[N], const B (&b)[N], const W& c )
{ for ( unsigned i = 0; i < N; a[i] = b[i]*c, i ++ ); };

template<typename A, typename B, unsigned N> inline
void scdiv( A (&a)[N], const B& s )
{ for ( unsigned i = 0; i < N; a[i] /= s, i ++  ); };

template<typename A, typename B, unsigned N, typename W> inline
void scdiv( A (&a)[N], const B (&b)[N], const W& c )
{ for ( unsigned i = 0; i < N; a[i] = b[i]/c, i ++ ); };

template<typename A, typename B, unsigned N> inline
typename texp::ringof<A,B>::T  dotprod( const A (&a)[N], const B (&b)[N] )
{
  typename texp::ringof<A,B>::T res(0);
  for ( unsigned i = 0; i < N; res += a[i]*b[i], i ++ );
  return res;
};

template<class C, unsigned N> inline 
C norm( const C (&v)[N], int p )
{
  C a(0);
  for ( unsigned i = 0; i < N;  a += pow(v[i],p), i ++ );
  return pow(a,C(1)/p);
};

template<class A, class B, unsigned N> inline
void init( const A (&v)[N], const B& k )
{ for ( unsigned i = 0; i < N; v[i++] = k ); };

template<class A, class B, unsigned N> inline
void copy( const A (&a)[N], const B (&b)[N] )
{
  for ( unsigned i = 0; i < N; i ++ ) a[i] = b[i];
};

template<class C, unsigned N> inline
void print( std::ostream & o, const C (&a)[N] )
{
  o << "{";
  for ( unsigned i = 0; i < N-1; i ++ ) o << a[i] << ",";
  o << a[N-1];
  o << "}";
};

template<class C, unsigned N> inline
void urand( C (&v)[N], const C & a, const C & b )
{
  for ( int i = 0; i < N; i ++ )
    {
      v[i] = ((double)rand()/RAND_MAX)*(b-a)+a ;
    };
};

template<class C, unsigned N> inline
bool eqxual( const C (&a)[N], const C (&b)[N] )
{
  for ( int i = 0; i < N; i ++ ) if ( a[i] != b[i] ) return false;
  return true;
};


template<class C, unsigned N> inline
const C & squared_distance( const C (&a)[N], const C (&b)[N] )
{
  C s(0);
  C sv;
  for ( int i = 0; i < N; i ++ ) { sv = b[i]-a[i]; s += sv*sv; };
  return s;
};

template<class C, unsigned N> inline
const C & distance( const C (&a)[N], const C (&b)[N] )
{
  return sqrt(squared_distance(a,b));
};
} //namespace mmx

#endif
