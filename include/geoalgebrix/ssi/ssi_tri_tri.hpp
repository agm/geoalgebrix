#ifndef geoalgebrix_ssi_tritri_hpp
#define geoalgebrix_ssi_tritri_hpp

namespace geom
{
template<class Point3>
bool intersectp_coplanar_triangles
(const Point3& N,
 const Point3& V0, const Point3& V1, const Point3& V2,
 const Point3& U0, const Point3& U1, const Point3& U2 )
{

    Point3 A;
    short i0, i1;

    A[0] = std::abs(N[0]);
    A[1] = std::abs(N[1]);
    A[2] = std::abs(N[2]);
    
    if (A[0] > A[1])
    {
        if (A[0] > A[2])
        {
            i0 = 1;
            i1 = 2;
        }
        else
        {
            i0 = 0;
            i1 = 1;
        }
    }
    else
    {
        if (A[2] > A[1])
        {
            i0 = 0;
            i1 = 1;
        }
        else
        {
            i0 = 0;
            i1 = 2;
        }
    }

    {
        typename Point3::value_type Ax, Ay, Bx, By, Cx, Cy, e, d, f;
        Ax = V1[i0] - V0[i0];
        Ay = V1[i1] - V0[i1];
        Bx = U0[i0] - U1[i0];
        By = U0[i1] - U1[i1];
        Cx = V0[i0] - U0[i0];
        Cy = V0[i1] - U0[i1];
        f = Ay * Bx - Ax * By;
        d = By * Cx - Bx * Cy;
        if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f))
        {
            e = Ax * Cy - Ay * Cx;
            if (f > 0)
            {
                if (e >= 0 && e <= f)
                    return true;
            }
            else
            {
                if (e <= 0 && e >= f)
                    return true;
            }
        };
        Bx = U1[i0] - U2[i0];
        By = U1[i1] - U2[i1];
        Cx = V0[i0] - U1[i0];
        Cy = V0[i1] - U1[i1];
        f = Ay * Bx - Ax * By;
        d = By * Cx - Bx * Cy;
        if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f))
        {
            e = Ax * Cy - Ay * Cx;
            if (f > 0)
            {
                if (e >= 0 && e <= f)
                    return true;
            }
            else
            {
                if (e <= 0 && e >= f)
                    return true;
            }
        };
        Bx = U2[i0] - U0[i0];
        By = U2[i1] - U0[i1];
        Cx = V0[i0] - U2[i0];
        Cy = V0[i1] - U2[i1];
        f = Ay * Bx - Ax * By;
        d = By * Cx - Bx * Cy;
        if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f))
        {
            e = Ax * Cy - Ay * Cx;
            if (f > 0)
            {
                if (e >= 0 && e <= f)
                    return true;
            }
            else
            {
                if (e <= 0 && e >= f)
                    return true;
            }
        };
    };
    {
        typename Point3::value_type Ax, Ay, Bx, By, Cx, Cy, e, d, f;
        Ax = V2[i0] - V1[i0];
        Ay = V2[i1] - V1[i1];
        Bx = U0[i0] - U1[i0];
        By = U0[i1] - U1[i1];
        Cx = V1[i0] - U0[i0];
        Cy = V1[i1] - U0[i1];
        f = Ay * Bx - Ax * By;
        d = By * Cx - Bx * Cy;
        if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f))
        {
            e = Ax * Cy - Ay * Cx;
            if (f > 0)
            {
                if (e >= 0 && e <= f)
                    return true;
            }
            else
            {
                if (e <= 0 && e >= f)
                    return true;
            }
        };
        Bx = U1[i0] - U2[i0];
        By = U1[i1] - U2[i1];
        Cx = V1[i0] - U1[i0];
        Cy = V1[i1] - U1[i1];
        f = Ay * Bx - Ax * By;
        d = By * Cx - Bx * Cy;
        if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f))
        {
            e = Ax * Cy - Ay * Cx;
            if (f > 0)
            {
                if (e >= 0 && e <= f)
                    return true;
            }
            else
            {
                if (e <= 0 && e >= f)
                    return true;
            }
        };
        Bx = U2[i0] - U0[i0];
        By = U2[i1] - U0[i1];
        Cx = V1[i0] - U2[i0];
        Cy = V1[i1] - U2[i1];
        f = Ay * Bx - Ax * By;
        d = By * Cx - Bx * Cy;
        if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f))
        {
            e = Ax * Cy - Ay * Cx;
            if (f > 0)
            {
                if (e >= 0 && e <= f)
                    return true;
            }
            else
            {
                if (e <= 0 && e >= f)
                    return true;
            }
        };
    };
    {
        typename Point3::value_type Ax, Ay, Bx, By, Cx, Cy, e, d, f;
        Ax = V0[i0] - V2[i0];
        Ay = V0[i1] - V2[i1];
        Bx = U0[i0] - U1[i0];
        By = U0[i1] - U1[i1];
        Cx = V2[i0] - U0[i0];
        Cy = V2[i1] - U0[i1];
        f = Ay * Bx - Ax * By;
        d = By * Cx - Bx * Cy;
        if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f))
        {
            e = Ax * Cy - Ay * Cx;
            if (f > 0)
            {
                if (e >= 0 && e <= f)
                    return true;
            }
            else
            {
                if (e <= 0 && e >= f)
                    return true;
            }
        };
        Bx = U1[i0] - U2[i0];
        By = U1[i1] - U2[i1];
        Cx = V2[i0] - U1[i0];
        Cy = V2[i1] - U1[i1];
        f = Ay * Bx - Ax * By;
        d = By * Cx - Bx * Cy;
        if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f))
        {
            e = Ax * Cy - Ay * Cx;
            if (f > 0)
            {
                if (e >= 0 && e <= f)
                    return true;
            }
            else
            {
                if (e <= 0 && e >= f)
                    return true;
            }
        };
        Bx = U2[i0] - U0[i0];
        By = U2[i1] - U0[i1];
        Cx = V2[i0] - U2[i0];
        Cy = V2[i1] - U2[i1];
        f = Ay * Bx - Ax * By;
        d = By * Cx - Bx * Cy;
        if ((f > 0 && d >= 0 && d <= f) || (f < 0 && d <= 0 && d >= f))
        {
            e = Ax * Cy - Ay * Cx;
            if (f > 0)
            {
                if (e >= 0 && e <= f)
                    return true;
            }
            else
            {
                if (e <= 0 && e >= f)
                    return true;
            }
        };
    };


    {
        typename Point3::value_type a, b, c, d0, d1, d2;
        a = U1[i1] - U0[i1];
        b = -(U1[i0] - U0[i0]);
        c = -a * U0[i0] - b * U0[i1];
        d0 = a * V0[i0] + b * V0[i1] + c;
        a = U2[i1] - U1[i1];
        b = -(U2[i0] - U1[i0]);
        c = -a * U1[i0] - b * U1[i1];
        d1 = a * V0[i0] + b * V0[i1] + c;
        a = U0[i1] - U2[i1];
        b = -(U0[i0] - U2[i0]);
        c = -a * U2[i0] - b * U2[i1];
        d2 = a * V0[i0] + b * V0[i1] + c;
        if (d0 * d1 > 0.0)
        {
            if (d0 * d2 > 0.0)
                return true;
        }
    };
    {
        typename Point3::value_type a, b, c, d0, d1, d2;
        a = V1[i1] - V0[i1];
        b = -(V1[i0] - V0[i0]);
        c = -a * V0[i0] - b * V0[i1];
        d0 = a * U0[i0] + b * U0[i1] + c;
        a = V2[i1] - V1[i1];
        b = -(V2[i0] - V1[i0]);
        c = -a * V1[i0] - b * V1[i1];
        d1 = a * U0[i0] + b * U0[i1] + c;
        a = V0[i1] - V2[i1];
        b = -(V0[i0] - V2[i0]);
        c = -a * V2[i0] - b * V2[i1];
        d2 = a * U0[i0] + b * U0[i1] + c;
        if (d0 * d1 > 0.0)
        {
            if (d0 * d2 > 0.0)
                return true;
        }
    };

    return false;
}

template<class Point3>
bool intersectp_triangles3_div
(const Point3& V0, const Point3& V1, const Point3& V2,
 const Point3& U0, const Point3& U1, const Point3& U2,
 const typename Point3::value_type& prec)
{
    Point3 E1, E2;
    Point3 N1, N2;
    typename Point3::value_type d1, d2;
    typename Point3::value_type du0, du1, du2, dv0, dv1, dv2;
    Point3 D;
    typename Point3::value_type isect1[2], isect2[2];
    typename Point3::value_type du0du1, du0du2, dv0dv1, dv0dv2;
    short index;
    typename Point3::value_type vp0, vp1, vp2;
    typename Point3::value_type up0, up1, up2;
    typename Point3::value_type b, c, max;


    E1[0] = V1[0] - V0[0];
    E1[1] = V1[1] - V0[1];
    E1[2] = V1[2] - V0[2];;
    E2[0] = V2[0] - V0[0];
    E2[1] = V2[1] - V0[1];
    E2[2] = V2[2] - V0[2];;
    N1[0] = E1[1] * E2[2] - E1[2] * E2[1];
    N1[1] = E1[2] * E2[0] - E1[0] * E2[2];
    N1[2] = E1[0] * E2[1] - E1[1] * E2[0];;
    d1 = -(N1[0] * V0[0] + N1[1] * V0[1] + N1[2] * V0[2]);



    du0 = (N1[0] * U0[0] + N1[1] * U0[1] + N1[2] * U0[2]) + d1;
    du1 = (N1[0] * U1[0] + N1[1] * U1[1] + N1[2] * U1[2]) + d1;
    du2 = (N1[0] * U2[0] + N1[1] * U2[1] + N1[2] * U2[2]) + d1;



    if (std::abs (du0) < prec)
        du0 = 0.0;
    if (std::abs (du1) < prec)
        du1 = 0.0;
    if (std::abs (du2) < prec)
        du2 = 0.0;

    du0du1 = du0 * du1;
    du0du2 = du0 * du2;

    if (du0du1 > 0.0f && du0du2 > 0.0f)
        return false;


    E1[0] = U1[0] - U0[0];
    E1[1] = U1[1] - U0[1];
    E1[2] = U1[2] - U0[2];;
    E2[0] = U2[0] - U0[0];
    E2[1] = U2[1] - U0[1];
    E2[2] = U2[2] - U0[2];;
    N2[0] = E1[1] * E2[2] - E1[2] * E2[1];
    N2[1] = E1[2] * E2[0] - E1[0] * E2[2];
    N2[2] = E1[0] * E2[1] - E1[1] * E2[0];;
    d2 = -(N2[0] * U0[0] + N2[1] * U0[1] + N2[2] * U0[2]);



    dv0 = (N2[0] * V0[0] + N2[1] * V0[1] + N2[2] * V0[2]) + d2;
    dv1 = (N2[0] * V1[0] + N2[1] * V1[1] + N2[2] * V1[2]) + d2;
    dv2 = (N2[0] * V2[0] + N2[1] * V2[1] + N2[2] * V2[2]) + d2;


    if (std::abs (dv0) < prec)
        dv0 = 0.0;
    if (std::abs (dv1) < prec)
        dv1 = 0.0;
    if (std::abs (dv2) < prec)
        dv2 = 0.0;


    dv0dv1 = dv0 * dv1;
    dv0dv2 = dv0 * dv2;

    if (dv0dv1 > 0.0f && dv0dv2 > 0.0f)
        return false;


    D[0] = N1[1] * N2[2] - N1[2] * N2[1];
    D[1] = N1[2] * N2[0] - N1[0] * N2[2];
    D[2] = N1[0] * N2[1] - N1[1] * N2[0];;


    max = std::abs (D[0]);
    index = 0;
    b = std::abs (D[1]);
    c = std::abs (D[2]);
    if (b > max)
        max = b, index = 1;
    if (c > max)
        max = c, index = 2;


    vp0 = V0[index];
    vp1 = V1[index];
    vp2 = V2[index];
    
    up0 = U0[index];
    up1 = U1[index];
    up2 = U2[index];


    if (dv0dv1 > 0.0f)
    {
        isect1[0] = vp2 + (vp0 - vp2) * dv2 / (dv2 - dv0);
        isect1[1] = vp2 + (vp1 - vp2) * dv2 / (dv2 - dv1);;
    }
    else if (dv0dv2 > 0.0f)
    {
        isect1[0] = vp1 + (vp0 - vp1) * dv1 / (dv1 - dv0);
        isect1[1] = vp1 + (vp2 - vp1) * dv1 / (dv1 - dv2);;
    }
    else if (dv1 * dv2 > 0.0f || dv0 != 0.0f)
    {
        isect1[0] = vp0 + (vp1 - vp0) * dv0 / (dv0 - dv1);
        isect1[1] = vp0 + (vp2 - vp0) * dv0 / (dv0 - dv2);;
    }
    else if (dv1 != 0.0f)
    {
        isect1[0] = vp1 + (vp0 - vp1) * dv1 / (dv1 - dv0);
        isect1[1] = vp1 + (vp2 - vp1) * dv1 / (dv1 - dv2);;
    }
    else if (dv2 != 0.0f)
    {
        isect1[0] = vp2 + (vp0 - vp2) * dv2 / (dv2 - dv0);
        isect1[1] = vp2 + (vp1 - vp2) * dv2 / (dv2 - dv1);;
    }
    else
    {
        return intersectp_coplanar_triangles (N1, V0, V1, V2, U0, U1, U2);
    };


    if (du0du1 > 0.0f)
    {
        isect2[0] = up2 + (up0 - up2) * du2 / (du2 - du0);
        isect2[1] = up2 + (up1 - up2) * du2 / (du2 - du1);;
    }
    else if (du0du2 > 0.0f)
    {
        isect2[0] = up1 + (up0 - up1) * du1 / (du1 - du0);
        isect2[1] = up1 + (up2 - up1) * du1 / (du1 - du2);;
    }
    else if (du1 * du2 > 0.0f || du0 != 0.0f)
    {
        isect2[0] = up0 + (up1 - up0) * du0 / (du0 - du1);
        isect2[1] = up0 + (up2 - up0) * du0 / (du0 - du2);;
    }
    else if (du1 != 0.0f)
    {
        isect2[0] = up1 + (up0 - up1) * du1 / (du1 - du0);
        isect2[1] = up1 + (up2 - up1) * du1 / (du1 - du2);;
    }
    else if (du2 != 0.0f)
    {
        isect2[0] = up2 + (up0 - up2) * du2 / (du2 - du0);
        isect2[1] = up2 + (up1 - up2) * du2 / (du2 - du1);;
    }
    else
    {
        return intersectp_coplanar_triangles (N1, V0, V1, V2, U0, U1, U2);
    };

    if (isect1[0] > isect1[1])
    {
        typename Point3::value_type c;
        c = isect1[0];
        isect1[0] = isect1[1];
        isect1[1] = c;
    };
    if (isect2[0] > isect2[1])
    {
        typename Point3::value_type c;
        c = isect2[0];
        isect2[0] = isect2[1];
        isect2[1] = c;
    };

    if (isect1[1] < isect2[0] || isect2[1] < isect1[0])
        return false;
    return true;
}




template<class Point3>
bool intersectp_triangle3
( const Point3& V0, const Point3& V1, const Point3& V2,
  const Point3& U0, const Point3& U1, const Point3& U2,
  const typename Point3::value_type& prec )
{
    Point3 E1, E2;
    Point3 N1, N2;
    typename Point3::value_type d1, d2;
    typename Point3::value_type du0, du1, du2, dv0, dv1, dv2;
    Point3 D;
    typename Point3::value_type isect1[2], isect2[2];
    typename Point3::value_type du0du1, du0du2, dv0dv1, dv0dv2;
    short index;
    typename Point3::value_type vp0, vp1, vp2;
    typename Point3::value_type up0, up1, up2;
    typename Point3::value_type bb, cc, max;
    typename Point3::value_type a, b, c, x0, x1;
    typename Point3::value_type d, e, f, y0, y1;
    typename Point3::value_type xx, yy, xxyy, tmp;


    E1[0] = V1[0] - V0[0];
    E1[1] = V1[1] - V0[1];
    E1[2] = V1[2] - V0[2];;
    E2[0] = V2[0] - V0[0];
    E2[1] = V2[1] - V0[1];
    E2[2] = V2[2] - V0[2];;
    N1[0] = E1[1] * E2[2] - E1[2] * E2[1];
    N1[1] = E1[2] * E2[0] - E1[0] * E2[2];
    N1[2] = E1[0] * E2[1] - E1[1] * E2[0];;
    d1 = -(N1[0] * V0[0] + N1[1] * V0[1] + N1[2] * V0[2]);



    du0 = (N1[0] * U0[0] + N1[1] * U0[1] + N1[2] * U0[2]) + d1;
    du1 = (N1[0] * U1[0] + N1[1] * U1[1] + N1[2] * U1[2]) + d1;
    du2 = (N1[0] * U2[0] + N1[1] * U2[1] + N1[2] * U2[2]) + d1;



    if (((typename Point3::value_type) std::abs (du0)) < prec)
        du0 = 0.0;
    if (((typename Point3::value_type) std::abs (du1)) < prec)
        du1 = 0.0;
    if (((typename Point3::value_type) std::abs (du2)) < prec)
        du2 = 0.0;

    du0du1 = du0 * du1;
    du0du2 = du0 * du2;

    if (du0du1 > 0.0f && du0du2 > 0.0f)
        return false;


    E1[0] = U1[0] - U0[0];
    E1[1] = U1[1] - U0[1];
    E1[2] = U1[2] - U0[2];;
    E2[0] = U2[0] - U0[0];
    E2[1] = U2[1] - U0[1];
    E2[2] = U2[2] - U0[2];;
    N2[0] = E1[1] * E2[2] - E1[2] * E2[1];
    N2[1] = E1[2] * E2[0] - E1[0] * E2[2];
    N2[2] = E1[0] * E2[1] - E1[1] * E2[0];;
    d2 = -(N2[0] * U0[0] + N2[1] * U0[1] + N2[2] * U0[2]);



    dv0 = (N2[0] * V0[0] + N2[1] * V0[1] + N2[2] * V0[2]) + d2;
    dv1 = (N2[0] * V1[0] + N2[1] * V1[1] + N2[2] * V1[2]) + d2;
    dv2 = (N2[0] * V2[0] + N2[1] * V2[1] + N2[2] * V2[2]) + d2;


    if (((typename Point3::value_type) std::abs (dv0)) < prec)
        dv0 = 0.0;
    if (((typename Point3::value_type) std::abs (dv1)) < prec)
        dv1 = 0.0;
    if (((typename Point3::value_type) std::abs (dv2)) < prec)
        dv2 = 0.0;


    dv0dv1 = dv0 * dv1;
    dv0dv2 = dv0 * dv2;

    if (dv0dv1 > 0.0f && dv0dv2 > 0.0f)
        return false;


    D[0] = N1[1] * N2[2] - N1[2] * N2[1];
    D[1] = N1[2] * N2[0] - N1[0] * N2[2];
    D[2] = N1[0] * N2[1] - N1[1] * N2[0];;


    max = (typename Point3::value_type) ((typename Point3::value_type) std::abs (D[0]));
    index = 0;
    bb = (typename Point3::value_type) ((typename Point3::value_type) std::abs (D[1]));
    cc = (typename Point3::value_type) ((typename Point3::value_type) std::abs (D[2]));
    if (bb > max)
        max = bb, index = 1;
    if (cc > max)
        max = cc, index = 2;


    vp0 = V0[index];
    vp1 = V1[index];
    vp2 = V2[index];

    up0 = U0[index];
    up1 = U1[index];
    up2 = U2[index];


    {
        if (dv0dv1 > 0.0f)
        {
            a = vp2;
            b = (vp0 - vp2) * dv2;
            c = (vp1 - vp2) * dv2;
            x0 = dv2 - dv0;
            x1 = dv2 - dv1;
        }
        else if (dv0dv2 > 0.0f)
        {
            a = vp1;
            b = (vp0 - vp1) * dv1;
            c = (vp2 - vp1) * dv1;
            x0 = dv1 - dv0;
            x1 = dv1 - dv2;
        }
        else if (dv1 * dv2 > 0.0f || dv0 != 0.0f)
        {
            a = vp0;
            b = (vp1 - vp0) * dv0;
            c = (vp2 - vp0) * dv0;
            x0 = dv0 - dv1;
            x1 = dv0 - dv2;
        }
        else if (dv1 != 0.0f)
        {
            a = vp1;
            b = (vp0 - vp1) * dv1;
            c = (vp2 - vp1) * dv1;
            x0 = dv1 - dv0;
            x1 = dv1 - dv2;
        }
        else if (dv2 != 0.0f)
        {
            a = vp2;
            b = (vp0 - vp2) * dv2;
            c = (vp1 - vp2) * dv2;
            x0 = dv2 - dv0;
            x1 = dv2 - dv1;
        }
        else
        {
            return intersectp_coplanar_triangles (N1, V0, V1, V2, U0, U1, U2);
        }
    };


    {
        if (du0du1 > 0.0f)
        {
            d = up2;
            e = (up0 - up2) * du2;
            f = (up1 - up2) * du2;
            y0 = du2 - du0;
            y1 = du2 - du1;
        }
        else if (du0du2 > 0.0f)
        {
            d = up1;
            e = (up0 - up1) * du1;
            f = (up2 - up1) * du1;
            y0 = du1 - du0;
            y1 = du1 - du2;
        }
        else if (du1 * du2 > 0.0f || du0 != 0.0f)
        {
            d = up0;
            e = (up1 - up0) * du0;
            f = (up2 - up0) * du0;
            y0 = du0 - du1;
            y1 = du0 - du2;
        }
        else if (du1 != 0.0f)
        {
            d = up1;
            e = (up0 - up1) * du1;
            f = (up2 - up1) * du1;
            y0 = du1 - du0;
            y1 = du1 - du2;
        }
        else if (du2 != 0.0f)
        {
            d = up2;
            e = (up0 - up2) * du2;
            f = (up1 - up2) * du2;
            y0 = du2 - du0;
            y1 = du2 - du1;
        }
        else
        {
            return intersectp_coplanar_triangles (N1, V0, V1, V2, U0, U1, U2);
        }
    };

    xx = x0 * x1;
    yy = y0 * y1;
    xxyy = xx * yy;

    tmp = a * xxyy;
    isect1[0] = tmp + b * x1 * yy;
    isect1[1] = tmp + c * x0 * yy;

    tmp = d * xxyy;
    isect2[0] = tmp + e * xx * y1;
    isect2[1] = tmp + f * xx * y0;

    if (isect1[0] > isect1[1])
    {
        typename Point3::value_type c;
        c = isect1[0];
        isect1[0] = isect1[1];
        isect1[1] = c;
    };
    if (isect2[0] > isect2[1])
    {
        typename Point3::value_type c;
        c = isect2[0];
        isect2[0] = isect2[1];
        isect2[1] = c;
    };

    if (isect1[1] < isect2[0] || isect2[1] < isect1[0])
        return false;
    return true;
}

template<class Point3>
inline void
isect2 (const Point3& VTX0, const Point3& VTX1, const Point3& VTX2,
        typename Point3::value_type VV0, typename Point3::value_type VV1, typename Point3::value_type VV2,
        typename Point3::value_type D0, typename Point3::value_type D1, typename Point3::value_type D2, typename Point3::value_type& isect0, typename Point3::value_type& isect1,
        Point3& isectpoint0, Point3& isectpoint1)
{
    typename Point3::value_type tmp = D0 / (D0 - D1);
    Point3 diff;
    isect0 = VV0 + (VV1 - VV0) * tmp;
    diff[0] = VTX1[0] - VTX0[0];
    diff[1] = VTX1[1] - VTX0[1];
    diff[2] = VTX1[2] - VTX0[2];;
    diff[0] = tmp * diff[0];
    diff[1] = tmp * diff[1];
    diff[2] = tmp * diff[2];;
    isectpoint0[0] = diff[0] + VTX0[0];
    isectpoint0[1] = diff[1] + VTX0[1];
    isectpoint0[2] = diff[2] + VTX0[2];;
    tmp = D0 / (D0 - D2);
    isect1 = VV0 + (VV2 - VV0) * tmp;
    diff[0] = VTX2[0] - VTX0[0];
    diff[1] = VTX2[1] - VTX0[1];
    diff[2] = VTX2[2] - VTX0[2];;
    diff[0] = tmp * diff[0];
    diff[1] = tmp * diff[1];
    diff[2] = tmp * diff[2];;
    isectpoint1[0] = VTX0[0] + diff[0];
    isectpoint1[1] = VTX0[1] + diff[1];
    isectpoint1[2] = VTX0[2] + diff[2];;
}

template<class Point3>
inline bool
compute_intervals_isectline
(const Point3& VERT0, const Point3& VERT1, const Point3& VERT2,
 const typename Point3::value_type& VV0, const typename Point3::value_type& VV1, const typename Point3::value_type& VV2, const typename Point3::value_type& D0,
 const typename Point3::value_type& D1, const typename Point3::value_type& D2, const typename Point3::value_type& D0D1, const typename Point3::value_type& D0D2,
 typename Point3::value_type& isect0, typename Point3::value_type& isect1,
 Point3& isectpoint0, Point3& isectpoint1)
{
    if (D0D1 > 0.0f)
    {
        isect2 (VERT2, VERT0, VERT1, VV2, VV0, VV1, D2, D0, D1, isect0, isect1,
                isectpoint0, isectpoint1);
    }
    else if (D0D2 > 0.0f)
    {

        isect2 (VERT1, VERT0, VERT2, VV1, VV0, VV2, D1, D0, D2, isect0, isect1,
                isectpoint0, isectpoint1);
    }
    else if (D1 * D2 > 0.0f || D0 != 0.0f)
    {

        isect2 (VERT0, VERT1, VERT2, VV0, VV1, VV2, D0, D1, D2, isect0, isect1,
                isectpoint0, isectpoint1);
    }
    else if (D1 != 0.0f)
    {
        isect2 (VERT1, VERT0, VERT2, VV1, VV0, VV2, D1, D0, D2, isect0, isect1,
                isectpoint0, isectpoint1);
    }
    else if (D2 != 0.0f)
    {
        isect2 (VERT2, VERT0, VERT1, VV2, VV0, VV1, D2, D0, D1, isect0, isect1,
                isectpoint0, isectpoint1);
    }
    else
    {

        return true;
    }
    return false;
}

template<class Point3>
bool intersectp_triangles3_isegment (bool& coplanar,
                                     Point3& isectpt1, Point3& isectpt2,
                                     const Point3& V0, const Point3& V1, const Point3& V2,
                                     const Point3& U0, const Point3& U1, const Point3& U2,
                                     const typename Point3::value_type& prec )
{
    coplanar = false;
    Point3 E1, E2;
    Point3 N1, N2;
    typename Point3::value_type d1, d2;
    typename Point3::value_type du0, du1, du2, dv0, dv1, dv2;
    Point3 D;
    typename Point3::value_type isect1[2], isect2[2];
    isect1[0]=0;isect1[1]=0;
    isect2[0]=0;isect2[1]=0;
    Point3 isectpointA1;
    Point3 isectpointA2;
//    Point3 isectpointB1;
//    Point3 isectpointB2;
    typename Point3::value_type du0du1, du0du2, dv0dv1, dv0dv2;
    short index;
    typename Point3::value_type vp0, vp1, vp2;
    typename Point3::value_type up0, up1, up2;
    typename Point3::value_type b, c, max;
    //typename Point3::value_type tmp, diff[3];
    int smallest1, smallest2;


    E1[0] = V1[0] - V0[0];
    E1[1] = V1[1] - V0[1];
    E1[2] = V1[2] - V0[2];;
    E2[0] = V2[0] - V0[0];
    E2[1] = V2[1] - V0[1];
    E2[2] = V2[2] - V0[2];;
    N1[0] = E1[1] * E2[2] - E1[2] * E2[1];
    N1[1] = E1[2] * E2[0] - E1[0] * E2[2];
    N1[2] = E1[0] * E2[1] - E1[1] * E2[0];;
    d1 = -(N1[0] * V0[0] + N1[1] * V0[1] + N1[2] * V0[2]);



    du0 = (N1[0] * U0[0] + N1[1] * U0[1] + N1[2] * U0[2]) + d1;
    du1 = (N1[0] * U1[0] + N1[1] * U1[1] + N1[2] * U1[2]) + d1;
    du2 = (N1[0] * U2[0] + N1[1] * U2[1] + N1[2] * U2[2]) + d1;

    
    using namespace std;
    if (std::abs (du0) < prec)
        du0 = 0.0;
    if (std::abs (du1) < prec)
        du1 = 0.0;
    if (std::abs (du2) < prec)
        du2 = 0.0;

    du0du1 = du0 * du1;
    du0du2 = du0 * du2;

    if (du0du1 > 0.0f && du0du2 > 0.0f)
        return false;


    E1[0] = U1[0] - U0[0];
    E1[1] = U1[1] - U0[1];
    E1[2] = U1[2] - U0[2];;
    E2[0] = U2[0] - U0[0];
    E2[1] = U2[1] - U0[1];
    E2[2] = U2[2] - U0[2];;
    N2[0] = E1[1] * E2[2] - E1[2] * E2[1];
    N2[1] = E1[2] * E2[0] - E1[0] * E2[2];
    N2[2] = E1[0] * E2[1] - E1[1] * E2[0];;
    d2 = -(N2[0] * U0[0] + N2[1] * U0[1] + N2[2] * U0[2]);



    dv0 = (N2[0] * V0[0] + N2[1] * V0[1] + N2[2] * V0[2]) + d2;
    dv1 = (N2[0] * V1[0] + N2[1] * V1[1] + N2[2] * V1[2]) + d2;
    dv2 = (N2[0] * V2[0] + N2[1] * V2[1] + N2[2] * V2[2]) + d2;


    if (std::abs (dv0) < prec)
        dv0 = 0.0;
    if (std::abs (dv1) < prec)
        dv1 = 0.0;
    if (std::abs (dv2) < prec)
        dv2 = 0.0;


    dv0dv1 = dv0 * dv1;
    dv0dv2 = dv0 * dv2;

    if (dv0dv1 > 0.0f && dv0dv2 > 0.0f)
        return false;


    D[0] = N1[1] * N2[2] - N1[2] * N2[1];
    D[1] = N1[2] * N2[0] - N1[0] * N2[2];
    D[2] = N1[0] * N2[1] - N1[1] * N2[0];;


    max = std::abs (D[0]);
    index = 0;
    b = std::abs (D[1]);
    c = std::abs (D[2]);
    if (b > max)
        max = b, index = 1;
    if (c > max)
        max = c, index = 2;


    vp0 = V0[index];
    vp1 = V1[index];
    vp2 = V2[index];

    up0 = U0[index];
    up1 = U1[index];
    up2 = U2[index];


    coplanar =
            compute_intervals_isectline (V0, V1, V2, vp0, vp1, vp2, dv0, dv1, dv2,
                                         dv0dv1, dv0dv2, isect1[0], isect1[1],
                                         isectpointA1, isectpointA2);
    if (coplanar)
        return intersectp_coplanar_triangles (N1, V0, V1, V2, U0, U1, U2);

    
    Point3 isectpointB1;isectpointB1[0]=0;isectpointB1[1]=0;isectpointB1[2]=0;
    Point3 isectpointB2;isectpointB2[0]=0;isectpointB2[1]=0;isectpointB2[2]=0;
    compute_intervals_isectline (U0, U1, U2, up0, up1, up2, du0, du1, du2,
                                 du0du1, du0du2, isect2[0], isect2[1],
                                 isectpointB1, isectpointB2);
    
    if (isect1[0] > isect1[1])
    {
        typename Point3::value_type c;
        c = isect1[0];
        isect1[0] = isect1[1];
        isect1[1] = c;
        smallest1 = 1;
    }
    else
        smallest1 = 0;;
    if (isect2[0] > isect2[1])
    {
        typename Point3::value_type c;
        c = isect2[0];
        isect2[0] = isect2[1];
        isect2[1] = c;
        smallest2 = 1;
    }
    else
        smallest2 = 0;;

    if (isect1[1] < isect2[0] || isect2[1] < isect1[0])
        return false;



    if (isect2[0] < isect1[0])
    {
        if (smallest1 == 0)
        {
            isectpt1[0] = isectpointA1[0];
            isectpt1[1] = isectpointA1[1];
            isectpt1[2] = isectpointA1[2];;
        }
        else
        {
            isectpt1[0] = isectpointA2[0];
            isectpt1[1] = isectpointA2[1];
            isectpt1[2] = isectpointA2[2];;
        }

        if (isect2[1] < isect1[1])
        {
            if (smallest2 == 0)
            {
                isectpt2[0] = isectpointB2[0];
                isectpt2[1] = isectpointB2[1];
                isectpt2[2] = isectpointB2[2];;
            }
            else
            {
                isectpt2[0] = isectpointB1[0];
                isectpt2[1] = isectpointB1[1];
                isectpt2[2] = isectpointB1[2];;
            }
        }
        else
        {
            if (smallest1 == 0)
            {
                isectpt2[0] = isectpointA2[0];
                isectpt2[1] = isectpointA2[1];
                isectpt2[2] = isectpointA2[2];;
            }
            else
            {
                isectpt2[0] = isectpointA1[0];
                isectpt2[1] = isectpointA1[1];
                isectpt2[2] = isectpointA1[2];;
            }
        }
    }
    else
    {
        if (smallest2 == 0)
        {
            isectpt1[0] = isectpointB1[0];
            isectpt1[1] = isectpointB1[1];
            isectpt1[2] = isectpointB1[2];;
        }
        else
        {
            isectpt1[0] = isectpointB2[0];
            isectpt1[1] = isectpointB2[1];
            isectpt1[2] = isectpointB2[2];;
        }

        if (isect2[1] > isect1[1])
        {
            if (smallest1 == 0)
            {
                isectpt2[0] = isectpointA2[0];
                isectpt2[1] = isectpointA2[1];
                isectpt2[2] = isectpointA2[2];;
            }
            else
            {
                isectpt2[0] = isectpointA1[0];
                isectpt2[1] = isectpointA1[1];
                isectpt2[2] = isectpointA1[2];;
            }
        }
        else
        {
            if (smallest2 == 0)
            {
                isectpt2[0] = isectpointB2[0];
                isectpt2[1] = isectpointB2[1];
                isectpt2[2] = isectpointB2[2];;
            }
            else
            {
                isectpt2[0] = isectpointB1[0];
                isectpt2[1] = isectpointB1[1];
                isectpt2[2] = isectpointB1[2];;
            }
        }
    }
    return true;
}
};

#endif //geoalgebrix_ssi_tritri_hpp
