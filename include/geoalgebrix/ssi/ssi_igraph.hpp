#ifndef ID_GRAPH_H
#define ID_GRAPH_H
#include <vector>
#include <set>
#include <list>
#include <map>
#include <iostream>
struct igraph
{
  typedef std::set<unsigned>     nset_t;
  typedef std::vector<nset_t>  graph_t;
  typedef nset_t::iterator  l_iterator; 
  typedef graph_t::iterator n_iterator;
  
  graph_t nodes;
  
  unsigned add_node()
  { nodes.push_back( nset_t() ) ;
    return nodes.size() - 1; };
  
  void add_link( unsigned a, unsigned b )
  {  nodes[a].insert( b ); };

  nset_t& operator[]( unsigned i ) 
  { return nodes[i]; };
  
  const 
  nset_t& operator[]( unsigned i ) const 
  { return nodes[i]; };
  
  bool neighbors( unsigned a, unsigned b )
  {
    return (nodes[a].find(b) != nodes[a].end()) || (nodes[b].find(a) != nodes[b].end());
  };
  void dump( std::ostream& o )
    {
      for ( unsigned i = 0; i < nodes.size(); i++ )
		{
	  o << i << " :\n";
	  for ( nset_t::iterator it = nodes[i].begin(); it != nodes[i].end(); it++ )
	    o << *it << " ";
	  o << "\n";
	};
   };
};



#endif
