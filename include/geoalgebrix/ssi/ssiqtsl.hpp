# ifndef SHAPE_SSIQTSL_HPP
# define SHAPE_SSIQTSL_HPP
# include <cmath>
# include <vector>
# include <fstream>
# include <geoalgebrix/ssi/ssi_def.hpp>
# include <geoalgebrix/ssi/ssiqts.hpp>
# include <geoalgebrix/ssi/ssiqts_tri_tri.hpp>

# define SSIQTS ssiqts<C,V>
# define SSIQTSL ssiqtsl<C,V>
# define TMPL template<class C, class V>

//# define ParametricSurface geoalgebrix::parametric_surface<double>

namespace mmx {

template<class C, class V= geoalgebrix::default_env>
struct ssiqtsl : SSIQTS
{
    typedef typename SSIQTS::vector3 vector3;
    typedef typename geoalgebrix::use<geoalgebrix::ssi_def,C,V>::ParametricSurface  ParametricSurface;
    typedef fxv<double,2> vector2;

    std::vector< vector2 > lpts;
    std::vector< vector2 > rpts;
    std::vector< vector3 > spcs;
    std::vector< double  > errs;
    std::vector< vector3 > spcsorig;
    double errmax;
    void gmvdump();
    void gpdump();

    ssiqtsl (ParametricSurface * srfa, ParametricSurface * srfb, int n );

    static void space2prm(vector2 & pa, vector2 & pb,  const vector3& sa,
                          const vector3& sb,
                          const vector3& base,
                          const vector3& pu,
                          const vector3& pv );
    static void merge( typename SSIQTS::aabb3 & box, const typename SSIQTS::aabb3& a, const typename SSIQTS::aabb3 & b );
    static void solve_cf( vector3 ** qpa, vector3 ** qpb, int a, int b, SSIQTSL & ssi  );
    static void sample_curve3d( ParametricSurface * srfa, ParametricSurface * srfb, SSIQTSL& ssi );
    static void solve( SSIQTSL & ssi );
};


void POLYLINE( std::ostream& o, double * src, int nv, float * colors, int nc )
{
    o << "{ = VECT\n";
    o << 1 << " " << nv << " " << nc << std::endl;
    o << nv << std::endl;
    o << nc << std::endl;
    for ( int i = 0; i < nv; i++, src += 3 )
        o << src[0] << " " << src[1] << " " << src[2] << std::endl;
    for ( int i = 0; i < nc; i++ )
        o << colors[i*4] << " " << colors[i*4+1] << " " << colors[i*4+2] << " " << colors[i*4+3] << std::endl;
    o << "}\n";
};

void UVSMP( std::ostream& o, double * src, unsigned m, unsigned n )
{
#define _ij_(i,j) (i)*n+j
    o << " { = MESH\n";
    o << m << " " << n << std::endl;
    for ( unsigned i = 0; i < m*n; i ++ )
        o << (src+i*3)[0] << " " <<  (src+i*3)[1] << " " << (src+i*3)[2] << std::endl;
    o << " }\n";
};

#define __up__   0
#define __down__ 1
/* return the up triangle of quad q */
#define __up__triangle__(q)         q[0],q[3],q[2]
/* return the down triangle of quad q */
#define __down__triangle__(q)       q[0],q[1],q[2]
/* return the parametric plane of up triangle of q */
#define __up__param_triangle__(q)   q[3],q[2],q[0]
/* return the parametric plane of down triangle of q */
#define __down__param_triangle__(q) q[1],q[0],q[2]
#define __convert_order__(i,point) { point[(i+1)%2] = 1.0-point[(i+1)%2]; }

/* check for the intersection beetween {UP,DOWN}/{UP,DOWN} triangles */
#define __ssiqtsl__triangle_triangle_case__(trig0,trig1)                            \
{                                                                          \
    intersection =                                                          \
    intersectp_triangles3_isegment                                     \
    ( coplanar, seg[0], seg[1],                                         \
    trig0##triangle__(qa),                                            \
    trig1##triangle__(qb), double(1e-9) );                            \
    if ( intersection )                                                \
{                                                                      \
    if ( !coplanar )                                                     \
{                                                                  \
    SSIQTSL::space2prm(/* points in parameter space */                        \
    seg0[0], seg0[1],                                       \
    /* corresponding points in R3 */                        \
    seg[0],  seg[1],                                        \
    /* parametric plane = base + 2 vectors  */              \
    trig0##param_triangle__(qa)                             \
    );                                                      \
    SSIQTSL::space2prm( seg1[0], seg1[1],                                     \
    seg[0], seg[1],                                       \
    trig1##param_triangle__(qb) );                         \
    /* retrieve the correct offset coordinates in quad */            \
    __convert_order__(trig0,seg0[0]);                                \
    __convert_order__(trig0,seg0[1]);                                \
    __convert_order__(trig1,seg1[0]);                                \
    __convert_order__(trig1,seg1[1]);                                \
    \
    double * left_uvals = ssi.smpa->m_uvals;                         \
    double * left_vvals = ssi.smpa->m_vvals;                         \
    double * righ_uvals = ssi.smpb->m_uvals;                         \
    double * righ_vvals = ssi.smpb->m_vvals;                         \
    for ( int i = 0; i < 2; i ++ )                                   \
{                                                              \
    seg0[i][0] = left_uvals[a/ssi.m] + seg0[i][0] * (left_uvals[a/ssi.m+1]-left_uvals[a/ssi.m]);\
    seg0[i][1] = left_vvals[a%ssi.m] + seg0[i][1] * (left_vvals[a%ssi.m+1]-left_vvals[a%ssi.m]);\
};                                                             \
    for ( int i = 0; i < 2; i ++ )                                   \
{                                                              \
    seg1[i][0] = righ_uvals[b/ssi.m] + seg1[i][0] * (righ_uvals[b/ssi.m+1]-righ_uvals[b/ssi.m]);\
    seg1[i][1] = righ_vvals[b%ssi.m] + seg1[i][1] * (righ_vvals[b%ssi.m+1]-righ_vvals[b%ssi.m]);\
};                                                             \
    ssi.lpts.push_back(seg0[0]);                                     \
    ssi.lpts.push_back(seg0[1]);                                     \
    ssi.rpts.push_back(seg1[0]);                                     \
    ssi.rpts.push_back(seg1[1]);                                     \
}                                                                  \
}                                                                      \
}

TMPL
//static
void SSIQTSL::space2prm( vector2 & pa,
                         vector2 & pb,
                         const vector3& sa,
                         const vector3& sb,
                         const vector3& base,
                         const vector3& pu,
                         const vector3& pv )
{
    /* T(u,v) = base + u*bu +v*bv
     =>
     spc[0] - base[0] = delta[0]  = / bu[0] bv[0]\  / u \
     spc[1] - base[1] = delta[1]  = | bu[1] bv[1]|  |   |
     spc[2] - base[2] = delta[2]  = \ bu[2] bv[2]/  \ v /
  */
    typename SSIQTSL::vector3 bu;
    for ( int i = 0; i < 3; i ++ ) bu[i] = pu[i]-base[i];
    typename SSIQTSL::vector3 bv;
    for ( int i = 0; i < 3; i ++ ) bv[i] = pv[i]-base[i];
    double muu, mvv, muv;
    muu = 0;
    for ( int i = 0; i < 3; i ++ ) muu += bu[i]*bu[i];
    mvv = 0;
    for ( int i = 0; i < 3; i ++ ) mvv += bv[i]*bv[i];
    muv = 0;
    for ( int i = 0; i < 3; i ++ ) muv += bu[i]*bv[i];
    double detm = muu*mvv - muv*muv;
    typename SSIQTSL::vector3 delta;
    double x, y;
    for ( int k = 0; k < 3; k ++ ) delta[k] = sa[k]-base[k];
    x = 0;
    for ( int k = 0; k < 3; k ++ ) x += bu[k]*delta[k];
    y = 0;
    for ( int k = 0; k < 3; k ++ ) y += bv[k]*delta[k];
    pa[0] = (mvv * x - muv * y)/detm;
    pa[1] = (muu * y - muv * x)/detm;
    for ( int k = 0; k < 3; k ++ ) delta[k] = sb[k]-base[k];
    x = 0;
    for ( int k = 0; k < 3; k ++ ) x += bu[k]*delta[k];
    y = 0;
    for ( int k = 0; k < 3; k ++ ) y += bv[k]*delta[k];
    pb[0] = (mvv * x - muv * y)/detm;
    pb[1] = (muu * y - muv * x)/detm;
};

TMPL
//static
void SSIQTSL::merge( typename SSIQTS::aabb3 & box, const typename SSIQTS::aabb3& a, const typename SSIQTS::aabb3 & b )
{
    for ( unsigned i = 0; i < 3; i ++ )
    {
        box[i][0] = std::min(a[i][0],b[i][0]);
        box[i][1] = std::max(a[i][1],b[i][1]);
    };
};

TMPL
//static
void SSIQTSL::solve_cf( typename SSIQTSL::vector3 ** qpa, typename SSIQTSL::vector3 ** qpb, int a, int b, SSIQTSL & ssi  )
{
    // scaling
    typename SSIQTSL::aabb3 zoom;
    double mx = 0;
    merge(zoom,ssi.boxa[a],ssi.boxb[b]);
    /*
  std::cout << "boxa\n";
  for ( int i = 0; i < 3; i ++ )
    std::cout << boxa[a][i][0] << " " << boxa[a][i][1] << std::endl;
  std::cout << "boxb\n";
  for ( int i = 0; i < 3; i ++ )
    std::cout << boxb[b][i][0] << " " << boxb[b][i][1] << std::endl;
  */
    for ( int k = 0; k < 3; k ++ ) {
        zoom[k][1] -= zoom[k][0];
        if ( zoom[k][1] > mx ) mx = zoom[k][1];
    };

    double sc = double(1.0)/mx;
    typename SSIQTSL::vector3 qa[4];
    typename SSIQTSL::vector3 qb[4];

    for ( int i = 0; i < 4; i ++ )
        for ( int k = 0; k < 3; k ++ )
        {
            qa[i][k] = ((*(qpa[i]))[k] - zoom[k][0]) * sc;
            qb[i][k] = ((*(qpb[i]))[k] - zoom[k][0]) * sc;
        };

    typename SSIQTSL::vector3 seg [2];
    typename SSIQTSL::vector2 seg0[2];
    typename SSIQTSL::vector2 seg1[2];
    bool coplanar = false;
    bool intersection;
    __ssiqtsl__triangle_triangle_case__(__up__,__up__);
    __ssiqtsl__triangle_triangle_case__(__up__,__down__);
    __ssiqtsl__triangle_triangle_case__(__down__,__up__);
    __ssiqtsl__triangle_triangle_case__(__down__,__down__);
};


TMPL
void SSIQTSL::solve( SSIQTSL & ssi )
{
    typename SSIQTSL::vector3 * sleft  = (typename SSIQTSL::vector3*)(&(ssi.smpa->m_svals)[0]);
    typename SSIQTSL::vector3 * sright = (typename SSIQTSL::vector3*)(&(ssi.smpb->m_svals)[0]);


    for( int * cf = ssi.m_bcf; cf != ssi.m_ecf; cf += *cf )
    {
        if ( cf[cfbg_] != -1 )
        {
            int lu = cf[cflq_]/ssi.m;
            int lv = cf[cflq_]%ssi.m;
            typename SSIQTSL::vector3 * qpl[4];
            qpl[0] = sleft  + lu*(ssi.m+1)+lv;
            qpl[1] = sleft  + (lu+1)*(ssi.m+1)+lv;
            qpl[2] = qpl[1] + 1;
            qpl[3] = qpl[0] + 1;
            int * rpq = cf+cfbg_;
            while ( *rpq != -1 && rpq != cf+*cf )
            {
                int ru = (*rpq/ssi.m);
                int rv = (*rpq%ssi.m);
                typename SSIQTSL::vector3 * qpr[4];
                qpr[0] = sright + ru*(ssi.m+1)+rv;
                qpr[1] = sright + (ru+1)*(ssi.m+1)+rv;
                qpr[2] = qpr[1] + 1;
                qpr[3] = qpr[0] + 1;
                solve_cf(qpl,qpr,cf[cflq_],*rpq,ssi);
                rpq++;
            };
        }
    };
};


TMPL
void SSIQTSL::gpdump()
{
    std::ofstream gpl("SSIQTSL-L.dat");
    std::ofstream gpr("SSIQTSL-R.dat");

    for ( unsigned i = 0; i < lpts.size(); i ++ )
        gpl << lpts[i][0] << " " << lpts[i][1] << std::endl;
    for ( unsigned i = 0; i < rpts.size(); i ++ )
        gpr << rpts[i][0] << " " << rpts[i][1] << std::endl;
};

TMPL
void SSIQTSL::gmvdump()
{
    std::ofstream o("SSIQTSL.gmv");
    o << "LIST\n";
    float colora[] = { 0.0, 0.0, 1.0 };
    float colorb[] = { 0.0, 1.0, 0.0 };
    o << "appearance {\n material{\n diffuse " <<
         colora[0] << " " <<  colora[1] << " " << colora[2] << "\n";
    o << "}\n}\n";
    UVSMP(o,&(this->smpa->m_svals[0][0]),this->m+1,this->m+1);
    o << "appearance {\n material{\n diffuse " <<
         colorb[0] << " " <<  colorb[1] << " " << colorb[2] << "\n";
    o << "}\n}\n";
    UVSMP(o,&(this->smpb->m_svals[0][0]),this->m+1,this->m+1);
    float color[] = { 1.0, 0.0, 0.0, 1.0 };
    for ( unsigned i = 0; i < spcs.size(); i += 2 )
    {
        color[1] = double(i % 2);
        color[2] = double((i+1)%2);
        POLYLINE(o,&(spcs[i][0]),2,color,1);
    };
};

TMPL
void SSIQTSL::sample_curve3d( ParametricSurface * srfa,
                              ParametricSurface * srfb, SSIQTSL& ssi )
{
    //  std::cout << " left = " << ssi.lpts.size() << std::endl;
    int nl = ssi.lpts.size();
    //  std::cout << " right = " << ssi.rpts.size() << std::endl;
    ssi.spcs.resize( nl );
    std::vector<typename SSIQTSL::vector3> srfb_samples(nl);
    geoalgebrix::use<geoalgebrix::ssi_def,C,V>::eval(srfa, (double*)(&(ssi.spcs[0])), (const double*)(&ssi.lpts[0]), ssi.lpts.size() );
    geoalgebrix::use<geoalgebrix::ssi_def,C,V>::eval(srfb, (double*)(&(srfb_samples[0])), (const double*)(&ssi.rpts[0]), ssi.rpts.size() );
    //  std::vector<double> errs;
    //  ssi.errs.resize( ssi.lpts.size() );
    ssi.errmax = 0;
    for ( unsigned i = 0; i < ssi.lpts.size(); i ++ )
    {

        //      int lk0, lk1, stat;
        //      SSIQTSL::vector3 pa,pb;
        typename SSIQTSL::vector3 delta;
        double s = 0;
        //      std::cout << ssi.spcs[i] << std::endl;
        //      std::cout << srfb_samples[i] << std::endl;
        for ( int k =0; k < 3; k ++ )
            delta[k] = ssi.spcs[i][k]-srfb_samples[i][k];
        s = max_abs( delta );
        for ( int k = 0; k < 3; k ++ )
            ssi.spcs[i][k] = (ssi.spcs[i][k]+srfb_samples[i][k])/2;
        if ( s > ssi.errmax ) ssi.errmax = s;
    };
};

TMPL
SSIQTSL::ssiqtsl( ParametricSurface *  srfa,
                  ParametricSurface *  srfb,
                  int n ) : SSIQTS(srfa,srfb,n,1,1)
{
    solve(*this);
    sample_curve3d( srfa, srfb, *this );
    //  std::cout << errmax << std::endl;
    // gmvdump();
};


// gmv::stream & operator<<( gmv::stream & o,
// 			  const ssi_linear & ssi )
// {
//   float colora[] = { 0.0, 0.0, 1.0 };
//   float colorb[] = { 0.0, 1.0, 0.0 };
//   o.matcolor(colora);
//   o << (*ssi.smpa);
//   o << (*ssi.smpb);
//   float color[] = { 1.0, 0.0, 0.0, 1.0 };
//   for ( int i = 0; i < ssi.spcs.size(); i += 2 )
//     {
//       //    color[1] = double(i % 2);
//       //      color[2] = double((i+1)%2);
//       o.polyline( &(ssi.spcs[i][0]), 2, color, 1);
//     };
//   return o;
// };

} //namespace mmx

# undef TMPL
# undef SSIQTS
# undef SSIQTSL
# endif


