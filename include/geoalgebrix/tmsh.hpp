/**********************************************************************
 * PACKAGE  : geoalgebrix
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once

#include <vector>
#include <geoalgebrix/mdebug.hpp>
#include <geoalgebrix/tmsh_cell.hpp>
#include <geoalgebrix/tmsh_vertex.hpp>

//====================================================================
namespace mmx {
//--------------------------------------------------------------------
template <class M, class P>
void set_middle(M& m, const P& a, const P& b) {
    //mdebug()<<"middle"<<M::dim;
    for(unsigned i=0;i<M::dim;i++) m[i]=(a[i]+b[i])/2.;
}
//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================

#include <geoalgebrix/tmsh_controler2.hpp>
#include <geoalgebrix/tmsh_controler3.hpp>

//--------------------------------------------------------------------
#undef SELF
#undef TMPL

