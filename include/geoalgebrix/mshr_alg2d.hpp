/**********************************************************************
 * PACKAGE  : geoalgebrix
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once

#include <geoalgebrix/foreach.hpp>
#include <geoalgebrix/bounding_box.hpp>
#include <geoalgebrix/tmsh_cell.hpp>
#include <geoalgebrix/tmsh.hpp>
#include <realroot/polynomial_bernstein.hpp>
#include <realroot/polynomial_sparse.hpp>
#include <geoalgebrix/solver_cell2d.hpp>
#include <geoalgebrix/polygonizer2d_region.hpp>
#include <geoalgebrix/mesher.hpp>

//====================================================================
namespace mmx {
//--------------------------------------------------------------------
struct mshr_alg2d_cell: public tmsh::cell<2> {

    typedef double               C;
    typedef mshr_alg2d_cell     Cell;
    //typedef polynomial< Interval<C>, with<Bernstein> > Polynomial;
    typedef polynomial< C, with<Bernstein> > Polynomial;
    //typedef polynomial< C, with<Sparse,DegRevLex> > Polynomial;

public:

    mshr_alg2d_cell(): cell<2>(), m_polynomial() {}
    mshr_alg2d_cell(const char*, double xmin, double xmax, double ymin, double ymax);
    mshr_alg2d_cell(const mshr_alg2d_cell& cel):
        cell<2>(cel), m_polynomial(cel.m_polynomial) {}

    void subdivide(int v, mshr_alg2d_cell &left, mshr_alg2d_cell &right);

    const Polynomial& equation(void) const { return m_polynomial; }

    Polynomial       m_polynomial;
};
//--------------------------------------------------------------------
mshr_alg2d_cell::mshr_alg2d_cell(const char* s, double xmin, double xmax, double ymin, double ymax):
    cell<2>()
{
    std::vector<double> bx(4);
    bx[0]=xmin;
    bx[1]=xmax;
    bx[2]=ymin;
    bx[3]=ymax;

    polynomial< C, with<Sparse,DegRevLex> > p(s);
    let::assign(m_polynomial.rep(), tensor::bernstein<C>(p.rep(),bx));

}

//--------------------------------------------------------------------
void mshr_alg2d_cell::subdivide(int v, mshr_alg2d_cell & left, mshr_alg2d_cell & right) {

    tensor::split(left.m_polynomial, right.m_polynomial, v);
}

//--------------------------------------------------------------------
#define TMPL template<class CELL, class VERTEX>
#define CTRL mshr_alg2d_controler<CELL,VERTEX>
//--------------------------------------------------------------------
template<class CELL, class VERTEX>
struct mshr_alg2d_controler: tmsh::controler<2,CELL,VERTEX>
{
    typedef double    C;
    typedef CELL      Cell;
    typedef VERTEX    Vertex;
    typedef typename Cell::Polynomial EdgePolynomial;
    typedef typename Cell::Polynomial Polynomial;
    typedef polynomial< C, with<Sparse,DegRevLex> > SparsePolynomial;

    typedef slvr::solver_cell2d<CTRL> Solver;

    mshr_alg2d_controler() {}

    Cell* init_cell(const char* s, double xmin, double xmax, double ymin, double ymax);

    Cell* init_cell(const SparsePolynomial& p, double xmin, double xmax, double ymin, double ymax);


    int  regularity(Cell* cl);

    void process_regular (Cell* cl);
    void process_singular(Cell* cl);

    void subdivide(int v, Cell* cl, Cell*& left, Cell*& right) {
        left  = new Cell(*cl);
        right = new Cell(*cl);
        this->tmsh::controler<2,CELL,VERTEX>::subdivide(v,*cl,*left,*right);
        cl->subdivide(v,*left,*right);
    }

    void tag_corner(Cell* cl);
    int  tag_of_middle(Cell*, int i0, int i1);

    void boundary_point(Cell* cl);
    void boundary_point_edge(Cell* cl, int v, int s);

    void center_point(Cell* cl);

    std::vector<Cell*> m_regular;
    std::vector<Cell*> m_singular;

};

//--------------------------------------------------------------------
TMPL
CELL* CTRL::init_cell(const SparsePolynomial& pol, double xmin, double xmax, double ymin, double ymax) {

    Vertex p[4] = {
        Vertex(xmin,ymin),
        Vertex(xmax,ymin),
        Vertex(xmin,ymax),
        Vertex(xmax,ymax)
    };

    this->tmsh::controler<2,CELL,VERTEX>::init(p);

    CELL* res= new CELL;
    std::vector<double> bx(4);
    bx[0]=xmin;
    bx[1]=xmax;
    bx[2]=ymin;
    bx[3]=ymax;

    //mdebug()<<"Polynomial"<<p;
    let::assign(res->m_polynomial.rep(), tensor::bernstein<C>(pol.rep(),bx));
    //mdebug()<<"Polynomial"<<res->m_polynomial.rep();

    return res;
}


TMPL
CELL* CTRL::init_cell(const char* s, double xmin, double xmax, double ymin, double ymax) {

    SparsePolynomial pol(s);
    return init_cell(pol,xmin,xmax,ymin,ymax);

}

TMPL
int CTRL::regularity(Cell *cl) {
    if(!has_sign_variation(cl->m_polynomial)) {
        //mdebug()<<"no sign variation";
        if(cl->m_polynomial.rep()[0]>=0) {
            cl->regularity(INSIDE);
            return INSIDE;
        } else {
            cl->regularity(OUTSIDE);
            return OUTSIDE;
        }
    }

    typename CELL::Polynomial dy = mmx::diff(cl->m_polynomial,1);
    if(!has_sign_variation(dy)) {
        cl->regularity(BOUNDARY_REGULAR1);
        return BOUNDARY_REGULAR1;
    }

    typename CELL::Polynomial dx = mmx::diff(cl->m_polynomial,0);
    if(!has_sign_variation(dx)) {
        cl->regularity(BOUNDARY_REGULAR2);
        return BOUNDARY_REGULAR2;
    }

    // Case where both derivatives change signs
    cl->regularity(UNKNOWN);
    return UNKNOWN;

}

TMPL
int CTRL::tag_of_middle(Cell * cl, int i0, int i1) {
    double x = (this->vertex(i0)[0]+this->vertex(i1)[0])/2.0,
           y = (this->vertex(i0)[1]+this->vertex(i1)[1])/2.0;
    x = (x -this->xmin(*cl))/(this->xmax(*cl)- this->xmin(*cl));
    y = (y -this->ymin(*cl))/(this->ymax(*cl)- this->ymin(*cl));
    double v = cl->m_polynomial(x,y);
    if (v > 0)
        return INSIDE;
    else
        return OUTSIDE;
}

TMPL
void CTRL::tag_corner(CELL *cl) {

    //mdebug()<<"vertex_regularity";
    typename Cell::Polynomial f;
    tensor::face(f,cl->m_polynomial,1,0);

    //if(lower(cl->m_polynomial.rep()[0])>0) {
    if (lower(f[0])>0) {
        this->vertex(cl->idx(0)).tag(INSIDE);
        //mdebug()<<"vertex 0 >0"<<this->vertex(cl->idx(0)).regularity();
    }

    //if(lower(cl->m_polynomial.rep()[sz[0]-1])>0) {
    if (lower(f[f.size()-1])>0) {
        this->vertex(cl->idx(1)).tag(INSIDE);
        //mdebug()<<"vertex 1 >0"<<this->vertex(cl->idx(1)).regularity();
    }

    tensor::face(f,cl->m_polynomial,1,1);

    //if(lower(cl->m_polynomial.rep()[sz[1]-sz[0]])>0) {
    if (lower(f[0])>0) {
        this->vertex(cl->idx(2)).tag(INSIDE);
        //mdebug()<<"vertex 2 >0"<<this->vertex(cl->idx(2)).regularity();
    }
    //if(lower(cl->m_polynomial.rep()[sz[1]-1])>0) {
    if (lower(f[f.size()-1])>0) {
        this->vertex(cl->idx(3)).tag(INSIDE);
        //mdebug()<<"vertex 3 >0"<<this->vertex(cl->idx(3)).regularity();
    }

}

TMPL
void CTRL::boundary_point(Cell* cl) {
    Solver slv(this);
    slv.edge_point(cl,cl->m_polynomial);
    for(unsigned k=0; k< slv.nb_sol(); k++) {
        cl->add_boundary_point(slv.solution(k), slv.mu(k));
    }
}

TMPL
void CTRL::center_point(Cell* cl) {
    int idx = this->insert_vertex(VERTEX( (this->xmin(*cl)+this->xmax(*cl))/2.,
                                          (this->ymin(*cl)+this->ymax(*cl))/2. ));
    cl->add_center(idx);
    //mdebug()<<"dx, dy sign variation";
}

TMPL
void CTRL::process_regular(Cell* cl) {
    int r = cl->regularity();
    if( r == OUTSIDE) {
        for(unsigned k=0; k<4; k++) {
            this->vertex(cl->idx(k)).tag(OUTSIDE);
        }
    } else {
        this->tag_corner(cl);
        this->boundary_point(cl);
    }

    m_regular.push_back(cl);
    // mdebug() << "boundary" << cl->m_boundary.size();
}

TMPL
void CTRL::process_singular(Cell* cl) {

    // Compute the singular points in the Cell;
    std::vector<typename Cell::Polynomial> system;
    system.push_back(cl->equation());
    system.push_back(diff(cl->equation(),0));
    system.push_back(diff(cl->equation(),1));

    std::vector<Interval<C> > dmn;
    dmn.push_back(Interval<C>(this->xmin(*cl),this->xmax(*cl)));
    dmn.push_back(Interval<C>(this->ymin(*cl),this->ymax(*cl)));

    typedef solver< MvBrnApprox<C,2> > Solver;
    Solver slv;
    Solver::Solutions sol = slv.solve(system,dmn);
    int idx;
    for(unsigned i=0; i<sol.size(); i++) {
        idx = this->insert_vertex(VERTEX( (sol[i][0].lower()+sol[i][0].upper())/2.,
                (sol[i][1].lower()+sol[i][1].upper())/2. ) );
        cl->add_center(idx);
    }

    this->tag_corner(cl);
    this->boundary_point(cl);
    if(sol.size()>0) {
        m_singular.push_back(cl);
    } else {
        // XXX: Not necessarily Xregular.
        cl->regularity(BOUNDARY_REGULAR1);
        m_regular.push_back(cl);
    }
}

//--------------------------------------------------------------------
template<class C>
struct mshr_alg2d_curve {
    typedef  mesh<C> Mesh;
    typedef  tmsh::vertex<2,double> Vertex;
    typedef  mshr_alg2d_cell   Cell;
    typedef  mshr_alg2d_controler<Cell, Vertex> Controler;
    typedef  polygonizer2d<Controler>  Polygonizer;
    //typedef  Solver;
};

//--------------------------------------------------------------------
template<class C>
struct mshr_alg2d_region {
    typedef  mesh<C> Mesh;
    typedef  tmsh::vertex<2,double> Vertex;
    typedef  mshr_alg2d_cell   Cell;
    typedef  mshr_alg2d_controler<Cell, Vertex> Controler;
    typedef  polygonizer2d_region<Controler>  Polygonizer;
    //typedef  Solver;
};

//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================
#undef TMPL
#undef CTRL
