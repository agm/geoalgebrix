/**********************************************************************
 * PACKAGE  : geoalgebrix 
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once
 
#define TMPL template <class C>

//====================================================================
namespace mmx {
namespace tmsh {
//--------------------------------------------------------------------

template <int N, class C> struct vertex;

TMPL
struct vertex<2,C> {

    vertex(void): m_id(-1), m_tag(0) {
        m_coord[0]=0; m_coord[1]=0; m_coord[2]=0;
        for(unsigned i=0;i<4;i++) m_neighbor[i]=-1;
    }

    vertex(const C& X, const C& Y): m_id(-1), m_tag(0) {
        m_coord[0]=X; m_coord[1]=Y; m_coord[2]=0;
        for(unsigned i=0;i<4;i++) m_neighbor[i]=-1;
    }

    vertex(const vertex<2,C>& p): m_id(p.m_id), m_tag(p.m_tag) {
        for(unsigned i=0;i<2;i++) m_coord[i]=p[i];
        for(unsigned i=0;i<4;i++) m_neighbor[i]=p.m_neighbor[i];
    }

    int        next (int v) const { return m_neighbor[2*v]; }
    int    previous (int v) const { return m_neighbor[2*v+1]; }
    int    neighbor (int& v) const;    

    C    operator[] (int v) const { return m_coord[v]; }
    C&   operator[] (int v)       { return m_coord[v]; }

    int  id(void)    const { return m_id; }
    int& id(void)          { return m_id; }

    int  tag(void)   const { return m_tag; }
    void tag(int r)        { m_tag = r; }

    bool has_no_tag (void) const { return m_tag==0; }

    static unsigned dim;

    C   m_coord[3];
    int m_neighbor[4];

    // The index of the point
    int m_id;
    // The tag of the point
    int m_tag;
};

TMPL unsigned vertex<2,C>::dim = 2;

TMPL
struct vertex<3,C> {

    vertex(void): m_id(-1), m_tag(0) {
        m_coord[0]=0; m_coord[1]=0; m_coord[2]=0;
        for(unsigned i=0;i<6;i++) m_neighbor[i]=-1;
    };

    vertex(const C& X, const C& Y, const C& Z): m_id(-1), m_tag(0) {
        m_coord[0]=X; m_coord[1]=Y; m_coord[2]=Z;
        for(unsigned i=0;i<6;i++) m_neighbor[i]=-1;
    };

    vertex(const vertex<3,C>& p): m_id(p.m_id), m_tag(p.m_tag) {
        for(unsigned i=0;i<3;i++) m_coord[i]=p[i];
        for(unsigned i=0;i<6;i++) m_neighbor[i]=p.m_neighbor[i];
    }


    int    next     (int v) const { return m_neighbor[2*v]; }
    int    previous (int v) const { return m_neighbor[2*v+1]; }

    C    operator[] (int v) const { return m_coord[v]; }
    C&   operator[] (int v)       { return m_coord[v]; }

    C    x(void) const { return m_coord[0]; }
    C    y(void) const { return m_coord[1]; }
    C    z(void) const { return m_coord[2]; }

    int  tag(void)   const { return m_tag; }
    void tag(int r)        { m_tag = r; }

    static unsigned dim;
    C   m_coord[3];
    int m_neighbor[6];

    // The index of the point
    int m_id;

    // The tag of the point
    int m_tag;

};

TMPL unsigned vertex<3,C>::dim = 3;
//--------------------------------------------------------------------
} /* namespace tmsh */
//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================
#undef TMPL
