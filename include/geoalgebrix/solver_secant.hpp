#pragma once
#include <math.h>
#include <vector>

#define TMPL template<class POINT, class FUNCTION>
#define SELF solver_secant<POINT,FUNCTION>
//====================================================================
namespace mmx {
namespace slvr {
//--------------------------------------------------------------------

template<class POINT, class FCT>
int solve_secant(POINT& res, const FCT& f, const POINT& A, const POINT& B) {

    //mdebug()<<"solve_secant:"<<A[0]<<A[1]<<A[2]<<"-"<<B[0]<<B[1]<<B[2];
    double v0 = f(A[0],A[1],A[2]), v1 = f(B[0],B[1],B[2]);
    if(v0*v1>0) mdebug()<<"====> solve_secant: values same sign";
    if(v0==0)
        return -1;
    else if(v1==0)
        return 1;

    double a[3], b[3];
    for (unsigned i=0;i<3;i++) {
        a[i] = A[i];
        b[i] = B[i];
    }
    //double v0 = f(a[0],a[1],a[2]), v1 = f(b[0],b[1],b[2]);

    double mu = -v0/(v1-v0);
    for (unsigned i=0;i<3;i++) res[i]= a[i]+mu*(b[i]-a[i]);
    double vm = f(res[0],res[1],res[2]);

    //mdebug()<<"init. values:"<<v0<<v1;
    for(int c=0; fabs(vm)>1.e-6 && c< 70; c++) {
        //mdebug()<<"value:"<<vm<<"c:"<<c<<res[0]<<res[1]<<res[2];
        if(vm*v0<0) {
            for (unsigned i=0;i<3;i++) b[i]= res[i];
            v1 = vm;
        } else {
            for (unsigned i=0;i<3;i++) a[i]= res[i];
            v0 = vm;
        }

        mu = -v0/(v1-v0);
        for (unsigned i=0;i<3;i++) res[i]= a[i]+mu*(b[i]-a[i]);
        vm = f(res[0],res[1],res[2]);
    }
    return 0;
}

//--------------------------------------------------------------------
TMPL
/**
 * @brief The solver_secant class
 */
class solver_secant {
public:
    typedef typename POINT::Scalar               Scalar;
    typedef POINT                                Point;
    typedef FUNCTION                             Function;

    solver_secant(Function* f): m_eps(1e-8), m_delta(1e-1), m_fi(0) { m_fct.push_back(f); }

    void add_function(Function* f)    { m_fct.push_back(f); }

    unsigned nb_sol(void)       const { return m_sol.size(); }

    Point  solution(unsigned i) const { return m_sol[i]; }
    Scalar val     (unsigned i) const { return m_val[i]; }

    void clear(void) { m_sol.resize(0); m_val.resize(0); }

    Scalar absval(const Point& P);
    Scalar diff  (const POINT& A, Scalar va, const POINT& dA);

    void critical_points (const POINT& A, const POINT& B, Scalar va, int sa, Scalar vb , int sb);
    void critical_points (const POINT& A, const POINT& B);

    void minimizer (const POINT& A, const POINT& B);
    void minimizer (const POINT& A,  const Scalar& va, const POINT& B, const Scalar& vb );
    void minimizer (const POINT& A, const POINT& B, const POINT& C, const POINT& D);
    void minimizer (const POINT& A, const POINT& B, const POINT& C, const POINT& D,
                    const POINT& E, const POINT& F, const POINT& G, const POINT& H);

    void add_sol(const POINT& A, Scalar va) {
        m_sol.push_back(A);
        m_val.push_back(va);
    }

    void set_sol(const POINT& A, Scalar va) {
        if(m_sol.size()>0) {
            m_sol.resize(1);
            m_sol[0][0]=A[0];
            m_sol[0][1]=A[1];
            m_sol[0][2]=A[2];
            m_val.resize(1);
            m_val[0]=va;
        } else
            this->add_sol(A,va);
    }

    Scalar                 m_eps, m_delta;

private:

    std::vector<Function*> m_fct;
    std::vector<POINT>     m_sol;
    std::vector<Scalar>    m_val;
    //Scalar                 m_pt[3];
    unsigned               m_fi;
    int                    m_sign;
};

TMPL
typename SELF::Scalar SELF::absval(const POINT& A) {

    //m_pt[0]=A[0];//m_pt[1]=A[0];
    //m_pt[1]=A[1];//m_pt[3]=A[1];
    //m_pt[2]=A[2];//m_pt[5]=A[2];

    Scalar r = (*m_fct[0])(A[0],A[1],A[2]), v, s;
    //mdebug()<<"r:"<<r;
    if(r<0) {
        r = -r;
        m_sign = -1;
    } else {
        m_sign = 1;
    }
    m_fi = 0;

    for(unsigned i=1;i<m_fct.size();i++) {
        v = (*m_fct[i])(A[0],A[1],A[2]);
        if(v<0) {
            v = -v;
            s = -1;
        } else {
            s = 1;
        }

        if (v>r) {
            m_fi=i;
            m_sign = s;
            r=v;
        }
    }
    //mdebug()<<"-->>> val:"<<r<<"at"<<A;
    return r;
}

TMPL
typename SELF::Scalar SELF::diff(const POINT& A, Scalar va, const POINT& dA) {
    return (this->absval(A+dA)-va)/m_eps;
}

TMPL
/**
 * @brief SELF::critical_points
 * @param A point
 * @param B point
 *
 * Compute the critical points of the function absval between the points A and B.
 * Store the result in the list of points m_sol with the corresponding values m_val.
 *
 */
void SELF::critical_points(const POINT& A, const POINT& B) {
    this->clear();
    Scalar va= absval(A);
    int sa = m_sign;
    Scalar vb= absval(B);
    int sb = m_sign;

    this->critical_points(A, B, va, sa, vb, sb);

    if(vb<m_eps) {
        this->add_sol(B, vb);
    }
}

TMPL
void SELF::critical_points(const POINT& A, const POINT& B, Scalar va, int sa, Scalar vb , int sb) {
    //mdebug()<<":::>>>"<<A<<va<<sa<<B<<vb<<sb;
    Scalar d = std::sqrt(A.dist2(B));

    if(d>m_delta) {
        Point M = (A+B)/2.0;
        Scalar vm = absval(M); int sm = m_sign;
        if(vm<m_eps) {
            //mdebug()<<"middle"<<M<<"is solution";
            this->add_sol(M, vm);
            Point Delta = M-A, Mm = M - Delta*(m_delta/100.), Mp= M + Delta*(m_delta/100.);
            vm = absval(Mm); sm = m_sign;
            this->critical_points(A, Mm, va, sa, vm, sm);
            vm = absval(Mm); sm = m_sign;
            this->critical_points(Mp, B, vm, sm, vb, sb);
        } else {
            //mdebug()<<"   split"<<A<<B<<  "---> "<<M;
            this->critical_points(A, M, va, sa, vm, sm);
            this->critical_points(M, B, vm, sm, vb, sb);
            return;
        }
    } else if(d<m_delta*1.e-3) {
        //mdebug()<<"   small interval"<<A<<va<< sa<< B<< vb <<sb;
        if(va<m_eps & vb<m_eps) {
            //mdebug()<<"add middle";
            this->add_sol((A+B)/2.0, (va+vb)/2.0);
        } else if(va<m_eps) {
            //mdebug()<<"add A"<<A<<va;
            this->add_sol(A, va);
        } else if(vb<m_eps) {
            //mdebug()<<"does not add B"<<B<<vb;
            return;
        } else {
            //mdebug()<<"   value not small";
            //Point D = (B-A)*m_delta/100;
            //Scalar dfA = this->diff(A, va, D),
            //        dfB = this->diff(B, vb, D);
            if(sa*sb<0) {
                //mdebug()<<"   add middle";
                Point M = A + (B-A)*(va/(va+vb));
                Scalar vm = absval(M);
                if(vm<m_eps) {
                    this->add_sol(M, vm);
                    //mdebug()<<"   add M"<<M<<vm;
                }
                //this->add_sol( (A+B)/2.0, (va+vb)/2.0 );
            }
        }
        return;
    } else {
        //mdebug()<<"   ===";
        if(va<m_eps) {
            this->add_sol(A, va);
            //mdebug()<<"   add A"<<A<<va;
            return;
        } if(vb<m_eps) {
            //mdebug()<<"   does not add B"<<B<<vb;
            return;
        } else if(sa*sb<0) {
            Point M = A + (B-A)*(va/(va+vb));
            //mdebug()<<"   split"<<A<<B<<  "---> "<<M;
            Scalar vm = absval(M);
            int sm = m_sign;
            this->critical_points(A, M, va, sa, vm, sm);
            this->critical_points(M, B, vm, sm, vb, sb);
        } else {
            //mdebug()<<"   local minimum";
            Point D = (B-A)*m_eps;

            Scalar dfA = this->diff(A, va, D),
                    dfB = this->diff(B, vb, D);
            //mdebug()<<"diff"<<dfA<<dfB;

            if(std::fabs(dfA)<m_eps) {
                //mdebug()<<"   small derivative";
                this->add_sol(A, va);
            }

            if(std::fabs(dfB)<m_eps) {
                //mdebug()<<"   small derivative";
                this->add_sol(B, vb);
            }

            if(dfA*dfB<0) {
                //mdebug()<<"   sign change in derivatives"<<A<<B<<D;
                Point M = A + (B-A)*(va/(va+vb));
                Scalar vm = absval(M);
                int sm = m_sign;
                if(vm<m_eps) {
                    this->add_sol(M, vm);
                    //mdebug()<<"   add M"<<M<<vm;
                }
                //else mdebug()<<"   does nat add a point";
                else {
                    this->critical_points(A, M, va, sa, vm, sm);
                    this->critical_points(M, B, vm, sm, vb, sb);
                }
            }
        }
    }
}

TMPL
/**
 * @brief SELF::minimizer
 * @param A point
 * @param B point
 *
 * Compute a minimizer of the function absval between the points A and B.
 * Store the result in the list of points m_sol with the corresponding values m_val.
 *
 */
void SELF::minimizer(const POINT& A, const POINT& B) {
    Scalar va = absval(A);
    Scalar vb = absval(B);
    if(va<vb)
        this->set_sol(A,va);
    else
        this->set_sol(B,vb);

    this->minimizer(A, va, B, vb);
}

TMPL
void SELF::minimizer(const POINT& A, const Scalar& va, const POINT& B, const Scalar& vb ) {
    //mdebug()<<":::>>>"<<A<<va<<B<<vb<< ":::: min:"<<this->solution(0)<<this->m_val[0];
    if(this->val(0) > m_eps) {

        Scalar d = distance(A,B);

        if(d > m_delta*1.e-1) {
            Point M = (A+B)/2.0;
            Scalar vm = absval(M);
            if(vm < this->val(0)) {
                this->set_sol(M,vm);
                //mdebug()<< "   add M"<<this->solution(0)<<this->m_val[0];
            } else {
                //mdebug()<< "   don't add M";
            }
            this->minimizer(A, va, M, vm);
            this->minimizer(M, vm, B, vb);
        } else if(d > m_delta*1.e-3) {
            Point D = (B-A)*(m_eps);
            Scalar dfA = this->diff(A, va, D),
                    dfB = this->diff(B, vb, D);
            //mdebug()<<"    diff"<<dfA<<dfB;

            if(dfA<0 && dfB>0) {
                //mdebug()<<"   sign change in derivatives"<<A<<B<<D;
                Point M = A + (B-A)*(va/(va+vb));
                Scalar vm = absval(M);

                if(vm<this->val(0)) {
                    this->set_sol(M, vm);
                    //mdebug()<< "   --- add M"<<this->solution(0)<<this->m_val[0];
                    //Point D = (B-A)*(m_eps/d);
                    Scalar dfM = this->diff(M, vm, D);
                    //mdebug()<<"   add M"<<M<<vm;
                    if(dfM>0) {
                        this->minimizer(A, va, M, vm);
                    } else {
                        this->minimizer(M, vm, B, vb);
                    }
                }
            }
        }
    }
}

TMPL
/**
 * @brief SELF::min_point
 * @param A point
 * @param B point
 * @param C point
 * @param D point
 *
 *  Compute the minimizer point with the value of the function in the square [A,B;C,D]
 *  by alternate direction search, using the computation of the critical
 *  points on a horizontal or vertical segment.
 *
 */
void SELF::minimizer (const POINT& A, const POINT& B, const POINT& C, const POINT& D) {

    this->clear();

    //mdebug()<<"min:"<<A<<B<<C<<D;

    Point U((A+C)/2.), V((B+D)/2.), S;
    Scalar vs = 0,
            d = distance(A,B), s;
    int v = 1, c=0;

    while(d>m_eps && c < 100) {

        this->minimizer(U,V);
        //mdebug()<<"--------------";
        /*mdebug()<<">>> "<< this->nb_sol()<<" critical sol(s) in "<<U<<" "<<V<<":";
        for(unsigned i=0;i<this->nb_sol();i++)
            mdebug()<<"    "<<this->solution(i)<<" "<<this->val(i);
        */

        if(this->nb_sol()>0) {
            vs=this->val(0);
            unsigned i=0;

            for(unsigned j=1; j< this->nb_sol(); j++) {
                if(this->val(j)<=vs) {
                    vs = this->val(j);
                    i =  j;
                }
            }
            d = distance(this->solution(i),S);
            S = this->solution(i);
            //mdebug()<<">>>"<<S<<"in"<<U<<V;
        }
        s = distance(S,U)/distance(V,U);
        if(v>0) {
            U = A + (B-A)*s;
            V = C + (D-C)*s;
        } else {
            U = A + (C-A)*s;
            V = B + (D-B)*s;
        }
        v*=-1;
        c++;
    }
    if(this->nb_sol()>0) {
        this->clear();
        this->add_sol(S,vs);
    }
}


TMPL
/**
 * @brief SELF::min_point
 * @param A
 * @param B
 * @param C
 * @param D
 * @param E
 * @param F
 * @param G
 * @param H
 *
 * Compute the minimizer of the function in the cell
 * by alternate direction search, using the computation of the minimizer
 * in a (square) section of the cell.
 *
 */
void SELF::minimizer (const POINT& A, const POINT& B, const POINT& C, const POINT& D,
                      const POINT& E, const POINT& F, const POINT& G, const POINT& H) {

    Point M((A+E)/2.), N((B+F)/2.), O((C+G)/2.), P((D+H)/2.), S;//(A);
    Scalar val = 1.,
            d = distance(A,B),
            s = 0;
    int v = 2, c=0;

    while(d>m_eps &&
          val > m_eps && c < 50) {
        mdebug()<<"---"<<v <<":"<<M<<N<<O<<P;

        this->clear();
        this->minimizer(M,N,O,P);

        if(this->nb_sol()>0) {
            val=this->val(0);
            unsigned i=0;

            for(unsigned j=1; j< this->nb_sol(); j++) {
                if(this->val(j)<= val) {
                    val = this->val(j);
                    i =  j;
                }
            }
            d = distance(this->solution(i),S);
            S = this->solution(i);
            mdebug()<<"==>"<<S<<" "<<this->val(i);
        }

        v = (v+1)%3;
        s = (S[v]-M[v])/(N[v]-M[v]);

        if(v==0) {
            M = A + (B-A)*s;
            N = C + (D-C)*s;
            O = E + (F-E)*s;
            P = G + (H-G)*s;
        } else if(v==1) {
            M = A + (C-A)*s;
            N = B + (D-B)*s;
            O = E + (G-E)*s;
            P = F + (H-F)*s;
        } else {
            s = (S[v]-M[v])/(O[v]-M[v]);
            M = A + (E-A)*s;
            N = B + (F-B)*s;
            O = C + (G-C)*s;
            P = D + (H-D)*s;
        }
        mdebug()<<"    s:"<<s;

        //mdebug()<<"var"<<v;

        c++;
    }

    if(this->nb_sol()>0) {
        this->clear();
        this->add_sol(S,val);
    }
}

} /* namespace slvr */
} /* namespace mmx */
//====================================================================
#undef TMPL
#undef SELF

//====================================================================
