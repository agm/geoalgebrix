/*****************************************************************************
 * M a t h e m a  g i x
 *****************************************************************************
 * BoundingBox
 * 2008-03-20
 * Julien Wintz & Bernard Mourrain
 *****************************************************************************
 *               Copyright (C) 2008 INRIA Sophia-Antipolis
 *****************************************************************************
 * Comments :
 ****************************************************************************/

# ifndef geoalgebrix_boundingbox_hpp
# define geoalgebrix_boundingbox_hpp

# include <iostream>
#include  <vector>
# include <geoalgebrix/geoalgebrix.hpp>
//# include <geoalgebrix/edge.hpp>

# define TMPL template<class C>
# define STMPL template<>
# define SELF bounding_box<C>

namespace mmx {
namespace geoalgebrix {

TMPL class bounding_box; // TMPL struct bounding_box;

// TMPL struct with_bounding_box { 
//   typedef bounding_box<K> BoundingBox; 
// };

// TMPL struct bounding_box_def 
//   :public geoalgebrix_def<K>
//   ,public with_bounding_box<K>
// {};

struct bounding_box_def {}; 

template<> struct use<bounding_box_def> {
    typedef bounding_box<double> BoundingBox;
}; 

template<class C>
class bounding_box
//        : public use<geoalgebrix_def,C,V>::Shape
{

public:
    bounding_box(void) ;
    bounding_box(double xmin, double xmax) ;
    bounding_box(double xmin, double xmax, double ymin, double ymax) ;
    bounding_box(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax) ;
    bounding_box(const SELF&) ;
    ~bounding_box(void) {};
    
    inline double x(int p) { return (*this)(0,(p&1)) ; }
    inline double y(int p) { return (*this)(1,(p>>1)&1) ; }
    inline double z(int p) { return (*this)(2,(p>>2)&1) ; }


    inline double xmin(void) { return m_xmin ; }
    inline double xmax(void) { return m_xmax ; }
    inline double ymin(void) { return m_ymin ; }
    inline double ymax(void) { return m_ymax ; }
    inline double zmin(void) { return m_zmin ; }
    inline double zmax(void) { return m_zmax ; }

    inline double xmin(void) const { return m_xmin ; }
    inline double xmax(void) const { return m_xmax ; }
    inline double ymin(void) const { return m_ymin ; }
    inline double ymax(void) const { return m_ymax ; }
    inline double zmin(void) const { return m_zmin ; }
    inline double zmax(void) const { return m_zmax ; }

    inline double xsize(void) const { return m_xmax-m_xmin ; }
    inline double ysize(void) const { return m_ymax-m_ymin ; }
    inline double zsize(void) const { return m_zmax-m_zmin ; }

    inline void set_xmin(double x) { this->m_xmin = x ; }
    inline void set_xmax(double x) { this->m_xmax = x ; }
    inline void set_ymin(double y) { this->m_ymin = y ; }
    inline void set_ymax(double y) { this->m_ymax = y ; }
    inline void set_zmin(double z) { this->m_zmin = z ; }
    inline void set_zmax(double z) { this->m_zmax = z ; }

    inline bool is0D(void) const { return ((m_xmin == m_xmax) && (m_ymin == m_ymax) && (m_zmin == m_zmax)) ; }
    inline bool is1D(void) const { return ((m_xmin != m_xmax) && (m_ymin == m_ymax) && (m_zmin == m_zmax)) ; }
    inline bool is2D(void) const { return ((m_xmin != m_xmax) && (m_ymin != m_ymax) && (m_zmin == m_zmax)) ; }
    inline bool is3d(void) const { return ((m_xmin != m_xmax) && (m_ymin != m_ymax) && (m_zmin != m_zmax)) ; }

    double  operator()(unsigned v, unsigned s) const;
    double& operator()(unsigned v, unsigned s);

    double  coord(int v, int s) const;

    double size(void) ;

    bool contains(double x, bool strict = false) ;
    bool contains(double x, double y, bool strict = false) ;
    bool contains(double x, double y, double z, bool strict = false) ;

    bool intersects(SELF * other, bool strict = true) ;
    bool     unites(SELF * other, bool strict = true) ;

    void intersected(SELF * other) ;
    void      united(SELF * other) ;

    SELF * intersect(const SELF& other) ;
    SELF *     unite(SELF * other) ;

    inline SELF * operator * (const SELF& other) { return intersect(other) ; }
    inline SELF * operator + (const SELF& other) { return     unite(other) ; }

    double cornerX(int i) { if (i%2==0) return this->xmin(); else return this->xmax(); }
    double cornerY(int i) { if ((i/2)%2==0) return this->ymin(); else return this->ymax(); }
    double cornerZ(int i) { if ((i/4)%2==0) return this->zmin(); else return this->zmax(); }

    static std::vector<int> points(int v, int s);
    static std::vector<int> points(int v0, int s0, int v1, int s1);
    static std::vector<std::pair<int,int> > edges();
    static std::vector<std::pair<int,int> > edges(int v, int s);

protected:
    double m_xmin, m_xmax ;
    double m_ymin, m_ymax ;
    double m_zmin, m_zmax ;
} ;

TMPL inline double 
lower(const SELF& bx, int v) {
    switch(v) {
    case 0:
        return bx.xmin(); break ;
    case 1:
        return bx.ymin(); break ;
    default:
        return bx.zmin(); break ;
    }
} 

TMPL inline double 
upper(const SELF& bx, int v) {
    switch(v) {
    case 0:
        return bx.xmax(); break ;
    case 1:
        return bx.ymax(); break ;
    default:
        return bx.zmax(); break ;
    }
}


inline double mmxmin(double a, double b) 
{
    return (a <= b) ? a : b ;
}

inline double mmxmax(double a, double b) 
{
    return (a >= b) ? a : b ;
}

TMPL SELF::bounding_box(void)
{
    this->m_xmin = 0.0 ;
    this->m_xmax = 1.0 ;
    this->m_ymin = 0.0 ;
    this->m_ymax = 1.0 ;
    this->m_zmin = 0.0 ;
    this->m_zmax = 1.0 ;
}

TMPL SELF::bounding_box(double xmin, double xmax)
{
    this->m_xmin = xmin ;
    this->m_xmax = xmax ;
    this->m_ymin = 0.0 ;
    this->m_ymax = 0.0 ;
    this->m_zmin = 0.0 ;
    this->m_zmax = 0.0 ;
}

TMPL SELF::bounding_box(double xmin, double xmax, double ymin, double ymax)
{
    this->m_xmin = xmin ;
    this->m_xmax = xmax ;
    this->m_ymin = ymin ;
    this->m_ymax = ymax ;
    this->m_zmin = 0.0 ;
    this->m_zmax = 0.0 ;
}

TMPL SELF::bounding_box(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax)
{
    this->m_xmin = xmin ;
    this->m_xmax = xmax ;
    this->m_ymin = ymin ;
    this->m_ymax = ymax ;
    this->m_zmin = zmin ;
    this->m_zmax = zmax ;
}

TMPL SELF::bounding_box(const SELF& bx)
{
    m_xmin = bx.xmin() ;
    m_xmax = bx.xmax() ;
    m_ymin = bx.ymin() ;
    m_ymax = bx.ymax() ;
    m_zmin = bx.zmin() ;
    m_zmax = bx.zmax() ;
}


TMPL double 
SELF::size(void)
{
    //std::cout<<"Size "<<m_xmax<< " "<< m_xmin<<" "<< m_ymax<<" "<< m_ymin<<" "<< m_zmax<< " "<<m_zmin<<std::endl;
    return std::max(m_xmax-m_xmin, std::max(m_ymax-m_ymin, m_zmax-m_zmin)) ;
}

TMPL bool 
SELF::contains(double x, bool strict)
{
    if(!strict)
        return (((m_xmin <= x) && (x <= m_xmax))) ;
    else
        return (((m_xmin <  x) && (x <  m_xmax))) ;
}

TMPL bool 
SELF::contains(double x, double y, bool strict)
{
    if(!strict)
        return (((m_xmin <= x) && (x <= m_xmax))
                &&   ((m_ymin <= y) && (y <= m_ymax))) ;
    else
        return (((m_xmin <  x) && (x <  m_xmax))
                &&   ((m_ymin <  y) && (y <  m_ymax))) ;
}

TMPL bool 
SELF::contains(double x, double y, double z, bool strict)
{
    if(!strict)
        return (((m_xmin <= x) && (x <= m_xmax))
                &&   ((m_ymin <= y) && (y <= m_ymax))
                &&   ((m_zmin <= z) && (z <= m_zmax)))  ;
    else
        return (((m_xmin <  x) && (x <  m_xmax))
                &&   ((m_ymin <  y) && (y <  m_ymax))
                &&   ((m_zmin <  z) && (z <  m_zmax)))  ;
}

TMPL bool 
SELF::intersects(SELF * other, bool strict)
{
    if(this->is0D())
        return (this->xmin() == other->xmin()) ;
    else if(this->is1D())
        if(strict)
            return ((mmxmax(this->xmin(), other->xmin()) <  mmxmin(this->xmax(), other->xmax()))) ;
        else
            return ((mmxmax(this->xmin(), other->xmin()) <= mmxmin(this->xmax(), other->xmax()))) ;
    else if(this->is2D())
        if(strict)
            return ((mmxmax(this->xmin(), other->xmin()) <  mmxmin(this->xmax(), other->xmax())) &&
                    (mmxmax(this->ymin(), other->ymin()) <  mmxmin(this->ymax(), other->ymax()))) ;
        else
            return ((mmxmax(this->xmin(), other->xmin()) <= mmxmin(this->xmax(), other->xmax())) &&
                    (mmxmax(this->ymin(), other->ymin()) <= mmxmin(this->ymax(), other->ymax()))) ;
    else if(this->is3d()) {
        if(strict)
            return ((mmxmax(this->xmin(), other->xmin()) <  mmxmin(this->xmax(), other->xmax())) &&
                    (mmxmax(this->ymin(), other->ymin()) <  mmxmin(this->ymax(), other->ymax())) &&
                    (mmxmax(this->zmin(), other->zmin()) <  mmxmin(this->zmax(), other->zmax()))) ;
        else
            return ((mmxmax(this->xmin(), other->xmin()) <= mmxmin(this->xmax(), other->xmax())) &&
                    (mmxmax(this->ymin(), other->ymin()) <= mmxmin(this->ymax(), other->ymax())) &&
                    (mmxmax(this->zmin(), other->zmin()) <= mmxmin(this->zmax(), other->zmax()))) ;
    }
    return false ;
}

TMPL bool 
SELF::unites(SELF * other, bool strict)
{
    if(this->is0D())
        return (this->xmin() == other->xmin()) ;
    else if(this->is1D()) {
        if(strict)
            return ((mmxmin(this->xmin(), other->xmin()) <  mmxmax(this->xmax(), other->xmax()))) ;
        else
            return ((mmxmin(this->xmin(), other->xmin()) <= mmxmax(this->xmax(), other->xmax()))) ;
    } else if(this->is2D()) {
        if(strict)
            return ((mmxmin(this->xmin(), other->xmin()) <  mmxmax(this->xmax(), other->xmax())) &&
                    (mmxmin(this->ymin(), other->ymin()) <  mmxmax(this->ymax(), other->ymax()))) ;
        else
            return ((mmxmin(this->xmin(), other->xmin()) <= mmxmax(this->xmax(), other->xmax())) &&
                    (mmxmin(this->ymin(), other->ymin()) <= mmxmax(this->ymax(), other->ymax()))) ;
    } else if(this->is3d()) {
        if(strict)
            return ((mmxmin(this->xmin(), other->xmin()) <  mmxmax(this->xmax(), other->xmax())) &&
                    (mmxmin(this->ymin(), other->ymin()) <  mmxmax(this->ymax(), other->ymax())) &&
                    (mmxmin(this->zmin(), other->zmin()) <  mmxmax(this->zmax(), other->zmax()))) ;
        else
            return ((mmxmin(this->xmin(), other->xmin()) <= mmxmax(this->xmax(), other->xmax())) &&
                    (mmxmin(this->ymin(), other->ymin()) <= mmxmax(this->ymax(), other->ymax())) &&
                    (mmxmin(this->zmin(), other->zmin()) <= mmxmax(this->zmax(), other->zmax()))) ;
    }
    return false ;
}

TMPL void 
SELF::intersected(SELF * other) {
    set_xmin(mmxmax(this->xmin(), other->xmin())) ;
    set_xmax(mmxmin(this->xmax(), other->xmax())) ;
    set_ymin(mmxmax(this->ymin(), other->ymin())) ;
    set_ymax(mmxmin(this->ymax(), other->ymax())) ;
    set_zmin(mmxmax(this->zmin(), other->zmin())) ;
    set_zmax(mmxmin(this->zmax(), other->zmax())) ;
}

TMPL void 
SELF::united(SELF * other) {
    set_xmin(mmxmin(this->xmin(), other->xmin())) ;
    set_xmax(mmxmax(this->xmax(), other->xmax())) ;
    set_ymin(mmxmin(this->ymin(), other->ymin())) ;
    set_ymax(mmxmax(this->ymax(), other->ymax())) ;
    set_zmin(mmxmin(this->zmin(), other->zmin())) ;
    set_zmax(mmxmax(this->zmax(), other->zmax())) ;
}

TMPL SELF* 
SELF::intersect(const SELF& other) {
    SELF * bcell = new SELF ;
    bcell->set_xmin(mmxmax(this->xmin(), other.xmin())) ;
    bcell->set_xmax(mmxmin(this->xmax(), other.xmax())) ;
    bcell->set_ymin(mmxmax(this->ymin(), other.ymin())) ;
    bcell->set_ymax(mmxmin(this->ymax(), other.ymax())) ;
    bcell->set_zmin(mmxmax(this->zmin(), other.zmin())) ;
    bcell->set_zmax(mmxmin(this->zmax(), other.zmax())) ;
    return bcell ;
}

TMPL SELF * 
SELF::unite(SELF * other) {
    SELF * bcell = new SELF ;
    bcell->set_xmin(mmxmin(this->xmin(), other->xmin())) ;
    bcell->set_xmax(mmxmax(this->xmax(), other->xmax())) ;
    bcell->set_ymin(mmxmin(this->ymin(), other->ymin())) ;
    bcell->set_ymax(mmxmax(this->ymax(), other->ymax())) ;
    bcell->set_zmin(mmxmin(this->zmin(), other->zmin())) ;
    bcell->set_zmax(mmxmax(this->zmax(), other->zmax())) ;
    return bcell ;
}

TMPL double
SELF::coord(int v, int s) const {
    switch(v) {
    case 0:
        if(s==0) return m_xmin; else return m_xmax;
    case 1:
        if(s==0) return m_ymin; else return m_ymax;
    default:
        if(s==0) return m_zmin; else return m_zmax;
    }

}


TMPL double 
SELF::operator()(unsigned v, unsigned s) const {
    switch(v) {
    case 0:
        if(s==0) return m_xmin; else return m_xmax;
    case 1:
        if(s==0) return m_ymin; else return m_ymax;
    default:
        if(s==0) return m_zmin; else return m_zmax;
    }

}

TMPL double& 
SELF::operator()(unsigned v, unsigned s) {
    switch(v) {
    case 0:
        if(s==0) return m_xmin; else return m_xmax;
    case 1:
        if(s==0) return m_ymin; else return m_ymax;
    default:
        if(s==0) return m_zmin; else return m_zmax;
    }

}

TMPL
std::vector<int>
SELF::points(int v, int s) {
    //std::cout<<"points "<<v<<" "<<s<<" w ";
    std::vector<int> idx(4,s*(1<<v));

    int w[2]= {(v+1)%3,(v+2)%3};
    if(w[1]<w[0]) std::swap(w[0],w[1]);
    //std::cout<<" "<<w[0]<<" "<<w[1]<<std::endl;

    for(unsigned s0=0; s0<2; s0++)
      for(unsigned s1=0; s1<2; s1++)
          idx.at(s0+2*s1) += s0*(1<<w[0])+s1*(1<<w[1]);

    //for(unsigned j=0;j<4;j++) std::cout<<"   points     "<<idx.at(j)<<std::endl;
    return idx;
}

TMPL
std::vector<int>
SELF::points(int v0, int s0, int v1, int s1) {
    std::vector<int> idx(2, s0*(1<<v0)+s1*(1<<v1));
    unsigned w = (2*(v0+v1))%3;
    idx.at(1) += (1<<w);
    return idx;
}

TMPL
std::vector< std::pair<int,int> > SELF::edges() {

    std::vector<std::pair<int,int> > res(12);
    res.at(0)=std::pair<int,int>(0,1);
    res.at(1)=std::pair<int,int>(2,3);
    res.at(2)=std::pair<int,int>(0,2);
    res.at(3)=std::pair<int,int>(1,3);

    res.at(4)=std::pair<int,int>(4,5);
    res.at(5)=std::pair<int,int>(6,7);
    res.at(6)=std::pair<int,int>(4,6);
    res.at(7)=std::pair<int,int>(5,7);

    res.at(8)=std::pair<int,int>(0,4);
    res.at(9)=std::pair<int,int>(1,5);
    res.at(10)=std::pair<int,int>(2,6);
    res.at(11)=std::pair<int,int>(3,7);

    return res;
}

TMPL
std::vector< std::pair<int,int> > SELF::edges(int v, int s) {
    int i0 = s*(1<<v);
    int idx[4] = {i0,i0,i0,i0};
    int w[2]   = {(v+1)%3, (v+2)%3};
    if(w[0]>w[1]) std::swap(w[0],w[1]);
    for(unsigned s0=0; s0<2; s0++)
      for(unsigned s1=0; s1<2; s1++)
          idx[s0+2*s1] += s0*(1<<w[0])+s1*(1<<w[1]);

    std::vector<std::pair<int,int> > res(4);
    res.at(0)=std::pair<int,int>(idx[0],idx[1]);
    res.at(1)=std::pair<int,int>(idx[2],idx[3]);
    res.at(2)=std::pair<int,int>(idx[0],idx[2]);
    res.at(3)=std::pair<int,int>(idx[1],idx[3]);

    return res;
}


TMPL std::ostream& 
operator << (std::ostream& stream, const SELF & c) {
    if(c.is0D())
        return stream << "[]" ;
    else if(c.is1D())
        return stream << "[" << c.xmin() << ", " << c.xmax() << "]" ;
    else if(c.is2D())
        return stream << "[" << c.xmin() << ", " << c.xmax() << "] x [" << c.ymin() << ", " << c.ymax() << "]" ;
    else if(c.is3d())
        return stream << "[" << c.xmin() << ", " << c.xmax() << "] x [" << c.ymin() << ", " << c.ymax() << "] x [" << c.zmin() << ", " << c.zmax() << "]" ;
    else
        return stream << "???" ;
}

//--------------------------------------------------------------------
template<class C, class V, class T> void
insert_bbx(T* t,SELF* bx) {
    typedef typename T::Point Point;
    typedef typename T::Edge   Edge;
    Point
            *p0= new Point(bx->xmin(),bx->ymin(),bx->zmin()),
            *p1= new Point(bx->xmin(),bx->ymax(),bx->zmin()),
            *p2= new Point(bx->xmax(),bx->ymax(),bx->zmin()),
            *p3= new Point(bx->xmax(),bx->ymin(),bx->zmin());
    t->insert(p0);t->insert(p1); t->insert(new Edge(p0,p1));
    t->insert(p1);t->insert(p2); t->insert(new Edge(p1,p2));
    t->insert(p2);t->insert(p3); t->insert(new Edge(p2,p3));
    t->insert(p3);t->insert(p0); t->insert(new Edge(p3,p0));

    Point
            *q0= new Point(bx->xmin(),bx->ymin(),bx->zmax()),
            *q1= new Point(bx->xmin(),bx->ymax(),bx->zmax()),
            *q2= new Point(bx->xmax(),bx->ymax(),bx->zmax()),
            *q3= new Point(bx->xmax(),bx->ymin(),bx->zmax());
    t->insert(q0);t->insert(q1); t->insert(new Edge(q0,q1));
    t->insert(q1);t->insert(q2); t->insert(new Edge(q1,q2));
    t->insert(q2);t->insert(q3); t->insert(new Edge(q2,q3));
    t->insert(q3);t->insert(q0); t->insert(new Edge(q3,q0));

    t->insert(p0);t->insert(q0);t->insert(new Edge(p0,q0));
    t->insert(p1);t->insert(q1);t->insert(new Edge(p1,q1));
    t->insert(p2);t->insert(q2);t->insert(new Edge(p2,q2));
    t->insert(p3);t->insert(q3);t->insert(new Edge(p3,q3));
}



template<class C, class V, class T> void
insert_bbx(T* t,SELF* bx, int v, int s) {
    typedef typename T::Point Point;
    typedef typename T::Edge   Edge;

    Point
            *p0= new Point(bx->xmin(),bx->ymin(),bx->zmin()),
            *p1= new Point(bx->xmin(),bx->ymax(),bx->zmin()),
            *p2= new Point(bx->xmax(),bx->ymax(),bx->zmin()),
            *p3= new Point(bx->xmax(),bx->ymin(),bx->zmin());
    Point
            *q0= new Point(bx->xmin(),bx->ymin(),bx->zmax()),
            *q1= new Point(bx->xmin(),bx->ymax(),bx->zmax()),
            *q2= new Point(bx->xmax(),bx->ymax(),bx->zmax()),
            *q3= new Point(bx->xmax(),bx->ymin(),bx->zmax());

    if(v==2) {
        if(s==0) {
            t->insert(p0);t->insert(p1); t->insert(new Edge(p0,p1));
            t->insert(p1);t->insert(p2); t->insert(new Edge(p1,p2));
            t->insert(p2);t->insert(p3); t->insert(new Edge(p2,p3));
            t->insert(p3);t->insert(p0); t->insert(new Edge(p3,p0));
        } else {
            t->insert(q0);t->insert(q1); t->insert(new Edge(q0,q1));
            t->insert(q1);t->insert(q2); t->insert(new Edge(q1,q2));
            t->insert(q2);t->insert(q3); t->insert(new Edge(q2,q3));
            t->insert(q3);t->insert(q0); t->insert(new Edge(q3,q0));
        }
    } else if(v==1) {
        if(s==0) {
            t->insert(p0);t->insert(q0);t->insert(new Edge(p0,q0));
            t->insert(q0);t->insert(q3);t->insert(new Edge(q0,q3));
            t->insert(q3);t->insert(p3);t->insert(new Edge(q3,p3));
            t->insert(p3);t->insert(p0);t->insert(new Edge(p3,p0));
        } else {
            t->insert(p1);t->insert(q1);t->insert(new Edge(p1,q1));
            t->insert(q1);t->insert(q2);t->insert(new Edge(q1,q2));
            t->insert(q2);t->insert(p2);t->insert(new Edge(q2,p2));
            t->insert(p2);t->insert(p1);t->insert(new Edge(p2,p1));
        }
    } else if (v==0) {
        if(s==0) {
            t->insert(p0);t->insert(q0);t->insert(new Edge(p0,q0));
            t->insert(q0);t->insert(q1);t->insert(new Edge(q0,q1));
            t->insert(q1);t->insert(p1);t->insert(new Edge(p1,q1));
            t->insert(p1);t->insert(p0);t->insert(new Edge(p1,p0));
        } else {
            t->insert(p2);t->insert(q2);t->insert(new Edge(p2,q2));
            t->insert(q2);t->insert(q3);t->insert(new Edge(q2,q3));
            t->insert(q3);t->insert(p3);t->insert(new Edge(p3,q3));
            t->insert(p3);t->insert(p2);t->insert(new Edge(p3,p2));
        }
    }
}


//--------------------------------------------------------------------
} ; // namespace geoalgebrix
} ; // namespace mmx
# undef TMPL
# undef SELF
# endif // geoalgebrix_bounding_box_hpp
