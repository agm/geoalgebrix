/**********************************************************************
 * PACKAGE  : geoalgebrix 
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once

#include <fstream>
#include <vector>
#include <geoalgebrix/mesh.hpp>
#include <geoalgebrix/subdivider.hpp>

#define TMPL template<class Variant>
#define SELF mesher<Variant>

//====================================================================
namespace mmx {
//--------------------------------------------------------------------

/** @brief mesher of shapes.
 *  
 **/
template<class Variant>
struct mesher {

    typedef typename Variant::Controler          Controler;
    typedef typename Controler::Cell             Input;
    typedef struct  subdivider<Controler>                Subdivider;
    typedef typename Variant::Polygonizer        Polygonizer;
 
//    typedef typename Variant::Mesh               Output;
    typedef Polygonizer                          Output;
    typedef typename Output::Point               Point;

    mesher(double e1= 0.5, double e2=0.1);
    mesher(Controler* dtr, double e1= 0.5, double e2=0.1);
    ~mesher(void) ;

    void   set_input(Input* c);
    void   set_max_size(double epsilon) { m_max_size = epsilon; }
    void   set_min_size(double epsilon) { m_min_size = epsilon; }

    Input*     input()               { return m_input; }
    Output*    output()              { return m_polygn; }
    Output*    output() const        { return m_polygn; }

    Controler*   controler()         { return d; }
    Subdivider*  subdivider()        {return m_subdiv;}
    Polygonizer* polygonizer ()      { return m_polygn; }

    void run (void);

private:

    Controler*   d;
    double       m_max_size, m_min_size;
    Input*       m_input;
    Polygonizer* m_polygn;
    Subdivider*  m_subdiv;

};

//--------------------------------------------------------------------
TMPL SELF::mesher(double e1, double e2): m_max_size(e1), m_min_size(e2)
{
    d = new Controler;
    m_input = NULL; 
    m_subdiv = NULL;
    m_polygn = NULL;
}
//--------------------------------------------------------------------
TMPL SELF::mesher(Controler* f, double e1, double e2): m_max_size(e1), m_min_size(e2)
{
    d = f;
    m_input = NULL;
    m_polygn = new Output;
}
//--------------------------------------------------------------------
TMPL SELF::~mesher(void) {

}

TMPL void SELF::set_input(Input* cl){
    //mdebug()<<"set_input";
     m_input = cl;
}

//--------------------------------------------------------------------
TMPL
void SELF::run(void) {

    if (m_input==NULL) return;

    //mdebug()<<"Start mesher";
    m_subdiv = new Subdivider(d,m_max_size, m_min_size);
    m_subdiv->set_input(this->input());
    m_subdiv->run();
    //mdebug()<<"Subdivision"<<this->controler()->m_regular.size() <<this->controler()->m_singular.size();

    m_polygn = new Polygonizer(this->controler());
    m_polygn->set_input(m_subdiv->root());
    m_polygn->set_regular(&this->controler()->m_regular);
    m_polygn->set_singular(&this->controler()->m_singular);
    m_polygn->run();
    mdebug()<<"Polygonizer done";

    //m_polygn=p.output();
    //p.get_output(m_polygn);

//    mdebug()<<"Mesh:"<<m_polygn->nbv()<<m_polygn->nbe()<<m_polygn->nbf();
}
//--------------------------------------------------------------------
template<class MESH, class Variant>
void get_mesh2d(MESH& out,const SELF& mshr ) {
    typedef typename MESH::Point Point;

    for(unsigned i=0; i < mshr.output()->nbv() ; i++)
        out.add_vertex(new Point(mshr.output()->vertex(i)[0], mshr.output()->vertex(i)[1]));

    for(unsigned i=0; i < mshr.output()->nbe() ; i++)
        out.add_edge(mshr.output()->edge(i).first, mshr.output()->edge(i).second);

}

//--------------------------------------------------------------------
template<class MESH, class Variant>
void get_mesh3d(MESH& out,const SELF& mshr ) {
    typedef typename MESH::Point Point;
    typedef typename MESH::Face  Face;

    for(unsigned i=0; i < mshr.output()->nbv() ; i++)
        out.add_vertex(new Point(mshr.output()->vertex(i)[0],mshr.output()->vertex(i)[1],mshr.output()->vertex(i)[2]));

    for(unsigned i=0; i < mshr.output()->nbe() ; i++)
        out.add_edge(mshr.output()->edge(i).first, mshr.output()->edge(i).second);

    for(unsigned i=0; i < mshr.output()->nbf() ; i++) {
        Face * f = new Face(mshr.output()->face(i));
        out.add_face(f);
    }
}
//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================
#undef TMPL
#undef SELF
