/**********************************************************************
 * PACKAGE  : geoalgebrix
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once



#include <geoalgebrix/foreach.hpp>
#include <geoalgebrix/regularity.hpp>
#include <geoalgebrix/tmsh.hpp>
#include <geoalgebrix/tmsh_cell.hpp>
#include <geoalgebrix/algebraic2d.hpp>
#include <geoalgebrix/solver_cell2d.hpp>

#include <geoalgebrix/polygonizer2d_region.hpp>
#include <geoalgebrix/mesher.hpp>

#define TMPL template<class CELL, class VERTEX>
#define CTRL controler2d<CELL,VERTEX>
//====================================================================
namespace mmx {

namespace csg {
    template<class CELL, class VERTEX>  struct controler2d;
}
//--------------------------------------------------------------------
namespace slvr {

template<class R> struct evaluator;

TMPL
/*!
 * \brief The evaluator<CTRL> struct
    It provides the edge polynomial for a given polynomial on a cell.
*/
struct evaluator< csg::controler2d<CELL,VERTEX> > {
    typedef csg::controler2d<CELL,VERTEX> Controler;
    typedef typename Controler::EdgePolynomial EdgePolynomial;
    template<class CL, class POL>
    static void edge_polynomial(EdgePolynomial& f, Controler* ctrl, CL* cl,  const POL& pol, int v, int s) {
        std::vector<double> bx(4);
        bx[0]=ctrl->xmin(*cl);
        bx[1]=ctrl->xmax(*cl);
        bx[2]=ctrl->ymin(*cl);
        bx[3]=ctrl->ymax(*cl);
        polynomial<double, with<Bernstein> > p;
        let::assign(p.rep(), tensor::bernstein<double>(pol.rep(),bx));
        tensor::face(f,p,v,s);
    }
};
} /* namespace slvr */
//--------------------------------------------------------------------
namespace csg {
//--------------------------------------------------------------------
/*!
 * \brief The csg::controler2d struct
 */
template<class CELL, class VERTEX>

struct controler2d: tmsh::controler<2,CELL,VERTEX>
{
    typedef double  C;
    typedef CELL    Cell;
    typedef VERTEX  Vertex;
    typedef CTRL    Controler;

    typedef slvr::solver_cell2d<CTRL> Solver;
    typedef polynomial< C, with<Bernstein> >        EdgePolynomial;
    typedef polynomial< C, with<Sparse,DegRevLex> > Polynomial;
    typedef std::vector<VERTEX*> Solution;
    typedef std::vector<int>     Multiplicity;

    controler2d() {}

    Cell* init_cell(csg::generic* o, double xmin, double xmax, double ymin, double ymax);

    regularity_t regularity(Cell* cl);

    void tag_corner    (Cell* cl);
    int  tag_of_middle (Cell* cl, int i0, int i1);

    void process_regular (Cell* cl);
    void process_singular(Cell* cl);

    void subdivide(int v, Cell* cl, Cell*& left, Cell*& right) {
        left  = new Cell(*cl);
        right = new Cell(*cl);
        this->tmsh::controler<2,CELL,VERTEX>::subdivide(v,*cl,*left,*right);
    }

    void boundary_point (Cell* cl);
    void center_point (Cell* cl);

    std::vector<Cell*> m_regular;
    std::vector<Cell*> m_singular;
    csg::generic*      m_obj;

};
//--------------------------------------------------------------------
/*!
 * \brief CTRL::init_cell
 * \param o
 * \param xmin
 * \param xmax
 * \param ymin
 * \param ymax
 * \return the cell associated to the csg object @c o with given min-max y-x coordinates.
 */
TMPL
CELL* CTRL::init_cell(csg::generic* o, double xmin, double xmax, double ymin, double ymax) {

    Vertex p[4] = {
        Vertex(xmin,ymin),
        Vertex(xmax,ymin),
        Vertex(xmin,ymax),
        Vertex(xmax,ymax)
    };
    this->tmsh::controler<2,CELL,VERTEX>::init(p);

    CELL* res= new CELL;
    std::vector<double> bx(4);
    bx[0]=xmin;
    bx[1]=xmax;
    bx[2]=ymin;
    bx[3]=ymax;

    m_obj= o;

    return res;
}


/*!
 * \brief CTRL::regularity
 * \param cl cell pointer
 * \return the regularity of the cell.
 */
TMPL regularity_t CTRL::regularity(CELL *cl) {

    double box[4] = {this->xmin(*cl), this->xmax(*cl), this->ymin(*cl), this->ymax(*cl)};
    regularity_t r = m_obj->regularity(box, true);
    cl->regularity(r);

    return r;
}

TMPL
void CTRL::tag_corner(CELL *cl) {

    this->vertex(cl->idx(0)).tag(m_obj->tag(this->xmin(*cl),this->ymin(*cl)));
    this->vertex(cl->idx(1)).tag(m_obj->tag(this->xmax(*cl),this->ymin(*cl)));
    this->vertex(cl->idx(2)).tag(m_obj->tag(this->xmin(*cl),this->ymax(*cl)));
    this->vertex(cl->idx(3)).tag(m_obj->tag(this->xmax(*cl),this->ymax(*cl)));

}

TMPL int CTRL::tag_of_middle(CELL* cl, int i0, int i1) {

    double x = (this->vertex(i0)[0]+this->vertex(i1)[0])/2.0,
            y = (this->vertex(i0)[1]+this->vertex(i1)[1])/2.0;
    return m_obj->tag(x,y);
}


TMPL
void CTRL::boundary_point(CELL* cl) {

    // The active leaves of the csg tree on the cell.
    std::vector<csg::generic* > seq;
    m_obj->active(seq);
    //mdebug()<<"boundary reg:"<<m_obj->get_reg()<<"size"<<seq.size();

    if(seq.size()==0) return;

    //The box of the cell
    std::vector<double> bx(4);
    bx[0]=this->xmin(*cl);
    bx[1]=this->xmax(*cl);
    bx[2]=this->ymin(*cl);
    bx[3]=this->ymax(*cl);

    typedef slvr::solver_cell2d<CTRL> Solver;
    Solver slv(this);

    typedef typename Controler::Polynomial Polynomial;
    Polynomial pol;
    std::vector<Polynomial> system;
    int nb, bd=0;
    double pt[4] = {0.,0.,0.,0.};

    for(unsigned i=0; i<seq.size(); i++) {
        //mdebug()<<"csg: active"<<seq.size();
        csg::algebraic2d<double>* a = dynamic_cast<csg::algebraic2d<double>*>(seq[i]);
        if(a != NULL) {

            //mdebug()<<"Polynomial"<<a->m_polynomial<<bx[0]<<bx[1]<<bx[2]<<bx[3];
            //let::assign(pol.rep(), tensor::bernstein<double>(a->m_polynomial.rep(),bx));
            nb = cl->boundary().size();

            //slv.edge_point(cl,pol);
            slv.edge_point(cl,a->m_polynomial);

            //Counts the number of curves intersecting the boundary
            if(slv.nb_sol()>0) {
                system.push_back(a->m_polynomial);
                bd++;
            }

            for(unsigned k=0;k< slv.nb_sol();k++) {
                if(seq.size()==1) {
                    cl->add_boundary_point(slv.solution(k),slv.mu(k));
                } else {
                    pt[0] = this->vertex(slv.solution(k))[0];
                    pt[1] = pt[0];
                    pt[2] = this->vertex(slv.solution(k))[1];
                    pt[3] = pt[2];

                    // Insert the point if it is not outside the region
                    // of the other constraintes.
                    bool insert = true;
                    for(unsigned j=0; j<seq.size() && insert; j++) {
                        if(j!=i)
                            insert = (seq[j]->tag(pt[0],pt[2])!=OUTSIDE);
                    }

                    if(insert) {
                        cl->add_boundary_point(slv.solution(k), slv.mu(k));
                    } else {
                        this->vertex(slv.solution(k)).tag(OUTSIDE);
                    }
                }
            }
        }
    }

    if(bd >1) {
        mdebug()<<"add center from boundary";
        slv.interior_point(cl,system);
        for(unsigned i=0; i< slv.nb_sol(); i++)
            cl->add_center(slv.solution(i));
        //mdebug()<<"center"<<cl->center(0);
    }
}

TMPL
void CTRL::center_point(CELL* cl) {

    std::vector<csg::generic* > seq;
    m_obj->active(seq);

    Solver slv(this);

    typedef typename Controler::Polynomial Polynomial;
    Polynomial pol;

    for(unsigned i=0; i<seq.size(); i++) {
        std::vector<Polynomial> system;
        csg::algebraic2d<double>* a = dynamic_cast<csg::algebraic2d<double>*>(seq[i]);
        if(a != NULL) {

            system.push_back(diff(a->m_polynomial,0));
            system.push_back(diff(a->m_polynomial,1));

            slv.interior_point(cl,system);
            for(unsigned i=0; i< slv.nb_sol(); i++)
                cl->add_center(slv.solution(i));
        }
    }
}

TMPL
void CTRL::process_regular(CELL* cl) {
    this->tag_corner(cl);
    int r = cl->regularity();
    if( r == BOUNDARY_SINGULAR ) {
        this->center_point(cl);
        this->boundary_point(cl);
        m_regular.push_back(cl);
    } else if( r != OUTSIDE ) {
        this->boundary_point(cl);
        m_regular.push_back(cl);
    }
}

TMPL
void CTRL::process_singular(CELL* cl) {
    //this->process_regular(cl); return;
    this->tag_corner(cl);
    mdebug()<<"process singular";
    //int idx = this->insert_vertex(VERTEX( (this->xmin(*cl)+this->xmax(*cl))/2.,
    //                                      (this->ymin(*cl)+this->ymax(*cl))/2. ));

    //cl->add_center(idx);
    this->boundary_point(cl);
    m_regular.push_back(cl);
}

//--------------------------------------------------------------------
} /* namespace csg */
} /* namespace mmx */
//====================================================================

//====================================================================
namespace mmx {
//namespace tmsh {
//--------------------------------------------------------------------
template<class C>
struct mshr_csg2d_curve {
    typedef  mesh<C>                       Mesh;
    typedef  tmsh::vertex<2,double>              Vertex;
    typedef  tmsh::cell<2>                       Cell;
    typedef  csg::controler2d<Cell, Vertex> Controler;
    typedef  polygonizer2d<Controler>      Polygonizer;
    //typedef  Solver;
};

//--------------------------------------------------------------------
template<class C>
struct mshr_csg2d_region {
    typedef  mesh<C>                         Mesh;
    typedef  tmsh::vertex<2,double>          Vertex;
    typedef  tmsh::cell<2>                   Cell;
    typedef  csg::controler2d<Cell, Vertex>   Controler;
    typedef  polygonizer2d_region<Controler> Polygonizer;
    //typedef  Solver;
};

//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================
#undef TMPL
#undef CTRL

