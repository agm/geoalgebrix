/**********************************************************************
 * PACKAGE  : geoalgebrix
 * COPYRIGHT: (C) 2015, Bernard Mourrain, Inria
 **********************************************************************/
#pragma once

#include <geoalgebrix/regularity.hpp>
#include <realroot/Interval.hpp>
#include <realroot/polynomial_sparse.hpp>

//====================================================================
namespace mmx {
//--------------------------------------------------------------------
namespace csg {
//--------------------------------------------------------------------
typedef Interval<double> Range;
//typedef Point;
//typedef std::vector<Point> Solution;
/** @brief generic class for CSG.

   Abstract class with the following methods:

   - @c eval: the range for the evaluation at the box.
   - @c regularity: the regularity of the box.
   - @c active: the sequence of active CSG objects.

**/
struct generic {
    typedef std::vector<generic*> Sequence;

    generic(void) {}

    virtual Range eval  (double* cl) = 0;

    virtual regularity_t regularity(double* cl, bool store) = 0;

    int  get_reg() const { return m_reg; }
    void set_reg(int r)  { m_reg = r; }

    virtual void  active (Sequence& seq) = 0;

    virtual int   tag(double x, double y) = 0;
    virtual int   tag(double x, double y, double z) { return 0; }

private:
    int m_reg;
};



//--------------------------------------------------------
struct Intersection: public generic {

    Intersection(generic* o1, generic* o2);
    Intersection(generic* o1, generic* o2, generic* o3);
    Intersection(generic* o1, generic* o2, generic* o3, generic* o4);

    virtual Range eval(double* cl);
    virtual regularity_t regularity(double* cl, bool store = true);
    virtual int   tag(double x, double y);
    virtual int   tag(double x, double y, double z);
    virtual void  active(Sequence& seq);
private:
    std::vector<generic*> m_ops;
};


//--------------------------------------------------------
struct Union: public generic {

    Union(generic* o1, generic* o2);

    virtual Range eval(double* cl);
    virtual regularity_t regularity(double* cl, bool store = true);
    virtual void  active(Sequence& seq) {};

private:
    std::vector<generic*> m_ops;
};
//--------------------------------------------------------------------
} /* namespace geoalgebrix */
//--------------------------------------------------------------------
} /* namespace mmx */
//====================================================================
