#pragma once

#include <utility>
#include <realroot/Interval.hpp>
#include <realroot/solver_uv_sleeve.hpp>
#include <realroot/polynomial_bernstein.hpp>
#include <realroot/polynomial_sparse.hpp>
#include <realroot/bernstein_univariate.hpp>
#include <realroot/solver_mv_bernstein.hpp>
#include <realroot/solver_MvBrnApprox.hpp>
#include <realroot/solver_MvBoxApprox.hpp>

//#define OUTSIDE 0
//#define BOUNDARY 1
//#define INSIDE  2
#define UNDETERMINED -1
//====================================================================
namespace mmx {
//namespace csg {

inline double lower(double x) { return x; }
inline double upper(double x) { return x; }

//--------------------------------------------------------------------
#define TMPL template<class CONTROLER>
#define SLVR solver_cell2d<CONTROLER>
//--------------------------------------------------------------------
namespace slvr {

TMPL
struct evaluator {
    typedef typename CONTROLER::EdgePolynomial EdgePolynomial;
    template<class CELL, class POL>
    void static edge_polynomial(EdgePolynomial& f, CONTROLER* d, CELL* cl ,const POL& pol, int v, int s) {
        mmx::tensor::face(f,pol,v,s);
    }
};

//--------------------------------------------------------------------
TMPL
class solver_cell2d {
public:
    typedef CONTROLER                            Controler;
    typedef typename Controler::Cell             Cell;
    typedef typename Controler::Vertex           Vertex;
    typedef typename Controler::EdgePolynomial   EdgePolynomial;
    //typedef polynomial< Interval<double>, with<Bernstein> > Polynomial;
    //typedef polynomial< double, with<Bernstein> > Polynomial;
    //typedef polynomial< double, with<Sparse,DegRevLex> > Polynomial;
    typedef typename Controler::Polynomial             Polynomial;
    typedef std::vector<int>                      Multiplicity;

    solver_cell2d(Controler* ctrl): d(ctrl) {}

    void interior_point(Cell* cl, const std::vector<Polynomial>& pols);
    void edge_point(Cell* cl, const Polynomial& pol);
    void edge_point(Cell* cl, const Polynomial& pol, int v, int s);

    unsigned nb_sol(void)    const { return m_sol.size(); };
    int solution(unsigned i) const { return m_sol[i]; };
    int mu(unsigned i)       const { return m_mul[i]; };

    void clear(void) { m_sol.resize(0); m_mul.resize(0); }

private:
    Controler* d;
    std::vector<int>   m_sol;
    std::vector<int>   m_mul;
};

TMPL
void SLVR::edge_point(Cell* cl, const Polynomial& pol, int v, int s) {

    EdgePolynomial f;
    evaluator<CONTROLER>::edge_polynomial(f, d, cl, pol, v,s);

    //tensor::face(f,pol,v,s);
    //mdebug()<<"edge_point"<<f;

    polynomial<double, with<Bernstein> > dw(1,f.size()-1), up(1,f.size()-1);
    for(unsigned i=0; i<f.size();i++) {
        up[i]= upper(f[i]);
        dw[i]= lower(f[i]);
    }

    int n0 = cl->idx(tmsh::cell<2>::Face[v][s][0]),
            n1 = cl->idx(tmsh::cell<2>::Face[v][s][1]);

    Seq<double> sol;
    double U = (v==1?d->xmin(*cl):d->ymin(*cl)),
            V = (v==1?d->xmax(*cl):d->ymax(*cl));

    Multiplicity mu;
    solver< double, Sleeve<Approximate> >::solve(sol,mu,dw,up,U,V);

    int idx;
    if(v==1){
        double y=(s==0?d->ymin(*cl):d->ymax(*cl));
        for (unsigned i=0; i<sol.size(); i++) {
            //lp.push_back(new VERTEX(sol[i],y));
            idx = d->insert_vertex(Vertex(sol[i],y), n0, n1);
            d->vertex(idx).tag(UNDETERMINED);
            m_sol.push_back(idx);
            m_mul.push_back(mu[i]);
            //cl->insert_boundary(idx, mu[i]);
            //mdebug()<<"+ xvertex"<<"    "<<sol[i]<<y <<"  "<<mult[i]<<" "<<df0;
        }
    } else {
        double x=(s==0?d->xmin(*cl):d->xmax(*cl));
        for (unsigned i=0; i<sol.size(); i++) {
            //lp.push_back(new VERTEX(x,sol[i]));
            idx = d->insert_vertex(Vertex(x,sol[i]), n0, n1);
            d->vertex(idx).tag(UNDETERMINED);
            m_sol.push_back(idx);
            m_mul.push_back(mu[i]);
            //cl->insert_boundary(idx, mu[i]);
            //mdebug()<<"+ yvertex"<<"    "<<x<<sol[i]<<"  "<<mult[i]<<df0;;
        }
    }
    // mdebug()<<"edge_point"<<sol.size();
}

TMPL
void SLVR::edge_point(Cell* cl, const Polynomial& pol) {
    this->clear();
    edge_point(cl, pol, 1, 0);
    edge_point(cl, pol, 0, 1);
    edge_point(cl, pol, 1, 1);
    edge_point(cl, pol, 0, 0);
}

TMPL
void SLVR::interior_point(Cell* cl, const std::vector<Polynomial>& system) {
    this->clear();
    mdebug()<<"Interior_point Solving system"<<system.size();
    for(unsigned i=0; i<system.size();i++)
        mdebug()<<"  "<<system[i];

    box<double,2> domain;
    domain[0]=Interval<double>(d->xmin(*cl),d->xmax(*cl));
    domain[1]=Interval<double>(d->ymin(*cl),d->ymax(*cl));
    mdebug()<<"Domain:"<< domain;

    typedef solver<MvBoxApprox<double> > MvSolver;
    MvSolver::Solutions sol = MvSolver().solve(system, domain);
    mdebug()<<"Solution: ";
    for(unsigned i=0;i<sol.size();i++)
        mdebug()<<" "<<i<<":"<< sol[i];

    int idx;
    for(unsigned i= 0; i< sol.size(); i++) {
        idx = d->insert_vertex(Vertex(sol[i][0].middle(),sol[i][1].middle()));
        m_sol.push_back(idx);
        m_mul.push_back(1);
    }
    //mdebug()<<"Pol 0:"<<system[0].rep()<<"\nPol 1:"<<system[1].rep();

}

//--------------------------------------------------------------------
} /* namespace slvr */
} /* namespace mmx */
//====================================================================
#undef TMPL
#undef SLVR
