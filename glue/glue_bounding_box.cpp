
#include <basix/double.hpp>
#include <basix/int.hpp>
#include <basix/vector.hpp>
#include <basix/port.hpp>
#include <basix/literal.hpp>
#include <numerix/integer.hpp>
#include <numerix/modular.hpp>
#include <numerix/modular_integer.hpp>
#include <numerix/rational.hpp>
#include <numerix/floating.hpp>
#include <shape/axel_glue.hpp>
#include <shape/bounding_box_glue.hpp>
#include <basix/tuple.hpp>
#include <basix/alias.hpp>
#include <basix/glue.hpp>

#define double_literal(x) as_double (as_string (x))
#define int_literal(x) as_int (as_string (x))
#define is_generic_literal is<literal>
#define gen_literal_apply(f,v) gen (as<generic> (f), v)
#define gen_literal_access(f,v) access (as<generic> (f), v)

namespace mmx {
  static shape_axel
  GLUE_1 () {
    return shape_axel ();
  }
  
  static shape_axel
  GLUE_2 (const string &arg_1) {
    return shape_axel_string (arg_1);
  }
  
  static void
  GLUE_3 (const shape_axel &arg_1) {
    shape_axel_view (arg_1);
  }
  
  static shape_bounding_box
  GLUE_4 () {
    return shape_bounding_box ();
  }
  
  static shape_bounding_box
  GLUE_5 (const tuple<double> &arg_1) {
    return shape_bounding_box_from_vector (as_vector (arg_1));
  }
  
  static shape_bounding_box
  GLUE_6 (const tuple<mmx_floating> &arg_1) {
    return shape_bounding_box_from_vector (as_vector (arg_1));
  }
  
  static alias<shape_axel>
  GLUE_7 (const alias<shape_axel> &arg_1, const shape_bounding_box &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static int
  GLUE_8 (const shape_axel &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_9 (const shape_axel &arg_1, const shape_axel &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_10 (const shape_axel &arg_1, const shape_axel &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_11 (const shape_bounding_box &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_12 (const shape_bounding_box &arg_1, const shape_bounding_box &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_13 (const shape_bounding_box &arg_1, const shape_bounding_box &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  void
  glue_bounding_box () {
    static bool done = false;
    if (done) return;
    done = true;
    call_glue (string ("glue_double"));
    call_glue (string ("glue_floating"));
    define_type<shape_axel > (lit ("Axel"));
    define ("axel", GLUE_1);
    define ("axel", GLUE_2);
    define ("view", GLUE_3);
    define_type<shape_bounding_box > (lit ("BoundingBox"));
    define ("bounding_box", GLUE_4);
    define ("bounding_box", GLUE_5);
    define ("bounding_box", GLUE_6);
    define ("<<", GLUE_7);
    define ("hash%", GLUE_8);
    define ("=%", GLUE_9);
    define ("!=%", GLUE_10);
    define ("hash%", GLUE_11);
    define ("=%", GLUE_12);
    define ("!=%", GLUE_13);
  }
}
