
#include <basix/double.hpp>
#include <basix/int.hpp>
#include <basix/vector.hpp>
#include <basix/port.hpp>
#include <shape/axel_glue.hpp>
#include <basix/literal.hpp>
#include <numerix/integer.hpp>
#include <numerix/modular.hpp>
#include <numerix/modular_integer.hpp>
#include <numerix/rational.hpp>
#include <numerix/floating.hpp>
#include <shape/point_glue.hpp>
#include <shape/color_glue.hpp>
#include <shape/mesh_glue.hpp>
#include <basix/alias.hpp>
#include <basix/glue.hpp>

#define double_literal(x) as_double (as_string (x))
#define int_literal(x) as_int (as_string (x))
#define is_generic_literal is<literal>
#define gen_literal_apply(f,v) gen (as<generic> (f), v)
#define gen_literal_access(f,v) access (as<generic> (f), v)
#define shape_point shape::point

namespace mmx {
  static shape_mesh
  GLUE_1 () {
    return shape_mesh ();
  }
  
  static alias<shape_axel>
  GLUE_2 (const alias<shape_axel> &arg_1, const shape_mesh &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static alias<shape_mesh>
  GLUE_3 (const alias<shape_mesh> &arg_1, const vector<generic> &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static int
  GLUE_4 (const shape_mesh &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_5 (const shape_mesh &arg_1, const shape_mesh &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_6 (const shape_mesh &arg_1, const shape_mesh &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  void
  glue_mesh () {
    static bool done = false;
    if (done) return;
    done = true;
    call_glue (string ("glue_double"));
    call_glue (string ("glue_string"));
    call_glue (string ("glue_basix_vector_generic"));
    call_glue (string ("glue_axel"));
    call_glue (string ("glue_point_floating"));
    call_glue (string ("glue_color"));
    define_type<shape_mesh > (lit ("Mesh"));
    define ("mesh", GLUE_1);
    define ("<<", GLUE_2);
    define ("<<", GLUE_3);
    define ("hash%", GLUE_4);
    define ("=%", GLUE_5);
    define ("!=%", GLUE_6);
  }
}
