
#include <basix/double.hpp>
#include <basix/int.hpp>
#include <basix/vector.hpp>
#include <basix/port.hpp>
#include <basix/literal.hpp>
#include <numerix/integer.hpp>
#include <numerix/modular.hpp>
#include <numerix/modular_integer.hpp>
#include <numerix/rational.hpp>
#include <numerix/floating.hpp>
#include <numerix/kernel.hpp>
#include <realroot/polynomial.hpp>
#include <realroot/polynomial_glue.hpp>
#include <realroot/ring_sparse_glue.hpp>
#include <shape/axel_glue.hpp>
#include <shape/algebraic_curve_glue.hpp>
#include <shape/algebraic_surface_glue.hpp>
#include <basix/alias.hpp>
#include <basix/glue.hpp>

#define double_literal(x) as_double (as_string (x))
#define int_literal(x) as_int (as_string (x))
#define is_generic_literal is<literal>
#define gen_literal_apply(f,v) gen (as<generic> (f), v)
#define gen_literal_access(f,v) access (as<generic> (f), v)
#define set_of_generic set_of(generic)
#define set_of_double set_of(double)
#define set_of_integer set_of(integer)
#define set_of_rational set_of(rational)
#define set_of_bigfloat set_of(bigfloat)
#define set_of_complex_bigfloat set_of(complex_bigfloat)

namespace mmx {
  static alias<shape_axel>
  GLUE_1 (const alias<shape_axel> &arg_1, const shape_algebraic_surface &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static shape_algebraic_curve
  GLUE_2 (const shape_algebraic_surface &arg_1, const shape_algebraic_surface &arg_2) {
    return shape_algebraic_surface_intersection (arg_1, arg_2);
  }
  
  static int
  GLUE_3 (const shape_algebraic_surface &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_4 (const shape_algebraic_surface &arg_1, const shape_algebraic_surface &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_5 (const shape_algebraic_surface &arg_1, const shape_algebraic_surface &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  void
  glue_algebraic_surface () {
    static bool done = false;
    if (done) return;
    done = true;
    call_glue (string ("glue_double"));
    call_glue (string ("glue_string"));
    call_glue (string ("glue_basix_vector_generic"));
    call_glue (string ("glue_ring_sparse_rational"));
    call_glue (string ("glue_algebraic_curve"));
    define_type<shape_algebraic_surface > (lit ("AlgebraicSurface"));
    define ("<<", GLUE_1);
    define ("*", GLUE_2);
    define ("hash%", GLUE_3);
    define ("=%", GLUE_4);
    define ("!=%", GLUE_5);
  }
}
