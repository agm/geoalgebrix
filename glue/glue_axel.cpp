
#include <shape/axel_glue.hpp>
#include <basix/glue.hpp>

namespace mmx {
  static shape_axel
  GLUE_1 () {
    return shape_axel ();
  }
  
  static shape_axel
  GLUE_2 (const string &arg_1) {
    return shape_axel_string (arg_1);
  }
  
  static void
  GLUE_3 (const shape_axel &arg_1) {
    shape_axel_view (arg_1);
  }
  
  void
  glue_axel () {
    static bool done = false;
    if (done) return;
    done = true;
    define_type<shape_axel > (lit ("Axel"));
    define ("axel", GLUE_1);
    define ("axel", GLUE_2);
    define ("view", GLUE_3);
  }
}
