/******************************************************************************
* MODULE     : implicit_curve_glue.hpp
* DESCRIPTION: Glue for implicit curve
* COPYRIGHT  : (C) 2007 Bernard Mourrain
*******************************************************************************
* This software falls under the GNU general public license and comes WITHOUT
* ANY WARRANTY WHATSOEVER. See the file $TEXMACS_PATH/LICENSE for more details.
* If you don't have this file, write to the Free Software Foundation, Inc.,
* 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
******************************************************************************/
#ifndef __MMX_SHAPE_IMPLICIT_CURVE_GLUE_HPP
#define __MMX_SHAPE_IMPLICIT_CURVE_GLUE_HPP
#include <basix/glue.hpp>
#include <basix/tuple.hpp>
#include <realroot/ring_sparse_glue.hpp>
#include <glue_axel_viewer.hpp>
#include <shape/BoundingBox.hpp>
#include <shape/AlgebraicCurve.hpp>


namespace mmx {

#define CURVE shape::ImplicitCurve
#define BOX   shape::BoundingBox
#define Polynomial polynom< ring<mmx::rational,Sparse<DegReveLex> > >

  namespace shape {
    inline bool operator ==(const CURVE& v1, const CURVE& v2) {return true;}
    inline bool operator !=(const CURVE& v1, const CURVE& v2) {return !(v1==v2);}
    inline bool  eq(const CURVE& v1, const CURVE& v2) {return v1==v2;}
    inline bool neq(const CURVE& v1, const CURVE& v2) {return v1!=v2;}
    
    inline unsigned hash (const CURVE& v) 
    {
      register unsigned i, h= 214365, n=1;
      for (i=0; i<n; i++) h= (h<<1) ^ (h<<5) ^ (h>>27) ;//^ hash(v[i]);
      return h;
    }
    inline unsigned soft_hash (const CURVE& m) {return hash(m);}

    syntactic flatten (const CURVE& s) 
    {
      using namespace shape;
      
      syntactic res = "ImplicitCurve";
      
      vector<syntactic> box;
      box<<mmx::flatten(s.boundingBox()->xmin());
      box<<mmx::flatten(s.boundingBox()->xmax());
      box<<mmx::flatten(s.boundingBox()->ymin());
      box<<mmx::flatten(s.boundingBox()->ymax());
      
      return apply(res, mmx::flatten(s.equation()), mmx::flatten(box));
      
    }
  }
  
  CURVE implicit_curve_string (const string& eq)
  {
    return CURVE (as_charp(eq));
  }

  CURVE implicit_curve_string (const string& eq, const BOX& bx)
  {
    BOX* b = new BOX(bx);
    return CURVE(as_charp(eq), b);
  }

  CURVE implicit_curve(const Polynomial& eq, const BOX& bx)
  {
    BOX* b = new BOX(bx);
    return CURVE(eq, b);
  }
  

  axel::ostream& operator<<(axel::ostream& os, const CURVE& c)
  {
    os<<"<curve type=\"implicit\">\n";
    os<<"  <domain>"
      <<c.boundingBox()->xmin()<<" "<<c.boundingBox()->xmax()<<" "
      <<c.boundingBox()->ymin()<<" "<<c.boundingBox()->ymax()
      <<"</domain>\n";
    os<<"  <polynomial>";
    MPOLDST::print(os,c.equation(),Variables("x y"));
    os<<"</polynomial>\n";
    os<<"</curve>\n";
  return os;
  }


#undef BOX
#undef CURVE
#undef Polynomial
//====================================================================
} // namespace mmx
#endif // __MMX_IMPLICIT_CURVE_GLUE_HPP
