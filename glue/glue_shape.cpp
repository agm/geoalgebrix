
#include <basix/system.hpp>
#include <basix/glue.hpp>

namespace mmx {
  extern void glue_algebraic_curve ();
  extern void glue_algebraic_surface ();
  extern void glue_axel ();
  extern void glue_bounding_box ();
  extern void glue_color ();
  extern void glue_mesh ();
  extern void glue_point ();
  extern void glue_point_floating ();
  extern void glue_rational_curve ();
  
  void
  glue_shape () {
    static bool done = false;
    if (done) return;
    done = true;
    register_glue (string ("glue_algebraic_curve"), (& (glue_algebraic_curve)));
    register_glue (string ("glue_algebraic_surface"), (& (glue_algebraic_surface)));
    register_glue (string ("glue_axel"), (& (glue_axel)));
    register_glue (string ("glue_bounding_box"), (& (glue_bounding_box)));
    register_glue (string ("glue_color"), (& (glue_color)));
    register_glue (string ("glue_mesh"), (& (glue_mesh)));
    register_glue (string ("glue_point"), (& (glue_point)));
    register_glue (string ("glue_point_floating"), (& (glue_point_floating)));
    register_glue (string ("glue_rational_curve"), (& (glue_rational_curve)));
    register_glue (string ("glue_shape"), (& (glue_shape)));
    dl_link ("realroot");
    glue_algebraic_curve ();
    glue_algebraic_surface ();
    glue_axel ();
    glue_bounding_box ();
    glue_color ();
    glue_mesh ();
    glue_point ();
    glue_point_floating ();
    glue_rational_curve ();
  }
}

void (*define_shape) () = mmx::glue_shape;
