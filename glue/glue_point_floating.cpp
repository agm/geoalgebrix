
#include <basix/int.hpp>
#include <basix/vector.hpp>
#include <basix/port.hpp>
#include <basix/literal.hpp>
#include <numerix/integer.hpp>
#include <numerix/modular.hpp>
#include <numerix/modular_integer.hpp>
#include <numerix/rational.hpp>
#include <numerix/floating.hpp>
#include <shape/axel_glue.hpp>
#include <shape/point_glue.hpp>
#include <basix/tuple.hpp>
#include <basix/alias.hpp>
#include <basix/glue.hpp>

#define int_literal(x) as_int (as_string (x))
#define is_generic_literal is<literal>
#define gen_literal_apply(f,v) gen (as<generic> (f), v)
#define gen_literal_access(f,v) access (as<generic> (f), v)
#define shape_point shape::point

namespace mmx {
  static shape_axel
  GLUE_1 () {
    return shape_axel ();
  }
  
  static shape_axel
  GLUE_2 (const string &arg_1) {
    return shape_axel_string (arg_1);
  }
  
  static void
  GLUE_3 (const shape_axel &arg_1) {
    shape_axel_view (arg_1);
  }
  
  static shape_point<mmx_floating>
  GLUE_4 (const tuple<mmx_floating> &arg_1) {
    return shape_point_tuple<mmx_floating > (as_vector (arg_1));
  }
  
  static mmx_floating
  GLUE_5 (const shape_point<mmx_floating> &arg_1, const int &arg_2) {
    return arg_1[arg_2];
  }
  
  static alias<mmx_floating>
  GLUE_6 (const alias<shape_point<mmx_floating> > &arg_1, const int &arg_2) {
    return alias_access<mmx_floating > (arg_1, arg_2);
  }
  
  static shape_point<mmx_floating>
  GLUE_7 (const shape_point<mmx_floating> &arg_1, const shape_point<mmx_floating> &arg_2) {
    return arg_1 - arg_2;
  }
  
  static alias<shape_axel>
  GLUE_8 (const alias<shape_axel> &arg_1, const shape_point<mmx_floating> &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static int
  GLUE_9 (const shape_axel &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_10 (const shape_axel &arg_1, const shape_axel &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_11 (const shape_axel &arg_1, const shape_axel &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_12 (const shape_point<mmx_floating> &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_13 (const shape_point<mmx_floating> &arg_1, const shape_point<mmx_floating> &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_14 (const shape_point<mmx_floating> &arg_1, const shape_point<mmx_floating> &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static vector<mmx_floating>
  GLUE_15 (const tuple<mmx_floating> &arg_1) {
    return vector<mmx_floating > (as_vector (arg_1));
  }
  
  static vector<mmx_floating>
  GLUE_16 (const tuple<mmx_floating> &arg_1) {
    return vector<mmx_floating > (as_vector (arg_1));
  }
  
  static iterator<generic>
  GLUE_17 (const vector<mmx_floating> &arg_1) {
    return as<iterator<generic> > (iterate (arg_1));
  }
  
  static int
  GLUE_18 (const vector<mmx_floating> &arg_1) {
    return N (arg_1);
  }
  
  static mmx_floating
  GLUE_19 (const vector<mmx_floating> &arg_1, const int &arg_2) {
    return arg_1[arg_2];
  }
  
  static alias<mmx_floating>
  GLUE_20 (const alias<vector<mmx_floating> > &arg_1, const int &arg_2) {
    return alias_access<mmx_floating > (arg_1, arg_2);
  }
  
  static vector<mmx_floating>
  GLUE_21 (const vector<mmx_floating> &arg_1, const int &arg_2, const int &arg_3) {
    return range (arg_1, arg_2, arg_3);
  }
  
  static vector<mmx_floating>
  GLUE_22 (const vector<mmx_floating> &arg_1) {
    return reverse (arg_1);
  }
  
  static vector<mmx_floating>
  GLUE_23 (const vector<mmx_floating> &arg_1, const vector<mmx_floating> &arg_2) {
    return append (arg_1, arg_2);
  }
  
  static alias<vector<mmx_floating> >
  GLUE_24 (const alias<vector<mmx_floating> > &arg_1, const vector<mmx_floating> &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static void
  GLUE_25 (const vector<mmx_floating> &arg_1, const vector<mmx_floating> &arg_2) {
    inside_append (arg_1, arg_2);
  }
  
  static vector<mmx_floating>
  GLUE_26 (const mmx_floating &arg_1, const vector<mmx_floating> &arg_2) {
    return cons (arg_1, arg_2);
  }
  
  static mmx_floating
  GLUE_27 (const vector<mmx_floating> &arg_1) {
    return car (arg_1);
  }
  
  static vector<mmx_floating>
  GLUE_28 (const vector<mmx_floating> &arg_1) {
    return cdr (arg_1);
  }
  
  static bool
  GLUE_29 (const vector<mmx_floating> &arg_1) {
    return is_nil (arg_1);
  }
  
  static bool
  GLUE_30 (const vector<mmx_floating> &arg_1) {
    return is_atom (arg_1);
  }
  
  static vector<mmx_floating>
  GLUE_31 (const vector<mmx_floating> &arg_1, const mmx_floating &arg_2) {
    return insert (arg_1, arg_2);
  }
  
  static int
  GLUE_32 (const vector<mmx_floating> &arg_1, const mmx_floating &arg_2) {
    return find (arg_1, arg_2);
  }
  
  static bool
  GLUE_33 (const vector<mmx_floating> &arg_1, const mmx_floating &arg_2) {
    return contains (arg_1, arg_2);
  }
  
  static vector<generic>
  GLUE_34 (const vector<mmx_floating> &arg_1) {
    return as<vector<generic> > (arg_1);
  }
  
  static shape_point<mmx_floating>
  GLUE_35 (const shape_point<mmx_floating> &arg_1, const vector<mmx_floating> &arg_2) {
    return arg_1 + arg_2;
  }
  
  static int
  GLUE_36 (const vector<mmx_floating> &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_37 (const vector<mmx_floating> &arg_1, const vector<mmx_floating> &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_38 (const vector<mmx_floating> &arg_1, const vector<mmx_floating> &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  void
  glue_point_floating () {
    static bool done = false;
    if (done) return;
    done = true;
    call_glue (string ("glue_floating"));
    define_type<shape_axel > (lit ("Axel"));
    define ("axel", GLUE_1);
    define ("axel", GLUE_2);
    define ("view", GLUE_3);
    define_type<shape_point<mmx_floating> > (gen (lit ("Point"), lit ("Floating")));
    define ("point", GLUE_4);
    define (".[]", GLUE_5);
    define (".[]", GLUE_6);
    define ("-", GLUE_7);
    define ("<<", GLUE_8);
    define ("hash%", GLUE_9);
    define ("=%", GLUE_10);
    define ("!=%", GLUE_11);
    define ("hash%", GLUE_12);
    define ("=%", GLUE_13);
    define ("!=%", GLUE_14);
    define_type<vector<mmx_floating> > (gen (lit ("Vector"), lit ("Floating")));
    define ("vector", GLUE_15);
    define ("[]", GLUE_16);
    define_converter (":>", GLUE_17, PENALTY_CAST);
    define ("#", GLUE_18);
    define (".[]", GLUE_19);
    define (".[]", GLUE_20);
    define (".[]", GLUE_21);
    define ("reverse", GLUE_22);
    define ("><", GLUE_23);
    define ("<<", GLUE_24);
    define ("append", GLUE_25);
    define ("cons", GLUE_26);
    define ("car", GLUE_27);
    define ("cdr", GLUE_28);
    define ("nil?", GLUE_29);
    define ("atom?", GLUE_30);
    define ("insert", GLUE_31);
    define ("find", GLUE_32);
    define ("contains?", GLUE_33);
    define_converter (":>", GLUE_34, PENALTY_PROMOTE_GENERIC);
    define ("+", GLUE_35);
    define ("hash%", GLUE_36);
    define ("=%", GLUE_37);
    define ("!=%", GLUE_38);
  }
}
