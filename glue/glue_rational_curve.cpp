
#include <basix/double.hpp>
#include <basix/int.hpp>
#include <basix/vector.hpp>
#include <basix/port.hpp>
#include <basix/literal.hpp>
#include <numerix/integer.hpp>
#include <numerix/modular.hpp>
#include <numerix/modular_integer.hpp>
#include <numerix/rational.hpp>
#include <numerix/floating.hpp>
#include <realroot/Interval_glue.hpp>
#include <numerix/kernel.hpp>
#include <realroot/polynomial.hpp>
#include <realroot/polynomial_glue.hpp>
#include <realroot/ring_sparse_glue.hpp>
#include <shape/axel_glue.hpp>
#include <shape/rational_curve_glue.hpp>
#include <basix/alias.hpp>
#include <basix/glue.hpp>

#define double_literal(x) as_double (as_string (x))
#define int_literal(x) as_int (as_string (x))
#define is_generic_literal is<literal>
#define gen_literal_apply(f,v) gen (as<generic> (f), v)
#define gen_literal_access(f,v) access (as<generic> (f), v)
#define set_of_generic set_of(generic)
#define set_of_double set_of(double)
#define set_of_integer set_of(integer)
#define set_of_rational set_of(rational)
#define set_of_bigfloat set_of(bigfloat)
#define set_of_complex_bigfloat set_of(complex_bigfloat)

namespace mmx {
  static interval<double>
  GLUE_1 (const double &arg_1) {
    return interval<double > (arg_1);
  }
  
  static interval<double>
  GLUE_2 (const double &arg_1, const double &arg_2) {
    return interval_from_pair (arg_1, arg_2);
  }
  
  static interval<double>
  GLUE_3 (const interval<double> &arg_1) {
    return -arg_1;
  }
  
  static interval<double>
  GLUE_4 (const interval<double> &arg_1, const interval<double> &arg_2) {
    return arg_1 + arg_2;
  }
  
  static interval<double>
  GLUE_5 (const interval<double> &arg_1, const interval<double> &arg_2) {
    return arg_1 - arg_2;
  }
  
  static interval<double>
  GLUE_6 (const interval<double> &arg_1, const interval<double> &arg_2) {
    return arg_1 * arg_2;
  }
  
  static interval<double>
  GLUE_7 (const double &arg_1, const interval<double> &arg_2) {
    return arg_1 + arg_2;
  }
  
  static interval<double>
  GLUE_8 (const interval<double> &arg_1, const double &arg_2) {
    return arg_1 + arg_2;
  }
  
  static interval<double>
  GLUE_9 (const double &arg_1, const interval<double> &arg_2) {
    return arg_1 - arg_2;
  }
  
  static interval<double>
  GLUE_10 (const interval<double> &arg_1, const double &arg_2) {
    return arg_1 - arg_2;
  }
  
  static interval<double>
  GLUE_11 (const double &arg_1, const interval<double> &arg_2) {
    return arg_1 * arg_2;
  }
  
  static interval<double>
  GLUE_12 (const interval<double> &arg_1, const double &arg_2) {
    return arg_1 * arg_2;
  }
  
  static interval<double>
  GLUE_13 (const interval<double> &arg_1, const interval<double> &arg_2) {
    return arg_1 / arg_2;
  }
  
  static double
  GLUE_14 (const interval<double> &arg_1) {
    return interval_lower (arg_1);
  }
  
  static double
  GLUE_15 (const interval<double> &arg_1) {
    return interval_upper (arg_1);
  }
  
  static alias<shape_axel>
  GLUE_16 (const alias<shape_axel> &arg_1, const shape_rational_curve &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static int
  GLUE_17 (const shape_axel &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_18 (const shape_axel &arg_1, const shape_axel &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_19 (const shape_axel &arg_1, const shape_axel &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_20 (const interval<double> &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_21 (const interval<double> &arg_1, const interval<double> &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_22 (const interval<double> &arg_1, const interval<double> &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_23 (const shape_rational_curve &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_24 (const shape_rational_curve &arg_1, const shape_rational_curve &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_25 (const shape_rational_curve &arg_1, const shape_rational_curve &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  void
  glue_rational_curve () {
    static bool done = false;
    if (done) return;
    done = true;
    call_glue (string ("glue_double"));
    call_glue (string ("glue_string"));
    call_glue (string ("glue_basix_vector_generic"));
    call_glue (string ("glue_interval"));
    call_glue (string ("glue_ring_sparse_rational"));
    call_glue (string ("glue_axel"));
    define_type<interval<double> > (gen (lit ("Interval"), lit ("Double")));
    define ("interval", GLUE_1);
    define ("interval", GLUE_2);
    define ("-", GLUE_3);
    define ("+", GLUE_4);
    define ("-", GLUE_5);
    define ("*", GLUE_6);
    define ("+", GLUE_7);
    define ("+", GLUE_8);
    define ("-", GLUE_9);
    define ("-", GLUE_10);
    define ("*", GLUE_11);
    define ("*", GLUE_12);
    define ("/", GLUE_13);
    define ("lower", GLUE_14);
    define ("upper", GLUE_15);
    define_type<shape_rational_curve > (lit ("RationalCurve"));
    define ("<<", GLUE_16);
    define ("hash%", GLUE_17);
    define ("=%", GLUE_18);
    define ("!=%", GLUE_19);
    define ("hash%", GLUE_20);
    define ("=%", GLUE_21);
    define ("!=%", GLUE_22);
    define ("hash%", GLUE_23);
    define ("=%", GLUE_24);
    define ("!=%", GLUE_25);
  }
}
