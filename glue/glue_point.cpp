
#include <basix/int.hpp>
#include <basix/vector.hpp>
#include <basix/port.hpp>
#include <basix/literal.hpp>
#include <numerix/integer.hpp>
#include <numerix/modular.hpp>
#include <numerix/modular_integer.hpp>
#include <numerix/rational.hpp>
#include <numerix/floating.hpp>
#include <shape/axel_glue.hpp>
#include <shape/point_glue.hpp>
#include <basix/tuple.hpp>
#include <basix/alias.hpp>
#include <basix/glue.hpp>

#define int_literal(x) as_int (as_string (x))
#define is_generic_literal is<literal>
#define gen_literal_apply(f,v) gen (as<generic> (f), v)
#define gen_literal_access(f,v) access (as<generic> (f), v)
#define shape_point shape::point

namespace mmx {
  static int
  GLUE_1 (const int &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_2 (const int &arg_1, const int &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_3 (const int &arg_1, const int &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_4 (const string &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_5 (const string &arg_1, const string &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_6 (const string &arg_1, const string &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_7 (const literal &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_8 (const literal &arg_1, const literal &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_9 (const literal &arg_1, const literal &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_10 (const literal &arg_1) {
    return int_literal (arg_1);
  }
  
  static int
  GLUE_11 (const string &arg_1) {
    return as_int (arg_1);
  }
  
  static string
  GLUE_12 (const int &arg_1) {
    return as_string (arg_1);
  }
  
  static string
  GLUE_13 (const int &arg_1) {
    return as_string_hexa (arg_1);
  }
  
  static int
  GLUE_14 (const int &arg_1) {
    return -arg_1;
  }
  
  static int
  GLUE_15 (const int &arg_1) {
    return square (arg_1);
  }
  
  static int
  GLUE_16 (const int &arg_1, const int &arg_2) {
    return arg_1 + arg_2;
  }
  
  static int
  GLUE_17 (const int &arg_1, const int &arg_2) {
    return arg_1 - arg_2;
  }
  
  static int
  GLUE_18 (const int &arg_1, const int &arg_2) {
    return arg_1 * arg_2;
  }
  
  static int
  GLUE_19 (const int &arg_1, const int &arg_2) {
    return arg_1 / arg_2;
  }
  
  static int
  GLUE_20 (const int &arg_1, const int &arg_2) {
    return quo (arg_1, arg_2);
  }
  
  static int
  GLUE_21 (const int &arg_1, const int &arg_2) {
    return rem (arg_1, arg_2);
  }
  
  static bool
  GLUE_22 (const int &arg_1, const int &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_23 (const int &arg_1, const int &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_24 (const int &arg_1, const int &arg_2) {
    return arg_1 < arg_2;
  }
  
  static bool
  GLUE_25 (const int &arg_1, const int &arg_2) {
    return arg_1 <= arg_2;
  }
  
  static bool
  GLUE_26 (const int &arg_1, const int &arg_2) {
    return arg_1 > arg_2;
  }
  
  static bool
  GLUE_27 (const int &arg_1, const int &arg_2) {
    return arg_1 >= arg_2;
  }
  
  static int
  GLUE_28 (const int &arg_1) {
    return sign (arg_1);
  }
  
  static int
  GLUE_29 (const int &arg_1) {
    return abs (arg_1);
  }
  
  static int
  GLUE_30 (const int &arg_1, const int &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static int
  GLUE_31 (const int &arg_1, const int &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static int
  GLUE_32 (const int &arg_1, const int &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static int
  GLUE_33 (const int &arg_1, const int &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static int
  GLUE_34 (const int &arg_1, const int &arg_2) {
    return arg_1 & arg_2;
  }
  
  static int
  GLUE_35 (const int &arg_1, const int &arg_2) {
    return arg_1 | arg_2;
  }
  
  static int
  GLUE_36 (const int &arg_1, const int &arg_2) {
    return arg_1 ^ arg_2;
  }
  
  static int
  GLUE_37 (const int &arg_1) {
    return floor_sqrt (arg_1);
  }
  
  static bool
  GLUE_38 (const int &arg_1, const int &arg_2) {
    return divides (arg_1, arg_2);
  }
  
  static int
  GLUE_39 () {
    return rand ();
  }
  
  static bool
  GLUE_40 (const generic &arg_1) {
    return is_vector (arg_1);
  }
  
  static vector<generic>
  GLUE_41 (const tuple<generic> &arg_1) {
    return vector<generic > (as_vector (arg_1));
  }
  
  static vector<generic>
  GLUE_42 (const tuple<generic> &arg_1) {
    return vector<generic > (as_vector (arg_1));
  }
  
  static iterator<generic>
  GLUE_43 (const vector<generic> &arg_1) {
    return iterate (arg_1);
  }
  
  static int
  GLUE_44 (const vector<generic> &arg_1) {
    return N (arg_1);
  }
  
  static generic
  GLUE_45 (const vector<generic> &arg_1, const int &arg_2) {
    return arg_1[arg_2];
  }
  
  static alias<generic>
  GLUE_46 (const alias<vector<generic> > &arg_1, const int &arg_2) {
    return alias_access<generic > (arg_1, arg_2);
  }
  
  static vector<generic>
  GLUE_47 (const vector<generic> &arg_1, const int &arg_2, const int &arg_3) {
    return range (arg_1, arg_2, arg_3);
  }
  
  static vector<generic>
  GLUE_48 (const vector<generic> &arg_1) {
    return reverse (arg_1);
  }
  
  static vector<generic>
  GLUE_49 (const vector<generic> &arg_1, const vector<generic> &arg_2) {
    return append (arg_1, arg_2);
  }
  
  static alias<vector<generic> >
  GLUE_50 (const alias<vector<generic> > &arg_1, const vector<generic> &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static void
  GLUE_51 (const vector<generic> &arg_1, const vector<generic> &arg_2) {
    inside_append (arg_1, arg_2);
  }
  
  static vector<generic>
  GLUE_52 (const generic &arg_1, const vector<generic> &arg_2) {
    return cons (arg_1, arg_2);
  }
  
  static generic
  GLUE_53 (const vector<generic> &arg_1) {
    return car (arg_1);
  }
  
  static vector<generic>
  GLUE_54 (const vector<generic> &arg_1) {
    return cdr (arg_1);
  }
  
  static bool
  GLUE_55 (const vector<generic> &arg_1) {
    return is_nil (arg_1);
  }
  
  static bool
  GLUE_56 (const vector<generic> &arg_1) {
    return is_atom (arg_1);
  }
  
  static vector<generic>
  GLUE_57 (const vector<generic> &arg_1, const generic &arg_2) {
    return insert (arg_1, arg_2);
  }
  
  static int
  GLUE_58 (const vector<generic> &arg_1, const generic &arg_2) {
    return find (arg_1, arg_2);
  }
  
  static bool
  GLUE_59 (const vector<generic> &arg_1, const generic &arg_2) {
    return contains (arg_1, arg_2);
  }
  
  static bool
  GLUE_60 (const generic &arg_1) {
    return generic_is_string (arg_1);
  }
  
  static int
  GLUE_61 (const string &arg_1) {
    return N (arg_1);
  }
  
  static string
  GLUE_62 (const string &arg_1, const int &arg_2, const int &arg_3) {
    return arg_1 (arg_2, arg_3);
  }
  
  static string
  GLUE_63 (const string &arg_1, const string &arg_2) {
    return arg_1 * arg_2;
  }
  
  static string
  GLUE_64 (const string &arg_1, const string &arg_2) {
    return arg_1 * arg_2;
  }
  
  static alias<string>
  GLUE_65 (const alias<string> &arg_1, const string &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static bool
  GLUE_66 (const string &arg_1, const string &arg_2) {
    return arg_1 < arg_2;
  }
  
  static bool
  GLUE_67 (const string &arg_1, const string &arg_2) {
    return arg_1 <= arg_2;
  }
  
  static bool
  GLUE_68 (const string &arg_1, const string &arg_2) {
    return arg_1 > arg_2;
  }
  
  static bool
  GLUE_69 (const string &arg_1, const string &arg_2) {
    return arg_1 >= arg_2;
  }
  
  static bool
  GLUE_70 (const string &arg_1, const string &arg_2) {
    return starts (arg_1, arg_2);
  }
  
  static bool
  GLUE_71 (const string &arg_1, const string &arg_2) {
    return ends (arg_1, arg_2);
  }
  
  static string
  GLUE_72 (const string &arg_1, const string &arg_2, const string &arg_3) {
    return replace (arg_1, arg_2, arg_3);
  }
  
  static int
  GLUE_73 (const string &arg_1, const string &arg_2, const int &arg_3) {
    return search_forwards (arg_1, arg_2, arg_3);
  }
  
  static int
  GLUE_74 (const string &arg_1, const string &arg_2, const int &arg_3) {
    return search_backwards (arg_1, arg_2, arg_3);
  }
  
  static string
  GLUE_75 (const string &arg_1) {
    return upcase (arg_1);
  }
  
  static string
  GLUE_76 (const string &arg_1) {
    return locase (arg_1);
  }
  
  static string
  GLUE_77 (const string &arg_1) {
    return upcase_first (arg_1);
  }
  
  static string
  GLUE_78 (const string &arg_1) {
    return locase_first (arg_1);
  }
  
  static string
  GLUE_79 (const string &arg_1) {
    return quote (arg_1);
  }
  
  static string
  GLUE_80 (const string &arg_1) {
    return unquote (arg_1);
  }
  
  static string
  GLUE_81 (const int &arg_1) {
    return charcode_as_string (arg_1);
  }
  
  static int
  GLUE_82 (const string &arg_1) {
    return string_as_charcode (arg_1);
  }
  
  static bool
  GLUE_83 (const string &arg_1) {
    return is_integer_string (arg_1);
  }
  
  static bool
  GLUE_84 (const string &arg_1) {
    return is_floating_string (arg_1);
  }
  
  static bool
  GLUE_85 (const string &arg_1) {
    return is_alpha (arg_1);
  }
  
  static bool
  GLUE_86 (const string &arg_1) {
    return is_numeric (arg_1);
  }
  
  static bool
  GLUE_87 (const string &arg_1) {
    return is_alpha_numeric (arg_1);
  }
  
  static bool
  GLUE_88 (const string &arg_1) {
    return is_mmx_identifier (arg_1);
  }
  
  static bool
  GLUE_89 (const generic &arg_1) {
    return is_generic_literal (arg_1);
  }
  
  static generic
  GLUE_90 (const literal &arg_1, const tuple<generic> &arg_2) {
    return gen_literal_apply (arg_1, as_vector (arg_2));
  }
  
  static generic
  GLUE_91 (const literal &arg_1, const tuple<generic> &arg_2) {
    return gen_literal_access (arg_1, as_vector (arg_2));
  }
  
  static literal
  GLUE_92 (const string &arg_1) {
    return literal (arg_1);
  }
  
  static string
  GLUE_93 (const literal &arg_1) {
    return *arg_1;
  }
  
  static integer
  GLUE_94 (const literal &arg_1) {
    return make_literal_integer (arg_1);
  }
  
  static integer
  GLUE_95 (const int &arg_1) {
    return integer (arg_1);
  }
  
  static integer
  GLUE_96 (const int &arg_1) {
    return integer (arg_1);
  }
  
  static bool
  GLUE_97 (const integer &arg_1) {
    return is_int (arg_1);
  }
  
  static int
  GLUE_98 (const integer &arg_1) {
    return as_int (arg_1);
  }
  
  static integer
  GLUE_99 (const string &arg_1) {
    return integer (arg_1);
  }
  
  static string
  GLUE_100 (const integer &arg_1) {
    return as_string (arg_1);
  }
  
  static generic
  GLUE_101 (const int &arg_1) {
    return integer_construct (arg_1);
  }
  
  static int
  GLUE_102 (const integer &arg_1) {
    return as_int (arg_1);
  }
  
  static integer
  GLUE_103 (const integer &arg_1) {
    return -arg_1;
  }
  
  static integer
  GLUE_104 (const integer &arg_1) {
    return square (arg_1);
  }
  
  static integer
  GLUE_105 (const integer &arg_1, const integer &arg_2) {
    return arg_1 + arg_2;
  }
  
  static integer
  GLUE_106 (const integer &arg_1, const integer &arg_2) {
    return arg_1 - arg_2;
  }
  
  static integer
  GLUE_107 (const integer &arg_1, const integer &arg_2) {
    return arg_1 * arg_2;
  }
  
  static integer
  GLUE_108 (const integer &arg_1, const integer &arg_2) {
    return arg_1 / arg_2;
  }
  
  static integer
  GLUE_109 (const integer &arg_1, const integer &arg_2) {
    return quo (arg_1, arg_2);
  }
  
  static integer
  GLUE_110 (const integer &arg_1, const integer &arg_2) {
    return rem (arg_1, arg_2);
  }
  
  static bool
  GLUE_111 (const integer &arg_1, const integer &arg_2) {
    return divides (arg_1, arg_2);
  }
  
  static integer
  GLUE_112 (const integer &arg_1, const integer &arg_2) {
    return gcd (arg_1, arg_2);
  }
  
  static integer
  GLUE_113 (const integer &arg_1, const integer &arg_2) {
    return lcm (arg_1, arg_2);
  }
  
  static integer
  GLUE_114 (const integer &arg_1, const integer &arg_2) {
    return invert_modulo (arg_1, arg_2);
  }
  
  static bool
  GLUE_115 (const integer &arg_1, const integer &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_116 (const integer &arg_1, const integer &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_117 (const integer &arg_1, const integer &arg_2) {
    return arg_1 < arg_2;
  }
  
  static bool
  GLUE_118 (const integer &arg_1, const integer &arg_2) {
    return arg_1 <= arg_2;
  }
  
  static bool
  GLUE_119 (const integer &arg_1, const integer &arg_2) {
    return arg_1 > arg_2;
  }
  
  static bool
  GLUE_120 (const integer &arg_1, const integer &arg_2) {
    return arg_1 >= arg_2;
  }
  
  static int
  GLUE_121 (const integer &arg_1) {
    return sign (arg_1);
  }
  
  static integer
  GLUE_122 (const integer &arg_1) {
    return abs (arg_1);
  }
  
  static integer
  GLUE_123 (const integer &arg_1, const integer &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static integer
  GLUE_124 (const integer &arg_1, const integer &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static integer
  GLUE_125 (const integer &arg_1, const integer &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static integer
  GLUE_126 (const integer &arg_1, const integer &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static integer
  GLUE_127 (const integer &arg_1) {
    return factorial (arg_1);
  }
  
  static integer
  GLUE_128 (const integer &arg_1, const integer &arg_2) {
    return binomial (arg_1, arg_2);
  }
  
  static bool
  GLUE_129 (const integer &arg_1) {
    return pr_is_prime (arg_1);
  }
  
  static integer
  GLUE_130 (const integer &arg_1) {
    return pr_next_prime (arg_1);
  }
  
  static integer
  GLUE_131 (const integer &arg_1, const integer &arg_2) {
    return arg_1 & arg_2;
  }
  
  static integer
  GLUE_132 (const integer &arg_1, const integer &arg_2) {
    return arg_1 | arg_2;
  }
  
  static integer
  GLUE_133 (const integer &arg_1, const integer &arg_2) {
    return arg_1 ^ arg_2;
  }
  
  static integer
  GLUE_134 (const integer &arg_1) {
    return ~arg_1;
  }
  
  static int
  GLUE_135 (const integer &arg_1) {
    return bit_size (arg_1);
  }
  
  static bool
  GLUE_136 (const integer &arg_1, const int &arg_2) {
    return arg_1[arg_2];
  }
  
  static int
  GLUE_137 (const integer &arg_1) {
    return hamming_norm (arg_1);
  }
  
  static int
  GLUE_138 (const integer &arg_1, const integer &arg_2) {
    return hamming_distance (arg_1, arg_2);
  }
  
  static integer
  GLUE_139 (const integer &arg_1, const int &arg_2) {
    return arg_1 + arg_2;
  }
  
  static integer
  GLUE_140 (const integer &arg_1, const int &arg_2) {
    return arg_1 - arg_2;
  }
  
  static integer
  GLUE_141 (const integer &arg_1, const int &arg_2) {
    return arg_1 * arg_2;
  }
  
  static integer
  GLUE_142 (const int &arg_1, const integer &arg_2) {
    return arg_1 + arg_2;
  }
  
  static integer
  GLUE_143 (const int &arg_1, const integer &arg_2) {
    return arg_1 - arg_2;
  }
  
  static integer
  GLUE_144 (const int &arg_1, const integer &arg_2) {
    return arg_1 * arg_2;
  }
  
  static integer
  GLUE_145 (const integer &arg_1, const int &arg_2) {
    return arg_1 / arg_2;
  }
  
  static integer
  GLUE_146 (const integer &arg_1, const int &arg_2) {
    return quo (arg_1, arg_2);
  }
  
  static integer
  GLUE_147 (const integer &arg_1, const int &arg_2) {
    return rem (arg_1, arg_2);
  }
  
  static bool
  GLUE_148 (const integer &arg_1, const int &arg_2) {
    return divides (arg_1, arg_2);
  }
  
  static integer
  GLUE_149 (const int &arg_1, const integer &arg_2) {
    return arg_1 / arg_2;
  }
  
  static integer
  GLUE_150 (const int &arg_1, const integer &arg_2) {
    return quo (arg_1, arg_2);
  }
  
  static integer
  GLUE_151 (const int &arg_1, const integer &arg_2) {
    return rem (arg_1, arg_2);
  }
  
  static bool
  GLUE_152 (const int &arg_1, const integer &arg_2) {
    return divides (arg_1, arg_2);
  }
  
  static bool
  GLUE_153 (const integer &arg_1, const int &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_154 (const integer &arg_1, const int &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_155 (const integer &arg_1, const int &arg_2) {
    return arg_1 < arg_2;
  }
  
  static bool
  GLUE_156 (const integer &arg_1, const int &arg_2) {
    return arg_1 <= arg_2;
  }
  
  static bool
  GLUE_157 (const integer &arg_1, const int &arg_2) {
    return arg_1 > arg_2;
  }
  
  static bool
  GLUE_158 (const integer &arg_1, const int &arg_2) {
    return arg_1 >= arg_2;
  }
  
  static bool
  GLUE_159 (const int &arg_1, const integer &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_160 (const int &arg_1, const integer &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_161 (const int &arg_1, const integer &arg_2) {
    return arg_1 < arg_2;
  }
  
  static bool
  GLUE_162 (const int &arg_1, const integer &arg_2) {
    return arg_1 <= arg_2;
  }
  
  static bool
  GLUE_163 (const int &arg_1, const integer &arg_2) {
    return arg_1 > arg_2;
  }
  
  static bool
  GLUE_164 (const int &arg_1, const integer &arg_2) {
    return arg_1 >= arg_2;
  }
  
  static integer
  GLUE_165 (const integer &arg_1, const int &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static integer
  GLUE_166 (const integer &arg_1, const int &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static integer
  GLUE_167 (const int &arg_1, const integer &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static integer
  GLUE_168 (const int &arg_1, const integer &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static integer
  GLUE_169 (const integer &arg_1, const int &arg_2) {
    return arg_1 & arg_2;
  }
  
  static integer
  GLUE_170 (const integer &arg_1, const int &arg_2) {
    return arg_1 | arg_2;
  }
  
  static integer
  GLUE_171 (const integer &arg_1, const int &arg_2) {
    return arg_1 ^ arg_2;
  }
  
  static integer
  GLUE_172 (const int &arg_1, const integer &arg_2) {
    return arg_1 & arg_2;
  }
  
  static integer
  GLUE_173 (const int &arg_1, const integer &arg_2) {
    return arg_1 | arg_2;
  }
  
  static integer
  GLUE_174 (const int &arg_1, const integer &arg_2) {
    return arg_1 ^ arg_2;
  }
  
  static modulus<int>
  GLUE_175 (const int &arg_1) {
    return modulus<int > (arg_1);
  }
  
  static modulus<int>
  GLUE_176 (const int &arg_1) {
    return modulus<int > (arg_1);
  }
  
  static int
  GLUE_177 (const modulus<int> &arg_1) {
    return *arg_1;
  }
  
  static mmx_modular(int)
  GLUE_178 (const int &arg_1, const int &arg_2) {
    return (mmx_modular(int ) (arg_1, arg_2));
  }
  
  static mmx_modular(int)
  GLUE_179 (const int &arg_1, const modulus<int> &arg_2) {
    return (mmx_modular(int ) (arg_1, arg_2));
  }
  
  static mmx_modular(int)
  GLUE_180 (const int &arg_1, const modulus<int> &arg_2) {
    return (mmx_modular(int ) (arg_1, arg_2));
  }
  
  static modulus<int>
  GLUE_181 (const mmx_modular(int) &arg_1) {
    return get_modulus (arg_1);
  }
  
  static int
  GLUE_182 (const mmx_modular(int) &arg_1) {
    return *arg_1;
  }
  
  static mmx_modular(int)
  GLUE_183 (const mmx_modular(int) &arg_1) {
    return -arg_1;
  }
  
  static mmx_modular(int)
  GLUE_184 (const mmx_modular(int) &arg_1) {
    return square (arg_1);
  }
  
  static mmx_modular(int)
  GLUE_185 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 + arg_2;
  }
  
  static mmx_modular(int)
  GLUE_186 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 - arg_2;
  }
  
  static mmx_modular(int)
  GLUE_187 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 * arg_2;
  }
  
  static mmx_modular(int)
  GLUE_188 (const int &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 + arg_2;
  }
  
  static mmx_modular(int)
  GLUE_189 (const mmx_modular(int) &arg_1, const int &arg_2) {
    return arg_1 + arg_2;
  }
  
  static mmx_modular(int)
  GLUE_190 (const int &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 - arg_2;
  }
  
  static mmx_modular(int)
  GLUE_191 (const mmx_modular(int) &arg_1, const int &arg_2) {
    return arg_1 - arg_2;
  }
  
  static mmx_modular(int)
  GLUE_192 (const int &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 * arg_2;
  }
  
  static mmx_modular(int)
  GLUE_193 (const mmx_modular(int) &arg_1, const int &arg_2) {
    return arg_1 * arg_2;
  }
  
  static mmx_modular(int)
  GLUE_194 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 / arg_2;
  }
  
  static mmx_modular(int)
  GLUE_195 (const int &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 / arg_2;
  }
  
  static mmx_modular(int)
  GLUE_196 (const mmx_modular(int) &arg_1, const int &arg_2) {
    return arg_1 / arg_2;
  }
  
  static mmx_modular(int)
  GLUE_197 (const mmx_modular(int) &arg_1, const int &arg_2) {
    return binpow (arg_1, arg_2);
  }
  
  static mmx_modular(int)
  GLUE_198 (const mmx_modular(int) &arg_1, const integer &arg_2) {
    return binpow (arg_1, arg_2);
  }
  
  static bool
  GLUE_199 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_200 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_201 (const mmx_modular(int) &arg_1, const int &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_202 (const mmx_modular(int) &arg_1, const int &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_203 (const int &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_204 (const int &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_205 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return divides (arg_1, arg_2);
  }
  
  static mmx_modular(int)
  GLUE_206 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return arg_1 / arg_2;
  }
  
  static mmx_modular(int)
  GLUE_207 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return gcd (arg_1, arg_2);
  }
  
  static mmx_modular(int)
  GLUE_208 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return lcm (arg_1, arg_2);
  }
  
  static modulus<integer>
  GLUE_209 (const integer &arg_1) {
    return modulus<integer > (arg_1);
  }
  
  static modulus<integer>
  GLUE_210 (const integer &arg_1) {
    return modulus<integer > (arg_1);
  }
  
  static integer
  GLUE_211 (const modulus<integer> &arg_1) {
    return *arg_1;
  }
  
  static mmx_modular(integer)
  GLUE_212 (const integer &arg_1, const integer &arg_2) {
    return (mmx_modular(integer ) (arg_1, arg_2));
  }
  
  static mmx_modular(integer)
  GLUE_213 (const integer &arg_1, const modulus<integer> &arg_2) {
    return (mmx_modular(integer ) (arg_1, arg_2));
  }
  
  static mmx_modular(integer)
  GLUE_214 (const integer &arg_1, const modulus<integer> &arg_2) {
    return (mmx_modular(integer ) (arg_1, arg_2));
  }
  
  static modulus<integer>
  GLUE_215 (const mmx_modular(integer) &arg_1) {
    return get_modulus (arg_1);
  }
  
  static integer
  GLUE_216 (const mmx_modular(integer) &arg_1) {
    return *arg_1;
  }
  
  static mmx_modular(integer)
  GLUE_217 (const mmx_modular(integer) &arg_1) {
    return -arg_1;
  }
  
  static mmx_modular(integer)
  GLUE_218 (const mmx_modular(integer) &arg_1) {
    return square (arg_1);
  }
  
  static mmx_modular(integer)
  GLUE_219 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 + arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_220 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 - arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_221 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 * arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_222 (const integer &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 + arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_223 (const mmx_modular(integer) &arg_1, const integer &arg_2) {
    return arg_1 + arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_224 (const integer &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 - arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_225 (const mmx_modular(integer) &arg_1, const integer &arg_2) {
    return arg_1 - arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_226 (const integer &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 * arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_227 (const mmx_modular(integer) &arg_1, const integer &arg_2) {
    return arg_1 * arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_228 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 / arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_229 (const integer &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 / arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_230 (const mmx_modular(integer) &arg_1, const integer &arg_2) {
    return arg_1 / arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_231 (const mmx_modular(integer) &arg_1, const int &arg_2) {
    return binpow (arg_1, arg_2);
  }
  
  static mmx_modular(integer)
  GLUE_232 (const mmx_modular(integer) &arg_1, const integer &arg_2) {
    return binpow (arg_1, arg_2);
  }
  
  static bool
  GLUE_233 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_234 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_235 (const mmx_modular(integer) &arg_1, const integer &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_236 (const mmx_modular(integer) &arg_1, const integer &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_237 (const integer &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_238 (const integer &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_239 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return divides (arg_1, arg_2);
  }
  
  static mmx_modular(integer)
  GLUE_240 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return arg_1 / arg_2;
  }
  
  static mmx_modular(integer)
  GLUE_241 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return gcd (arg_1, arg_2);
  }
  
  static mmx_modular(integer)
  GLUE_242 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return lcm (arg_1, arg_2);
  }
  
  static rational
  GLUE_243 (const integer &arg_1) {
    return rational (arg_1);
  }
  
  static rational
  GLUE_244 (const integer &arg_1, const integer &arg_2) {
    return rational_new (arg_1, arg_2);
  }
  
  static rational
  GLUE_245 (const int &arg_1) {
    return rational (arg_1);
  }
  
  static rational
  GLUE_246 (const integer &arg_1) {
    return rational (arg_1);
  }
  
  static double
  GLUE_247 (const rational &arg_1) {
    return as_double (arg_1);
  }
  
  static rational
  GLUE_248 (const integer &arg_1, const integer &arg_2) {
    return rational_new (arg_1, arg_2);
  }
  
  static integer
  GLUE_249 (const rational &arg_1) {
    return numerator (arg_1);
  }
  
  static integer
  GLUE_250 (const rational &arg_1) {
    return denominator (arg_1);
  }
  
  static bool
  GLUE_251 (const rational &arg_1) {
    return is_integer (arg_1);
  }
  
  static rational
  GLUE_252 (const rational &arg_1) {
    return -arg_1;
  }
  
  static rational
  GLUE_253 (const rational &arg_1) {
    return square (arg_1);
  }
  
  static rational
  GLUE_254 (const rational &arg_1, const rational &arg_2) {
    return arg_1 + arg_2;
  }
  
  static rational
  GLUE_255 (const rational &arg_1, const rational &arg_2) {
    return arg_1 - arg_2;
  }
  
  static rational
  GLUE_256 (const rational &arg_1, const rational &arg_2) {
    return arg_1 * arg_2;
  }
  
  static rational
  GLUE_257 (const rational &arg_1, const rational &arg_2) {
    return arg_1 / arg_2;
  }
  
  static rational
  GLUE_258 (const rational &arg_1, const integer &arg_2) {
    return arg_1 + arg_2;
  }
  
  static rational
  GLUE_259 (const rational &arg_1, const integer &arg_2) {
    return arg_1 - arg_2;
  }
  
  static rational
  GLUE_260 (const rational &arg_1, const integer &arg_2) {
    return arg_1 * arg_2;
  }
  
  static rational
  GLUE_261 (const rational &arg_1, const integer &arg_2) {
    return arg_1 / arg_2;
  }
  
  static rational
  GLUE_262 (const integer &arg_1, const rational &arg_2) {
    return arg_1 + arg_2;
  }
  
  static rational
  GLUE_263 (const integer &arg_1, const rational &arg_2) {
    return arg_1 - arg_2;
  }
  
  static rational
  GLUE_264 (const integer &arg_1, const rational &arg_2) {
    return arg_1 * arg_2;
  }
  
  static rational
  GLUE_265 (const integer &arg_1, const rational &arg_2) {
    return arg_1 / arg_2;
  }
  
  static generic
  GLUE_266 (const integer &arg_1, const integer &arg_2) {
    return old_integer_pow (arg_1, arg_2);
  }
  
  static rational
  GLUE_267 (const rational &arg_1, const integer &arg_2) {
    return pow (arg_1, arg_2);
  }
  
  static rational
  GLUE_268 (const rational &arg_1, const rational &arg_2) {
    return arg_1 / arg_2;
  }
  
  static bool
  GLUE_269 (const rational &arg_1, const rational &arg_2) {
    return divides (arg_1, arg_2);
  }
  
  static rational
  GLUE_270 (const rational &arg_1, const rational &arg_2) {
    return gcd (arg_1, arg_2);
  }
  
  static rational
  GLUE_271 (const rational &arg_1, const rational &arg_2) {
    return lcm (arg_1, arg_2);
  }
  
  static bool
  GLUE_272 (const rational &arg_1, const rational &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_273 (const rational &arg_1, const rational &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_274 (const rational &arg_1, const rational &arg_2) {
    return arg_1 < arg_2;
  }
  
  static bool
  GLUE_275 (const rational &arg_1, const rational &arg_2) {
    return arg_1 <= arg_2;
  }
  
  static bool
  GLUE_276 (const rational &arg_1, const rational &arg_2) {
    return arg_1 > arg_2;
  }
  
  static bool
  GLUE_277 (const rational &arg_1, const rational &arg_2) {
    return arg_1 >= arg_2;
  }
  
  static bool
  GLUE_278 (const rational &arg_1, const integer &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_279 (const rational &arg_1, const integer &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_280 (const rational &arg_1, const integer &arg_2) {
    return arg_1 < arg_2;
  }
  
  static bool
  GLUE_281 (const rational &arg_1, const integer &arg_2) {
    return arg_1 <= arg_2;
  }
  
  static bool
  GLUE_282 (const rational &arg_1, const integer &arg_2) {
    return arg_1 > arg_2;
  }
  
  static bool
  GLUE_283 (const rational &arg_1, const integer &arg_2) {
    return arg_1 >= arg_2;
  }
  
  static bool
  GLUE_284 (const integer &arg_1, const rational &arg_2) {
    return arg_1 == arg_2;
  }
  
  static bool
  GLUE_285 (const integer &arg_1, const rational &arg_2) {
    return arg_1 != arg_2;
  }
  
  static bool
  GLUE_286 (const integer &arg_1, const rational &arg_2) {
    return arg_1 < arg_2;
  }
  
  static bool
  GLUE_287 (const integer &arg_1, const rational &arg_2) {
    return arg_1 <= arg_2;
  }
  
  static bool
  GLUE_288 (const integer &arg_1, const rational &arg_2) {
    return arg_1 > arg_2;
  }
  
  static bool
  GLUE_289 (const integer &arg_1, const rational &arg_2) {
    return arg_1 >= arg_2;
  }
  
  static int
  GLUE_290 (const rational &arg_1) {
    return sign (arg_1);
  }
  
  static rational
  GLUE_291 (const rational &arg_1) {
    return abs (arg_1);
  }
  
  static rational
  GLUE_292 (const rational &arg_1, const rational &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static rational
  GLUE_293 (const rational &arg_1, const rational &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static rational
  GLUE_294 (const rational &arg_1, const integer &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static rational
  GLUE_295 (const rational &arg_1, const integer &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static rational
  GLUE_296 (const integer &arg_1, const rational &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static rational
  GLUE_297 (const integer &arg_1, const rational &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static rational
  GLUE_298 (const rational &arg_1, const rational &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static rational
  GLUE_299 (const rational &arg_1, const rational &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static rational
  GLUE_300 (const rational &arg_1, const integer &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static rational
  GLUE_301 (const rational &arg_1, const integer &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static rational
  GLUE_302 (const integer &arg_1, const rational &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static rational
  GLUE_303 (const integer &arg_1, const rational &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static rational
  GLUE_304 (const rational &arg_1) {
    return floor (arg_1);
  }
  
  static rational
  GLUE_305 (const rational &arg_1) {
    return ceil (arg_1);
  }
  
  static rational
  GLUE_306 (const rational &arg_1) {
    return trunc (arg_1);
  }
  
  static rational
  GLUE_307 (const rational &arg_1) {
    return round (arg_1);
  }
  
  static mmx_floating
  GLUE_308 (const literal &arg_1) {
    return make_literal_floating (arg_1);
  }
  
  static mmx_floating
  GLUE_309 (const int &arg_1) {
    return mmx_floating (arg_1);
  }
  
  static mmx_floating
  GLUE_310 (const integer &arg_1) {
    return mmx_floating (arg_1);
  }
  
  static mmx_floating
  GLUE_311 (const rational &arg_1) {
    return mmx_floating (arg_1);
  }
  
  static mmx_floating
  GLUE_312 (const int &arg_1) {
    return mmx_floating (arg_1);
  }
  
  static mmx_floating
  GLUE_313 (const double &arg_1) {
    return mmx_floating (arg_1);
  }
  
  static mmx_floating
  GLUE_314 (const integer &arg_1) {
    return mmx_floating (arg_1);
  }
  
  static mmx_floating
  GLUE_315 (const rational &arg_1) {
    return mmx_floating (arg_1);
  }
  
  static mmx_floating
  GLUE_316 (const string &arg_1) {
    return mmx_floating (arg_1);
  }
  
  static int
  GLUE_317 (const mmx_floating &arg_1) {
    return as_int (arg_1);
  }
  
  static double
  GLUE_318 (const mmx_floating &arg_1) {
    return as_double (arg_1);
  }
  
  static integer
  GLUE_319 (const mmx_floating &arg_1) {
    return as_integer (arg_1);
  }
  
  static string
  GLUE_320 (const mmx_floating &arg_1) {
    return as_string (arg_1);
  }
  
  static mmx_floating
  GLUE_321 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return uniform_deviate (arg_1, arg_2);
  }
  
  static mmx_floating
  GLUE_322 (const mmx_floating &arg_1) {
    return -arg_1;
  }
  
  static mmx_floating
  GLUE_323 (const mmx_floating &arg_1) {
    return square (arg_1);
  }
  
  static mmx_floating
  GLUE_324 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return arg_1 + arg_2;
  }
  
  static mmx_floating
  GLUE_325 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return arg_1 - arg_2;
  }
  
  static mmx_floating
  GLUE_326 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return arg_1 * arg_2;
  }
  
  static mmx_floating
  GLUE_327 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return arg_1 / arg_2;
  }
  
  static mmx_floating
  GLUE_328 (const mmx_floating &arg_1) {
    return sqrt (arg_1);
  }
  
  static mmx_floating
  GLUE_329 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return pow (arg_1, arg_2);
  }
  
  static mmx_floating
  GLUE_330 (const mmx_floating &arg_1) {
    return exp (arg_1);
  }
  
  static mmx_floating
  GLUE_331 (const mmx_floating &arg_1) {
    return log (arg_1);
  }
  
  static mmx_floating
  GLUE_332 (const mmx_floating &arg_1) {
    return cos (arg_1);
  }
  
  static mmx_floating
  GLUE_333 (const mmx_floating &arg_1) {
    return sin (arg_1);
  }
  
  static mmx_floating
  GLUE_334 (const mmx_floating &arg_1) {
    return tan (arg_1);
  }
  
  static mmx_floating
  GLUE_335 (const mmx_floating &arg_1) {
    return acos (arg_1);
  }
  
  static mmx_floating
  GLUE_336 (const mmx_floating &arg_1) {
    return asin (arg_1);
  }
  
  static mmx_floating
  GLUE_337 (const mmx_floating &arg_1) {
    return atan (arg_1);
  }
  
  static bool
  GLUE_338 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return arg_1 < arg_2;
  }
  
  static bool
  GLUE_339 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return arg_1 <= arg_2;
  }
  
  static bool
  GLUE_340 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return arg_1 > arg_2;
  }
  
  static bool
  GLUE_341 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return arg_1 >= arg_2;
  }
  
  static int
  GLUE_342 (const mmx_floating &arg_1) {
    return sign (arg_1);
  }
  
  static mmx_floating
  GLUE_343 (const mmx_floating &arg_1) {
    return abs (arg_1);
  }
  
  static mmx_floating
  GLUE_344 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static mmx_floating
  GLUE_345 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static mmx_floating
  GLUE_346 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return min (arg_1, arg_2);
  }
  
  static mmx_floating
  GLUE_347 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return max (arg_1, arg_2);
  }
  
  static mmx_floating
  GLUE_348 (const mmx_floating &arg_1) {
    return floor (arg_1);
  }
  
  static mmx_floating
  GLUE_349 (const mmx_floating &arg_1) {
    return ceil (arg_1);
  }
  
  static mmx_floating
  GLUE_350 (const mmx_floating &arg_1) {
    return trunc (arg_1);
  }
  
  static mmx_floating
  GLUE_351 (const mmx_floating &arg_1) {
    return round (arg_1);
  }
  
  static bool
  GLUE_352 (const mmx_floating &arg_1) {
    return is_finite (arg_1);
  }
  
  static bool
  GLUE_353 (const mmx_floating &arg_1) {
    return is_infinite (arg_1);
  }
  
  static bool
  GLUE_354 (const mmx_floating &arg_1) {
    return is_nan (arg_1);
  }
  
  static mmx_floating
  GLUE_355 (const mmx_floating &arg_1) {
    return times_infinity (arg_1);
  }
  
  static int
  GLUE_356 (const mmx_floating &arg_1) {
    return precision (arg_1);
  }
  
  static mmx_floating
  GLUE_357 (const mmx_floating &arg_1, const int &arg_2) {
    return change_precision (arg_1, arg_2);
  }
  
  static mmx_floating
  GLUE_358 (const mmx_floating &arg_1) {
    return next_above (arg_1);
  }
  
  static mmx_floating
  GLUE_359 (const mmx_floating &arg_1) {
    return next_below (arg_1);
  }
  
  static int
  GLUE_360 (const mmx_floating &arg_1) {
    return exponent (arg_1);
  }
  
  static double
  GLUE_361 (const mmx_floating &arg_1) {
    return magnitude (arg_1);
  }
  
  static mmx_floating
  GLUE_362 (const mmx_floating &arg_1, const int &arg_2) {
    return incexp2 (arg_1, arg_2);
  }
  
  static mmx_floating
  GLUE_363 (const mmx_floating &arg_1, const int &arg_2) {
    return decexp2 (arg_1, arg_2);
  }
  
  static mmx_floating
  GLUE_364 (const mmx_floating &arg_1) {
    return rounding_error (arg_1);
  }
  
  static mmx_floating
  GLUE_365 (const mmx_floating &arg_1) {
    return additive_error (arg_1);
  }
  
  static mmx_floating
  GLUE_366 (const mmx_floating &arg_1) {
    return multiplicative_error (arg_1);
  }
  
  static mmx_floating
  GLUE_367 (const mmx_floating &arg_1) {
    return elementary_error (arg_1);
  }
  
  static shape_axel
  GLUE_368 () {
    return shape_axel ();
  }
  
  static shape_axel
  GLUE_369 (const string &arg_1) {
    return shape_axel_string (arg_1);
  }
  
  static void
  GLUE_370 (const shape_axel &arg_1) {
    shape_axel_view (arg_1);
  }
  
  static shape_point<rational>
  GLUE_371 (const tuple<rational> &arg_1) {
    return shape_point_tuple<rational > (as_vector (arg_1));
  }
  
  static rational
  GLUE_372 (const shape_point<rational> &arg_1, const int &arg_2) {
    return arg_1[arg_2];
  }
  
  static alias<rational>
  GLUE_373 (const alias<shape_point<rational> > &arg_1, const int &arg_2) {
    return alias_access<rational > (arg_1, arg_2);
  }
  
  static shape_point<rational>
  GLUE_374 (const shape_point<rational> &arg_1, const shape_point<rational> &arg_2) {
    return arg_1 - arg_2;
  }
  
  static alias<shape_axel>
  GLUE_375 (const alias<shape_axel> &arg_1, const shape_point<rational> &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static int
  GLUE_376 (const vector<generic> &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_377 (const vector<generic> &arg_1, const vector<generic> &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_378 (const vector<generic> &arg_1, const vector<generic> &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_379 (const integer &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_380 (const integer &arg_1, const integer &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_381 (const integer &arg_1, const integer &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_382 (const modulus<int> &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_383 (const modulus<int> &arg_1, const modulus<int> &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_384 (const modulus<int> &arg_1, const modulus<int> &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_385 (const mmx_modular(int) &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_386 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_387 (const mmx_modular(int) &arg_1, const mmx_modular(int) &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_388 (const modulus<integer> &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_389 (const modulus<integer> &arg_1, const modulus<integer> &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_390 (const modulus<integer> &arg_1, const modulus<integer> &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_391 (const mmx_modular(integer) &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_392 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_393 (const mmx_modular(integer) &arg_1, const mmx_modular(integer) &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_394 (const rational &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_395 (const rational &arg_1, const rational &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_396 (const rational &arg_1, const rational &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_397 (const mmx_floating &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_398 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_399 (const mmx_floating &arg_1, const mmx_floating &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_400 (const shape_axel &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_401 (const shape_axel &arg_1, const shape_axel &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_402 (const shape_axel &arg_1, const shape_axel &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_403 (const shape_point<rational> &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_404 (const shape_point<rational> &arg_1, const shape_point<rational> &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_405 (const shape_point<rational> &arg_1, const shape_point<rational> &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static vector<integer>
  GLUE_406 (const tuple<integer> &arg_1) {
    return vector<integer > (as_vector (arg_1));
  }
  
  static vector<integer>
  GLUE_407 (const tuple<integer> &arg_1) {
    return vector<integer > (as_vector (arg_1));
  }
  
  static iterator<generic>
  GLUE_408 (const vector<integer> &arg_1) {
    return as<iterator<generic> > (iterate (arg_1));
  }
  
  static int
  GLUE_409 (const vector<integer> &arg_1) {
    return N (arg_1);
  }
  
  static integer
  GLUE_410 (const vector<integer> &arg_1, const int &arg_2) {
    return arg_1[arg_2];
  }
  
  static alias<integer>
  GLUE_411 (const alias<vector<integer> > &arg_1, const int &arg_2) {
    return alias_access<integer > (arg_1, arg_2);
  }
  
  static vector<integer>
  GLUE_412 (const vector<integer> &arg_1, const int &arg_2, const int &arg_3) {
    return range (arg_1, arg_2, arg_3);
  }
  
  static vector<integer>
  GLUE_413 (const vector<integer> &arg_1) {
    return reverse (arg_1);
  }
  
  static vector<integer>
  GLUE_414 (const vector<integer> &arg_1, const vector<integer> &arg_2) {
    return append (arg_1, arg_2);
  }
  
  static alias<vector<integer> >
  GLUE_415 (const alias<vector<integer> > &arg_1, const vector<integer> &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static void
  GLUE_416 (const vector<integer> &arg_1, const vector<integer> &arg_2) {
    inside_append (arg_1, arg_2);
  }
  
  static vector<integer>
  GLUE_417 (const integer &arg_1, const vector<integer> &arg_2) {
    return cons (arg_1, arg_2);
  }
  
  static integer
  GLUE_418 (const vector<integer> &arg_1) {
    return car (arg_1);
  }
  
  static vector<integer>
  GLUE_419 (const vector<integer> &arg_1) {
    return cdr (arg_1);
  }
  
  static bool
  GLUE_420 (const vector<integer> &arg_1) {
    return is_nil (arg_1);
  }
  
  static bool
  GLUE_421 (const vector<integer> &arg_1) {
    return is_atom (arg_1);
  }
  
  static vector<integer>
  GLUE_422 (const vector<integer> &arg_1, const integer &arg_2) {
    return insert (arg_1, arg_2);
  }
  
  static int
  GLUE_423 (const vector<integer> &arg_1, const integer &arg_2) {
    return find (arg_1, arg_2);
  }
  
  static bool
  GLUE_424 (const vector<integer> &arg_1, const integer &arg_2) {
    return contains (arg_1, arg_2);
  }
  
  static vector<rational>
  GLUE_425 (const tuple<rational> &arg_1) {
    return vector<rational > (as_vector (arg_1));
  }
  
  static vector<rational>
  GLUE_426 (const tuple<rational> &arg_1) {
    return vector<rational > (as_vector (arg_1));
  }
  
  static iterator<generic>
  GLUE_427 (const vector<rational> &arg_1) {
    return as<iterator<generic> > (iterate (arg_1));
  }
  
  static int
  GLUE_428 (const vector<rational> &arg_1) {
    return N (arg_1);
  }
  
  static rational
  GLUE_429 (const vector<rational> &arg_1, const int &arg_2) {
    return arg_1[arg_2];
  }
  
  static alias<rational>
  GLUE_430 (const alias<vector<rational> > &arg_1, const int &arg_2) {
    return alias_access<rational > (arg_1, arg_2);
  }
  
  static vector<rational>
  GLUE_431 (const vector<rational> &arg_1, const int &arg_2, const int &arg_3) {
    return range (arg_1, arg_2, arg_3);
  }
  
  static vector<rational>
  GLUE_432 (const vector<rational> &arg_1) {
    return reverse (arg_1);
  }
  
  static vector<rational>
  GLUE_433 (const vector<rational> &arg_1, const vector<rational> &arg_2) {
    return append (arg_1, arg_2);
  }
  
  static alias<vector<rational> >
  GLUE_434 (const alias<vector<rational> > &arg_1, const vector<rational> &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static void
  GLUE_435 (const vector<rational> &arg_1, const vector<rational> &arg_2) {
    inside_append (arg_1, arg_2);
  }
  
  static vector<rational>
  GLUE_436 (const rational &arg_1, const vector<rational> &arg_2) {
    return cons (arg_1, arg_2);
  }
  
  static rational
  GLUE_437 (const vector<rational> &arg_1) {
    return car (arg_1);
  }
  
  static vector<rational>
  GLUE_438 (const vector<rational> &arg_1) {
    return cdr (arg_1);
  }
  
  static bool
  GLUE_439 (const vector<rational> &arg_1) {
    return is_nil (arg_1);
  }
  
  static bool
  GLUE_440 (const vector<rational> &arg_1) {
    return is_atom (arg_1);
  }
  
  static vector<rational>
  GLUE_441 (const vector<rational> &arg_1, const rational &arg_2) {
    return insert (arg_1, arg_2);
  }
  
  static int
  GLUE_442 (const vector<rational> &arg_1, const rational &arg_2) {
    return find (arg_1, arg_2);
  }
  
  static bool
  GLUE_443 (const vector<rational> &arg_1, const rational &arg_2) {
    return contains (arg_1, arg_2);
  }
  
  static vector<generic>
  GLUE_444 (const vector<integer> &arg_1) {
    return as<vector<generic> > (arg_1);
  }
  
  static vector<rational>
  GLUE_445 (const vector<integer> &arg_1) {
    return as<vector<rational> > (arg_1);
  }
  
  static vector<generic>
  GLUE_446 (const vector<rational> &arg_1) {
    return as<vector<generic> > (arg_1);
  }
  
  static rational
  GLUE_447 (const mmx_modular(int) &arg_1) {
    return reconstruct (arg_1);
  }
  
  static rational
  GLUE_448 (const mmx_modular(integer) &arg_1) {
    return reconstruct (arg_1);
  }
  
  static shape_point<rational>
  GLUE_449 (const shape_point<rational> &arg_1, const vector<rational> &arg_2) {
    return arg_1 + arg_2;
  }
  
  static int
  GLUE_450 (const vector<integer> &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_451 (const vector<integer> &arg_1, const vector<integer> &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_452 (const vector<integer> &arg_1, const vector<integer> &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_453 (const vector<rational> &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_454 (const vector<rational> &arg_1, const vector<rational> &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_455 (const vector<rational> &arg_1, const vector<rational> &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  void
  glue_point () {
    static bool done = false;
    if (done) return;
    done = true;
    define ("hash%", GLUE_1);
    define ("=%", GLUE_2);
    define ("!=%", GLUE_3);
    define ("hash%", GLUE_4);
    define ("=%", GLUE_5);
    define ("!=%", GLUE_6);
    define ("hash%", GLUE_7);
    define ("=%", GLUE_8);
    define ("!=%", GLUE_9);
    define_type<int > (lit ("Int"));
    define ("literal_integer", GLUE_10);
    define ("as_int", GLUE_11);
    define ("as_string", GLUE_12);
    define ("as_string_hexa", GLUE_13);
    define ("-", GLUE_14);
    define ("square", GLUE_15);
    define ("+", GLUE_16);
    define ("-", GLUE_17);
    define ("*", GLUE_18);
    define ("div", GLUE_19);
    define ("quo", GLUE_20);
    define ("rem", GLUE_21);
    define ("=", GLUE_22);
    define ("!=", GLUE_23);
    define ("<", GLUE_24);
    define ("<=", GLUE_25);
    define (">", GLUE_26);
    define (">=", GLUE_27);
    define ("sign", GLUE_28);
    define ("abs", GLUE_29);
    define ("min", GLUE_30);
    define ("max", GLUE_31);
    define ("inf", GLUE_32);
    define ("sup", GLUE_33);
    define ("/\\", GLUE_34);
    define ("\\/", GLUE_35);
    define ("xor", GLUE_36);
    define ("floor_sqrt", GLUE_37);
    define ("divides?", GLUE_38);
    define ("random", GLUE_39);
    define_constant<int > ("int_minus_infinity", int_minus_infinity);
    define_constant<int > ("int_plus_infinity", int_plus_infinity);
    define_type<vector<generic> > (gen (lit ("Vector"), lit ("Generic")));
    define ("vector?", GLUE_40);
    define ("vector", GLUE_41);
    define ("[]", GLUE_42);
    define_converter (":>", GLUE_43, PENALTY_PROMOTE_GENERIC);
    define ("#", GLUE_44);
    define (".[]", GLUE_45);
    define (".[]", GLUE_46);
    define (".[]", GLUE_47);
    define ("reverse", GLUE_48);
    define ("><", GLUE_49);
    define ("<<", GLUE_50);
    define ("append", GLUE_51);
    define ("cons", GLUE_52);
    define ("car", GLUE_53);
    define ("cdr", GLUE_54);
    define ("nil?", GLUE_55);
    define ("atom?", GLUE_56);
    define ("insert", GLUE_57);
    define ("find", GLUE_58);
    define ("contains?", GLUE_59);
    define ("string?", GLUE_60);
    define ("#", GLUE_61);
    define (".[]", GLUE_62);
    define ("*", GLUE_63);
    define ("><", GLUE_64);
    define ("<<", GLUE_65);
    define ("<", GLUE_66);
    define ("<=", GLUE_67);
    define (">", GLUE_68);
    define (">=", GLUE_69);
    define ("starts?", GLUE_70);
    define ("ends?", GLUE_71);
    define ("replace", GLUE_72);
    define ("search_forwards", GLUE_73);
    define ("search_backwards", GLUE_74);
    define ("upcase", GLUE_75);
    define ("locase", GLUE_76);
    define ("upcase_first", GLUE_77);
    define ("locase_first", GLUE_78);
    define ("quote", GLUE_79);
    define ("unquote", GLUE_80);
    define ("ascii", GLUE_81);
    define ("ascii_code", GLUE_82);
    define ("integer?", GLUE_83);
    define ("floating?", GLUE_84);
    define ("alpha?", GLUE_85);
    define ("numeric?", GLUE_86);
    define ("alpha_numeric?", GLUE_87);
    define ("mmx_identifier?", GLUE_88);
    define ("literal?", GLUE_89);
    define (".()", GLUE_90);
    define (".[]", GLUE_91);
    define ("as_literal", GLUE_92);
    define ("as_string", GLUE_93);
    define_type<integer > (lit ("Integer"));
    define ("literal_integer", GLUE_94);
    define ("integer", GLUE_95);
    define_converter ("upgrade", GLUE_96, PENALTY_INCLUSION);
    define ("int?", GLUE_97);
    define ("as_int", GLUE_98);
    define ("as_integer", GLUE_99);
    define ("as_string", GLUE_100);
    define_constructor<int > (GLUE_101);
    define_converter (":>", GLUE_102, PENALTY_INCLUSION);
    define ("-", GLUE_103);
    define ("square", GLUE_104);
    define ("+", GLUE_105);
    define ("-", GLUE_106);
    define ("*", GLUE_107);
    define ("div", GLUE_108);
    define ("quo", GLUE_109);
    define ("rem", GLUE_110);
    define ("divides?", GLUE_111);
    define ("gcd", GLUE_112);
    define ("lcm", GLUE_113);
    define ("invert_modulo", GLUE_114);
    define ("=", GLUE_115);
    define ("!=", GLUE_116);
    define ("<", GLUE_117);
    define ("<=", GLUE_118);
    define (">", GLUE_119);
    define (">=", GLUE_120);
    define ("sign", GLUE_121);
    define ("abs", GLUE_122);
    define ("min", GLUE_123);
    define ("max", GLUE_124);
    define ("inf", GLUE_125);
    define ("sup", GLUE_126);
    define (".!", GLUE_127);
    define ("binomial", GLUE_128);
    define ("prime%?", GLUE_129);
    define ("next_prime%", GLUE_130);
    define ("/\\", GLUE_131);
    define ("\\/", GLUE_132);
    define ("xor", GLUE_133);
    define ("!", GLUE_134);
    define ("#", GLUE_135);
    define (".[]", GLUE_136);
    define ("hamming_norm", GLUE_137);
    define ("hamming_distance", GLUE_138);
    define ("+", GLUE_139);
    define ("-", GLUE_140);
    define ("*", GLUE_141);
    define ("+", GLUE_142);
    define ("-", GLUE_143);
    define ("*", GLUE_144);
    define ("div", GLUE_145);
    define ("quo", GLUE_146);
    define ("rem", GLUE_147);
    define ("divides?", GLUE_148);
    define ("div", GLUE_149);
    define ("quo", GLUE_150);
    define ("rem", GLUE_151);
    define ("divides?", GLUE_152);
    define ("=", GLUE_153);
    define ("!=", GLUE_154);
    define ("<", GLUE_155);
    define ("<=", GLUE_156);
    define (">", GLUE_157);
    define (">=", GLUE_158);
    define ("=", GLUE_159);
    define ("!=", GLUE_160);
    define ("<", GLUE_161);
    define ("<=", GLUE_162);
    define (">", GLUE_163);
    define (">=", GLUE_164);
    define ("min", GLUE_165);
    define ("max", GLUE_166);
    define ("min", GLUE_167);
    define ("max", GLUE_168);
    define ("/\\", GLUE_169);
    define ("\\/", GLUE_170);
    define ("xor", GLUE_171);
    define ("/\\", GLUE_172);
    define ("\\/", GLUE_173);
    define ("xor", GLUE_174);
    define_type<modulus<int> > (gen (lit ("Modulus"), lit ("Int")));
    define_type<modulus<integer> > (gen (lit ("Modulus"), lit ("Integer")));
    define_type<mmx_modular(int) > (gen (lit ("Modular"), lit ("Int")));
    define_type<mmx_modular(integer) > (gen (lit ("Modular"), lit ("Integer")));
    define ("modulus", GLUE_175);
    define_converter ("upgrade", GLUE_176, PENALTY_HOMOMORPHISM);
    define_converter (":>", GLUE_177, PENALTY_CAST);
    define ("modular", GLUE_178);
    define ("modular", GLUE_179);
    define ("mod", GLUE_180);
    define ("get_modulus", GLUE_181);
    define ("preimage", GLUE_182);
    define ("-", GLUE_183);
    define ("square", GLUE_184);
    define ("+", GLUE_185);
    define ("-", GLUE_186);
    define ("*", GLUE_187);
    define ("+", GLUE_188);
    define ("+", GLUE_189);
    define ("-", GLUE_190);
    define ("-", GLUE_191);
    define ("*", GLUE_192);
    define ("*", GLUE_193);
    define ("/", GLUE_194);
    define ("/", GLUE_195);
    define ("/", GLUE_196);
    define ("^", GLUE_197);
    define ("^", GLUE_198);
    define ("=", GLUE_199);
    define ("!=", GLUE_200);
    define ("=", GLUE_201);
    define ("!=", GLUE_202);
    define ("=", GLUE_203);
    define ("!=", GLUE_204);
    define ("divides?", GLUE_205);
    define ("div", GLUE_206);
    define ("gcd", GLUE_207);
    define ("lcm", GLUE_208);
    define ("modulus", GLUE_209);
    define_converter ("upgrade", GLUE_210, PENALTY_HOMOMORPHISM);
    define_converter (":>", GLUE_211, PENALTY_CAST);
    define ("modular", GLUE_212);
    define ("modular", GLUE_213);
    define ("mod", GLUE_214);
    define ("get_modulus", GLUE_215);
    define ("preimage", GLUE_216);
    define ("-", GLUE_217);
    define ("square", GLUE_218);
    define ("+", GLUE_219);
    define ("-", GLUE_220);
    define ("*", GLUE_221);
    define ("+", GLUE_222);
    define ("+", GLUE_223);
    define ("-", GLUE_224);
    define ("-", GLUE_225);
    define ("*", GLUE_226);
    define ("*", GLUE_227);
    define ("/", GLUE_228);
    define ("/", GLUE_229);
    define ("/", GLUE_230);
    define ("^", GLUE_231);
    define ("^", GLUE_232);
    define ("=", GLUE_233);
    define ("!=", GLUE_234);
    define ("=", GLUE_235);
    define ("!=", GLUE_236);
    define ("=", GLUE_237);
    define ("!=", GLUE_238);
    define ("divides?", GLUE_239);
    define ("div", GLUE_240);
    define ("gcd", GLUE_241);
    define ("lcm", GLUE_242);
    define_type<rational > (lit ("Rational"));
    define ("rational", GLUE_243);
    define ("rational", GLUE_244);
    define_converter ("upgrade", GLUE_245, PENALTY_INCLUSION);
    define_converter ("upgrade", GLUE_246, PENALTY_INCLUSION);
    define_converter ("upgrade", GLUE_247, PENALTY_INCLUSION);
    define ("/", GLUE_248);
    define ("numerator", GLUE_249);
    define ("denominator", GLUE_250);
    define ("integer?", GLUE_251);
    define ("-", GLUE_252);
    define ("square", GLUE_253);
    define ("+", GLUE_254);
    define ("-", GLUE_255);
    define ("*", GLUE_256);
    define ("/", GLUE_257);
    define ("+", GLUE_258);
    define ("-", GLUE_259);
    define ("*", GLUE_260);
    define ("/", GLUE_261);
    define ("+", GLUE_262);
    define ("-", GLUE_263);
    define ("*", GLUE_264);
    define ("/", GLUE_265);
    define ("^", GLUE_266);
    define ("^", GLUE_267);
    define ("div", GLUE_268);
    define ("divides?", GLUE_269);
    define ("gcd", GLUE_270);
    define ("lcm", GLUE_271);
    define ("=", GLUE_272);
    define ("!=", GLUE_273);
    define ("<", GLUE_274);
    define ("<=", GLUE_275);
    define (">", GLUE_276);
    define (">=", GLUE_277);
    define ("=", GLUE_278);
    define ("!=", GLUE_279);
    define ("<", GLUE_280);
    define ("<=", GLUE_281);
    define (">", GLUE_282);
    define (">=", GLUE_283);
    define ("=", GLUE_284);
    define ("!=", GLUE_285);
    define ("<", GLUE_286);
    define ("<=", GLUE_287);
    define (">", GLUE_288);
    define (">=", GLUE_289);
    define ("sign", GLUE_290);
    define ("abs", GLUE_291);
    define ("min", GLUE_292);
    define ("max", GLUE_293);
    define ("min", GLUE_294);
    define ("max", GLUE_295);
    define ("min", GLUE_296);
    define ("max", GLUE_297);
    define ("inf", GLUE_298);
    define ("sup", GLUE_299);
    define ("inf", GLUE_300);
    define ("sup", GLUE_301);
    define ("inf", GLUE_302);
    define ("sup", GLUE_303);
    define ("floor", GLUE_304);
    define ("ceil", GLUE_305);
    define ("trunc", GLUE_306);
    define ("round", GLUE_307);
    static alias<int> mmx_significant_digits_alias = global_alias (((int&) mmx_significant_digits));
    define_constant<alias<int> > ("significant_digits", mmx_significant_digits_alias);
    static alias<int> mmx_bit_precision_alias = global_alias (((int&) mmx_bit_precision));
    define_constant<alias<int> > ("bit_precision", mmx_bit_precision_alias);
    static alias<int> mmx_discrepancy_alias = global_alias (((int&) mmx_discrepancy));
    define_constant<alias<int> > ("discrepancy", mmx_discrepancy_alias);
    static alias<bool> mmx_pretty_exponents_alias = global_alias (((bool&) mmx_pretty_exponents));
    define_constant<alias<bool> > ("pretty_exponents", mmx_pretty_exponents_alias);
    define_type<mmx_floating > (lit ("Floating"));
    define ("literal_floating", GLUE_308);
    define_converter ("upgrade", GLUE_309, PENALTY_INCLUSION);
    define_converter ("upgrade", GLUE_310, PENALTY_INCLUSION);
    define_converter ("upgrade", GLUE_311, PENALTY_INCLUSION);
    define ("as_floating", GLUE_312);
    define ("as_floating", GLUE_313);
    define ("as_floating", GLUE_314);
    define ("as_floating", GLUE_315);
    define ("as_floating", GLUE_316);
    define ("as_int", GLUE_317);
    define ("as_double", GLUE_318);
    define ("as_integer", GLUE_319);
    define ("as_string", GLUE_320);
    define ("uniform_deviate", GLUE_321);
    define ("-", GLUE_322);
    define ("square", GLUE_323);
    define ("+", GLUE_324);
    define ("-", GLUE_325);
    define ("*", GLUE_326);
    define ("/", GLUE_327);
    define ("sqrt", GLUE_328);
    define ("^", GLUE_329);
    define ("exp", GLUE_330);
    define ("log", GLUE_331);
    define ("cos", GLUE_332);
    define ("sin", GLUE_333);
    define ("tan", GLUE_334);
    define ("arccos", GLUE_335);
    define ("arcsin", GLUE_336);
    define ("arctan", GLUE_337);
    define ("<", GLUE_338);
    define ("<=", GLUE_339);
    define (">", GLUE_340);
    define (">=", GLUE_341);
    define ("sign", GLUE_342);
    define ("abs", GLUE_343);
    define ("min", GLUE_344);
    define ("max", GLUE_345);
    define ("inf", GLUE_346);
    define ("sup", GLUE_347);
    define ("floor", GLUE_348);
    define ("ceil", GLUE_349);
    define ("trunc", GLUE_350);
    define ("round", GLUE_351);
    define ("finite?", GLUE_352);
    define ("infinite?", GLUE_353);
    define ("nan?", GLUE_354);
    define ("times_infinity", GLUE_355);
    define ("precision", GLUE_356);
    define ("change_precision", GLUE_357);
    define ("next_above", GLUE_358);
    define ("next_below", GLUE_359);
    define ("exponent", GLUE_360);
    define ("magnitude", GLUE_361);
    define ("increase_exponent", GLUE_362);
    define ("decrease_exponent", GLUE_363);
    define ("rounding_error", GLUE_364);
    define ("additive_error", GLUE_365);
    define ("multiplicative_error", GLUE_366);
    define ("elementary_error", GLUE_367);
    define_type<shape_axel > (lit ("Axel"));
    define ("axel", GLUE_368);
    define ("axel", GLUE_369);
    define ("view", GLUE_370);
    define_type<shape_point<rational> > (gen (lit ("Point"), lit ("Rational")));
    define ("point", GLUE_371);
    define (".[]", GLUE_372);
    define (".[]", GLUE_373);
    define ("-", GLUE_374);
    define ("<<", GLUE_375);
    define ("hash%", GLUE_376);
    define ("=%", GLUE_377);
    define ("!=%", GLUE_378);
    define ("hash%", GLUE_379);
    define ("=%", GLUE_380);
    define ("!=%", GLUE_381);
    define ("hash%", GLUE_382);
    define ("=%", GLUE_383);
    define ("!=%", GLUE_384);
    define ("hash%", GLUE_385);
    define ("=%", GLUE_386);
    define ("!=%", GLUE_387);
    define ("hash%", GLUE_388);
    define ("=%", GLUE_389);
    define ("!=%", GLUE_390);
    define ("hash%", GLUE_391);
    define ("=%", GLUE_392);
    define ("!=%", GLUE_393);
    define ("hash%", GLUE_394);
    define ("=%", GLUE_395);
    define ("!=%", GLUE_396);
    define ("hash%", GLUE_397);
    define ("=%", GLUE_398);
    define ("!=%", GLUE_399);
    define ("hash%", GLUE_400);
    define ("=%", GLUE_401);
    define ("!=%", GLUE_402);
    define ("hash%", GLUE_403);
    define ("=%", GLUE_404);
    define ("!=%", GLUE_405);
    define_type<vector<integer> > (gen (lit ("Vector"), lit ("Integer")));
    define_type<vector<rational> > (gen (lit ("Vector"), lit ("Rational")));
    define ("vector", GLUE_406);
    define ("[]", GLUE_407);
    define_converter (":>", GLUE_408, PENALTY_CAST);
    define ("#", GLUE_409);
    define (".[]", GLUE_410);
    define (".[]", GLUE_411);
    define (".[]", GLUE_412);
    define ("reverse", GLUE_413);
    define ("><", GLUE_414);
    define ("<<", GLUE_415);
    define ("append", GLUE_416);
    define ("cons", GLUE_417);
    define ("car", GLUE_418);
    define ("cdr", GLUE_419);
    define ("nil?", GLUE_420);
    define ("atom?", GLUE_421);
    define ("insert", GLUE_422);
    define ("find", GLUE_423);
    define ("contains?", GLUE_424);
    define ("vector", GLUE_425);
    define ("[]", GLUE_426);
    define_converter (":>", GLUE_427, PENALTY_CAST);
    define ("#", GLUE_428);
    define (".[]", GLUE_429);
    define (".[]", GLUE_430);
    define (".[]", GLUE_431);
    define ("reverse", GLUE_432);
    define ("><", GLUE_433);
    define ("<<", GLUE_434);
    define ("append", GLUE_435);
    define ("cons", GLUE_436);
    define ("car", GLUE_437);
    define ("cdr", GLUE_438);
    define ("nil?", GLUE_439);
    define ("atom?", GLUE_440);
    define ("insert", GLUE_441);
    define ("find", GLUE_442);
    define ("contains?", GLUE_443);
    define_converter (":>", GLUE_444, PENALTY_PROMOTE_GENERIC);
    define_converter (":>", GLUE_445, PENALTY_INCLUSION);
    define_converter (":>", GLUE_446, PENALTY_PROMOTE_GENERIC);
    define ("reconstruct", GLUE_447);
    define ("reconstruct", GLUE_448);
    define ("+", GLUE_449);
    define ("hash%", GLUE_450);
    define ("=%", GLUE_451);
    define ("!=%", GLUE_452);
    define ("hash%", GLUE_453);
    define ("=%", GLUE_454);
    define ("!=%", GLUE_455);
  }
}
