
#include <basix/int.hpp>
#include <shape/axel_glue.hpp>
#include <shape/color_glue.hpp>
#include <basix/alias.hpp>
#include <basix/glue.hpp>

#define int_literal(x) as_int (as_string (x))

namespace mmx {
  static shape_color
  GLUE_1 (const int &arg_1, const int &arg_2, const int &arg_3) {
    return shape_color (arg_1, arg_2, arg_3);
  }
  
  static alias<shape_axel>
  GLUE_2 (const alias<shape_axel> &arg_1, const shape_color &arg_2) {
    return alias_write (arg_1, arg_2);
  }
  
  static int
  GLUE_3 (const shape_axel &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_4 (const shape_axel &arg_1, const shape_axel &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_5 (const shape_axel &arg_1, const shape_axel &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  static int
  GLUE_6 (const shape_color &arg_1) {
    return pr_hash (arg_1);
  }
  
  static bool
  GLUE_7 (const shape_color &arg_1, const shape_color &arg_2) {
    return pr_eq (arg_1, arg_2);
  }
  
  static bool
  GLUE_8 (const shape_color &arg_1, const shape_color &arg_2) {
    return pr_neq (arg_1, arg_2);
  }
  
  void
  glue_color () {
    static bool done = false;
    if (done) return;
    done = true;
    call_glue (string ("glue_int"));
    call_glue (string ("glue_axel"));
    define_type<shape_color > (lit ("Color"));
    define ("color", GLUE_1);
    define ("<<", GLUE_2);
    define ("hash%", GLUE_3);
    define ("=%", GLUE_4);
    define ("!=%", GLUE_5);
    define ("hash%", GLUE_6);
    define ("=%", GLUE_7);
    define ("!=%", GLUE_8);
  }
}
