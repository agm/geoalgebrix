The package provide tools for the manipulation and vizualization of algebraic curves and surfaces defined by implicit equations or rational parametrisations.

## Installation

* To setup the initial configuration with all subpackages:
```
  cd geoalgebrix; ./script/init.cfg
```

* To configure this package, use cmake and the following instructions
  in a folder `build-geoalgebrix`:
```
  mkdir ../build-geoalgebrix; cd ../build-geoalgebrix;
  cmake ../geoalgebrix -DAxl_DIR=/path-to/the-folder/where-you/build-axl
```
  You can also use 
```
  ccmake ../geoalgebrix
```

  and setup the variable `Axl_DIR` manually. 

* To compile it:
```
  make 
```



 
